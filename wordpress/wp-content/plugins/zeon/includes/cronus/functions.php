<?php

	/**
	 *	Unregister Widgets
	 * 	Some widgets are registred in child theme
	 */
	
	function cronus__unregister_widgets()
	{
		unregister_widget( 'cronus_widget_post_categories' );
		unregister_widget( 'cronus_widget_post_tags' );
		unregister_widget( 'cronus_widget_post_meta' );
	}

	add_action( 'widgets_init', 'cronus__unregister_widgets' );


	/**
	 *	Remove unused filters
	 *	Some filters are used in the child theme
	 */

	remove_filter( 'tempo_header_text_wrapper_style', 'cronus_header_text_wrapper_style' );

	remove_filter( 'tempo_layout_content_class', 'tempo__layout_content_class' );
	remove_filter( 'tempo_layout_sidebar_class', 'tempo__layout_sidebar_class' );

	remove_filter( 'tempo_front_page_section_length', 'cronus_front_page_section_length' );

	remove_filter( 'tempo_page_section_length', 'cronus_singular_section_length' );
    remove_filter( 'tempo_single_section_length', 'cronus_singular_section_length' );

    remove_filter( 'tempo_loop_section_length', 'cronus_loop_section_length' );

    remove_action( 'get_template_part_templates/section/before', 'cronus_left_sidebar' );
    remove_action( 'get_template_part_templates/section/after', 'cronus_right_sidebar' );

    remove_filter( 'tempo_merge_sidebars', 'cronus_merge_footer_dark_sidebars' );


    /**
     *	Extends the Parent function 
     *	with some special or / and additional cases
     */

    function cronus__has_header( $show_header )
    {
    	if( !$show_header && is_singular() ){
    		global $post;

    		$template = zeon_meta::g( $post -> ID, 'tempo-header-template', 'default' );

    		if( $template !== 'default' && $template !== 'gmap' && $template !== 'video' && $template !== 'sample-image' )
    			$show_header = is_file( zeon_locate_template( array( "templates/header/partial/default-template-{$template}.php" ) ) );
    	}

    	return $show_header;
    }

    add_filter( 'tempo_has_header', 'cronus__has_header', 10, 1 );


    function cronus__header_thumbnail_size( $size )
    {
    	return 'cronus-header';
    }

    add_filter( 'zeon_header_thumbnail_size', 'cronus__header_thumbnail_size' );


    {   /////   SINGLE SETTINGS

            function cronus__box_single_settings_save( $post_id )
            {
                if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                    return;

                if( isset( $_POST ) && !empty( $_POST ) ){

                    // overwrite breadcrumbs
                    if( isset( $_POST[ 'tempo-overwrite-breadcrumbs' ] ) && (bool)$_POST[ 'tempo-overwrite-breadcrumbs' ] ){
                        zeon_meta::set( $post_id, 'tempo-overwrite-breadcrumbs', 1 );

                        $breadcrumbs = isset( $_POST[ 'tempo-breadcrumbs' ] ) && (bool)$_POST[ 'tempo-breadcrumbs' ] ? 1 : 0;
                        zeon_meta::set( $post_id, 'tempo-breadcrumbs', $breadcrumbs );
                    }

                    else{
                        zeon_meta::delete( $post_id, 'tempo-overwrite-breadcrumbs' );
                        zeon_meta::delete( $post_id, 'tempo-breadcrumbs' );
                    }
                }   
            }

            function cronus__box_single_settings( $post )
            {
                $overwrite_breadcrumbs = (bool)zeon_meta::g( $post -> ID, 'tempo-overwrite-breadcrumbs' );

                echo tempo_html::field(array(
                    'title'         => __( 'Overwrite Breadcrumbs Settings', 'zeon' ),
                    'description'   => __( 'overwrite default breadcrumbs settings for this page.', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'logic',
                        'slug'          => 'tempo-overwrite-breadcrumbs',
                        'theme_mod'     => false,
                        'value'         => $overwrite_breadcrumbs,
                        'action'        => '.tempo-overwrite-breadcrumbs-wrapper'
                    )
                ));

                $classes = 'tempo-overwrite-breadcrumbs-wrapper';

                if( !$overwrite_breadcrumbs )
                    $classes = 'tempo-overwrite-breadcrumbs-wrapper hidden';

                $breadcrumbs = (bool)zeon_meta::get( $post -> ID, 'tempo-breadcrumbs', tempo_options::get( 'breadcrumbs' ) );

                echo tempo_html::field(array(
                    'title'         => __( 'Display Breadcrumbs', 'zeon' ),
                    'class'         => $classes,
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'logic',
                        'slug'          => 'tempo-breadcrumbs',
                        'theme_mod'     => false,
                        'value'         => $breadcrumbs
                    )
                ));
            }
        }
?>