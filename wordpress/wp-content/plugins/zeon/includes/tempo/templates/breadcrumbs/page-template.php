<?php
    global $post;

    if( !apply_filters( 'tempo_template_breadcrumbs', tempo_options::get( 'breadcrumbs' ), $post -> ID ) )
        return;
?>

    <!-- breadcrumbs wrapper -->
    <div class="tempo-breadcrumbs">

    	<?php tempo_get_template_part( 'templates/breadcrumbs/prepend' ); ?>


        <!-- main container -->
        <div <?php echo tempo_container_class( 'main' ); ?>>
            <div <?php echo tempo_row_class(); ?>>

                <!-- content -->
                <div <?php echo tempo_content_class(); ?>>
                    <div <?php echo tempo_row_class(); ?>>

                    	<?php tempo_get_template_part( 'templates/breadcrumbs/before-content' ); ?>


                        <!-- navigation and headline -->
                        <div <?php echo tempo_large_class(); ?>>

                        	<?php tempo_get_template_part( 'templates/breadcrumbs/prepend-content' ); ?>


                            <!-- navigation -->
                            <nav class="tempo-navigation">

                                <?php tempo_get_template_part( 'templates/breadcrumbs/prepend-nav' ); ?>

                                <ul class="tempo-menu-list">

                                	<?php tempo_get_template_part( 'templates/breadcrumbs/prepend-list' ); ?>

                                    <?php echo tempo_breadcrumbs::home(); ?>

                                    <?php tempo_get_template_part( 'templates/breadcrumbs/after-home' ); ?>

                                    <?php echo tempo_breadcrumbs::pages( $post ); ?>

                                    <?php tempo_get_template_part( 'templates/breadcrumbs/append-list' ); ?>

                                </ul>

                                <?php tempo_get_template_part( 'templates/breadcrumbs/append-nav' ); ?>

                            </nav><!-- end navigation -->


                            <?php tempo_get_template_part( 'templates/breadcrumbs/after-nav' ); ?>


                            <!-- headline / end -->
                            <h1 id="tempo-headline-page" class="tempo-headline"><?php the_title(); ?></h1>

                            <?php tempo_get_template_part( 'templates/breadcrumbs/append-content' ); ?>

                        </div><!-- end navigation and headline -->


                        <!-- counter -->
                        <div <?php echo tempo_small_class( 'details' ); ?>>

                            <?php global $wp_query; ?>
                            <?php echo tempo_breadcrumbs::count( $wp_query ); ?>

                        </div><!-- end counter -->


                        <?php tempo_get_template_part( 'templates/breadcrumbs/after-content' ); ?>

                    </div>
                </div><!-- end content -->

            </div>
        </div><!-- end main container -->


        <!-- delimiter container -->
        <div <?php echo tempo_container_class( 'delimiter' ); ?>>
            <div <?php echo tempo_row_class(); ?>>

                <!-- content -->
                <div <?php echo tempo_content_class(); ?>>
                    <div <?php echo tempo_row_class(); ?>>

                        <div <?php echo tempo_full_class(); ?>>
                            <hr/>
                        </div>

                    </div>
                </div><!-- end content -->

            </div>
        </div><!-- end delimiter container -->


        <?php tempo_get_template_part( 'templates/breadcrumbs/append' ); ?>

    </div><!-- end breadcrumbs wrapper -->