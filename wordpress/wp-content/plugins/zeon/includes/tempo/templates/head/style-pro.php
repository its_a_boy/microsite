<!-- HEADER BUTTON 1 -->
<style type="text/css" id="header-btn-1-text-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-text-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-text-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-text-color' );
			$transp = tempo_options::get( 'header-btn-1-text-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-text-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-text-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-text-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-text-h-color' );
			$transp = tempo_options::get( 'header-btn-1-text-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1:hover{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-bkg-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-bkg-color' );
			$transp = tempo_options::get( 'header-btn-1-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-bkg-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-bkg-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-bkg-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-bkg-h-color' );
			$transp = tempo_options::get( 'header-btn-1-bkg-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1:hover{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>


<!-- HEADER BUTTON 2 -->
<style type="text/css" id="header-btn-2-text-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-text-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-text-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-text-color' );
			$transp = tempo_options::get( 'header-btn-2-text-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-text-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-text-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-text-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-text-h-color' );
			$transp = tempo_options::get( 'header-btn-2-text-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2:hover{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-bkg-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-bkg-color' );
			$transp = tempo_options::get( 'header-btn-2-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-bkg-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-bkg-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-bkg-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-bkg-h-color' );
			$transp = tempo_options::get( 'header-btn-2-bkg-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2:hover{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>


<!-- HEADER SPACE AREA -->
<?php
	$headline       = tempo_options::get( 'header-headline' );
	$description    = tempo_options::get( 'header-description' );

	$btn_1 			= tempo_options::get( 'header-btn-1' );
	$btn_2 			= tempo_options::get( 'header-btn-2' );

	if( ( $headline || $description ) && ( $btn_1 || $btn_2 ) ){

        $text_space	= tempo_options::get( 'header-text-space' );
        $btns_space	= tempo_options::get( 'header-btns-space' );
?>
		<style type="text/css" id="header-text-space">
			.tempo-header div.tempo-header-partial .tempo-flex-container.tempo-header-text-wrapper{
				height: <?php echo absint( $text_space ); ?>px;
			}
		</style>

		<style type="text/css" id="header-btns-space">
			.tempo-header div.tempo-header-partial .tempo-flex-container.tempo-header-btns-wrapper{
				height: <?php echo absint( $btns_space ); ?>px;
			}
		</style>
<?php
	}
?>


<!-- FRONT PAGE SIDEBAR -->
<style type="text/css" id="header-front-page-sidebar-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-front-page-sidebar-bkg-color' ) ||
		 	tempo_options::is_set( 'header-front-page-sidebar-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-front-page-sidebar-bkg-color' );
			$transp = tempo_options::get( 'header-front-page-sidebar-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>
			aside.tempo-front-page.header-sidebar{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-front-page-sidebar-space">
	<?php if( tempo_options::is_set( 'header-front-page-sidebar-space' ) ) : ?>

		aside.tempo-front-page.header-sidebar{
			padding-top: <?php echo absint( tempo_options::get( 'header-front-page-sidebar-space' ) ); ?>px;
			padding-bottom: <?php echo absint( tempo_options::get( 'header-front-page-sidebar-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>

<style type="text/css" id="header-front-page-widgets-space">
	<?php if( tempo_options::is_set( 'header-front-page-widgets-space' ) ) : ?>

		aside.tempo-front-page.header-sidebar div.widget{
			margin-top: <?php echo absint( tempo_options::get( 'header-front-page-widgets-space' ) ); ?>px;
			margin-bottom: <?php echo absint( tempo_options::get( 'header-front-page-widgets-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>


<!-- BLOG PAGE SIDEBARS -->
<style type="text/css" id="header-blog-sidebar-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-blog-sidebar-bkg-color' ) ||
		 	tempo_options::is_set( 'header-blog-sidebar-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-blog-sidebar-bkg-color' );
			$transp = tempo_options::get( 'header-blog-sidebar-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>
			aside.tempo-blog.header-sidebar{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-blog-sidebar-space">
	<?php if( tempo_options::is_set( 'header-blog-sidebar-space' ) ) : ?>

		aside.tempo-blog.header-sidebar{
			padding-top: <?php echo absint( tempo_options::get( 'header-blog-sidebar-space' ) ); ?>px;
			padding-bottom: <?php echo absint( tempo_options::get( 'header-blog-sidebar-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>

<style type="text/css" id="header-blog-widgets-space">
	<?php if( tempo_options::is_set( 'header-blog-widgets-space' ) ) : ?>

		aside.tempo-blog.header-sidebar div.widget{
			margin-top: <?php echo absint( tempo_options::get( 'header-blog-widgets-space' ) ); ?>px;
			margin-bottom: <?php echo absint( tempo_options::get( 'header-blog-widgets-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>


<!-- CATEGORIES -->
<?php
	$categories = get_categories();

	foreach( $categories as $i => $cat ){
		echo '<style type="text/css" id="category-' . $cat -> term_id . '">';

		if( tempo_options::is_set( 'category-' . $cat -> term_id ) ){

			echo 'aside div.widget.zeon_widget_categories a span.tempo-category-' . $cat -> term_id . ',';
			echo 'header.tempo-header div.tempo-header-partial.tempo-portfolio div.tempo-categories a.tempo-category-' . $cat -> term_id . ',';
			echo 'article.tempo-article.portfolio div.tempo-categories a.tempo-category-' . $cat -> term_id . ',';
			echo 'article.tempo-article.grid div.tempo-categories a.tempo-category-' . $cat -> term_id . '{';
			echo 'background-color: ' . tempo_options::get( 'category-' . $cat -> term_id  ) . ';';
			echo '}';

		}

		echo '</style>';
	}
?>

<!-- FOOTER LIGHT SIDEBARS -->
<style type="text/css" id="footer-light-sidebar-bkg-color">
	<?php
		if( tempo_options::is_set( 'footer-light-sidebar-bkg-color' ) ||
		 	tempo_options::is_set( 'footer-light-sidebar-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'footer-light-sidebar-bkg-color' );
			$transp = tempo_options::get( 'footer-light-sidebar-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>
			aside.tempo-footer.light-sidebar{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="footer-light-sidebar-space">
	<?php if( tempo_options::is_set( 'footer-light-sidebar-space' ) ) : ?>

		aside.tempo-footer.light-sidebar{
			padding-top: <?php echo absint( tempo_options::get( 'footer-light-sidebar-space' ) ); ?>px;
			padding-bottom: <?php echo absint( tempo_options::get( 'footer-light-sidebar-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>

<style type="text/css" id="footer-light-widgets-space">
	<?php if( tempo_options::is_set( 'footer-light-widgets-space' ) ) : ?>

		aside.tempo-footer.light-sidebar div.widget{
			margin-top: <?php echo absint( tempo_options::get( 'footer-light-widgets-space' ) ); ?>px;
			margin-bottom: <?php echo absint( tempo_options::get( 'footer-light-widgets-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>


<!-- FOOTER DARK SIDEBARS -->
<style type="text/css" id="footer-dark-sidebar-bkg-color">
	<?php
		if( tempo_options::is_set( 'footer-dark-sidebar-bkg-color' ) ||
		 	tempo_options::is_set( 'footer-dark-sidebar-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'footer-dark-sidebar-bkg-color' );
			$transp = tempo_options::get( 'footer-dark-sidebar-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>
			aside.tempo-footer.dark-sidebar{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="footer-dark-sidebar-space">
	<?php if( tempo_options::is_set( 'footer-dark-sidebar-space' ) ) : ?>

		aside.tempo-footer.dark-sidebar{
			padding-top: <?php echo absint( tempo_options::get( 'footer-dark-sidebar-space' ) ); ?>px;
			padding-bottom: <?php echo absint( tempo_options::get( 'footer-dark-sidebar-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>

<style type="text/css" id="footer-dark-widgets-space">
	<?php if( tempo_options::is_set( 'footer-dark-widgets-space' ) ) : ?>

		aside.tempo-footer.dark-sidebar div.widget{
			margin-top: <?php echo absint( tempo_options::get( 'footer-dark-widgets-space' ) ); ?>px;
			margin-bottom: <?php echo absint( tempo_options::get( 'footer-dark-widgets-space' ) ); ?>px;
		}

	<?php endif; ?>
</style>
