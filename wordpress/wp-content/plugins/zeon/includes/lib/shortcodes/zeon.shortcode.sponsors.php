<?php

    /*
        [sponsors top="" bottom=""]

            [sponsor url="" image=""/]alt text[/sponsor]
            [sponsor url="" image=""/]alt text[/sponsor]
            [sponsor url="" image=""/]alt text[/sponsor]
            [sponsor url="" image=""/]alt text[/sponsor]

        [/sponsors]
    */

    class zeon_shortcode_sponsors
    {
        private static $do  = true;
        private static $run = false;
        private static $nr  = 3;

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( self::$do ){

                /* STOP DO RECURSIVE */
                self::$do   = false;
                self::$run  = true;

                $attr = shortcode_atts( array(
                    'nr'        => 3,
                    'top'       => 30,
                    'bottom'    => 30
                ), $_attr );

                /* VALIDATE */
                $top = (int)$attr[ 'top' ];
                $bottom = (int)$attr[ 'bottom' ];

                /* STYLE AND PROPERTIES */
                $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );;

                if( !empty( $top ) ){
                    $prop .= 'margin-top: ' . $top . 'px; ';
                }

                if( !empty( $bottom ) ){
                    $prop .= 'margin-bottom: ' . $bottom . 'px; ';
                }

                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . esc_attr( $prop ) . '"';
                }

                self::$nr = intval( $attr[ 'nr' ] );

                /* CONTENT */
                $rett .= '<div class="row tempo-shortcode sponsors" ' . $style . '>';
                $rett .= do_shortcode( strip_tags( $_content ) );
                $rett .= '</div>';

                /* ALLOW DO NEXT */
                self::$do   = true;
                self::$run  = false;
            }

            return $rett;
        }

        public $code = null;
        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Number of sponsors', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'id'        => 'sponsors-nr',
                        'type'      => 'select',
                        'name'      => 'sponsors-nr',
                        'theme_mod' => false,
                        'default'   => 3,
                        'options'   => array(
                            2           => number_format_i18n( 2 ),
                            3           => number_format_i18n( 3 ),
                            4           => number_format_i18n( 4 )
                        ),
                        'actions'   => array(
                            'hide'  => array( '.zeon-sponsor-3-wrapper', '.zeon-sponsor-4-wrapper' ),
                            '3'     => array( '.zeon-sponsor-3-wrapper' ),
                            '4'     => array( '.zeon-sponsor-3-wrapper', '.zeon-sponsor-4-wrapper' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'sponsors-top',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'sponsors-bottom',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                )
            );

            $fields[ 'sponsor-1' ] = array(
                array(
                    'title'     => __( 'Image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'upload',
                        'name'      => 'sponsor-1-image',
                        'theme_mod' => false,
                        'callback'  => 'shortcode',
                        'args'      => array(
                            'shortcode' => '.zeon-shortcode-sponsors-fields'
                        )
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'description'   => __( 'if the url is missing will be displaed a sample image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'sponsor-1-url',
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'         => __( 'Description' , 'zeon' ),
                    'description'   => __( 'the image description is used for attribute "alt".' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'textarea',
                        'name'          => 'sponsor-1-description',
                        'theme_mod'     => false
                    )
                )
            );

            $fields[ 'sponsor-2' ] = array(
                array(
                    'title'     => __( 'Image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'upload',
                        'name'      => 'sponsor-2-image',
                        'theme_mod' => false,
                        'callback'  => 'shortcode',
                        'args'      => array(
                            'shortcode' => '.zeon-shortcode-sponsors-fields'
                        ),
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'description'   => __( 'if the url is missing will be displaed a sample image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'sponsor-2-url',
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'         => __( 'Description' , 'zeon' ),
                    'description'   => __( 'the image description is used for attribute "alt".' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'textarea',
                        'name'          => 'sponsor-2-description',
                        'theme_mod'     => false
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Sponsors Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'title'     => __( 'First Sponsor', 'zeon' ),
                    'fields'    => $fields[ 'sponsor-1' ]
                ),
                array(
                    'title'     => __( 'Second Sponsor', 'zeon' ),
                    'fields'    => $fields[ 'sponsor-2' ]
                )
            );

            $fields[ 'sponsor-3' ] = array(
                array(
                    'title'     => __( 'Image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'upload',
                        'name'      => 'sponsor-3-image',
                        'theme_mod' => false,
                        'callback'  => 'shortcode',
                        'args'      => array(
                            'shortcode' => '.zeon-shortcode-sponsors-fields'
                        )
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'description'   => __( 'if the url is missing will be displaed a sample image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'sponsor-3-url',
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'         => __( 'Description' , 'zeon' ),
                    'description'   => __( 'the image description is used for attribute "alt".' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'textarea',
                        'name'          => 'sponsor-3-description',
                        'theme_mod'     => false
                    )
                )
            );

            $fields[ 'sponsor-4' ] = array(
                array(
                    'title'     => __( 'Image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'upload',
                        'name'      => 'sponsor-4-image',
                        'theme_mod' => false,
                        'callback'  => 'shortcode',
                        'args'      => array(
                            'shortcode' => '.zeon-shortcode-sponsors-fields'
                        )
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'description'   => __( 'if the url is missing will be displaed a sample image', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'sponsor-4-url',
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'         => __( 'Description' , 'zeon' ),
                    'description'   => __( 'the image description is used for attribute "alt".' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'textarea',
                        'name'          => 'sponsor-4-description',
                        'theme_mod'     => false
                    )
                )
            );

            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'Third Sponsor', 'zeon' ),
                    'class'     => 'zeon-sponsor-3-wrapper',
                    'fields'    => $fields[ 'sponsor-3' ]
                ),
                array(
                    'title'     => __( 'Fourth Sponsor', 'zeon' ),
                    'class'     => 'zeon-sponsor-4-wrapper hidden',
                    'fields'    => $fields[ 'sponsor-4' ]
                )
            );

            $img_1          = '<span class="sponsor-1-image attr">image="<span class="value"></span>"</span>';
            $url_1          = '<span class="sponsor-1-url attr">url="<span class="value"></span>"</span>';
            $desc_1         = '<span class="sponsor-1-description text"><span class="value"></span></span>';

            $img_2          = '<span class="sponsor-2-image attr">image="<span class="value"></span>"</span>';
            $url_2          = '<span class="sponsor-2-url attr">url="<span class="value"></span>"</span>';
            $desc_2         = '<span class="sponsor-2-description text"><span class="value"></span></span>';

            $img_3          = '<span class="sponsor-3-image attr">image="<span class="value"></span>"</span>';
            $url_3          = '<span class="sponsor-3-url attr">url="<span class="value"></span>"</span>';
            $desc_3         = '<span class="sponsor-3-description text"><span class="value"></span></span>';

            $img_4          = '<span class="sponsor-4-image attr">image="<span class="value"></span>"</span>';
            $url_4          = '<span class="sponsor-4-url attr">url="<span class="value"></span>"</span>';
            $desc_4         = '<span class="sponsor-4-description text"><span class="value"></span></span>';


            $top            = '<span class="sponsors-top attr">top="<span class="value">30</span>"</span>';
            $bottom         = '<span class="sponsors-bottom attr">bottom="<span class="value">30</span>"</span>';
            $nr             = '<span class="sponsors-nr attr">nr="<span class="value">3</span>"</span>';

            $this -> code   =   '[zeon_sponsors ' . $nr . ' ' . $top . ' ' . $bottom . ']' . '<br/>' . "\n" .
                                '[zeon_sponsor ' . $img_1 . ' ' . $url_1 . ']' . $desc_1 . '[/zeon_sponsor]' . '<br/>' . "\n" .
                                '[zeon_sponsor ' . $img_2 . ' ' . $url_2 . ']' . $desc_2 . '[/zeon_sponsor]' . '<br/>' . "\n" .
                                '<span class="zeon-sponsor-3 zeon-ln">[zeon_sponsor ' . $img_3 . ' ' . $url_3 . ']' . $desc_3 . '[/zeon_sponsor]' . '<br/>' . "\n" . '</span>' .
                                '<span class="zeon-sponsor-4 zeon-ln hide-ln">[zeon_sponsor ' . $img_4 . ' ' . $url_4 . ']' . $desc_4 . '[/zeon_sponsor]' . '<br/>' . "\n" . '</span>' .
                                '[/zeon_sponsors]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes[ 'right' ]
                )
            );
        }

        public static function is_run(){
            return self::$run;
        }

        public static function get_nr()
        {
            return intval( self::$nr );
        }
    }

    /*
        ...

        [sponsor url="" image=""/]alt text[/sponsor]

        ...

    */

    class zeon_shortcode_sponsor
    {
        private static $do = true;

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( self::$do && zeon_shortcode_sponsors::is_run() ){

                /* STOP DO RECURSIVE */
                self::$do = false;

                $attr = shortcode_atts( array(
                    'url' => null,
                    'image' => null
                ), $_attr );

                /* VALIDATE */
                $image  = esc_url( $attr[ 'image' ] );
                $url    = esc_url( $attr[ 'url' ] );
                $nr     = zeon_shortcode_sponsors::get_nr();

                $cols   = array(
                    2 => 'col-xs-6 col-sm-6 col-md-6 col-lg-6 sponsor-item',
                    3 => 'col-xs-6 col-sm-6 col-md-4 col-lg-4 sponsor-item',
                    4 => 'col-xs-6 col-sm-6 col-md-3 col-lg-3 sponsor-item'
                );

                /* CONTENT */
                if( !empty( $image ) ){

                    if( !in_array( $nr, array( 2,3,4 ) ) )
                        $nr = 3;

                    $rett .= '<div class="' . esc_attr( $cols[ $nr ] ) . '">';
                    $rett .= '<a href="' . $url . '" title="' . esc_attr( $_content ) . '" target="_blank">';
                    $rett .= '<img src="' . $image . '" alt="' . esc_attr( $_content ) . '"/>';
                    $rett .= '</a>';
                    $rett .= '</div>';
                }

                /* ALLOW DO NEXT */
                self::$do = true;
            }

            return $rett;
        }
    }
?>
