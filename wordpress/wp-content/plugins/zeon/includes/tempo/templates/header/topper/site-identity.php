<?php
    $site_logo          = tempo_options::get( 'icon-logo' );
    $site_title         = get_bloginfo( 'name' );
    $site_description   = get_bloginfo( 'description' );

    if( !empty( $site_logo ) ){
        echo '<div class="tempo-site-identity tempo-icon-logo">';

        $style = '';

        if( function_exists( 'getimagesize' ) ){
            $margin = -11;
            $image  = getimagesize( $site_logo );

            if( isset( $image[ 0 ] ) && isset( $image[ 1 ] ) ){
                $width  = absint( $image[ 0 ] );
                $height = absint( $image[ 1 ] );

                if( $width > 70 ){
                    $h = intval( 70 * $height / $width );
                    if( $h < 70 ){
                        $margin = intval( ( 48 - $h ) / 2 );
                    }              
                }
                else if( $height > 70 ){
                    $margin = -11;
                }
            }

            $style = 'style="margin-top:' . intval( $margin ) . 'px; margin-bottom:' . intval( $margin ) . 'px;"';
        }

        echo '<a class="tempo-site-icon-logo" href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( $site_title . ' - ' . $site_description ) . '" ' . $style . '>';
        echo '<img src="' . esc_url( $site_logo ) . '" alt="' . esc_attr( $site_title . ' - ' . $site_description ) . '"/>';
        echo '</a>';

        // blog title
        if( !empty( $site_title ) ){
            echo '<a class="tempo-site-title" href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( $site_title . ' - ' . $site_description ) . '">';
            bloginfo( 'name' );
            echo '</a>';
        }

        // blog description
        if( !empty( $site_description ) ){
            echo '<a class="tempo-site-description" href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( $site_title . ' - ' . $site_description ) . '">';
            bloginfo( 'description' );
            echo '</a>';
        }

        echo '</div>';
    }

    else{
        get_template_part( 'templates/header/topper/site-identity' );
    }
?>