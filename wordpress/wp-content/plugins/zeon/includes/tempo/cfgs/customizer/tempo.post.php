<?php
	
	/**
     *  Appearance / Customize / Single Post / Related Posts  - config settings
     */

	/* $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'tempo-post' => array(
			'sections'		=> array(
				'tempo-related' => array(
					'title'		=> __( 'Related Posts', 'zeon' ),
					'priority'	=> 15,
					'fields'	=> array(
						'related-posts' => array(
							'title' 		=> __( 'Display Related Posts', 'zeon' ),
							'deacription'	=> __( 'enable / disable related articles after the post content', 'zeon' ),
							'input'			=> array(
								'type'			=> 'logic',
								'default'		=> true
							)
						),
						'related-tax' => array(
							'title'			=> __( 'Dispaly Related Posts by', 'zeon' ),
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'post_tag',
								'options'		=> array(
									'post_tag'		=> __( 'Tags', 'zeon' ),
									'category'		=> __( 'Categories', 'zeon' )
								)
							)
						),
						'related-nr' => array(
							'title'			=> __( 'Number of Related Posts', 'zeon' ),
							'input'			=> array(
								'type'			=> 'number',
								'default'		=> 3
							)
						),
						'related-headline' => array(
							'title'			=> __( 'Display Headline', 'zeon' ),
							'input'			=> array(
								'type'			=> 'logic',
								'default'		=> true
							)
						),
						'related-headline-text' => array(
							'title'			=> __( 'Headline text "Related Posts"', 'zeon' ),
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'Related Posts', 'zeon' )
							)
						),
						'related-description' => array(
							'title'			=> __( 'Display related posts Description', 'zeon' ),
							'description'	=> __( 'enable / disable the quick description after the related post title.', 'zeon' ),
							'input'			=> array(
								'type'			=> 'logic',
								'default'		=> true
							)
						),
						'related-description-length' => array(
							'title'			=> __( 'Related posts Description length', 'zeon' ),
							'description'	=> __( 'The number of chars that will be show from excerpt or from content if excerpt is empty.', 'zeon' ),
							'input'			=> array(
								'type'			=> 'number',
								'default'		=> 110
							)
						)
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs ); */
?>