<?php

	/***
	 *	Template Name: Blog Classic
	 */

	function tempo__breadcrumbs_counter_query( $query )
	{
		global $post;

		$tag 		= zeon_meta::g( $post -> ID, 'tempo-template-tag' );
		$category	= zeon_meta::g( $post -> ID, 'tempo-template-category' );

		$args 		= array(
			'post_type'				=> 'post',
            'posts_per_page'        => intval( get_option( 'posts_per_page' ) ),
            'paged'                 => max( 1 , get_query_var( 'paged' ) ),
            'ignore_sticky_posts'   => 1
		);

		if( !empty( $tag ) )
			$args[ 'tag' ] = $tag;

		if( !empty( $category ) )
			$args[ 'category_name' ] = $category;

		return new WP_Query( $args );
	}

	add_filter( 'tempo_breadcrumbs_counter_query', 'tempo__breadcrumbs_counter_query' )
?>

<?php tempo_get_header( 'page' ); ?>

	<?php
		if( !is_front_page() ){
	?>
			<?php tempo_get_template_part( 'templates/prepend', 'page' ); ?>

			<?php tempo_get_template_part( 'templates/breadcrumbs/page' , 'template' ); ?>

			<?php tempo_get_template_part( 'templates/breadcrumbs/after', 'page' ); ?>
	<?php
		}
	?>

    <?php global $post; ?>

    <!-- page -->
    <div id="tempo-page" <?php echo tempo_page_class( 'template-blog-classic' ); ?>>

        <!-- container -->
        <div <?php echo tempo_container_class(); ?>>
            <div <?php echo tempo_row_class(); ?>>

                <!-- content -->
                <div <?php echo tempo_content_class(); ?>>
                    <div <?php echo tempo_row_class(); ?>>


		        		<?php tempo_get_template_part( 'templates/section/before', 'page' ); ?>

		                <section <?php echo tempo_page_section_class( $post -> ID, 'tempo-section page' ); ?>>

		                    <?php tempo_get_template_part( 'templates/section/prepend', 'page' ); ?>

		                    <?php tempo_get_template_part( 'templates/page/before' ); ?>

		                    <?php

		                    	$tag 		= zeon_meta::g( $post -> ID, 'tempo-template-tag' );
								$category	= zeon_meta::g( $post -> ID, 'tempo-template-category' );

								$attr 		= '';

			                    if( !empty( $tag ) )
									$attr .= 'tags="' . esc_attr( $tag ) . '" ';

								if( !empty( $category ) )
									$attr .= 'categories="' . esc_attr( $category ) . '" ';

		                    	echo do_shortcode( '[zeon_posts ' . $attr . ' pagination="true"/]' );
		                    ?>

		                    <?php tempo_get_template_part( 'templates/page/after' ); ?>

		                    <?php tempo_get_template_part( 'templates/section/append', 'page' ); ?>

		                </section>

		                <?php tempo_get_template_part( 'templates/section/after', 'page' ); ?>


					</div>
        		</div><!-- end content -->

            </div>
        </div><!-- end container -->

    </div><!-- end page -->

    <?php tempo_get_template_part( 'templates/append', 'page' ); ?>

<?php tempo_get_footer( 'page' ); ?>
