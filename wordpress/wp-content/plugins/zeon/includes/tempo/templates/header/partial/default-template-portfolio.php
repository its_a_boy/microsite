<?php 
	global $post;

    // header height
    $height 		= apply_filters( 'tempo_header_height', tempo_options::get( 'header-height' ) );

    // header mask
    $mask_rgb       = tempo_hex2rgb( tempo_options::get( 'header-mask-color' ) );
    $mask_transp    = number_format( floatval( tempo_options::get( 'header-mask-transp' ) / 100 ), 2 );
    $mask_rgba      = "rgba( {$mask_rgb}, {$mask_transp} );";

    // header image
    $thumbnail 		= get_post( get_post_thumbnail_id( $post -> ID ) );
    $image 			= null;

    if( has_post_thumbnail( $post -> ID ) && isset( $thumbnail -> ID ) ){

    	$media = wp_get_attachment_image_src( $thumbnail -> ID, apply_filters( 'zeon_header_thumbnail_size', 'full' ) );
			
		if( isset( $media[ 0 ] ) )
			$image = esc_url( $media[ 0 ] );
?>
		<div class="tempo-header-partial tempo-portfolio overflow-wrapper" style="height: <?php echo absint( $height ); ?>px; background-image: url(<?php echo esc_url( $image ); ?>);">

			<?php tempo_get_template_part( 'templates/header/partial/prepend', 'default-portfolio' ); ?>

			<!-- mask - a transparent foil over the header image -->
    		<div class="tempo-header-mask" style="background: <?php echo esc_attr( $mask_rgba ); ?>"></div>

			<!-- flex elements -->
		    <div class="tempo-flex-container tempo-valign-bottom">
		    	<div class="tempo-flex-item tempo-align-left">

		    		<!-- main container -->
		    		<div <?php echo tempo_container_class( 'main' ); ?>>
                		<div <?php echo tempo_row_class(); ?>>

                			<!-- content -->
	                		<div <?php echo tempo_content_class(); ?>>
	                			<div <?php echo tempo_row_class(); ?>>

					    		<?php
					    			$template = zeon_meta::get( $post -> ID, '_wp_page_template' );

					    			if( in_array( $template, array( 'blog-classic.php', 'blog-grid.php', 'blog-portfolio.php' ) ) ){
					    				tempo_get_template_part( 'templates/header/partial/portfolio/template' );	
					    			}

					    			else{
					    				$type = $post -> post_type;
					    				tempo_get_template_part( "templates/header/partial/portfolio/{$type}" );
					    			}
					    		?>

					    		</div>
		    				</div><!-- end content -->

		    			</div>
		    		</div><!-- end main container -->

		    	</div>
		    </div><!-- end flex elements -->

		</div>
<?php
	}
?>