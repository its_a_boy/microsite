<?php
if( !class_exists( 'zeon_resources' ) ){

	class zeon_resources
	{
		static function custom_posts()
		{
	        /**
	         *  Get zeon custom posts configs
	         */

	        $posts = apply_filters( 'zeon_resources_custom_posts', (array)tempo_cfgs::get( 'custom-posts' ) );

	        if( empty( $posts ) ){
	            return null;
	        }

	        foreach( $posts as $slug => & $post ){
	            if( isset( $post[ 'noregister' ] ) && $post[ 'noregister' ] ){
	                /* IF NOT REGISTER CUSTOM POST  */
	            }
	            else{
	                /* LABELS FOR ADMIN SIDE */
	                $labels = array(
	                    'name'          	=> $post[ 'plural-title' ],
	                    'singular_name' 	=> $post[ 'singular-title' ],
	                    'add_new'       	=> sprintf( __( 'Add new %s', 'zeon' ), 	$post[ 'singular-title' ] ),
	                    'add_new_item'  	=> sprintf( __( 'Add new %s' , 'zeon' ), 	$post[ 'singular-title' ] ),
	                    'edit_item'     	=> sprintf( __( 'Edit %s', 'zeon' ), 		$post[ 'singular-title' ] ),
	                    'new_item'      	=> sprintf( __( 'New %s', 'zeon' ), 		$post[ 'singular-title' ] ),
	                    'view_item'     	=> sprintf( __( 'View %s', 'zeon' ), 		$post[ 'singular-title' ] ),
	                    'search_items'  	=>  __( 'Search ', 'zeon' ),
	                    'not_found'     	=>  __( 'Nothing found' , 'zeon' ),
	                    'not_found_in_trash' => __( 'Nothing found in Trash' , 'zeon' )
	                );
	                

	                if( isset( $post[ 'fields' ] ) && is_array( $post[ 'fields' ] ) && !empty( $post[ 'fields' ] ) ){
	                    $fields = $post[ 'fields' ];
	                }else{
	                    /* DEAFULT LIST WITH FIELDS */
	                    $fields = array( 'title' , 'editor' , 'excerpt' , 'comments' , 'thumbnail' );
	                }

	                $args = array(
	                    'public'        => true,
	                    'hierarchical'  => false,
	                    'menu_position' => 5,
	                    'supports'      => $fields,
	                    'labels'        => $labels
	                );

	                /* REGISTER CUSTOM POST */
	                register_post_type( $slug , $args );
	            }
	        }
	    }

	    static function custom_taxonomies()
	    {
	    	/**
	         *  Get zeon custom taxonomies configs
	         */

	    	$taxonomies = apply_filters( 'zeon_resources_custom_taxonomies', (array)tempo_cfgs::get( 'custom-taxonomies' ) );
        
	        if( empty( $taxonomies ) || !is_array( $taxonomies ) ){
	            return null;
	        }
	        
	        foreach( $taxonomies as $post_slug => $post_taxonomies ){
	            foreach( $post_taxonomies as $taxonomy_slug => $taxonomy ){

	                $labels = array(
	                    'name'              => $taxonomy[ 'plural-title' ],
	                    'singular_name'     => $taxonomy[ 'singular-title' ],
	                    'menu_name'         => $taxonomy[ 'plural-title' ],
	                    'search_items'      => sprintf( __( 'Search %s' , 'zeon' ), 	$taxonomy[ 'plural-title' ] ),
	                    'all_items'         => sprintf( __( 'All %s' , 'zeon' ), 		$taxonomy[ 'plural-title' ] ),
	                    'parent_item'       => sprintf( __( 'Parent %s' , 'zeon' ), 	$taxonomy[ 'singular-title' ] ),
	                    'parent_item_colon' => sprintf( __( 'Parent %s' , 'zeon' ), 	$taxonomy[ 'singular-title' ] ),
	                    'edit_item'         => sprintf( __( 'Edit %s' , 'zeon' ), 		$taxonomy[ 'singular-title' ] ),
	                    'update_item'       => sprintf( __( 'Update %s' , 'zeon' ), 	$taxonomy[ 'singular-title' ] ),
	                    'add_new_item'      => sprintf( __( 'Add New %s' , 'zeon' ), 	$taxonomy[ 'singular-title' ] ),
	                    'new_item_name'     => sprintf( __( 'New %s name', 'zeon' ), 	$taxonomy[ 'singular-title' ] )
	                );

	                $args = array(
	                	'labels'    => $labels,
                    	'rewrite'	=> array( 'slug' => $taxonomy_slug )
                    );

	                if( $taxonomy[ 'hierarchical' ] ){
	                    $args = array(
	                    	'labels'        => $labels,
	                        'hierarchical'  => true,
	                        'rewrite'		=> array(
	                            'slug'          => $taxonomy_slug,
	                            'hierarchical'  => true,
	                        )
	                    );
	                }
	                 
	                register_taxonomy( $taxonomy_slug, array( $post_slug ), $args );

	                /*if( tempo_ln::is_multilang() ){
	                    if( function_exists( 'qtrans_modifyTermFormFor' ) ){
	                        add_action( $taxonomy_slug . '_add_form', 'qtrans_modifyTermFormFor');
	                        add_action( $taxonomy_slug . '_edit_form', 'qtrans_modifyTermFormFor');    
	                    }
	                }*/
	            }
	        }
	    }

	    static function custom_meta_boxes()
	    {
	    	/**
	         *  Get zeon custom meta boxes configs
	         */

	        $boxes = apply_filters( 'zeon_resources_custom_meta_boxes', (array)tempo_cfgs::get( 'custom-meta-boxes' ) );

	        if( !empty( $boxes ) ){
	            foreach( $boxes as $post_slug => & $post_boxes ) {
	                foreach( $post_boxes as $box_slug => $box ) {
	                    add_meta_box( $box_slug
	                        , $box[ 'title' ] 
	                        , $box[ 'callback' ] 
	                        , $post_slug 
	                        , $box[ 'context' ] 
	                        , $box[ 'priority' ] 
	                        , $box[ 'args' ] 
	                    );
	    				
	                    if( isset( $box[ 'save' ] ) ) {
	                        add_action( 'save_post', $box[ 'save' ], 10, 1 );
	                    }
	                }
	            }
	        }
	    }
	}
}
?>