<?php
	global $post;

	$thumbnail 			= get_post( get_post_thumbnail_id( $post ) );
	$has_post_thumbnail = has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) && wp_attachment_is( 'image', $thumbnail );

	if( $has_post_thumbnail ){
		tempo_get_template_part( 'templates/portfolio' );
		return;
	}
	
	tempo_get_template_part( 'templates/article', 'grid' );
?>