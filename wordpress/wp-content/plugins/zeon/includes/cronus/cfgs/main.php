<?php

	/**
	 *	Theme Configs
	 */

	include_once zeon_plugin_dir() . '/includes/cronus/cfgs/customizer/cronus.fonts.php';
	include_once zeon_plugin_dir() . '/includes/cronus/cfgs/meta-boxes/cfgs.php';
	include_once zeon_plugin_dir() . '/includes/cronus/cfgs/pages/advanced/sidebars.php';

	/**
	 *	Header Images Size for custom page / post header
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'images-size' ), array(
		'cronus-header'	=> array(
			'width' 	=> 2560,
			'height'	=> 1440,
			'crop' 		=> true
		)
	));

	tempo_cfgs::set( 'images-size', $cfgs );
?>
