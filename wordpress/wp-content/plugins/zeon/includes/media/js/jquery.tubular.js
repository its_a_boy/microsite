/**
 *  jQuery tubular plugin
 *  by Sean McCambridge
 *  http://www.seanmccambridge.com/tubular
 *
 *  Modified by Alexandru Popescu
 *  http://test.mythem.es/tempo-zeon-wordpress/
 *  version: 1.1
 *  updated: March 24, 2016
 *  since 2010
 *  licensed under the MIT License
 */

;(function($, window) {
    // kill for mobile devices
    var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    
    var defaults = {
        ratio                   : 16/9,
        videoId                 : 'ZCAnLxRvNNc', // toy robot in space is a good default, no?
        mute                    : true,
        repeat                  : true,
        wrapperZIndex           : 99,
        playButtonClass         : 'tubular-play',
        pauseButtonClass        : 'tubular-pause',
        muteButtonClass         : 'tubular-mute',
        volumeUpClass           : 'tubular-volume-up',
        volumeDownClass         : 'tubular-volume-down',
        increaseVolumeBy        : 10,
        start                   : 0,
        minimumSupportedWidth   : 600
    };

    var tubular = function( node, options ){
        var options = jQuery.extend({}, defaults, options);

        options.width = jQuery( node ).width();

        var container = 

        '<div class="container">' +
        '<div id="tempo-youtube-player"></div>' +
        '</div>' +
        '<div class="shield"></div>';

        jQuery( node ).prepend( container );
        jQuery( node ).css({ 'z-index': options.wrapperZIndex });

        // set up iframe player, use global scope so YT api can talk
        window.player;
        window.onYouTubeIframeAPIReady = function() {
            player = new YT.Player( 'tempo-youtube-player', {
                width       : options.width,
                height      : Math.ceil(options.width / options.ratio),
                videoId     : options.videoId,
                playerVars  : {
                    controls        : 0,
                    showinfo        : 0,
                    modestbranding  : 1,
                    wmode           : 'transparent'
                },
                events      : {
                    'onReady'       : onPlayerReady,
                    'onStateChange' : onPlayerStateChange
                }
            });
        }

        window.onPlayerReady = function( e ){
            resize();
            if (options.mute) e.target.mute();
            e.target.seekTo(options.start);
            e.target.playVideo();
        }

        window.onPlayerStateChange = function( state ){
            if( state.data === 0 && options.repeat )
                player.seekTo( options.start );
        }

        // on resize window
        var resize = function() {
            var width = jQuery( node ).width(),
                pWidth,
                height = jQuery( node ).height(),
                pHeight,
                $tplayer = jQuery( node ).find( 'div.container #tempo-youtube-player' );

            if( width / options.ratio < height ){
                pWidth = Math.ceil( height * options.ratio );
                $tplayer.width(pWidth).height(height).css({
                    'left'  : parseInt((width - pWidth) / 2),
                    'top'   : 0
                });
            }
            else{
                pHeight = Math.ceil(width / options.ratio);
                $tplayer.width(width).height( pHeight ).css({
                    'left'  : 0,
                    'top'   : parseInt((height - pHeight) / 2)
                });
            }

        }

        // events
        jQuery(window).on('resize.tubular', function() {
            resize();
        });

        jQuery( node ).on('click','.' + options.playButtonClass, function(e){
            e.preventDefault();
            player.playVideo();
        }).on('click', '.' + options.pauseButtonClass, function(e){
            e.preventDefault();
            player.pauseVideo();
        }).on('click', '.' + options.muteButtonClass, function(e){
            e.preventDefault();
            (player.isMuted()) ? player.unMute() : player.mute();
        }).on('click', '.' + options.volumeDownClass, function(e){
            e.preventDefault();
            var currentVolume = player.getVolume();
            if (currentVolume < options.increaseVolumeBy) currentVolume = options.increaseVolumeBy;
            player.setVolume(currentVolume - options.increaseVolumeBy);
        }).on('click', '.' + options.volumeUpClass, function(e){
            e.preventDefault();
            if (player.isMuted()) player.unMute();
            var currentVolume = player.getVolume();
            if (currentVolume > 100 - options.increaseVolumeBy) currentVolume = 100 - options.increaseVolumeBy;
            player.setVolume(currentVolume + options.increaseVolumeBy);
        });
    }

    var tag = document.createElement('script'); tag.type = 'text/javascript'; tag.async = true;
    tag.src = 'http://www.youtube.com/iframe_api';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(tag);

    // create plugin
    $.fn.tubular = function( options ){
        return this.each(function () {
            if (!$.data(this, 'tubular_instantiated')){
                $.data(this, 'tubular_instantiated',  tubular( jQuery( this ), options) );
            }
        });
    }

})(jQuery, window);



//if( typeof tempo_header_video_id == 'string' ){

    jQuery( document ).ready(function() {

        var options = {
            ratio: 16/9,
            videoId: 'NbESCYhKhxY', //tempo_header_video_id,
            start: 3
        };

        jQuery('div.tempo-youtube-background').tubular( options );
    });
//}