<?php
//-----------------------------------------------------------------------------
/*
Plugin Name: Kath Standort Probefahrt (Widget)
Version: 1.0
Description: AfterContent Widget to display a form for probefahrten. Has CoAuthorPlus and UserPhoto as dependencies.
Author: Jannik Clausen
Min WP Version: 3.0
*/
//-----------------------------------------------------------------------------
?>
<?php
add_action('widgets_init', create_function('', 'return register_widget("Kath_Standort_Probefahrt_Widget");'));

class Kath_Standort_Probefahrt_Widget extends WP_Widget {

	private $ansprechpartner = [];

	function __construct() {	   
		$widget_ops = array('classname' => 'Kath_Standort_Probefahrt_Widget', 'description' => "After content widget to display a Probefahrt-Formular on a post page." );
		/* Widget control settings. */
		$control_ops = array('width' => 200, 'height' => 300); // TODO - Needs to be adjusted
		parent::__construct('probefahrt', __('Kath Standort Probefahrt Widget'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {		

		// handle formulardata (if it got submitted)
		if (isset($_POST['kath_pf_submit'])) {

			var_dump($_POST);

		} else { // display the form if there was no post-data submitted

			//todo - search over all categories (not only he first one)
			if (get_the_category()[0]->name == 'Verkauf') { // only display form if post belongs to category (Verkauf)

				// If Co-Authors plus plugin exists fetch all Co-Authors emails one after another
				if(function_exists('coauthors_posts_links')) {
					$this->ansprechpartner = [];
					$i = new CoAuthorsIterator(); 
					while($i->iterate()){
						global $authordata;

						// save authors in the ansprechpartners array
						array_push($this->ansprechpartner, $authordata);
					}

					// sort the ansprechpartner array by user-role and last-name
					$this->_sortAnsprechpartners();

				} else {
					global $authordata;

					array_push($this->ansprechpartner, $authordata);
				}

				// display the form
				$this->_displayForm($args, $instance);		
			}
		}
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Kath Standort Probefahrt Widget') );
		$title = strip_tags($instance['title']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php	  
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
	
	// Sorts the ansprechpartners by role and last-name
	private function _sortAnsprechpartners() {
		// Vergleichsfunktion
		function cmp($a, $b) {
			if($a->roles[0] == $b->roles[0]) {
				if($a->last_name == $b->last_name) {
					return 0;
				}
				return ($a->last_name < $b->last_name) ? -1 : 1;
			}
			return ($a->roles[0] < $b->roles[0]) ? -1 : 1;
		}

		usort($this->ansprechpartner, 'cmp');
	}

	// Does the actual work of preparing the form markup
	private function _displayForm($args, $instance){
		extract( $args ); // extract arguments
		$isHome = is_home() || is_front_page(); //Don't show the Widget on home page

		if(!$isHome && (is_single() || is_page())){			
			echo $before_widget;

            ?><form method="post" class="row kath_pf">
                <input type="hidden" name="kath_pf_angebotstitel" value="<?=get_the_title()?>">
                <div class="form-group col-sm-6">
                    <label for="kath_pf_name">Ihr Name <span class="kath_pf_required">*</span></label>
                    <input type="text" name="kath_pf_name" required>
                </div>
                <div class="form-group col-sm-6">
                    <label for="kath_pf_email">Ihre E-Mail-Adresse <span class="kath_pf_required">*</span></label>
                    <input type="email" name="kath_pf_email" required>
                </div>
                <div class="form-group col-sm-6">
                    <label for="kath_pf_tel">Ihre Telefonnummer <span class="kath_pf_required">*</span></label>
                    <input type="text" name="kath_pf_tel" required>
                </div>
                <div class="form-group col-sm-6">
                    <label for="kath_pf_to">Standort <span class="kath_pf_required">*</span></label>
                    <select id="kath_pf_to" name="kath_pf_to" required>
                        <option value="" selected></option>
                        <option value="<?= get_cimyFieldValue($this->ansprechpartner[0]->ID, 'standort_email')?>">
                            <?=ucfirst($this->ansprechpartner[0]->roles[0])?>
                        </option>
                        <?php
                        $lastStandort = $this->ansprechpartner[0]->roles[0];

                        foreach ($this->ansprechpartner as $key => $p) {
                            // if last standort is not the same as current we need to make a new option
                            if($lastStandort != $p->roles[0]) {?>
                                <option value="<?=get_cimyFieldValue($p->ID, 'standort_email')?>">
                                    <?=ucfirst($p->roles[0])?>
                                </option>
                                <?php $lastStandort = $p->roles[0];
                            }
                        }?>
                    </select>
                </div><!-- //div.form-group -->
                <div class="form-group col-sm-12">
                    <label for="kath_pf_freitext">Was Sie uns sonst mitteilen möchten</label>
                    <textarea name="kath_pf_freitext" id="kath_pf_freitext"></textarea>
                </div>
                <div class="form-group col-sm-12 kath-star-text">
                	Die mit einem Stern (*) gekennzeichneten Felder sind Pflichtfelder.
                </div>
                <div class="form-group col-sm-12">
                	<button type="submit" class="btn btn-default kath_pf_submit" name="kath_pf_submit">Senden</button>
                </div>
            </form><?php
		}
	}
}
?>