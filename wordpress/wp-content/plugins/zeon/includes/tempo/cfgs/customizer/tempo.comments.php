<?php
	
	/**
     *  Appearance / Customize / Comments - config settings
     */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'tempo-comments' => array(
			'title'		=> __( 'Comments' , 'zeon' ),
			'priority' 	=> 57,
			'fields'	=> array(
				'comments-type' => array(
					'title' 		=> __( 'Comments Type', 'zeon' ),
					'input'			=> array(
						'type'			=> 'select',
						'default'		=> 'wordpress',
						'options'		=> array(
							'wordpress'		=> __( 'WordPress Classic', 'zeon' ),
							'facebook'		=> __( 'Facebook', 'zeon' ),
							'disqus'		=> __( 'DISQUS', 'zeon' )
						)
					)
				),
				'collapsing-classic'	=> array(
					'title'			=> __( 'Collapsing classic comments', 'zeon' ),
					'input'			=> array(
						'type'			=> 'logic',
						'default'		=> true
					)
				),
				'facebook-nr-comments' => array(
					'title'			=> __( 'Facebook number of comments', 'zeon' ),
					'description'	=> __( 'the number of comments in the thread.', 'zeon' ),
					'input'			=> array(
						'type'			=> 'int',
						'default'		=> 5
					)
				),
				'facebook-appid' => array(
					'title'			=> __( 'Facebook Application ID', 'zeon' ),
					'description'	=> __( 'is necessary to can manage the Facebook comments.', 'zeon' ),
					'input'			=> array(
						'type'			=> 'text',
						'validator'		=> 'attr'
					)
				),
				'disqus-shortname' => array(
					'title'			=> __( 'Disqus Shortname', 'zeon' ),
					'description'	=> __( 'is strictly necessary to display the Disqus comments.', 'zeon' ),
					'input'			=> array(
						'type'			=> 'text'
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>