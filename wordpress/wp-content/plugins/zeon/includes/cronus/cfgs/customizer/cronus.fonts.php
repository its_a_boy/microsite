<?php

    /**
	 *	Appearance / Customize / Typography / Theme Typography - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'easy-google-fonts' ), array(
        'fonts' => array(
            'content-sidebar-link-font'             => array(
                'priority'      => 211,
                'name' 			=> 'content-sidebar-link-font',
                'title' 		=> __( 'Content sidebar Link', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.sidebar-content-wrapper div.widget a',
                    'force_styles' 	=> null
                )
            ),
            'header-sidebar-link-font'              => array(
                'priority'      => 221,
                'name' 			=> 'header-sidebar-link-font',
                'title' 		=> __( 'Header sidebar Link', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.header-sidebar div.widget a',
                    'force_styles' 	=> null
                )
            ),
            'footer-light-sidebar-link-font'        => array(
                'priority'      => 231,
                'name' 			=> 'footer-light-sidebar-link-font',
                'title' 		=> __( 'Light footer sidebar Link', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.tempo-footer.light-sidebars div.widget a',
                    'force_styles' 	=> null
                )
            ),
            'footer-dark-sidebar-link-font'         => array(
                'priority'      => 241,
                'name' 			=> 'footer-dark-sidebar-link-font',
                'title' 		=> __( 'Dark footer sidebar Link', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.tempo-footer.dark-sidebars div.widget a',
                    'force_styles' 	=> null
                )
            )
        )
    ));

    tempo_cfgs::set( 'easy-google-fonts', $cfgs );
?>
