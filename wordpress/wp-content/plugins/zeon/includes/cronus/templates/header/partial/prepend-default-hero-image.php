<?php
	global $post;

	// header image
    $thumbnail 		= get_post( get_post_thumbnail_id( $post ) );
    $image 			= null;

    if( has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) && wp_attachment_is( 'image', $thumbnail ) ){

    	$media = wp_get_attachment_image_src( $thumbnail -> ID, 'cronus-header' );
			
		if( isset( $media[ 0 ] ) )
			$image = esc_url( $media[ 0 ] );
	}

	if( empty( $image ) )
		return;

    /**
     *  Header Image Alt Attribute
     */

    $header_image_alt   = get_the_title( $post -> ID );
?>
	<div class="parallax" style="background-image: url(<?php echo esc_url( $image ); ?>);">
        <img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $header_image_alt ); ?>" class="parallax-image"/>
    </div>