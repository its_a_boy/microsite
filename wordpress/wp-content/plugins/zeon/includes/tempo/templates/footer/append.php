<?php
	$args = array(
        'theme_location'    => 'tempo-footer',
        'container_class'   => 'tempo-menu-wrapper',
        'menu_class'        => 'tempo-menu-list',
        'depth'				=> 1
    );

    if( !has_nav_menu( 'tempo-footer' ) )
        return;
?>

<div class="tempo-nav">

	<div <?php echo tempo_container_class(); ?>>
        <div <?php echo tempo_row_class(); ?>>

            <div <?php echo tempo_full_class(); ?>>

				<nav class="tempo-navigation">
					<?php wp_nav_menu( $args ); ?>
				</nav>

			</div>

		</div>
	</div>

</div>