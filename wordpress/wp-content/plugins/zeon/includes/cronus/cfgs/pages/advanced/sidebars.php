<?php

	/**
	 *	Zeon / General - config settings
	 */

	$settings = tempo_cfgs::merge( (array)tempo_cfgs::get( 'settings' ), array(
		'advanced' => array(
			'tempo-sidebars' => array(
			    'sections' 	=> array(
			    	array(
			    		'columns' 		=> array(
							1 => array(
								'boxes' => array(
									0 => array(
										'fields'		=> array(
											0 => array(
												'input'			=> array(
													'default'		=> 3
												)
											)
										)
									)
								)
							),
							2 => array(
								'boxes' => array(
									1 => array(
										'fields'	=> array(
											0 => array(
												'input'			=> array(
													'default'		=> 4
												)
											)
										)
									)
								)
							)
			    		)
			    	)
				)
			)
		)
	));

	tempo_cfgs::set( 'settings', $settings );
?>