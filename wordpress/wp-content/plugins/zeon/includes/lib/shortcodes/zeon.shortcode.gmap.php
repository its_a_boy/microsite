<?php

    /*
        [gmap height="" zoom="" lat="" lng=""]

            ...

            [marker title="" lat="" lng=""]  description ... [/marker]
            [marker title="" lat="" lng=""]  description ... [/marker]
            [marker title="" lat="" lng=""]  description ... [/marker]
            [marker title="" lat="" lng=""]  description ... [/marker]

            ...

        [/gmap]
    */


    /**
     *  Google Map Config
     */

    $gmap = array(
        'height'    => 380,
        'zoom'      => 4,
        'lat'       => 47.772594932483,
        'lng'       => 1.8596535022735,
        'marker'    => array(
            'title'         => __( 'The Marker title' , 'zeon' ),
            'description'   => __( 'The google map marker with short description.', 'zeon' ),
            'lat'           => null,
            'lng'           => null,
            'icon'          => zeon_plugin_uri() . '/includes/lib/shortcodes/media/img/marker.png'
        )
    );

    tempo_cfgs::set( 'gmap', $gmap );


    /**
     *  Google Map Module
     */

    $cfgs = tempo_cfgs::merge( (array)zeon_modules::get( 'gmap' ), array(
        'require' => array(
            'api'       => '//maps.googleapis.com/maps/api/js?key=AIzaSyDxdTmx492T0Tb7VTUu0VHRqg9U28XnpUI',
            'module'    => zeon_plugin_uri() . '/includes/lib/shortcodes/media/js/gmap.js'
        ),
        'args' => array(
        )
    ));

    if( is_admin() ){
        $cfgs[ 'require' ][ 'module' ] = zeon_plugin_uri() . '/includes/lib/shortcodes/media/admin/js/gmap.js';
    }

    zeon_modules::set( 'gmap',  $cfgs );

    class zeon_shortcode_gmap
    {
        private static $run     = false;
        private static $do      = true;
        private static $cfgs = array();

        static function run( $_attr, $_content = null )
        {
            if( !self::$do ){
                return;
            }

            self::$do   = false;
            self::$run  = true;
            self::$cfgs = array();

            $attr = shortcode_atts( tempo_cfgs::get( 'gmap' ), $_attr );

            $prop  = esc_attr( zeon_shortcode_prop( $_attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            if( !empty( $attr[ 'zoom' ] ) && (int)$attr[ 'zoom' ] > 0 ){
                self::$cfgs[ 'zoom' ] = (int)$attr[ 'zoom' ];
            }

            if( !empty( $attr[ 'lat' ] ) && (float)$attr[ 'lat' ] !== null ){
                self::$cfgs[ 'lat' ] = (float)$attr[ 'lat' ];
            }

            if( !empty( $attr[ 'lng' ] ) && (float)$attr[ 'lng' ] !== null ){
                self::$cfgs[ 'lng' ] = (float)$attr[ 'lng' ];
            }

            $classes = 'tempo-canvas';

            if( !empty( $_attr[ 'class' ] ) ){
                $classes .= ' ' . esc_attr( $_attr[ 'class' ] );
            }

            $gmap = zeon_modules::get( 'gmap' );

            $rett  = '<div class="tempo-shortcode gmap" ' . $style . '>';
            $rett .= '<div class="' . esc_attr( $classes ) . '" style="height:' . $attr[ 'height' ] . 'px;"></div>';
            $rett .= '</div>';

            do_shortcode( $_content );

            $gmap[ 'args' ][] = self::$cfgs;
            zeon_modules::set( 'gmap', $gmap );


            self::$do   = true;
            self::$run  = false;

            return $rett;
        }

        public $code = null;
        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $boxes[ 'preview' ] = array(
                array(
                    'title'     => __( 'Google Map Preview', 'zeon' ),
                    'content'   => do_shortcode( '[zeon_gmap class="tempo-shortcode" zoom="4" lat="47.772594932483" lng="1.8596535022735"][zeon_marker title="' . __( 'Marker title' , 'zeon' ) . '"]' . __( 'Google map marker with short description.', 'zeon' ) . '[/zeon_marker][/zeon_gmap]' )
                )
            );

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Height', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'id'        => 'zeon-gmap-height',
                        'type'      => 'number',
                        'name'      => 'gmap-height',
                        'default'   => 380,
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'     => __( 'Zoom', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'id'        => 'zeon-gmap-zoom',
                        'type'      => 'number',
                        'name'      => 'gmap-zoom',
                        'default'   => 4,
                        'min'       => 1,
                        'max'       => 21,
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'         => __( 'Latitude' , 'zeon' ),
                    'description'   => __( 'the center of the map' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'id'            => 'zeon-gmap-lat',
                        'type'          => 'text',
                        'name'          => 'gmap-lat',
                        'default'       => '47.772594932483',
                        'theme_mod'     => false
                    )
                ),
                array(
                    'title'         => __( 'Longitude' , 'zeon' ),
                    'description'   => __( 'the center of the map' , 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'id'            => 'zeon-gmap-lng',
                        'type'          => 'text',
                        'name'          => 'gmap-lng',
                        'default'       => '1.8596535022735',
                        'theme_mod'     => false
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Google Map Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                )
            );

            $fields[ 'marker' ] = array(
                array(
                    'title'     => __( 'Title', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'id'        => 'zeon-gmap-marker-title',
                        'type'      => 'text',
                        'name'      => 'gmap-marker-title',
                        'default'   => __( 'The Marker title' , 'zeon' ),
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'     => __( 'Description', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'id'        => 'zeon-gmap-marker-description',
                        'type'      => 'textarea',
                        'name'      => 'gmap-marker-description',
                        'default'   => __( 'The google map marker with short description.' , 'zeon' ),
                        'theme_mod' => false
                    )
                )
            );

            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'Marker Settings', 'zeon' ),
                    'fields'    => $fields[ 'marker' ]
                )
            );

            $title          = '<span class="gmap-marker-title attr">title="<span class="value">' . __( 'The Marker title' , 'zeon' ) . '</span>"</span>';
            $description    = '<span class="gmap-marker-description text"><span class="value">' . __( 'The google map marker with short description.' , 'zeon' ) . '</span></span>';

            $height         = '<span class="gmap-height attr">height="<span class="value">380</span>"</span>';
            $zoom           = '<span class="gmap-zoom attr">zoom="<span class="value">4</span>"</span>';
            $lat            = '<span class="gmap-lat attr">lat="<span class="value">47.772594932483</span>"</span>';
            $lng            = '<span class="gmap-lng attr">lng="<span class="value">1.8596535022735</span>"</span>';

            $this -> code   =   '[zeon_gmap ' . $height . ' ' . $zoom  . ' ' . $lat . ' ' . $lng . ']' . '<br/>' . "\n" .
                                '[zeon_marker ' . $title . ']' . $description . '[/zeon_marker]' . '<br/>' . "\n" .
                                '[/zeon_gmap]';

            return array(
                array(
                    'boxes'     => $boxes[ 'preview' ],
                    'layout'    => array(
                        'sm' => 12,
                        'md' => 12,
                        'lg' => 12
                    )
                ),
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes[ 'right' ]
                )
            );
        }

        public static function is_run(){
            return self::$run;
        }

        public static function get_cfgs()
        {
            return self::$cfgs;
        }

        public static function set_cfgs( $cfgs )
        {
            self::$cfgs = $cfgs;
        }
    }

    class zeon_shortcode_marker
    {
        private static $do = true;

        public static function run( $_attr, $_content = null )
        {
            if( !( self::$do && zeon_shortcode_gmap::is_run() ) ){
                return;
            }

            self::$do = false;

            $gmap = tempo_cfgs::get( 'gmap' );

            $attr = shortcode_atts( $gmap[ 'marker' ], $_attr );

            $cfgs = zeon_shortcode_gmap::get_cfgs();

            if( empty( $attr[ 'lat' ] ) ){
                $attr[ 'lat' ] = (float)$cfgs[ 'lat' ];
            }
            else{
                $attr[ 'lat' ] = (float)$attr[ 'lat' ];
            }

            if( empty( $attr[ 'lng' ] ) ){
                $attr[ 'lng' ] = (float)$cfgs[ 'lng' ];
            }
            else{
                $attr[ 'lng' ] = (float)$attr[ 'lng' ];
            }

            $marker = array(
                'lat' => (float)$attr[ 'lat' ],
                'lng' => (float)$attr[ 'lng' ]
            );

            if( !empty( $attr[ 'icon' ] ) ){
                $marker[ 'icon' ] = esc_url( $attr[ 'icon' ] );
            }

            if( !empty( $attr[ 'title' ] ) ){
                $marker[ 'title' ] = strip_tags( $attr[ 'title' ] , '<span><b><i><em><strong><u>' );
            }

            if( !empty( $_content ) ){
                $marker[ 'description' ] = do_shortcode( $_content );
            }

            $cfgs[ 'markers' ][] = $marker;

            zeon_shortcode_gmap::set_cfgs( $cfgs );

            self::$do = true;
        }
    }
?>
