<?php
//-----------------------------------------------------------------------------
/*
Plugin Name: Kath Standort Ansprechpartner (Widget)
Version: 1.0
Description: Sidebar to display ansprechpartner. Has CoAuthorPlus and UserPhoto as dependencies.
Author: Jannik Clausen
Min WP Version: 3.0
*/
//-----------------------------------------------------------------------------
?>
<?php
add_action('widgets_init', create_function('', 'return register_widget("Kath_Standort_Ansprechpartner_Widget");'));

class Kath_Standort_Ansprechpartner_Widget extends WP_Widget {

	var $icon_image_url;

	private $ansprechpartner = [];

	function __construct() {	   
		$widget_ops = array('classname' => 'Kath_Standort_Ansprechpartner_Widget', 'description' => "Sidebar widget to display Ansprechpartner(s)' profile on a post page." );
		/* Widget control settings. */
		$control_ops = array('width' => 200, 'height' => 300);
		parent::__construct('ansprechpartner', __('Kath Standort Ansprechpartner Widget'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {		
		// If Co-Authors plus plugin exists display all Co-Author profiles one after another
		if(function_exists('coauthors_posts_links')) {
			$this->ansprechpartner = [];
			$i = new CoAuthorsIterator(); 
			while($i->iterate()){
				global $authordata;

				// save authors in the ansprechpartners array
				array_push($this->ansprechpartner, [$args, $instance, $authordata]);
			}

			// sort the ansprechpartner array by user-role
			$this->_sortAnsprechpartners();

		} else {
			global $authordata;

			array_push($this->ansprechpartner, [$args, $instance, $authordata]);
		}


		// display the ansprechpartners
		$it_end = sizeof($this->ansprechpartner) - 1;
		$last_role = $this->ansprechpartner[0][2]->roles[0];
		foreach ($this->ansprechpartner as $key => $p) {
			// override global authordata (used by many global wordpress-functions)
			$authordata = $p[2];

			$p[1]['seq'] = $key;
			$p[1]['isLast'] = $key == $it_end;
			$p[1]['lastRole'] = $last_role;

			$this->_displayAuthor($p[0], $p[1]);

			$last_role = $authordata->roles[0];
		}
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Kath Standort Ansprechpartner Widget') );
		$title = strip_tags($instance['title']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php	  
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
	
	// Sorts the ansprechpartners by role and last-name
	private function _sortAnsprechpartners() {
		// Vergleichsfunktion
		function cmp1($a, $b) {
			$u1 = $a[2];
			$u2 = $b[2];
			if($u1->roles[0] == $u2->roles[0]) {
				if($u1->last_name == $u2->last_name) {
					return 0;
				}
				return ($u1->last_name < $u2->last_name) ? -1 : 1;
			}
			return ($u1->roles[0] < $u2->roles[0]) ? -1 : 1;
		}

		usort($this->ansprechpartner, 'cmp1');
	}

	// Does the actual work of preparing the profile markup
	private function _displayAuthor($args, $instance){
		global $authordata;
		extract( $args ); // extract arguments
		$isHome = is_home() || is_front_page(); //Don't show the Widget on home page

		if(!$isHome && (is_single() || is_page()) && $authordata->ID){			
			if($instance['seq'] == 0){ // this is the first entry -> add opening tags
				echo $before_widget;
				if (class_exists('MultiPostThumbnails')) {
				    MultiPostThumbnails::the_post_thumbnail(
				        get_post_type(),
				        'secondary-image'
				    );
				}

				// open accordion
				echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';

				// open first panel
				echo '<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading' . $authordata->roles[0] . '">
				      <h4 class="panel-title">
				        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $authordata->roles[0] . '" aria-expanded="false" aria-controls="collapse' . $authordata->roles[0] . '">
				          Standort ' . ucwords(str_replace('_', '-', $authordata->roles[0])) . '
				        </a>
				      </h4>
				    </div>
				    <div id="collapse' . $authordata->roles[0] . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $authordata->roles[0] . '">
				      <div class="panel-body">';
			}			

			// neuer standort?
			if($authordata->roles[0] != $instance['lastRole']) {
				// close old panel
				echo '</div></div></div>';

				// open new one
				echo '<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading' . $authordata->roles[0] . '">
				      <h4 class="panel-title">
				        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $authordata->roles[0] . '" aria-expanded="false" aria-controls="collapse' . $authordata->roles[0] . '">
				          Standort ' . ucwords(str_replace('_', '-', $authordata->roles[0])) . '
				        </a>
				      </h4>
				    </div>
				    <div id="collapse' . $authordata->roles[0] . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $authordata->roles[0] . '">
				      <div class="panel-body">';
			}

			echo '<div class="ansprechpartner">';

			//Display User photo OR the Gravatar
			if(function_exists('userphoto_exists') && userphoto_exists($authordata)){
				userphoto_thumbnail($authordata);
			}
			else {
				echo get_avatar($authordata->ID, 96);	
			}	

			// Display author's name
			echo '<div class="ansprechpartner-data"><strong>'.get_the_author_meta('first_name').' '.get_the_author_meta('last_name').'</strong><br>';
			
			
			// additional profile info (position, telephone, email)
			echo get_the_author_meta('position') . '<br>';
			echo get_the_author_meta('telephone') . '<br>';
			echo get_the_author_meta('email') . '</div>';
			echo '<div class="clearfix"></div>';

			// closing tags for ansprechpartners
			echo "</div><!--.ansprechpartner-->";
			
			if($instance['isLast']) { // this is the last entry -> add closing tags
				echo '</div></div></div>'; // close panel
				echo '</div>'; // close accordion
				echo $after_widget;
			}
		}
	}
}
?>