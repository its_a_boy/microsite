<?php

	/**
     *  Post Categories
     *	This widget can be used only for single post template
     */

	if( !class_exists( 'zeon_widget_categories' ) ){
		class zeon_widget_categories extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_categories', __( 'Categories', 'zeon' ) . ' [zeon]', array( 
		            'classname'     => 'zeon_widget_categories',
		            'description'   => __( 'The all Categories with attached Colors and Nr. posts', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		        global $post;

		        // extract args
		        extract( $args , EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => __( 'Categories', 'zeon' )
		        ));

		        $categories = get_categories();


		        if( !empty( $categories ) ){

		            echo $before_widget;

		            if( !empty( $instance[ 'title' ] ) ){
		                echo $before_title;
		                echo apply_filters( 'widget_title', sanitize_text_field( $instance[ 'title' ] ), $instance, $this -> id_base );
		                echo $after_title;
		            }

		            $categories = get_categories();

		            echo '<div><ul>';

		            foreach( $categories as $c ){

		                $link = esc_url( get_term_link( $c -> term_id , 'category' ) );

		                if ( is_wp_error( $link ) )
		                    continue;

		                $classes = 'category tempo-category-' . absint( $c -> term_id );

		                if( tempo_options::is_set( 'category-' . $c -> term_id ) )
		                	$classes .= ' is-set-color';


		                echo '<li>';
		                echo '<a href="' . esc_url( $link ) . '" rel="category">' . $c -> name . ' <span class="' . esc_attr( $classes ) . '">' . absint( $c -> count ) . '</span></a>';
		                echo '</li>';
		            }

		            echo '</ul></div>';

		            echo $after_widget;
		        }
		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               = $old_instance;
		        $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );

		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => null
		        ));

		        $title = sanitize_text_field( $instance[ 'title' ] );

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
