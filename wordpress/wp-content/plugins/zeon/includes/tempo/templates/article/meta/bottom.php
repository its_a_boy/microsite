<?php
    global $post;

    $show_author    = tempo_options::get( 'blog-author' );
    $show_time      = tempo_options::get( 'blog-time' );

    if( !( $show_author || $show_time ) )
        return;
?>

<div class="tempo-meta bottom">

    <?php
        // author
        $name    = get_the_author_meta( 'display_name' , $post -> post_author );
        $author  = '<a class="author" href="' . esc_url( get_author_posts_url( $post-> post_author ) ) . '" title="' . sprintf( __( 'Posted by %s' , 'zeon' ) , esc_attr( $name ) ) . '">';
        $author .=  '<i class="tempo-icon-user-5"></i> ' . esc_html( $name ) . '</a>';

        // time
        $y      = esc_attr( get_post_time( 'Y', false, $post -> ID ) );
        $m      = esc_attr( get_post_time( 'm', false, $post -> ID ) );
        $d      = esc_attr( get_post_time( 'd', false, $post -> ID ) );
        $dtime  = get_post_time( 'Y-m-d', false, $post -> ID  );
        $ptime  = get_post_time( esc_attr( get_option( 'date_format' ) ), false , $post -> ID, true );
    
        $time   = '<a href="' . esc_url( get_day_link( $y , $m , $d ) )  . '" title="' . sprintf( __( 'posted on %s', 'zeon' ), $ptime ) . '"><time datetime="' . esc_attr( $dtime ) . '"><i class="tempo-icon-clock-1"></i> ' . esc_html( $ptime ) . '</time></a>';

        if( $show_author && $show_time ){
            echo $author . ' ' . $time;
        }
        else if( !$show_author ){
            echo $time;
        }
        else if( !$show_time ){
            echo $author;
        }
    ?>
</div>