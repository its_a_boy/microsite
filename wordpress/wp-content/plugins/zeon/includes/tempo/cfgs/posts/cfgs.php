<?php

	/**
	 *	Custom Posts - config file
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-posts' ), array(
		'header' 	=> array(
			'singular-title' 	=> __( 'Header', 'zeon' ),
		    'plural-title' 		=> __( 'Headers', 'zeon' ),
		    'archive-slug' 		=> 'portfolio',
		    'fields' 			=> array(
		        'title'
		    )
		)
	));

	//tempo_cfgs::set( 'custom-posts', $cfgs );
?>