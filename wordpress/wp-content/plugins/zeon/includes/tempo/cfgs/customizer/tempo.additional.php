<?php

	/**
	 *	Appearance / Customize / Additional - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
	 	'tempo-additional' => array(
			'fields'	=> array(
				'aside-scroll' => array(
					'title'			=> __( 'Aside Scrolling', 'zeon' ),
	                'description'	=> __( 'enable / disable aside scrolling if the content is biggest.', 'zeon' ),
	                'priority' 		=> 11,
					'input'		=> array(
						'type'		=> 'checkbox',
						'default'	=> true
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>