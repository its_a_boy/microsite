<?php global $post; ?>

<div <?php echo tempo_full_class(); ?>>


	<!-- CATEGORIES -->
	<?php
		if( tempo_options::get( 'post-categories' ) && has_category( null, $post ) ){
	?>
			<div class="tempo-categories">
				<?php the_category( '<span>/</span>' ); ?>
			</div>
	<?php
		}
	?>


	<!-- POST TITLE -->
	<h1 class="tempo-headline"><?php echo get_the_title( $post ); ?></h1>

	<hr/>


	<!-- HEADER META -->
	<?php
	    $show_author    = tempo_options::get( 'post-author' );
	    $show_time      = tempo_options::get( 'post-time' );

	    $meta      		= $show_author || $show_time;

	    if( !apply_filters( 'tempo_post_header_meta', $meta, $post -> ID ) )
	        return;
	?>

	<div class="tempo-meta header">

	    <?php
	        // author
	        $name   = get_the_author_meta( 'display_name' , $post -> post_author );
	        $author = '<i class="tempo-icon-user-5 author-icon"></i>  <a class="author" href="' . esc_url( get_author_posts_url( $post -> post_author ) ) . '" title="' . sprintf( __( 'Posted by %s' , 'zeon' ) , esc_attr( $name ) ) . '">' . $name . '</a>';

	        // time
	        $y      = esc_attr( get_post_time( 'Y', false, $post ) );
	        $m      = esc_attr( get_post_time( 'm', false, $post ) );
	        $d      = esc_attr( get_post_time( 'd', false, $post ) );
	        $dtime  = get_post_time( 'Y-m-d', false, $post  );
	        $ptime  = get_post_time( esc_attr( get_option( 'date_format' ) ), false , $post, true );
	    
	        $time   = '<i class="tempo-icon-clock-1 clock-icon"></i> <a href="' . esc_url( get_day_link( $y , $m , $d ) )  . '" title="' . sprintf( __( 'posted on %s', 'zeon' ), $ptime ) . '"><time datetime="' . esc_attr( $dtime ) . '">' . $ptime . '</time></a>';

	        if( $show_author && $show_time ){
	            printf( __( 'Posted by %s on %s', 'zeon' ), $author, $time );
	        }
	        else if( !$show_author ){
	            printf( __( 'Posted on %s', 'zeon' ), $time );
	        }
	        else if( !$show_time ){
	            printf( __( 'Posted by %s', 'zeon' ), $author );
	        }
	    ?>

	</div>

</div>