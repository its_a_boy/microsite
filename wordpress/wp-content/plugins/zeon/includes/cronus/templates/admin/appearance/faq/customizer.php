<p><?php _e( 'This theme comes with a set of options what allow you to customize content, header, layouts, social items and others. You can see next theme options panels if you go to:', 'zeon' ); ?></p>
<p><strong><?php _e( 'Appearance &rsaquo; Customize', 'zeon' ); ?></strong></p>
<p><?php _e( 'Here you can see:', 'zeon' ); ?></p>

<ol>
	<li><?php _e( 'Site Identity', 'zeon' ); ?></li>
	<li><?php _e( 'Colors', 'zeon' ); ?></li>
	<li><?php _e( 'Background Image', 'zeon' ); ?></li>
	<li><?php _e( 'Menu Appearance', 'zeon' ); ?></li>
	<li><?php _e( 'Header Image', 'zeon' ); ?></li>
	<li>
		<?php _e( 'Header Elements', 'zeon' ); ?>

		<ul>
			<li><?php _e( 'General', 'zeon' ) ?></li>
			<li><?php _e( 'Appearance', 'zeon' ) ?></li>
			<li><?php _e( 'Headline', 'zeon' ) ?></li>
			<li><?php _e( 'Description', 'zeon' ) ?></li>
			<li><?php _e( 'First Button', 'zeon' ) ?></li>
			<li><?php _e( 'Second Button', 'zeon' ) ?></li>
		</ul>
	</li>
	<li><?php _e( 'Additional', 'zeon' ); ?></li>
	<li><?php _e( 'Breadcrumbs', 'zeon' ); ?></li>
	<li>
		<?php _e( 'Sidebars [ available only if Zeon is Active ]', 'zeon' ); ?>

		<ul>
			<li><?php _e( 'Header Front Page', 'zeon' ) ?></li>
			<li><?php _e( 'Header Blog', 'zeon' ) ?></li>
			<li><?php _e( 'Footer Light Section', 'zeon' ) ?></li>
			<li><?php _e( 'Footer Dark Section', 'zeon' ) ?></li>
		</ul>
	</li>
	<li>
		<?php _e( 'Layout', 'zeon' ); ?>

		<ul>
			<li><?php _e( 'General', 'zeon' ) ?></li>
			<li><?php _e( 'Blog &amp; Archive', 'zeon' ) ?></li>
			<li><?php _e( 'Front Page', 'zeon' ) ?></li>
			<li><?php _e( 'Page', 'zeon' ) ?></li>
			<li><?php _e( 'Post', 'zeon' ) ?></li>
		</ul>
	</li>
	<li><?php _e( 'Blog', 'zeon' ); ?></li>
	<li><?php _e( 'Single Post', 'zeon' ); ?></li>
	<li><?php _e( 'Categories [ available only if Zeon is Active ]', 'zeon' ); ?></li>
	<li><?php _e( 'Comments [ available only if Zeon is Active ]', 'zeon' ); ?></li>
	<li><?php _e( 'Social', 'zeon' ); ?></li>
	<li>
		<?php _e( 'Others', 'zeon' ); ?>

		<ul>
			<li><?php _e( 'Copyright', 'zeon' ) ?></li>
			<li><?php _e( 'Custom CSS', 'zeon' ) ?></li>
		</ul>

	</li>
	<li><?php _e( 'Menus', 'zeon' ); ?></li>
	<li><?php _e( 'Widgets', 'zeon' ); ?></li>
	<li><?php _e( 'Static Front Page', 'zeon' ); ?></li>
</ol>

<p><?php _e( 'All you have to do is play with them and view live changes. Try and you will discover how easy it is to customize your own style.', 'zeon' ); ?></p>
