<?php

    /*
        Space - allow you to put space between the two elements.
        [space height="20"/]
    */

    class zeon_shortcode_space {
    	static function run( $_attr, $_content = null )
    	{
    		$attr = shortcode_atts( array(
    			'height' => 20,
            ), $_attr );

            /* STYLE AND CUSTOM PROPERTIES */
            $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

    		return '<div class="tempo-shortcode space" class="clearfix" ' . $style . '></div>';
    	}

        public $code;

        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields = array(
                array(
                    'title'     => __( 'Space Height', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'space-height',
                        'default'   => 50,
                        'theme_mod' => false
                    )
                )
            );

            $boxes[ 'left' ][] = array(
                'title'     => __( 'Space Settings', 'zeon' ),
                'fields'    => $fields
            );

            $height         = '<span class="space-height attr">height="<span class="value">50</span>"</span>';

            $this -> code   = '[zeon_space ' . $height . '/]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                )
            );
        }
    }

?>
