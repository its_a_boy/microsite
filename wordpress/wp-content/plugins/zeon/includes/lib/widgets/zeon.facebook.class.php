<?php

	/**
	 *	Facebook Like Box
	 */

	if( !class_exists( 'zeon_widget_facebook' ) ){

		class zeon_widget_facebook extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_facebook', __( 'Facebook Like Box', 'zeon' ) . ' [zeon]', array( 
		            'classname'     => 'zeon_widget_facebook',
		            'description'   => __( 'The facebook like box from your facebook page.', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		    	// extract args
		        extract( $args , EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => '',
		            'url'   => ''
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $url    = esc_url( $instance[ 'url' ] );

		        echo $before_widget;

		        if( !empty( $title ) ){
		            echo $before_title;
		            echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
		            echo $after_title;
		        }

		        if( zeon_is_url( $url ) ){
		            echo '<iframe src="http://www.facebook.com/plugins/likebox.php?href=' . urlencode( esc_url( $url ) ) . '&amp;colorscheme=light&amp;connections=8&amp;stream=false&amp;header=false&amp;&amp;show_border=false" style="border: none; visibility: visible; width: 340px; height: 214px;" allowfullscreen="true"></iframe>';
		        }
		        else{
		            echo '<p>' . __( 'The URL of Facebook page is invalid !', 'zeon' ) . '</p>';
		        }

		        echo $after_widget;

		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               = $old_instance;
		        $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );
		        $instance[ 'url' ]      = esc_url( $new_instance[ 'url' ] );

		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => '',
		            'url'   => ''
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $url    = esc_url( $instance[ 'url' ] );

		        if( empty( $url ) ){
		            echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "facebook page URL"', 'zeon' ) . '</p>';
		        }

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'url' ) . '">' . __( 'Facebook page URL', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'url' ) . '" name="' . $this -> get_field_name( 'url' ) . '" value="' . esc_url( $url ) . '" />';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
