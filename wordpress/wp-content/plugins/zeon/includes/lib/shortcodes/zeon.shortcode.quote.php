<?php

    /*
        [quote align="none/left/center/justify/right" top="" bottom=""]
            Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
            fermentum massa justo sit amet risus.
        [/quote]
    */

    class zeon_shortcode_quote
    {
        static function run( $_attr, $_content = null )
        {
            $attr = shortcode_atts( array(
                'align'     => 'none',
                'top'       => 25,
                'bottom'    => 25
            ), $_attr );

            /* STYLE AND PROPERTIES */
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $rett = '';

            $rett .= '<blockquote class="tempo-shortcode quote align-' . esc_attr( $attr[ 'align' ] ) . '" ' . $style . '>';
            $rett .= do_shortcode( strip_tags( $_content , '<p>' ) );
            $rett .= '</blockquote>';

            return $rett;
        }

        public $code;

        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Text Alignment', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'quote-align',
                        'default'   => 'none',
                        'theme_mod' => false,
                        'options'   => array(
                            'none'      => __( 'None' , 'zeon' ),
                            'left'      => __( 'Left' , 'zeon' ),
                            'center'    => __( 'Center' , 'zeon' ),
                            'justify'   => __( 'Justify' , 'zeon' ),
                            'right'     => __( 'Right' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'quote-top',
                        'theme_mod' => false,
                        'default'   => 25
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'quote-bottom',
                        'theme_mod' => false,
                        'default'   => 25
                    )
                )
            );

            $boxes[ 'left' ][] = array(
                'title'     => __( 'Quote Settings', 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );

            $fields[ 'content' ][] = array(
                'description'   => sprintf( __( 'can use only %s from HTML tags', 'zeon' ), '<code>&lt;p&gt;</code>' ),
                'format'        => 'across',
                'input'         => array(
                    'type'      => 'textarea',
                    'name'      => 'quote-content',
                    'theme_mod' => false,
                    'default'   => '...'
                )
            );

            $boxes[ 'right' ][] = array(
                'title'     => __( 'Quote Content', 'zeon' ),
                'fields'     => $fields[ 'content' ]
            );

            $align          = '<span class="quote-align attr">align="<span class="value">none</span>"</span>';
            $top            = '<span class="quote-top attr">top="<span class="value">25</span>"</span>';
            $bottom         = '<span class="quote-bottom attr">bottom="<span class="value">25</span>"</span>';
            $content        = '<span class="quote-content text"><span class="value">...</span></span>';

            $this -> code   = '[zeon_quote ' . $align . ' ' . $top . ' ' . $bottom . ']' . $content . '[/zeon_quote]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes[ 'right' ]
                )
            );
        }
    }
?>
