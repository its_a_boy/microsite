<?php

    /*
        [delimiter/]
    */

    class zeon_shortcode_delimiter
    {
        static function run( $_attr, $_content = null )
        {
            /* STYLE AND PROPERTIES */
            $prop = esc_attr( zeon_shortcode_prop( $_attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $rett = '';

            $rett .= '<div class="tempo-shortcode delimiter" ' . $style . '>';
            $rett .= '<hr/>';
            $rett .= '</div>';

            return $rett;
        }

        public $code = null;

        public function builder()
        {
            $fields = array();
            $boxes  = array();


            $fields = array(
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'delimiter-top',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'delimiter-bottom',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                )
            );

            $boxes[] = array(
                'title'     => __( 'Delimiter Settings', 'zeon' ),
                'fields'    => $fields
            );


            $top            = '<span class="delimiter-top attr">top="<span class="value">30</span>"</span>';
            $bottom         = '<span class="delimiter-bottom attr">bottom="<span class="value">30</span>"</span>';

            $this -> code   = '[zeon_delimiter ' . $top . ' ' . $bottom . '/]';


            return array(
                array(
                    'boxes' => $boxes
                )
            );
        }
    }
?>
