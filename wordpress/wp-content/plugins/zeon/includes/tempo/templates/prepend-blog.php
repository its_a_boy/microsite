<?php
    
    /**
     *  check if Number of Columns is not ZERO
     */

    if( 0 == tempo_options::get( 'header-blog-nr-sidebars' ) )
        return;

    /**
     *  check if is active Sidebar
     */

    if( !is_active_sidebar( 'blog-header' ) )
        return;
?>     

    <!-- aside -->
    <aside class="content tempo-blog header-sidebar">

        <!-- container -->
        <div <?php echo tempo_container_class(); ?>>
            <div <?php echo tempo_row_class(); ?>>

                <!-- content -->
                <div <?php echo tempo_content_class(); ?>>
                    <div <?php echo tempo_row_class( 'widgets-row' ); ?>>

                        <?php dynamic_sidebar( 'blog-header' ); ?>
                            
                    </div>
                </div><!-- end content -->

            </div>
        </div><!-- end container -->

    </aside><!-- end aside -->