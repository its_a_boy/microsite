<?php
$shortcodes = array(
    'icon',
    'dropcap',
    'quote',

    'delimiter',

    'button',
    'alert',
    'notify',                       'notify_title'          => 'notify',
                                    'notify_description'    => 'notify',
    'message',                      'message_text'          => 'message',
                                    'message_button'        => 'message',
    'space',

    'list',                         'list_item'             => 'list',
    'social',                       'social_item'           => 'social',
    'timeline',                     'timeline_item'         => 'timeline',
    'counters',                     'counter'               => 'counters',
                                    'counter_description'   => 'counters',

    'columns',                      'column'                => 'columns',
//    'feature',
    

    'sponsors',                     'sponsor'               => 'sponsors',

    'gmap',                         'marker'                => 'gmap',
    'code',
    'max',

    'posts'
);

return $shortcodes;
?>