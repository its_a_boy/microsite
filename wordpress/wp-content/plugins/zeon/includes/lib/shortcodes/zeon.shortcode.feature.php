<?php

    /*
        Features - allows creating and presenting detailed information about features
    */

    class zeon_shortcode_feature
    {
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            $attr = shortcode_atts( array(
                'src'           => null,
                'url'           => null,
                'type'          => null,
                'position'      => null,
                'text_align'    => null,
                'max_size'      => 240,
                'description'   => ''
            ), $_attr );

            if( empty( $attr[ 'src' ] ) )
                return '';

            unset( $_attr[ 'max_size' ] );

            /* STYLE AND CUSTOM PROPERTIES */
            $prop  = esc_attr( zeon_shortcode_prop( $_attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $rett  .= '<div class="tempo-shortcode feature row" ' . $style . '>';

            $image = '<img src="' . esc_url( $_attr[ 'src' ] ) . '" alt="' . esc_attr( strip_tags( $attr[ 'description' ] ) ) . '" ' . $style . '/>';
            $media = '';

            $url   = '';

            if( !empty( $attr[ 'url' ] ) ){
                $media .= '<a href="' . esc_url( $_attr[ 'url' ] ) . '" title="' . esc_attr( strip_tags( $attr[ 'description' ] ) ) . '" target="' . esc_attr( tempo_link::target() ) . '">';
                $media .= $image;
                $media .= '</a>';

                $url = '<a href="' . esc_url( $_attr[ 'url' ] ) . '"></a>';
            }

            else{
                $media .= $image;
            }

            $rett .= '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 feature-item ' . esc_attr( $attr[ 'position' ] ) . '">';
            $rett .= '<div class="valign-cell-wrapper">';
            $rett .= '<div class="valign-cell">';
            $rett .= '<div class="feature-media">';

            if( empty( $attr[ 'type' ] ) || $attr['type'] == 'sample' ){
                $rett .= $media;
            }

            else{
                $rett .= do_shortcode( '[zeon_figure top="0" bottom="0" type="' . esc_attr( $attr[ 'type' ] ) . '" align="center" max_size="' . esc_attr( $attr[ 'max_size' ] ) . '" bg_image="' . esc_attr( $attr[ 'src' ] ) . '"]' . $url . '[/zeon_figure]' );
            }

            $rett .= '</div>';

            $rett .= '</div>';
            $rett .= '</div>';
            $rett .= '</div>';

            /* CONTENT */
            $rett .= '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 feature-item">';
            $rett .= '<div class="valign-cell-wrapper">';
            $rett .= '<div class="valign-cell">';

            $rett .= '<div class="feature-content ' . esc_attr( $attr[ 'text_align' ] ) . '">';

            $rett .= do_shortcode( $_content );

            $rett .= '</div>';

            $rett .= '</div>';
            $rett .= '</div>';
            $rett .= '</div>';

            $rett  .= '<div class="clearfix"></div>';
            $rett  .= '</div>';

            /* ALLOW RUN JS FOR MODULE FEATURE */
            //tempo_scripts::set_js_sett( 'feature' );

            return $rett;
        }

        public $code;
        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Image Type', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'feature-image-type',
                        'theme_mod' => false,
                        'default'   => 'sample',
                        'options'   => array(
                            'sample'    => __( 'Sample' , 'zeon' ),
                            'squared'   => __( 'Squared' , 'zeon' ),
                            'rounded'   => __( 'Rounded' , 'zeon' ),
                            'circled'   => __( 'Circled' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Image Max Size', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'feature-max-size',
                        'theme_mod' => false,
                        'default'   => 240
                    )
                ),
                array(
                    'title'     => __( 'Text Align', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'feature-align',
                        'theme_mod' => false,
                        'default'   => 'left',
                        'options'   => array(
                            'left'      => __( 'Left' , 'zeon' ),
                            'right'     => __( 'Right' , 'zeon' ),
                            'center'    => __( 'Center' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'         => __( 'Inner Space', 'zeon' ),
                    'description'   => __( 'the space before and after content ( in pixels )', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'      => 'number',
                        'name'      => 'feature-inner',
                        'theme_mod' => false,
                        'default'   => 50
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Feature Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                )
            );

            $fields[ 'image-content' ] = array(
                array(
                    'title'         => __( 'Image', 'zeon' ),
                    'description'   => __( 'can be an external url.', 'zeon' ),
                    'format'        => 'across',
                    'input'             => array(
                        'type'          => 'upload',
                        'name'          => 'feature-src',
                        'theme_mod'     => false,
                        'callback'  => 'shortcode',
                        'args'      => array(
                            'shortcode' => '.zeon-shortcode-features-fields'
                        )
                    )
                ),
                array(
                    'title'         => __( 'Image Description', 'zeon' ),
                    'description'   => __( 'please use sample text ( not HTML or shortcodes)', 'zeon' ),
                    'format'        => 'across',
                    'input'             => array(
                        'type'          => 'text',
                        'name'          => 'feature-description',
                        'theme_mod'     => false,
                        'default'       => __( 'sample image description', 'zeon' )
                    )
                ),
                array(
                    'title'         => __( 'URL', 'zeon' ),
                    'description'   => __( 'You may add an url ( local / external ) for the image.', 'zeon' ),
                    'format'        => 'across',
                    'input'             => array(
                        'type'          => 'url',
                        'name'          => 'feature-url',
                        'theme_mod'     => false
                    )
                ),
                array(
                    'title'         => __( 'Image Position', 'zeon' ),
                    'description'   => __( 'You may add an url ( local / external ) for the image.', 'zeon' ),
                    'format'        => 'across',
                    'input'             => array(
                        'type'          => 'select',
                        'name'          => 'feature-position',
                        'theme_mod'     => false,
                        'default'       => 'left',
                        'options'       => array(
                            'left'          => __( 'Left' , 'zeon' ),
                            'right'         => __( 'Right' , 'zeon' )
                        )
                    )
                )
            );

            $fields[ 'text-content' ] = array(
                array(
                    'description'   => __( 'you may use HTML and others Shortcodes.', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'textarea',
                        'name'          => 'feature-content',
                        'theme_mod'     => false,
                        'default'       => '...'
                    )
                )
            );

            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'Image Content and Settings', 'zeon' ),
                    'fields'    => $fields[ 'image-content' ]
                ),
                array(
                    'title'     => __( 'Text Content', 'zeon' ),
                    'fields'    => $fields[ 'text-content' ]
                ),
            );

            $src            = '<span class="feature-src attr">src="<span class="value"></span>"</span>';
            $description    = '<span class="feature-description attr">description="<span class="value">' . __( 'sample image description' , 'zeon' ) . '</span>"</span>';
            $url            = '<span class="feature-url attr">url="<span class="value"></span>"</span>';
            $position       = '<span class="feature-position attr">position="<span class="value">left</span>"</span>';

            $content        = '<span class="feature-content text"><span class="value">...</span></span>';

            $type           = '<span class="feature-image-type attr">type="<span class="value">sample</span>"</span>';
            $max_size       = '<span class="feature-max-size hidden attr">max_size="<span class="value">240</span>"</span>';
            $align          = '<span class="feature-align attr">align="<span class="value">left</span>"</span>';
            $inner          = '<span class="feature-inner attr">inner="<span class="value">50</span>"</span>';

            $this -> code   =   '[zeon_feature ' . $src . ' ' . $type . ' ' . $max_size . ' ' . $url . ' ' . $position . ' ' . $description . ' ' . $align . ' ' . $inner .']' . '<br/>' . "\n" .
                                $content . '<br/>' . "\n" .
                                '[/zeon_feature]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }
    }
?>
