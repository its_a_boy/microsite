<?php
	if( is_singular() ){
		global $post;

		/**
	 	 *	Load Default Header
	  	 */

		if( 'default' == zeon_meta::get( $post -> ID, 'tempo-header-template', 'default' ) ){
			get_template_part( 'templates/header/partial/default' );
			return;
		}

		/**
		 *	Load Custom Header
		 */

		$template = zeon_meta::g( $post -> ID, 'tempo-header-template' );

		if( is_int( $template ) || ctype_digit( $template ) ){
			$header = get_post( $template );

			if( isset( $header -> ID ) ){
				$type = zeon_meta::g( $post -> ID, 'tempo-header-type' );
				tempo_get_template_part( 'templates/header/partial/custom', "template-{$type}" );
			}
		}

		else{
			tempo_get_template_part( 'templates/header/partial/default', "template-{$template}" );
		}
	}

	else{
		get_template_part( 'templates/header/partial/default' );
	}
?>