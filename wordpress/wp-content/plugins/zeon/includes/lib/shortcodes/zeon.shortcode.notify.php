<?php

    /*
        [notify bg_color=""]
            [notify_title color=""]Notify title[/notify_title]
            [notify_description color=""]Nullam quis risus eget urna mollis ornare vel eu leo. Curabitur blandit tempus porttitor.[/notify_descript]
        [/notify]
    */

    class zeon_shortcode_notify
    {
        private static $run = false;
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            self::$run = true;

            $attr = shortcode_atts( array(
                'type'      => 'note',
                'top'       => 30,
                'bottom'    => 30
            ), $_attr );

            /* STYLES AND PROPERTIES */
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            if( !empty( $attr[ 'bg_color' ] ) )
                $prop .= 'border-color: ' . $attr[ 'color' ] . '; ';

            $style = '';

            if( !empty( $prop ) )
                $style = 'style="' . esc_attr( $prop ) . '"';

            /* CONTENT */
            $content = strip_tags( $_content , '<h5><h6><p><a><span><b><strong><big><small><ul><ol><li><i><code><pre><button><input>' );

            $rett .= '<div class="tempo-shortcode notify  ' . esc_attr( $attr[ 'type' ] ) . '" ' . $style . '>';
            $rett .= do_shortcode( $content );
            $rett .= '</div>';

            self::$run = false;

            return $rett;
        }

        public $code;

        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Type', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'notify-type',
                        'theme_mod' => false,
                        'default'   => 'note',
                        'options'   => array(
                            'note'          => __( 'Note' , 'zeon' ),
                            'attention'     => __( 'Attention' , 'zeon' ),
                            'alert'         => __( 'Alert' , 'zeon' ),
                            'success'       => __( 'Success' , 'zeon' ),
                            'help'          => __( 'Help' , 'zeon' ),
                            'info'          => __( 'Info' , 'zeon' ),
                            'custom'        => __( 'Custom' , 'zeon' ),
                        ),
                        'actions'     => array(
                            'hide'          => array( '.zeon-notify-custom-settings' ),
                            'custom'        => array( '.zeon-notify-custom-settings' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'notify-top',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'notify-bottom',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                )
            );

            $fields[ 'custom-settings' ] = array(
                array(
                    'title'     => __( 'Text Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'notify-color',
                        'theme_mod' => false,
                        'default'   => '#333333'
                    )
                ),
                array(
                    'title'     => __( 'Background Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'notify-bg-color',
                        'theme_mod' => false,
                        'default'   => '#f0f9f1'
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Notify Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'class'     => 'zeon-notify-custom-settings hidden',
                    'title'     => __( 'Notify custom Settings', 'zeon' ),
                    'fields'    => $fields[ 'custom-settings' ]
                )
            );

            $fields[ 'content' ] = array(
                array(
                    'title'         => __( 'Title', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'      => 'text',
                        'name'      => 'notify-title',
                        'theme_mod' => false,
                        'default'   => __( 'Title', 'zeon' )
                    )
                ),
                array(
                    'title'         => __( 'Description', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'      => 'textarea',
                        'name'      => 'notify-description',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $boxes[ 'right' ][] = array(
                'title'         => __( 'Notify Content', 'zeon' ),
                'fields'        => $fields[ 'content' ]
            );

            $title              = '<span class="notify-title text"><span class="value">' . __( 'Title' , 'zeon' ) . '</span></span>';
            $description        = '<span class="notify-description text"><span class="value">...</span></span>';

            $type               = '<span class="notify-type attr">type="<span class="value">note</span>"</span>';
            $top                = '<span class="notify-top attr">top="<span class="value">30</span>"</span>';
            $bottom             = '<span class="notify-bottom attr">bottom="<span class="value">30</span>"</span>';

            $color              = '<span class="notify-color zeon-notify-custom-settings attr hidden">color="<span class="value">#333333</span>"</span>';
            $bg_color           = '<span class="notify-bg-color zeon-notify-custom-settings attr hidden">bg_color="<span class="value">#f0f9f1</span>"</span>';

            $this -> code       =   '[zeon_notify ' . $type . ' ' . $color .' ' . $bg_color . ' ' . $top .' ' . $bottom . ']' . '<br/>' . "\n" .
                                    '[zeon_notify_title]' . $title . '[/zeon_notify_title]' . '<br/>' . "\n" .
                                    '[zeon_notify_description]' . $description . '[/zeon_notify_description]'  . '<br/>' . "\n" .
                                    '[/zeon_notify]';

            return array(
                array(
                    'boxes' => $boxes['left']
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        public static function is_run(){
            return self::$run;
        }
    }

    class zeon_shortcode_notify_title
    {
        static function run( $_attr, $_content )
        {
            $rett = '';

            if( zeon_shortcode_notify::is_run() ){

                /* STYLES AND PROPERTIES */
                $prop = esc_attr( zeon_shortcode_prop( $_attr ) );

                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                if( !empty( $_content ) ){
                    $rett .= '<h4 class="notify-title" ' . $style . '>' . $_content . '</h4>';
                }
            }

            return $rett;
        }
    }

    class zeon_shortcode_notify_description
    {
        static function run( $_attr, $_content )
        {
            $rett = '';

            if( zeon_shortcode_notify::is_run() ){

                /* STYLES AND PROPERTIES */
                $prop = esc_attr( zeon_shortcode_prop( $_attr ) );

                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                if( !empty( $_content ) ){
                    $rett .= '<div class="notify-content" ' . $style . '>' . do_shortcode( $_content ) . '<div class="clearfix"></div></div>';
                }
            }

            return $rett;
        }
    }
?>
