<?php

    /**
	 *	Appearance / Customize / Typography / Theme Typography - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'easy-google-fonts' ), array(
        'fonts' => array(
            'body-font'                         => array(
                'priority'      => 10,
                'name' 			=> 'body-font',
                'title' 		=> __( 'Main Body', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'body, html',
                    'force_styles' 	=> null
                )
            ),
            'site-title-font'                   => array(
                'priority'      => 20,
                'name' 			=> 'site-title-font',
                'title' 		=> __( 'Site Title', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'header.tempo-header div.tempo-topper div.tempo-site-identity a.tempo-site-title',
                    'force_styles' 	=> null
                )
            ),
            'tagline-font'                      => array(
                'priority'      => 30,
                'name' 			=> 'tagline-font',
                'title' 		=> __( 'Site Description',  'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'header.tempo-header div.tempo-topper div.tempo-site-identity a.tempo-site-description',
                    'force_styles' 	=> null
                )
            ),
            'header-navigation-font'            => array(
                'priority'      => 40,
                'name'          => 'header-navigation-font',
                'title'         => __( 'Header Navigation',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'header div.tempo-topper div.tempo-navigation-wrapper li, header div.tempo-topper div.tempo-navigation-wrapper a',
                    'force_styles'  => null
                )
            ),
            'header-headline-font'              => array(
                'priority'      => 50,
                'name'          => 'header-headline-font',
                'title'         => __( 'Header Headline',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'header.tempo-header div.tempo-header-partial .tempo-header-headline',
                    'force_styles'  => null
                )
            ),
            'header-description-font'           => array(
                'priority'      => 60,
                'name'          => 'header-description-font',
                'title'         => __( 'Header Description',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'header.tempo-header div.tempo-header-partial .tempo-header-description',
                    'force_styles'  => null
                )
            ),
            'breadcrumbs-title-font'            => array(
                'priority'      => 70,
                'name' 			=> 'breadcrumbs-title-font',
                'title' 		=> __( 'Breadcrumbs Title', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'div.tempo-breadcrumbs h1.tempo-headline',
                    'force_styles' 	=> null
                )
            ),
            'breadcrumbs-counter-font'		    => array(
                'priority'      => 80,
                'name' 			=> 'breadcrumbs-counter-font',
                'title' 		=> __( 'Breadcrumbs Counter', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'div.tempo-breadcrumbs div.details span.counter-wrapper',
                    'force_styles' 	=> null
                )
            ),
            'classic-post-title-font'           => array(
                'priority'      => 90,
                'name' 			=> 'classic-post-title-font',
                'title' 		=> __( 'Classic view post Title',  'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'article.tempo-article.classic h2.tempo-title',
                    'force_styles' 	=> null
                )
            ),
            'grid-post-title-font'              => array(
                'priority'      => 100,
                'name'          => 'grid-post-title-font',
                'title'         => __( 'Grid view post Title',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'article.tempo-article.grid div.tempo-content h3.tempo-title',
                    'force_styles'  => null
                )
            ),
            'portfolio-post-title-font'         => array(
                'priority'      => 110,
                'name'          => 'portfolio-post-title-font',
                'title'         => __( 'Portfolio view post Title',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'article.tempo-article.portfolio div.tempo-content h3.tempo-title',
                    'force_styles'  => null
                )
            ),
            'more-link-font'                    => array(
                'priority'      => 120,
                'name'          => 'more-link-font',
                'title'         => __( 'Link "Read More"',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'article.tempo-article.classic a.more-link',
                    'force_styles'  => null
                )
            ),
            'meta-font'                         => array(
                'priority'      => 130,
                'name' 			=> 'meta-font',
                'title' 		=> __( 'Single and Blog Meta', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'article.tempo-article.classic div.tempo-meta.top, article.tempo-article.classic div.tempo-meta.bottom',
                    'force_styles' 	=> null
                )
            ),
            'categories-font'                   => array(
                'priority'      => 140,
                'name' 			=> 'meta-font',
                'title' 		=> __( 'Single and Blog Meta', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'article.tempo-article.classic div.tempo-categories a',
                    'force_styles' 	=> null
                )
            ),
            'blockquote-font'                   => array(
                'priority'      => 150,
                'name' 			=> 'blockquote-font',
                'title' 		=> __( 'Blockquote', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'blockquote',
                    'force_styles' 	=> null
                )
            ),
            'gallery-headline-font'             => array(
                'priority'      => 160,
                'name'          => 'gallery-headline-font',
                'title'         => __( 'Image gallery Headline',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => '.tempo-gallery[class^="tempo-gallery-colls-"] figure div.tempo-gallery-title, .tempo-gallery[class*="tempo-gallery-colls-"] figure div.tempo-gallery-title',
                    'force_styles'  => null
                )
            ),
            'buttons-font'                      => array(
                'priority'      => 170,
                'name'          => 'buttons-font',
                'title'         => __( 'Buttons',  'zeon' ),
                'type'          => 'font',
                'description'   => null,
                'section'       => 'default',
                'tab'           => 'theme-typography',
                'transport'     => 'postMessage',
                'since'         => 1.0,
                'properties'    => array(
                    'selector'      => 'input[type="submit"], input[type="button"], input[type="reset"], button, .button, .tempo-btn, .btn',
                    'force_styles'  => null
                )
            ),
            'post-author-name-font'             => array(
                'priority'      => 180,
                'name' 			=> 'post-author-name-font',
                'title' 		=> __( 'Post Author\'s Name', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'article.tempo-article.classic div.tempo-author h4',
                    'force_styles' 	=> null
                )
            ),
            'comments-headline-font'            => array(
                'priority'      => 190,
                'name' 			=> 'comments-headline-font',
                'title' 		=> __( 'Comments Headline', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'div.tempo-comments-wrapper h3.tempo-comments-title, div.tempo-comments-wrapper div.comment-respond h3.comment-reply-title',
                    'force_styles' 	=> null
                )
            ),
            'comment-headline-author-font'      => array(
                'priority'      => 200,
                'name' 			=> 'comment-headline-author-font',
                'title' 		=> __( 'Comment Headline Author', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'div.tempo-comments-wrapper ol.tempo-comments-list li.comment header span.tempo-comment-meta cite, div.tempo-comments-wrapper ol.tempo-comments-list li.comment header span.tempo-comment-meta cite a',
                    'force_styles' 	=> null
                )
            ),
            'content-widget-title-font'         => array(
                'priority'      => 210,
                'name' 			=> 'content-widget-title-font',
                'title' 		=> __( 'Content widget Title', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside div.widget h4.widget-title',
                    'force_styles' 	=> null
                )
            ),
            'header-widget-title-font'          => array(
                'priority'      => 220,
                'name' 			=> 'header-widget-title-font',
                'title' 		=> __( 'Header widget Title', 'zeon' ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.header-sidebar div.widget h3.widget-title',
                    'force_styles' 	=> null
                )
            ),
            'light-footer-widget-title-font'	=> array(
                'priority'      => 230,
                'name' 			=> 'light-footer-widget-title-font',
                'title' 		=> __( 'Light footer widget Title', 'zeon'  ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.tempo-footer.light-sidebars div.widget h5.widget-title',
                    'force_styles' 	=> null
                )
            ),
            'dark-footer-widget-title-font'     => array(
                'priority'      => 240,
                'name' 			=> 'dark-footer-widget-title-font',
                'title' 		=> __( 'Dark footer widget Title', 'zeon'  ),
                'type' 			=> 'font',
                'description' 	=> null,
                'section' 		=> 'default',
                'tab' 			=> 'theme-typography',
                'transport' 	=> 'postMessage',
                'since' 		=> 1.0,
                'properties' 	=> array(
                    'selector' 		=> 'aside.tempo-footer.dark-sidebars div.widget h5.widget-title',
                    'force_styles' 	=> null
                )
            )
        )
    ));

    tempo_cfgs::set( 'easy-google-fonts', $cfgs );


    /**
     *  Change Panel Priority
     */

    function tempo_get_panels( $panels ){
        if( isset( $panels[ 'tt_font_typography_panel' ] ) && is_array( $panels[ 'tt_font_typography_panel' ] ) )
            $panels[ 'tt_font_typography_panel' ][ 'priority' ] = 56;

        return $panels;
    }

    add_filter( 'tt_font_get_panels', 'tempo_get_panels' );


    /**
     *  Add Options to Easy Google Fonts Customize Panel
     */

    function tempo_get_option_parameters( $options ){
        $options[ 'tt_default_heading_1' ][ 'properties' ][ 'selector' ] = '.hentry h1';
        $options[ 'tt_default_heading_2' ][ 'properties' ][ 'selector' ] = '.hentry h2';
        $options[ 'tt_default_heading_3' ][ 'properties' ][ 'selector' ] = '.hentry h3';
        $options[ 'tt_default_heading_4' ][ 'properties' ][ 'selector' ] = '.hentry h4';
        $options[ 'tt_default_heading_5' ][ 'properties' ][ 'selector' ] = '.hentry h5';
        $options[ 'tt_default_heading_6' ][ 'properties' ][ 'selector' ] = '.hentry h6';

        $cfgs = tempo_cfgs::get( 'easy-google-fonts' );

        if( isset( $cfgs[ 'fonts' ] ) )
            $options = array_merge( $cfgs[ 'fonts' ], $options );

        return $options;
    }

    add_filter( 'tt_font_get_option_parameters', 'tempo_get_option_parameters' );
?>
