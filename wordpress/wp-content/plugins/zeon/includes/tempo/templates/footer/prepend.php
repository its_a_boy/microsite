<?php
    
    /**
     *  check if Number of Columns is not ZERO
     */

    if( 0 == tempo_options::get( 'footer-dark-nr-columns' ) )
        return;

    /**
     *  check if is active Sidebar
     */

    if( !is_active_sidebar( 'footer-dark' ) )
        return;
?>     

    <!-- aside -->
    <aside class="content tempo-footer dark-sidebars">

        <!-- container -->
        <div <?php echo tempo_container_class(); ?>>
            <div <?php echo tempo_row_class(); ?>>

                <!-- content -->
                <div <?php echo tempo_content_class(); ?>>
                    <div <?php echo tempo_row_class( 'widgets-row' ); ?>>

                        <?php dynamic_sidebar( 'footer-dark' ); ?>
                            
                    </div>
                </div><!-- end content -->

            </div>
        </div><!-- end container -->

    </aside><!-- end aside -->