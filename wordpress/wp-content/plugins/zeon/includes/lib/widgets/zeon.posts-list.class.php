<?php

    /**
     *  The Latest Posts List ( with thumbnail )
     */

    if( !class_exists( 'zeon_widget_posts_list' ) ){

        class zeon_widget_posts_list extends WP_Widget
        {
            /**
             *  Widget Constructor
             */

            function __construct()
            {
                parent::__construct( 'zeon_widget_posts_list', __( 'Posts in List', 'zeon' ) . ' [zeon]', array( 
                    'classname'     => 'zeon_widget_posts_list',
                    'description'   => __( 'The list of the latest posts with Thumbnail and Date.', 'zeon' )
                ));
            }

            /**
             *  Widget Preview
             */

            function widget( $args, $instance )
            {
                // extract args
                extract( $args , EXTR_SKIP );

                $instance = wp_parse_args( (array) $instance, array(
                    'title'         => __( 'Latest Posts', 'zeon' ),
                    'tag'           => null,
                    'category'      => null,
                    'offset'        => null,
                    'skip'          => null,
                    'number'        => 5
                ));

                $title          = sanitize_text_field( $instance[ 'title' ] );
                $tag            = esc_attr( $instance[ 'tag' ] );
                $category       = esc_attr( $instance[ 'category' ] );
                $offset         = absint( $instance[ 'offset' ] );
                $skip           = esc_attr( $instance[ 'skip' ] );
                $number         = absint( $instance[ 'number' ] );

                if( $number > 0 ){

                    echo $before_widget;

                    if( !empty( $title ) ){
                        echo $before_title;
                        echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
                        echo $after_title;
                    }

                    $query = array(
                        'posts_per_page' => absint( $number )
                    );

                    if( $offset > 0 )
                        $query[ 'offset' ] = $offset;

                    if( $s = !empty( $skip ) ? explode( ',', $skip ) : false )
                        $query[ 'post__not_in' ] = $s;

                    if( !empty( $category ) )
                        $query[ 'category_name' ] = $category;

                    if( !empty( $tag ) )
                        $query[ 'tag' ] = $tag;

                    $wp_query = new WP_Query( $query );

                    if( count( $wp_query -> posts ) ){

                        echo '<ul class="zeon-articles-list">';

                        foreach( $wp_query -> posts as $p ){

                            echo '<li>';

                            // post thumbnail wrapper
                            echo '<div class="post-thumbnail-wrapper overflow-wrapper">';

                            $thumbnail      = get_post( get_post_thumbnail_id( $p ) );
                            $post_thumbnail = has_post_thumbnail( $p  ) && isset( $thumbnail -> ID );

                            // post thumbnail image
                            if( $post_thumbnail ){
                                echo get_the_post_thumbnail( $p -> ID , 'thumbnail' , array(
                                    'alt'   => esc_attr( get_the_title( $p -> ID ) ),
                                    'class' => 'post-thumbnail'
                                ));
                            }

                            // default post thumbnail
                            else{
                                echo '<span class="post-thumbnail"><i class="tempo-icon-picture-1"></i></span>';
                            }

                            // post thumbnail mask + link
                            echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p ) ) . '" ' . tempo_flex_container_class( null, 'tempo-valign-middle' ) . '>';
                            echo '<span ' . tempo_flex_item_class( null, 'tempo-align-center' ) . '>';
                            echo '<i class="tempo-icon-link-2"></i>';
                            echo '</span>';
                            echo '</a>';

                            echo '</div>';

                            // post title
                            echo '<h5>';
                            echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p -> ID ) ) . '">';
                            echo get_the_title( $p -> ID );
                            echo '</a>';
                            echo '</h5>';

                            // post meta / date
                            $y          = esc_attr( get_post_time( 'Y', false, $p ) );
                            $m          = esc_attr( get_post_time( 'm', false, $p ) );
                            $d          = esc_attr( get_post_time( 'd', false, $p ) );
                            $time       = esc_attr( get_post_time( 'Y-m-d', false, $p ) );
                            $wp_time    = get_post_time( esc_attr( get_option( 'date_format' ) ), false , $p, true );

                            echo '<span class="post-meta time">';
                            echo '<i class="tempo-icon-clock-1"></i> ';
                            echo '<a href="' . esc_url( get_day_link( $y , $m , $d ) )  . '" title="' . sprintf( __( 'posted on %s', 'zeon' ), $wp_time ) . '">';
                            echo '<time datetime="' . esc_attr( $time ) . '">' . $wp_time . '</time>';
                            echo '</a>';
                            echo '</span>';

                            echo '<div class="clear clearfix"></div>';
                            echo '</li>';
                        }

                        echo '</ul>';
                    }

                    echo $after_widget;
                }
            }

            /**
             *  Widget Update
             */

            function update( $new_instance, $old_instance )
            {
                $new_instance = wp_parse_args( (array) $new_instance, array(
                    'title'         => null,
                    'tag'           => null,
                    'category'      => null,
                    'offset'        => null,
                    'skip'          => null,
                    'number'        => null
                ));

                $instance                   = $old_instance;

                $instance[ 'title' ]        = sanitize_text_field( $new_instance[ 'title' ] );
                $instance[ 'tag' ]          = esc_attr( $new_instance[ 'tag' ] );
                $instance[ 'category' ]     = esc_attr( $new_instance[ 'category' ] );
                $instance[ 'offset' ]       = absint( $new_instance[ 'offset' ] );
                $instance[ 'skip' ]         = esc_attr( $new_instance[ 'skip' ] );
                $instance[ 'number' ]       = absint( $new_instance[ 'number' ] );

                return $instance;
            }

            /**
             *  Widget Form ( admin side )
             */

            function form( $instance )
            {
                $instance = wp_parse_args( (array) $instance, array(
                    'title'         => null,
                    'tag'           => null,
                    'category'      => null,
                    'offset'        => null,
                    'skip'          => null,
                    'number'        => 5
                ));

                $title          = sanitize_text_field( $instance[ 'title' ] );
                $tag            = esc_attr( $instance[ 'tag' ] );
                $category       = esc_attr( $instance[ 'category' ] );
                $offset         = absint( $instance[ 'offset' ] );
                $skip           = esc_attr( $instance[ 'skip' ] );
                $number         = absint( $instance[ 'number' ] );

                if( empty( $number ) )
                    echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "Number of posts"', 'zeon' ) . '</p>';

                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
                echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
                echo '</label>';
                echo '</p>';

                // tags
                $tags = get_tags();

                if( !empty( $tags ) ){
                    $tag_options = array( __( ' -- Select a Tag --', 'zeon' ) );

                    foreach( $tags as $t )
                        $tag_options[ $t -> slug ] = $t -> name . " ({$t -> count})";

                    // field
                    echo '<p>';
                    echo '<label for="' . $this -> get_field_id( 'tag' ) . '">' . __( 'Filter by Tag', 'zeon' );

                    echo tempo_html::input(array(
                        'id'        => $this -> get_field_id( 'tag' ),
                        'name'      => $this -> get_field_name( 'tag' ),
                        'class'     => 'widefat',
                        'type'      => 'select',
                        'theme_mod' => false,
                        'value'     => esc_attr( $tag ),
                        'options'   => $tag_options
                    ));

                    echo '</label>';
                    echo '</p>';
                }

                // categories
                $categories = get_categories();

                if( !empty( $categories ) ){
                    $cat_options = array( __( ' -- Select a Category --', 'zeon' ) );

                    foreach( $categories as $c )
                        $cat_options[ $c -> slug ] = $c -> name . " ({$c -> count})";

                    // field
                    echo '<p>';
                    echo '<label for="' . $this -> get_field_id( 'category' ) . '">' . __( 'Filter by Category', 'zeon' );

                    echo tempo_html::input(array(
                        'id'        => $this -> get_field_id( 'category' ),
                        'name'      => $this -> get_field_name( 'category' ),
                        'class'     => 'widefat',
                        'type'      => 'select',
                        'theme_mod' => false,
                        'value'     => esc_attr( $category ),
                        'options'   => $cat_options
                    ));

                    echo '</label>';
                    echo '</p>';
                }

                // offset
                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'offset' ) . '">' . __( 'Skip Offset', 'zeon' );
                echo '<input type="number" class="widefat" id="' . $this -> get_field_id( 'offset' ) . '" name="' . $this -> get_field_name( 'offset' ) . '" value="' . absint( $offset ) . '" />';
                echo '<small style="color: #999999;">' . __( 'number of Posts to skip before display', 'zeon' ) . '</small>';
                echo '</label>';
                echo '</p>';

                // skip
                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'skip' ) . '">' . __( 'Skip Post IDs', 'zeon' );
                echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'skip' ) . '" name="' . $this -> get_field_name( 'skip' ) . '" value="' . absint( $skip ) . '" />';
                echo '<small style="color: #999999;">' . __( 'insert the Post IDs, separate the IDs with commas.', 'zeon' ) . '</small>';
                echo '</label>';
                echo '</p>';

                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'number' ) . '">' . __( 'Number of posts', 'zeon' );
                echo '<input type="number" class="widefat" id="' . $this -> get_field_id( 'number' ) . '" name="' . $this -> get_field_name( 'number' ) . '" value="' . absint( $number ) . '" />';
                echo '</label>';
                echo '</p>';
            }
        }
    }
?>
