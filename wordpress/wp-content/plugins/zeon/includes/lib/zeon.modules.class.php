<?php
	if( !class_exists( 'zeon_modules' ) ){
		class zeon_modules
		{
			static function get( $module = null )
			{
				$rett = array();
				$cfgs = (array)tempo_cfgs::get( 'modules' );

				if( empty( $module ) )
					$rett = $cfgs;

				if( isset( $cfgs[ $module ] ) )
					$rett = $cfgs[ $module ];

				return $rett;
			}

			static function set( $module, $args )
			{
				$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'modules' ), array(
					$module => $args
				));

				tempo_cfgs::set( 'modules', $cfgs );
			}
		}
	}
?>