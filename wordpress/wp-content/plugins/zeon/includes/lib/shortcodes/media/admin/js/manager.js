if( typeof zeon !== 'object' )
    var zeon = {};

zeon.manager = {
    panel : function(){
        jQuery(function(){

            jQuery( 'div.zeon-shortcodes-manager-shadow' ).show( 100 );
            jQuery( 'div.zeon-shortcodes-manager' ).show( 100, function(){

                var self = this;

                function zeon_manager_panel_height(){
                    var h = parseInt( jQuery( self ).height() ) - 71;
                    jQuery( 'table.zeon-shortcodes-panel td.zeon-shortcodes-panel-fields div.zeon-shortcodes-fields' ).css({ 'max-height' : h + 'px' , 'height' : h + 'px' });
                }

                zeon_manager_panel_height();

                jQuery( window ).resize(function(){
                    zeon_manager_panel_height();
                });
            });

            jQuery( 'div.zeon-shortcodes-manager' ).find( 'a.zeon-close' ).click(function(){
                jQuery( 'div.zeon-shortcodes-manager-shadow' ).hide( 100 );
                jQuery( 'div.zeon-shortcodes-manager' ).hide( 100 );
            });
        });
    },
    insert : function( name ){
        var code = this.code( name );

        jQuery( 'div.zeon-shortcodes-manager-shadow' ).hide( 100 );
        jQuery( 'div.zeon-shortcodes-manager' ).hide( 100 );

        jQuery(function(){

            if(  jQuery( 'div#wp-content-wrap' ).length && jQuery( 'div#wp-content-wrap' ).hasClass( 'html-active' ) ) {
                var text = jQuery( 'textarea#content' ).val();
                jQuery( 'textarea#content' ).val( text + code );

                /*if( typeof CodeMirror == 'function' && typeof tempo_editor == 'undefined' ){
                    var textArea = document.getElementById('content');
                    var tempo_editor = CodeMirror.fromTextArea( textArea );
                    tempo_editor.getDoc().setValue( text + code );
                }
                else if( typeof CodeMirror == 'function' ){
                    tempo_editor.getDoc().setValue( text + code );
                }*/
            }
            else{
                if( typeof tinyMCE == 'object' )
                    tinyMCE.execCommand( 'mceInsertRawHTML', false, code );
            }
        });
    },
    code : function( name ){
        var rett = '';

        jQuery(function(){
            var wrapper = jQuery( 'div.zeon-shortcodes-fields.zeon-shortcode-' + name + '-fields div.zeon-shortcode-code-wrapper' );
            var code    = jQuery( wrapper ).find( 'span.zeon-shortcode-code' ).html();
            var tmp     = jQuery( wrapper ).find( 'span.zeon-shortcode-tmp-code' ).html( code );

            tmp.find( 'span' ).each(function(){
                if( jQuery( this ).css( 'display' ) == 'none' ){
                    jQuery( this ).remove();
                }
            });

            rett = tmp.text().replace( '  ', ' ', "gi" );
            tmp.html( '' );
        });

        return rett.replace( '  ', ' ', "gi" );
    }
};

jQuery(function(){

    /* SHORTCODES */
    jQuery( 'span.zeon-shortcode-code' ).click(function(){
        var self = this;

        var code    = jQuery( this ).html();
        var tmp     = jQuery( this ).parent().find( 'span.zeon-shortcode-tmp-code' ).html( code );

        tmp.find( 'span' ).each(function(){
            if( jQuery( this ).css('display') == 'none' ){
                jQuery( this ).remove();
            }
        });

        var text = tmp.text().replace( '  ', ' ', "gi" );

        jQuery('<textarea id="zeon-temporary"/>').appendTo( jQuery( self ) )
            .val( text.replace( '  ', ' ', "gi" ) )
            .focus()
            .select();

        return false;
    });

    jQuery(':not(span.zeon-shortcode-code)').click(function(){
        jQuery('span.zeon-shortcode-code #zeon-temporary').remove();
    });

    jQuery( 'select#zeon-shortcodes-selector' ).change(function(){
        var name    = jQuery( this ).val();
        var fields  = jQuery( 'div.zeon-shortcodes-fields.' + name );

        jQuery( fields ).find( 'div.tempo-field' ).each(function(){

            /* NUMBER, TEXT, TEXTAREA, URL */
            if( jQuery( this ).hasClass( 'tempo-number' ) ||
                jQuery( this ).hasClass( 'tempo-textarea' ) ||
                jQuery( this ).hasClass( 'tempo-text' ) ||
                jQuery( this ).hasClass( 'tempo-url' ) ||
                jQuery( this ).hasClass( 'tempo-upload' ) ){

                if( jQuery( this ).hasClass( 'social' ) ){
                    var social = function(){
                        var name    = jQuery( this ).attr( 'name' );
                        var val     = jQuery( this ).val();

                        if( val.length ){
                            jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + '.hidden' ).show();
                        }
                        else{
                            jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + '.hidden' ).hide();
                        }

                        jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value' ).text( val );
                    };

                    jQuery( this ).find( 'input' ).click( social );
                    jQuery( this ).find( 'input' ).change( social );
                    jQuery( this ).find( 'input' ).keypress( social );
                    jQuery( this ).find( 'input' ).keyup( social );
                }
                else{
                    var text = function(){
                        var name    = jQuery( this ).attr( 'name' );
                        var val     = jQuery( this ).val();

                        jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value' ).text( val );
                    };

                    jQuery( this ).find( 'input,textarea' ).click( text );
                    jQuery( this ).find( 'input,textarea' ).change( text );
                    jQuery( this ).find( 'input,textarea' ).keypress( text );
                    jQuery( this ).find( 'input,textarea' ).keyup( text );
                }
            }

            /* SELECT / PERCENT */
            if( jQuery( this ).hasClass( 'tempo-select' ) ||
                jQuery( this ).hasClass( 'tempo-percent' ) ){

                var select = function(){
                    var name    = jQuery( this ).attr( 'name' );
                    var val     = jQuery( this ).val();

                    jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value' ).text( val );
                };

                jQuery( this ).find( 'input,select' ).change( select );

                /* COLUMNS */
                jQuery( this ).find( 'select#columns-nr' ).change(function(){
                    var val     = parseInt( jQuery( this ).val() );

                    if( val == 2 ){
                        if( !jQuery( 'span.zeon-column-3').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-3').addClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-column-4').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-4').addClass( 'hide-ln' );
                        }
                    }

                    if( val == 3 ){
                        if( jQuery( 'span.zeon-column-3').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-3').removeClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-column-4').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-4').addClass( 'hide-ln' );
                        }
                    }

                    if( val == 4 ){
                        if( jQuery( 'span.zeon-column-3' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-3' ).removeClass( 'hide-ln' );
                        }

                        if( jQuery( 'span.zeon-column-4').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-column-4').removeClass( 'hide-ln' );
                        }
                    }
                });

                jQuery( this ).find( 'select#sponsors-nr' ).change(function(){
                    var val     = parseInt( jQuery( this ).val() );

                    if( val == 2 ){
                        if( !jQuery( 'span.zeon-sponsor-3' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-3' ).addClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-sponsor-4' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-4' ).addClass( 'hide-ln' );
                        }
                    }

                    if( val == 3 ){
                        if( jQuery( 'span.zeon-sponsor-3' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-3' ).removeClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-sponsor-4' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-4' ).addClass( 'hide-ln' );
                        }
                    }

                    if( val == 4 ){
                        if( jQuery( 'span.zeon-sponsor-3' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-3' ).removeClass( 'hide-ln' );
                        }

                        if( jQuery( 'span.zeon-sponsor-4' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-sponsor-4' ).removeClass( 'hide-ln' );
                        }
                    }
                });

                /* COUNTERS */
                jQuery( this ).find( 'select#zeon-counters-nr' ).change(function(){
                    var val     = parseInt( jQuery( this ).val() );

                    if( val == 2 ){
                        if( !jQuery( 'span.zeon-counter-3').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-3').addClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-counter-4').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-4').addClass( 'hide-ln' );
                        }
                    }

                    if( val == 3 ){
                        if( jQuery( 'span.zeon-counter-3').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-3').removeClass( 'hide-ln' );
                        }

                        if( !jQuery( 'span.zeon-counter-4').hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-4').addClass( 'hide-ln' );
                        }
                    }

                    if( val == 4 ){
                        if( jQuery( 'span.zeon-counter-3' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-3' ).removeClass( 'hide-ln' );
                        }

                        if( jQuery( 'span.zeon-counter-4' ).hasClass( 'hide-ln' ) ){
                            jQuery( 'span.zeon-counter-4' ).removeClass( 'hide-ln' );
                        }
                    }
                });
            }

            /* ICON SELECT */
            if( jQuery( this ).hasClass( 'tempo-icon_select' ) ){
                var name  = jQuery( this ).find( 'input[type="hidden"]').attr( 'name' );

                jQuery( this ).find( '.tempo-icon-select-option' ).click(function(){
                    var val = jQuery( this ).find( 'i' ).attr( 'data-value' ).replace( "tempo-icon-" , '' , "gi" );
                    jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value').text( val );
                })
            }

            /* COLOR */
            if( jQuery( this ).hasClass( 'tempo-color' ) ){
                var color = jQuery( this ).find( 'input.tempo-pickcolor');
                jQuery( color ).wpColorPicker({
                    change: function( event, ui ){
                        if( event.type == 'irischange' ){
                            var self = jQuery( this );
                            var name = jQuery( this ).attr( 'name' );
                            setTimeout(function(){
                                var val = jQuery( self ).val();
                                jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value').text( val );
                            }, 300 );
                        }
                    }
                });
            }

            /* LOGIC */
            if( jQuery( this ).hasClass( 'tempo-logic' ) ){

                jQuery( this ).find( 'div.tempo-input-logic' ).click(function(){
                    var name    = jQuery( this ).find( 'input[type="hidden"]' ).attr( 'name' );
                    var val     = parseInt( jQuery( this ).find( 'input[type=hidden]' ).val() );

                    var v = 'true';
                    if( val == 0 ){
                        v = 'false';
                    }

                    jQuery( fields ).find( 'div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' ).find( 'span.' + name + ' span.value').text( v );
                });
            }
        });
    });
});
