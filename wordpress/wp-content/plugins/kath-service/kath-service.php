<?php
//-----------------------------------------------------------------------------
/*
Plugin Name: Kath Standort Service (Widget)
Version: 1.0
Description: AfterContent Widget to display a form for service requests. Has CoAuthorPlus and UserPhoto as dependencies.
Author: Jannik Clausen
Min WP Version: 3.0
*/
//-----------------------------------------------------------------------------
?>
<?php
add_action('widgets_init', create_function('', 'return register_widget("Kath_Standort_Service_Widget");'));

class Kath_Standort_Service_Widget extends WP_Widget {

	private $ansprechpartner = [];

	function __construct() {	   
		$widget_ops = array('classname' => 'Kath_Standort_Service_Widget', 'description' => "After content widget to display a Service-Formular on a post page." );
		/* Widget control settings. */
		$control_ops = array('width' => 200, 'height' => 300); // TODO - Needs to be adjusted
		parent::__construct('kath_standort', __('Kath Standort Service Widget'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {		

		// handle formulardata (if it got submitted)
		if (isset($_POST['kath_sv_submit'])) {

			var_dump($_POST);

		} else { // display the form if there was no post-data submitted

			//todo - search over all categories (not only he first one)
			if (get_the_category()[0]->name == 'Service') { // only display form if post belongs to category (Verkauf)

				// If Co-Authors plus plugin exists fetch all Co-Authors emails one after another
				if(function_exists('coauthors_posts_links')) {
					$this->ansprechpartner = [];
					$i = new CoAuthorsIterator(); 
					while($i->iterate()){
						global $authordata;

						// save authors in the ansprechpartners array
						array_push($this->ansprechpartner, $authordata);
					}

					// sort the ansprechpartner array by user-role and last-name
					$this->_sortAnsprechpartners();

				} else {
					global $authordata;

					array_push($this->ansprechpartner, $authordata);
				}

				// display the form
				$this->_displayForm($args, $instance);		
			}
		}
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Kath Standort Probefahrt Widget') );
		$title = strip_tags($instance['title']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php	  
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
	
	// Sorts the ansprechpartners by role and last-name
	private function _sortAnsprechpartners() {
		// Vergleichsfunktion
		function cmp_sv($a, $b) {
			if($a->roles[0] == $b->roles[0]) {
				if($a->last_name == $b->last_name) {
					return 0;
				}
				return ($a->last_name < $b->last_name) ? -1 : 1;
			}
			return ($a->roles[0] < $b->roles[0]) ? -1 : 1;
		}

		usort($this->ansprechpartner, 'cmp_sv');
	}

	// Does the actual work of preparing the form markup
	private function _displayForm($args, $instance){
		extract( $args ); // extract arguments
		$isHome = is_home() || is_front_page(); //Don't show the Widget on home page

		if(!$isHome && (is_single() || is_page())){			
			echo $before_widget;
			// echo $before_title . $instance['title'] . $after_title;

			// var_dump($this->ansprechpartner[0]->data->display_name);

			// open form
			echo '<form method="post">';

			// display select box to choose a ansprechpartner as recipient
			echo '<div class="form-group"><label for="kath_sv_to">Standort</label><select id="kath_pf_to" name="kath_pf_to" class="form-control"><option value="" selected></option>';
			echo '<optgroup label="'.ucfirst($this->ansprechpartner[0]->roles[0]).'">'; // open first optgroup
			$lastStandort = $this->ansprechpartner[0]->roles[0];

			foreach ($this->ansprechpartner as $key => $p) {
				// if last standort is not the same as current we need to open a new optgroup
				if($lastStandort != $p->roles[0]) {
					echo '</optgroup><optgroup label="'.ucfirst($p->roles[0]).'">';
					$lastStandort = $p->roles[0];
				}
				echo '<option value="' . $p->data->user_email . '">' . $p->data->display_name . '</option>';
			}

			echo '</optgroup>'; // close last optgroup (standort)
			echo '</select></div>'; // close select box

							?><div class="form-group">
					<label for="kath_pf_test">Standort</label><select id="kath_pf_test" name="kath_pf_test" class="form-control"><option value="">Select a person...</option>
						<option value="Thomas Edison">Thomas Edison</option>
						<option value="Nikola">Nikola</option>
						<option value="Nikola Tesla">Nikola Tesla</option>
<option value="Arnold Schwarzenegger">Arnold Schwarzenegger</option></select></div><?php
			
			echo '<button type="submit" class="btn btn-default" name="kath_sv_submit">Submit Service Request</button>';

			// close form
			echo "</form>";
		}
	}
}
?>