<?php global $post, $posts_total, $posts_index; ?>

<div <?php echo zeon_columns_class( 'tempo-portfolio-wrapper' ) ?>>

<article <?php post_class( 'tempo-article portfolio' ); ?>>

	<div class="tempo-thumbnail-wrapper overflow-wrapper">

		<?php tempo_get_template_part( 'templates/article/thumbnail', 'portfolio' ); ?>

		<div <?php echo tempo_flex_container_class( 'tempo-thumbnail-mask', 'tempo-valign-bottom' ); ?>>
	        <div <?php echo tempo_flex_item_class( 'tempo-content', 'tempo-align-left' ); ?>>

			    <?php tempo_get_template_part( 'templates/article/terms/categories', 'portfolio' ); ?>

			    <h3 class="tempo-title">

				    <?php if( !empty( $post -> post_title ) ) { ?>

				        <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title( $post ) ); ?>"><?php the_title(); ?></a>

				    <?php } else { ?>

				        <a href="<?php the_permalink() ?>"><?php _e( 'Read more about ..' , 'zeon' ) ?></a>

				    <?php } ?>
				</h3>

			    <?php tempo_get_template_part( 'templates/article/meta/bottom', 'portfolio' ); ?>

	    	</div>
	    </div>
	    
    </div>

</article>

</div>