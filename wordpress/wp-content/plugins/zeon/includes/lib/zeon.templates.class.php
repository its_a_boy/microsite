<?php
    /**
     *
     */

    if( !class_exists( 'zeon_templates' ) ){
        class zeon_templates
        {
            /**
             * A reference to an instance of this class.
             */
            private static $instance;

            /**
             * The array of templates that this plugin tracks.
             */
            protected $templates;


            /**
             * Returns an instance of this class.
             */
            public static function get( $args = null )
            {
                if( null == self::$instance ) {
    				self::$instance = new zeon_templates();
                }

                return self::$instance;
            }

            /**
             * Initializes the plugin by setting filters and administration functions.
             */
            private function __construct() {

                $this -> templates = array();


                // Add a filter to the attributes metabox to inject template into the cache.
                add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'register_templates' ) );


                // Add a filter to the save post to inject out template into the page cache
                add_filter( 'wp_insert_post_data', array( $this, 'register_templates' ) );


                // Add a filter to the template include to determine if the page has our
                // template assigned and return it's path
                add_filter( 'template_include', array( $this, 'template_include' ) );


                // Add your templates to this array.
                $packs = zeon_get_packs();

                foreach( $packs as $pack ){
                    $dir = zeon_plugin_dir() . '/includes/' . $pack . '/';

                    $handle = opendir( $dir );

                    while( false !== ( $file = readdir( $handle ) ) ) {

                        $not_dot    = $file[ 0 ] != '.';
                        $is_file    = is_file( $dir . $file );
                        $is_php     = pathinfo( $dir . $file, PATHINFO_EXTENSION ) == 'php';

                        if( $not_dot && $is_file && $is_php ){

                            $header = array();

                            if ( ! preg_match( '|Template Name:(.*)$|mi', file_get_contents( $dir . $file ), $header ) )
                                continue;

                            $this -> templates[ $file ] = _cleanup_header_comment( $header[ 1 ] );
                        }
                    }

                    closedir( $handle );
                }
            }

            /**
             * Adds our template to the pages cache in order to trick WordPress
             * into thinking the template file exists where it doens't really exist.
             */
            public function register_templates( $atts )
            {
                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list.
                // If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme() -> get_page_templates();

                if ( empty( $templates ) ) {
                    $templates = array();
                }

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key, 'themes' );

                // Now add our template to the list of templates by merging our templates
                // with the existing templates array from the cache.
                $templates = array_merge( $templates, $this -> templates );

                // Add the modified cache to allow WordPress to pick it up for listing
                // available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;
            }

            /**
             * Checks if the template is assigned to the page
             */
            public function template_include( $template ) {

                global $post;

                if( !isset( $post -> ID ) )
                    return $template;


                $key = get_post_meta( $post -> ID, '_wp_page_template', true  );

                if ( !isset( $this -> templates[ $key ] ) )
                    return $template;


                $theme 	= wp_get_theme();
                $pack 	= $theme -> get( 'TextDomain' );

                $file 	= get_post_meta( $post -> ID, '_wp_page_template', true );

                if ( file_exists( zeon_plugin_dir() . '/includes/' . $pack . '/' . $file ) ) {
                    return zeon_plugin_dir() . '/includes/' . $pack . '/' . $file;
                }

                else if ( file_exists( zeon_plugin_dir() . '/includes/tempo/' . $file ) ) {
                    return zeon_plugin_dir() . '/includes/tempo/' . $file;
                }

                else{
                    echo '<pre style="margin:10px 10px 10px 60px; border:1px dashed #999999; padding:10px; color:#333; background:#ffffff;">';
                    echo "[ File : /plugins/zeon/includes/lib/zeon.templates.class.php ][ Line : 147 ]\n";
                    echo "----------------------------------------------------------------------------\n";
                    echo $file;
                    echo "</pre>";
                }

                return $template;
            }
        }
    }
?>
