<?php

	/**
	 *	Appearance / Customize / Blog - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
	 	'tempo-blog' => array(
			'fields'	=> array(
				'blog-view' => array(
					'title'			=> __( 'Blog View', 'zeon' ),
					'priority' 		=> 1,
					'input'		=> array(
						'type'		=> 'select',
						'default'	=> 'classic',
						'options'	=> array(
							'classic'	=> __( 'Classic', 'zeon' ),
							'grid'		=> __( 'Grid', 'zeon' ),
							'portfolio'	=> __( 'Portfolio', 'zeon' )
						)
					)
				),
				'excerpt' => array(
					'title'			=> __( 'Display Excerpt', 'zeon' ),
					'description'	=> __( 'enable / disable the excerpt instead of the full content.', 'zeon' ),
					'priority' 		=> 25,
					'input'		=> array(
						'type'		=> 'checkbox',
						'default'	=> true
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );