/**
 *  ZEON GMAP ( ADMIN SIDE )
 *  jQuery and Module
 */

var zeon_gmap = {

    deff : {
        type    : google.maps.MapTypeId.ROADMAP,
        zoom    : 4,
        lat     : 47.772594932483,
        lng     : 1.8596535022735,
        markers : {}
    },

    maps : [],

    _class : function( cfgs ){


        /////   PRIVATE VARS    /////


        this.cfgs       = cfgs;
        this.map        = null;
        this.load       = null;
        this.canvas     = null;


        /////   PUBLIC VARS    /////


        /////   PRIVATE METHODS    /////


        this.init = function(){
            var self = this;
            var cfgs = this.cfgs;

            if( !cfgs.hasOwnProperty( 'canvas' ) ){
                return;
            }

            this.load = function(){
                var cfgs = self.cfgs;

                self.map = new google.maps.Map( cfgs.canvas, {
                    mapTypeId   : cfgs.type,
                    zoom        : cfgs.zoom,
                    scrollwheel : false
                });

                self.map.setCenter( new google.maps.LatLng( cfgs.lat , cfgs.lng ) );

                if( cfgs.hasOwnProperty( 'markers' ) && typeof cfgs.markers == 'object' && cfgs.markers.length > 0 ){
                    self.addMarkers();
                }
            }
        }

        this.addMarkers = function(){
            var self    = this;
            var map     = this.map;
            var markers = this.cfgs.markers;

            for( var i = 0; i < markers.length; i++ ){

                (function( i, markers ){

                    var args = {
                        position    : new google.maps.LatLng( markers[ i ].lat, markers[ i ].lng ),
                        map         : map,
                        draggable   : false,
                        visible     : true,
                        icon        : markers[ i ].icon
                    };
                
                    var marker = new google.maps.Marker( args );
                    
                    marker.setMap( map );

                    var _title       = markers[ i ].hasOwnProperty( 'title' ) && markers[ i ].title.length;
                    var _description = markers[ i ].hasOwnProperty( 'description' ) && markers[ i ].description.length;

                    if( _title || _description ){

                        var title       = '';
                        var description = '';

                        if( _title ){
                            title = '<h3 class="tempo-marker-title">' + markers[ i ].title + '</h3>';
                        }

                        if( _description ){
                            description = '<div class="tempo-marker-description">' + markers[ i ].description + '</div>';
                        }
                            
                        var html = title + description;

                        var tooltip = new google.maps.InfoWindow({
                            content: html
                        });

                        google.maps.event.addListener( marker, 'click', function() {
                            tooltip.open( map, marker );
                        });
                    }
                })( i, markers);
            }
        }

        this.init();
    }
};


/////   ZEON jQuery METHODS    /////

(function($){

    $.fn.zeon_gmap = function( args ){
        var args = $.extend( {}, zeon_gmap.deff, args );

        return this.each(function(){
            args.canvas = this;
            zeon_gmap.maps[ jQuery( this ).index() ] = new zeon_gmap._class( args );
            google.maps.event.addDomListener( window, 'load', zeon_gmap.maps[ jQuery( this ).index() ].load );
        });
    }


    /////   RELOAD    /////


    $.fn.zeon_gmap_load = function(){
        zeon_gmap.maps[ jQuery( this ).index() ].load();
    }

})(jQuery);


/////   ZEON GMAP MODULE    /////

jQuery(document).ready(function(){
    var args = gmap.args;
    jQuery( 'div.tempo-shortcode.gmap div.tempo-canvas' ).each(function( i ){

        var _this = this;

        jQuery( this ).zeon_gmap( args[ i ] );

        jQuery(window).resize(function(){
            jQuery( _this ).zeon_gmap_load();
        });
    });
});