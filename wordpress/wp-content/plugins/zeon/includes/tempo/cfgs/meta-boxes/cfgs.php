<?php

	/**
	 *	Meta Boxes - config file
	 */

	$layout = array(
		'title'     => __( 'Custom Layout' , 'zeon' ),
		'context'   => 'side',
		'priority' 	=> 'high',
		'callback'  => 'tempo__box_layout',
		'save' 		=> 'tempo__box_layout_save',
		'args'      => null
	);

	$header = array(
		'title'     => __( 'Custom Header' , 'zeon' ),
		'context'   => 'normal',
		'priority' 	=> 'high',
		'callback'  => 'tempo__box_header',
		'save' 		=> 'tempo__box_header_save',
		'args'      => null
	);

	$settings = array(
		'title'     => __( 'Settings' , 'zeon' ),
		'context'   => 'normal',
		'priority' 	=> 'high',
		'callback'  => 'tempo__box_single_settings',
		'save' 		=> 'tempo__box_single_settings_save',
		'args'      => null
	);

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-meta-boxes' ), array(
		'post'		=> array(
			'tempo-layout'			=> $layout,
			'tempo-header'			=> $header,
			'tempo-settings'		=> $settings,
		),
		'page'		=> array(
			'tempo-template-settings'	=> array(
				'title'     		=> __( 'Template Settings' , 'zeon' ),
				'context'   		=> 'side',
				'priority' 			=> 'high',
				'callback'  		=> 'tempo__box_template',
				'save' 				=> 'tempo__box_template_save',
				'args'      		=> null
			),
			'tempo-layout'		=> $layout,
			'tempo-header'		=> $header,
			'tempo-settings'	=> $settings,
		)/*,
		'header'	=> array(
			'settings'			=> array(
				'title'     		=> __( 'Settings' , 'zeon' ),
				'context'   		=> 'normal',
				'priority' 			=> 'high',
				'callback'  		=> 'tempo__box_settings',
				'save' 				=> 'tempo__box_settings_save',
				'args'      		=> null
			)
		)*/
	));

	tempo_cfgs::set( 'custom-meta-boxes', $cfgs );
?>