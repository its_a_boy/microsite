<!-- HEADER BUTTON 1 -->
<style type="text/css" id="header-btn-1-text-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-text-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-text-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-text-color' );
			$transp = tempo_options::get( 'header-btn-1-text-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-text-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-text-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-text-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-text-h-color' );
			$transp = tempo_options::get( 'header-btn-1-text-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1:hover{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-bkg-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-bkg-color' );
			$transp = tempo_options::get( 'header-btn-1-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-1-bkg-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-1-bkg-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-1-bkg-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-1-bkg-h-color' );
			$transp = tempo_options::get( 'header-btn-1-bkg-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-1:hover{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>


<!-- HEADER BUTTON 2 -->
<style type="text/css" id="header-btn-2-text-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-text-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-text-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-text-color' );
			$transp = tempo_options::get( 'header-btn-2-text-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-text-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-text-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-text-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-text-h-color' );
			$transp = tempo_options::get( 'header-btn-2-text-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2:hover{
				color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-bkg-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-bkg-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-bkg-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-bkg-color' );
			$transp = tempo_options::get( 'header-btn-2-bkg-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>

<style type="text/css" id="header-btn-2-bkg-h-color">
	<?php
		if( tempo_options::is_set( 'header-btn-2-bkg-h-color' ) ||
		 	tempo_options::is_set( 'header-btn-2-bkg-h-transp' ) ) :

			$hex 	= tempo_options::get( 'header-btn-2-bkg-h-color' );
			$transp = tempo_options::get( 'header-btn-2-bkg-h-transp' );
			$rgba 	= 'rgba( ' . tempo_hex2rgb( $hex ) . ', ' . number_format( floatval( $transp / 100 ), 2 ) . ' )';
	?>

			header.tempo-header div.tempo-header-partial div.tempo-header-btns-wrapper a.tempo-btn.btn-2:hover{
				background-color: <?php echo esc_attr( $rgba ); ?>;
			}

	<?php endif; ?>
</style>
