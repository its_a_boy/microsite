<?php
    /*
        [max width="" align=""]

            ...

        [/max]
    */

    class zeon_shortcode_max
    {
        static function run( $_attr, $_content )
        {
            $attr = shortcode_atts( array(
                'width' => '100%',
                'align' => 'center'
            ), $_attr );

            $rett  = '';
            $rett .= '<div class="tempo-shortcode max ' . esc_attr( $attr[ 'align' ] ) . '" style="max-width: ' . esc_attr( $attr[ 'width' ] )  . '">';

            if( $_content != strip_tags( $_content ) ){
                $rett .= do_shortcode( $_content );
            }

            else{
                if( $_content != strip_shortcodes( $_content ) ){
                    $rett .= do_shortcode( $_content );       
                }

                else{
                    $rett .= '<p>' . do_shortcode( $_content ) . '</p>';
                }
            }

            $rett .= '</div>';

            return $rett;
        }
    }