<?php

    /*
        [social style="squared|circled|rounded" item_size="small|normal|large"]

            [social_item type="facebook" url="https://facebook.com/myThemes/"/]
            [social_item type="twitter" url="https://twitter.com/my_themes/"/]

            ...

        [/social]
    */

    class zeon_shortcode_social
    {
        private static $run = false;
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( !self::is_run() ){
                self::$run = true;

                $attr = shortcode_atts( array(
                    'style'     => 'rounded',
                    'item_size' => 'normal'
                ), $_attr );

                /* STYLES AND PROPERTIES */
                $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                /* CONTENT */
                $rett .= '<div class="tempo-shortcode social ' . esc_attr( $attr[ 'style' ] ) . ' ' . esc_attr( $attr[ 'item_size' ] ) . '" ' . $style . '>';

                $rett .= do_shortcode( strip_tags( $_content ) );

                $rett .= '<div class="clearfix"></div>';
                $rett .= '</div>';

                self::$run = false;
            }

            return $rett;
        }

        public $code = null;
        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Style', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'social-style',
                        'default'   => 'rounded',
                        'theme_mod' => false,
                        'options'   => array(
                            'squared'   => __( 'Squared' , 'zeon' ),
                            'circled'   => __( 'Circled' , 'zeon' ),
                            'rounded'   => __( 'Rounded' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Size' , 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'social-size',
                        'default'   => 'normal',
                        'theme_mod' => false,
                        'options'   => array(
                            'small'     => __( 'Small' , 'zeon' ),
                            'normal'    => __( 'Normal', 'zeon' ),
                            'large'     => __( 'Large' , 'zeon' )
                        )
                    )
                )
            );

            $set_1 = array(
                'evernote',
                'vimeo',
                'twitter',
                'skype',
                'renren',
                'github',
                'rdio',
                'linkedin',
                'behance',
                'dropbox',
                'flickr',
                'instagram',
                'vkontakte'
            );

            $fields[ 'social-set-1' ] = array();

            foreach( $set_1 as $item ){
                $fields[ 'social-set-1' ][] = array(
                    'icon'      => $item,
                    'format'    => 'social',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'social-' . $item,
                        'theme_mod' => false
                    )
                );
            }

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Social Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'fields'    => $fields[ 'social-set-1' ]
                )
            );

            $set_2 = array(
                'facebook',
                'tumblr',
                'picasa',
                'dribbble',
                'stumbleupon',
                'lastfm',
                'gplus',
                'google-circles',
                'youtube-play',
                'youtube',
                'pinterest',
                'smashing',
                'soundcloud',
                'flattr',
                'odnoklassniki',
                'mixi',
                'rss'
            );

            $fields[ 'social-set-2' ] = array();

            foreach( $set_2 as $item ){
                $fields[ 'social-set-2' ][] = array(
                    'icon'      => $item,
                    'format'    => 'social',
                    'input'     => array(
                        'type'      => 'url',
                        'name'      => 'social-' . $item,
                        'theme_mod' => false
                    )
                );
            }

            $boxes[ 'right' ] = array(
                array(
                    'fields'    => $fields[ 'social-set-2' ]
                )
            );

            $style          = '<span class="social-style attr">style="<span class="value">rounded</span>"</span>';
            $size           = '<span class="social-size attr">size="<span class="value">normal</span>"</span>';

            $this -> code   =   '[zeon_social ' . $style . ' ' . $size . ']' . '<br/>' . "\n" .
                                '<span class="social-evernote hidden">[zeon_social_item type="evernote" <span class="social-evernote attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-vimeo hidden">[zeon_social_item type="vimeo" <span class="social-vimeo attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-twitter hidden">[zeon_social_item type="twitter" <span class="social-twitter attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-skype hidden">[zeon_social_item type="skype" <span class="social-skype attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-renren hidden">[zeon_social_item type="renren" <span class="social-renren attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-github hidden">[zeon_social_item type="github" <span class="social-github attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-rdio hidden">[zeon_social_item type="rdio" <span class="social-rdio attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-linkedin hidden">[zeon_social_item type="linkedin" <span class="social-linkedin attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-behance hidden">[zeon_social_item type="behance" <span class="social-behance attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-dropbox hidden">[zeon_social_item type="dropbox" <span class="social-dropbox attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-flickr hidden">[zeon_social_item type="flickr" <span class="social-flickr attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-instagram hidden">[zeon_social_item type="instagram" <span class="social-instagram attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-vkontakte hidden">[zeon_social_item type="vkontakte" <span class="social-vkontakte attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .

                                '<span class="social-facebook hidden">[zeon_social_item type="facebook" <span class="social-facebook attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-tumblr hidden">[zeon_social_item type="tumblr" <span class="social-tumblr attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-picasa hidden">[zeon_social_item type="picasa" <span class="social-picasa attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-dribbble hidden">[zeon_social_item type="dribbble" <span class="social-dribbble attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-stumbleupon hidden">[zeon_social_item type="stumbleupon" <span class="social-stumbleupon attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-lastfm hidden">[zeon_social_item type="lastfm" <span class="social-lastfm attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-gplus hidden">[zeon_social_item type="gplus" <span class="social-gplus attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-google-circles hidden">[zeon_social_item type="google-circles" <span class="social-google-circles attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-youtube-play hidden">[zeon_social_item type="youtube-play" <span class="social-youtube-play attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-youtube hidden">[zeon_social_item type="youtube" <span class="social-youtube attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-pinterest hidden">[zeon_social_item type="pinterest" <span class="social-pinterest attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-smashing hidden">[zeon_social_item type="smashing" <span class="social-smashing attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-soundcloud hidden">[zeon_social_item type="soundcloud" <span class="social-soundcloud attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-flattr hidden">[zeon_social_item type="flattr" <span class="social-flattr attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-odnoklassniki hidden">[zeon_social_item type="odnoklassniki" <span class="social-odnoklassniki attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-mixi hidden">[zeon_social_item type="mixi" <span class="social-mixi attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '<span class="social-rss hidden">[zeon_social_item type="rss" <span class="social-rss attr">url="<span class="value"></span>"</span>/]' . '<br/>' . "\n" . '</span>' .
                                '[/zeon_social]';

            return array(
                array(
                    'boxes' => $boxes['left']
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        static function is_run()
        {
            return self::$run;
        }
    }

    class zeon_shortcode_social_item
    {
        private static $run = false;

        static function run( $_attr )
        {
            $rett = '';

            if( !self::is_run() && zeon_shortcode_social::is_run() ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'type'  => null,
                    'url'   => null
                ), $_attr );

                if( !empty( $attr[ 'type' ] ) ){
                    $rett .= '<a class="tempo-icon-' . esc_attr( $attr[ 'type' ] )  . '"';

                    if( esc_url( $attr[ 'url' ] ) ){
                        $rett .= ' href="' . esc_url( $attr[ 'url' ] ) . '"';
                    }

                    $rett .= ' target="_blank"></a>';
                }

                self::$run = false;
            }

            return $rett;
        }

        static function is_run()
        {
            return self::$run;
        }
    }
?>
