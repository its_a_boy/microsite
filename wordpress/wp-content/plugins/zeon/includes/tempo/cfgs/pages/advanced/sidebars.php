<?php

	/**
	 *	Zeon / General - config settings
	 */

	$settings = tempo_cfgs::merge( (array)tempo_cfgs::get( 'settings' ), array(
		'advanced' => array(
			'zeon-sidebars' => array(
			    'menu' => array(
			        'title'     => __( 'Sidebars', 'zeon' ),
			        'parent'	=> __( 'Zeon', 'zeon' ),
			        'icon'		=> zeon_plugin_uri() . '/includes/media/img/icon.png'
			    ),
			    'priority'	=> 1,
			    'sections' 	=> array(
			    	array(
			    		'columns' 		=> array(

			    			array(
			    				'layout' 			=> array(
									'sm'	=> 12,
									'md'	=> 12,
									'lg'	=> 12,
								),

								'notification'	=> array(
									'type'			=> 'info',
									'title'			=> __( 'Appearance &rsaquo; Customize &rsaquo; Sidebars', 'zeon' ),
									'description' 	=> __( 'You can customize the appearance settings for each heade or footer sidebar.', 'zeon' ),
									'style'			=> array(
										'margin-bottom' => '15px'
									)
								),
			    			),

			    			// COLUMNS
							array(
								'layout' 			=> array(
									'sm'	=> 12,
									'md'	=> 6,
									'lg'	=> 6,
								),

								'boxes' => array(
									array(
										'title'			=> __( 'Header Front Page Sidebar', 'zeon' ),
										'fields'		=> array(
											array(
												'title' 		=> __( 'Number of Columns', 'zeon' ),
												'description'	=> __( 'Inside of the Sidebar the widgets will be arranged in columns.', 'zeon' ),
												'format'		=> 'across',
												'input'			=> array(
													'type'			=> 'select',
													'name'			=> 'header-front-page-nr-columns',
													'validator'		=> 'number',
													'default'		=> 0,
													'options'		=> array(
														0	=> number_format_i18n( 0 ),
														1	=> number_format_i18n( 1 ),
														2	=> number_format_i18n( 2 ),
														3 	=> number_format_i18n( 3 ),
														4 	=> number_format_i18n( 4 )
													)
												)
											)
										)
									),
									array(
										'title'		=> __( 'Header Blog Sidebar', 'zeon' ),
										'fields'	=> array(
											array(
												'title' 		=> __( 'Number of Columns', 'zeon' ),
												'description'	=> __( 'Inside of the Sidebar the widgets will be arranged in columns.', 'zeon' ),
												'format'		=> 'across',
												'input'			=> array(
													'type'			=> 'select',
													'name'			=> 'header-blog-nr-columns',
													'validator'		=> 'number',
													'default'		=> 0,
													'options'		=> array(
														0	=> number_format_i18n( 0 ),
														1	=> number_format_i18n( 1 ),
														2	=> number_format_i18n( 2 ),
														3 	=> number_format_i18n( 3 ),
														4 	=> number_format_i18n( 4 )
													)
												)
											)
										)
									)
								)
							),
							array(
								'layout' 			=> array(
									'sm'	=> 12,
									'md'	=> 6,
									'lg'	=> 6,
								),

								'boxes' => array(
									array(
										'title'		=> __( 'Footer Light Sidebar', 'zeon' ),
										'fields'	=> array(
											array(
												'title' 		=> __( 'Number of Columns', 'zeon' ),
												'description'	=> __( 'Inside of the Sidebar the widgets will be arranged in columns.', 'zeon' ),
												'format'		=> 'across',
												'input'			=> array(
													'type'			=> 'select',
													'name'			=> 'footer-light-nr-columns',
													'validator'		=> 'number',
													'default'		=> 0,
													'options'		=> array(
														0	=> number_format_i18n( 0 ),
														1	=> number_format_i18n( 1 ),
														2	=> number_format_i18n( 2 ),
														3 	=> number_format_i18n( 3 ),
														4 	=> number_format_i18n( 4 )
													)
												)
											)
										)
									),
									array(
										'title'		=> __( 'Footer Dark Sidebar', 'zeon' ),
										'fields'	=> array(
											array(
												'title' 		=> __( 'Number of Columns', 'zeon' ),
												'description'	=> __( 'Inside of the Sidebar the widgets will be arranged in columns.', 'zeon' ),
												'format'		=> 'across',
												'input'			=> array(
													'type'			=> 'select',
													'name'			=> 'footer-dark-nr-columns',
													'validator'		=> 'number',
													'default'		=> 0,
													'options'		=> array(
														0	=> number_format_i18n( 0 ),
														1	=> number_format_i18n( 1 ),
														2	=> number_format_i18n( 2 ),
														3 	=> number_format_i18n( 3 ),
														4 	=> number_format_i18n( 4 )
													)
												)
											)
										)
									)
								)
							)
			    		)
			    	)
				)
			)
		)
	));

	tempo_cfgs::set( 'settings', $settings );
?>
