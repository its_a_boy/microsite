<?php
	if( !class_exists( 'zeon_shortcodes' ) ){

		function zeon_autoload_shortcodes( $class_name ){
			if( preg_match( "/^zeon_shortcode_/", $class_name ) ){
				zeon_shortcodes::load( $class_name );
			}
		}

		spl_autoload_register( 'zeon_autoload_shortcodes' );


		class zeon_shortcodes
		{
			static $shortcode_files = array();

			static function init()
			{
				$directory  = zeon_plugin_dir() . '/includes/lib/shortcodes';
		        $shortcodes = include_once $directory . '/shortcodes.php';

		        foreach( $shortcodes as $node => $shortcode ){
		            $n = $node;
		            $s = $shortcode;

		            if( !is_string( $node ) ){
		                $n = $s;
		            }

		            self::$shortcode_files[ "zeon_shortcode_{$n}" ] = $directory . "/zeon.shortcode.{$s}.php";
		            add_shortcode( "zeon_{$n}", array( 'zeon_shortcodes', 'run' ) );
		        }
			}

			static function load( $class_name )
    		{
		        if( isset( self::$shortcode_files[ $class_name ] ) ) {
		            include_once self::$shortcode_files[ $class_name ];
		        }
		    }

			static function run( $attr , $content , $tag )
		    {
				$tag_name	= str_replace( 'zeon_', '', $tag );
		        $class_name = "zeon_shortcode_{$tag_name}";

		        return call_user_func_array( array( new $class_name , 'run' ) , array( $attr, $content, $tag ) );
		    }
		}



		/////	SHORTCODES MANAGER PANEL    /////



		/**
		 *	Shortcodes Manager Button
		 *	Add media button before WordPress Editor
		 */
		function zeon_shortcode_manager_button()
	    {
	        global $post;

	        if( isset( $post -> post_type ) && (
	            $post -> post_type == 'page' ||
	            $post -> post_type == 'post' ||
	            $post -> post_type == 'portfolio' ) )
	        {

	            echo '<a href="javascript:zeon.manager.panel();" class="button button-primary">';
	            _e( 'Shortcodes Manager' , 'zeon' );
	            echo '</a>';
	        }
	    }

	    add_action( 'media_buttons', 'zeon_shortcode_manager_button' );


	    function zeon_shortcode_manager_panel()
    	{
	        $groups = array(
	            'text'          => array(
	                'icon'                  => __( 'Icon', 'zeon' ),
	                'dropcap'               => __( 'Dropcap' , 'zeon' ),
	                'quote'                 => __( 'Quote' , 'zeon' )
	            ),

	            'separators'    => array(
	                'delimiter'             => __( 'Delimiter' , 'zeon' ),
	                'space'                 => __( 'Space' , 'zeon' )
	            ),

	            'messages'      => array(
	                'alert'                 => __( 'Alert' , 'zeon' ),
	                'notify'                => __( 'Notify' , 'zeon' )
	            ),

	            'actions'       => array(
	                'button'                => __( 'Button' , 'zeon' ),
	                'social'                => __( 'Social' , 'zeon' ),
	                'message'               => __( 'Message' , 'zeon' )

	            ),

	            'content'       => array(
	                'columns'               => __( 'Columns' , 'zeon' ),
	                'sponsors'              => __( 'Sponsors' , 'zeon' ),
	                'gmap'                  => __( 'Google Map' , 'zeon' ),
	                'code'                  => __( 'Code' , 'zeon' )
	            ),

	            'others'    => array(
	                'list'                  => __( 'List' , 'zeon' ),
	                'timeline'              => __( 'Timeline' , 'zeon' ),
//	                'feature'              	=> __( 'Feature' , 'zeon' ),
	                'counters'              => __( 'Scroll Counter' , 'zeon' ),
	            )
	        );

	        $selector_options 	= array(
	        	'0' => __( '-- Select Shortcode --' , 'zeon' )
	        );

        	$insert_buttons     = '';
        	$shortcode_fields   = '';

        	$selector_actions 	= array();
        	$hide_actions    	= array();
        	$actions        	= array();

	        foreach( $groups as $group => $shortcodes ){
	            foreach( $shortcodes as $name => $label ){

	                if( $fields = zeon_shortcode_manager_fields( $name ) ){

	                    $selector_options[ $group ][ 'zeon-shortcode-' . esc_attr( $name ) . '-fields' ] = $label;

	                    $hide_actions[] = '.zeon-shortcode-' . esc_attr( $name ) . '-fields';
	                    $actions[ 'zeon-shortcode-' . esc_attr( $name ) . '-fields' ] = array( '.zeon-shortcode-' . esc_attr( $name ) . '-fields' );

	                    $insert_buttons .= '<input type="button" class="tempo-button ' . 'zeon-shortcode-' . esc_attr( $name ) . '-fields" value="' . __( 'Insert Shortcode' , 'zeon' ) . '" onclick="zeon.manager.insert( \'' . esc_attr( $name ) . '\' );"/>';

	                    $shortcode_fields .= $fields;
	                }
	            }
	        }

	        $selector_actions = array_merge( array(
                '0' => array(
                    'hide' => $hide_actions,
                )
            ), $actions );



	        /**
	         *	The Shortcode Manager Panel and Shadow
	         */
	        $rett  = '<div class="zeon-shortcodes-manager-shadow"></div>';
	        $rett .= '<div class="zeon-shortcodes-manager">';

	        /**
	         *	Header
	         *	The header for the Shortcodes Manager Panel
	         */
	        $rett .= '<div class="zeon-shortcodes-panel-antet">';
	        $rett .= '<a href="javascript:void(null);" class="zeon-close"><i class="tempo-icon-cancel-2"></i></a>';
	        $rett .= '<h3>' . __( 'Shortcodes Manager' , 'zeon' ) . '</h3>';
	        $rett .= '</div>';

	        /**
	         *	Content
	         *	The content for the Shortcodes Manager Panel
	         */
	        $rett .= '<table width="100%" class="zeon-shortcodes-panel">';

	        $rett .= '<tr>';
	        $rett .= '<td colspan="2" class="zeon-shortcodes-panel-header">';
	        $rett .= '</td>';
	        $rett .= '</tr>';

        	$rett .= '<tr>';


    		/**
    		 *	Left Side
    		 *	Actions - Select and Insert Shortcode
    		 */
	        $rett .= '<td class="zeon-shortcodes-panel-actions">';

	        // 	The field to choose the shortcode.
	        $rett .= tempo_html::field( array(
	            'format'    => 'across',
	            'input'     => array(
	                'type'      => 'select',
	                'slug'      => 'zeon-shortcodes-selector',
	                'options'   => $selector_options,
	                'actions'   => $selector_actions
	            )
	        ));

	        // 	The buttons to insert the shortcodes to WordPress Editor
	        $rett .= $insert_buttons;

	        $rett .= '</td>';


	        /**
	         *	Right Side
	         *	The Shortcode Manager Fields
			 * 	The input fields to config the shortcodes
	         */
	        $rett .= '<td class="zeon-shortcodes-panel-fields">';
	        $rett .= $shortcode_fields;
	        $rett .= '</td>';

        	$rett .= '</tr>';
	        $rett .= '</table>';
	        $rett .= '</div>';

	        echo $rett;
	    }

    	function zeon_shortcode_manager_fields( $name )
	    {
	        $code               = null;
	        $columns            = array();
	        $shortcode_class    = "zeon_shortcode_{$name}";

	        if( class_exists( $shortcode_class ) && method_exists( new $shortcode_class, 'builder' ) ){
	            $shortcode  = new $shortcode_class;
	            $columns    = $shortcode -> builder();
	            $code       = $shortcode -> code;
	        }

	        if( empty( $columns ) ){
	            return false;
	        }

	        $rett = '<div class="zeon-shortcodes-fields hidden zeon-shortcode-' . esc_attr( $name ) . '-fields">';

	        /**
	         *	Shortcode Code
	         */
	        $rett .= tempo_html::notification( array(
	            'type'          => 'success',
	            'class'         => 'zeon-shortcode-code-wrapper',
	            'description'   => __( 'Also you can use the Shortcode by direct input ( click to select the code ).', 'zeon' )
	                            . '<br/><span class="zeon-shortcode-code">' . $code . '</span><span class="hidden zeon-shortcode-tmp-code"></span>',

	            'style'         => array(
	                'margin-top'    => '0px',
	                'margin-bottom' => '20px',
	            )
	        ));

	        /**
	         *	Shortcode Settings
	         */
	        $rett .= tempo_html::$column -> wrapper( 'before' );

	        foreach( $columns as $i => $args ){

	            $args = wp_parse_args( (array)$args, array(
	            	'layout' => array(
	                    'sm' 	=> 12,
	                    'md' 	=> 6,
	                    'lg' 	=> 6
	                )
	            ));

	            $rett .= tempo_html::$column -> get( $args );
	        }

	        $rett .= tempo_html::$column -> wrapper( 'after' );

	        $rett .= '<div class="clear clearfix"></div>';
	        $rett .= '</div>';

	        return $rett;
	    }

    	add_action( 'in_admin_header', 'zeon_shortcode_manager_panel' );


		/**
		 *	Shortcodes Manager Scripts and Styles
		 */
		function zeon_shortcodes_scripts()
		{
			// color picker
			wp_enqueue_style( 'wp-color-picker' );

			// styles
    		wp_register_style( 'zeon-shortcodes-manager', 	zeon_plugin_uri() . '/includes/lib/shortcodes/media/admin/css/manager.css' );
    		wp_enqueue_style( 'zeon-shortcodes-manager' );

    		// scripts
    		wp_register_script( 'zeon-shortcodes-manager', 	zeon_plugin_uri() . '/includes/lib/shortcodes/media/admin/js/manager.js', array( 'jquery' ), null, true );
    		wp_enqueue_script( 'zeon-shortcodes-manager' );
		}

		add_action( 'admin_init', 'zeon_shortcodes_scripts' );



		/////	SHORTCODE PROPERTIES    /////



		function zeon_shortcode_validate_prop( $_attr, $attr)
	    {
	        if( !empty( $attr ) ){
	            foreach ( $attr as $key => $value ) {
	                if( !isset( $_attr[ $key ] ) ){
	                    $_attr[ $key ] = $value;
	                }
	            }
	        }

	        return $_attr;
	    }

		function zeon_shortcode_prop( $_attr, $attr = null )
	    {
	        $_attr = zeon_shortcode_validate_prop( $_attr, $attr );

	        $prop = '';

	        /**
	         *	BACKGROUND
	         *
	         * 	Attributes:
	         * 	- bg_color
	         * 	- bg_opacity
	         * 	- bg_image
	         * 	- bg_repeat
	         * 	- bg_size
	         * 	- bg_attachment
	         * 	- bg_position
	         */

	        // background color
	        if( isset( $_attr[ 'bg_color' ] ) && !empty( $_attr[ 'bg_color' ] ) ){
	            $bg_color = $_attr[ 'bg_color' ];

	            // background opacity
	            if( isset( $_attr[ 'bg_opacity' ] ) && !empty( $_attr[ 'bg_opacity' ] ) ){
	                $bg_opacity = ((int)$_attr[ 'bg_opacity' ] ) / 100;
	                $prop .= 'background-color: rgba( ' . tempo_hex2rgb( $bg_color ) . ' , ' . $bg_opacity . ' ); ';
	            }

	            // missing the background opacity
	            else{
	                $prop .= 'background-color: ' . $bg_color . '; ';
	            }
	        }

	        // background image
	        if( isset( $_attr[ 'bg_image' ] ) && !empty( $_attr[ 'bg_image' ] ) ){

	            $bg_image = $_attr[ 'bg_image' ];
	            $prop .= 'background-image: url( ' . $bg_image . '); ';

	            // background repeat
	            if( isset( $_attr[ 'bg_repeat' ] ) && !empty( $_attr[ 'bg_repeat' ] ) ){

	                $bg_repeat = $_attr[ 'bg_repeat' ];
	                $prop .= 'background-repeat: ' . $bg_repeat . '; ';
	            }

	            // background size
	            if( isset( $_attr[ 'bg_size' ] ) && !empty( $_attr[ 'bg_size' ] ) ){

	                $bg_size = $_attr[ 'bg_size' ];
	                $prop .= 'background-size: ' . $bg_size . '; ';
	            }

	            // background attachment
	            if( isset( $_attr[ 'bg_attachment' ] ) && !empty( $_attr[ 'bg_attachment' ] ) ){

	                $bg_attachment = $_attr[ 'bg_attachment' ];
	                $prop .= 'background-attachment: ' . $bg_attachment . '; ';
	            }

	            // background position
	            if( isset( $_attr[ 'bg_position' ] ) && !empty( $_attr[ 'bg_position' ] ) ){

	                $bg_position = $_attr[ 'bg_position' ];
	                $prop .= 'background-position: ' . $bg_position . '; ';
	            }
	        }


	        /**
	         *	COLOR
	         *
	         *	Attributes:
	         *	- color
	         *	- opacity
	         */

	        if( isset( $_attr[ 'color' ] ) && !empty( $_attr[ 'color' ] ) ){
	            $color = $_attr[ 'color' ];

	            // color opacity
	            if( isset( $_attr[ 'opacity' ] ) && !empty( $_attr[ 'opacity' ] ) ){
	                $opacity = ((int)$_attr[ 'opacity' ] ) / 100;
	                $prop .= 'color: rgba( ' . tempo_hex2rgb( $color ) . ' , ' . $opacity . ' ); ';
	            }

	            // missing the opacity
	            else{
	                $prop .= 'color: ' . $color . '; ';
	            }
	        }


	        /**
	         *	MARGIN
	         *
	         *	Attributes:
	         *	- top
	         *	- bottom
	         */

	        if( isset( $_attr[ 'top' ] ) && !is_null( $_attr[ 'top' ] ) ){
	            $top = (int)$_attr[ 'top' ];
	            $prop .= 'margin-top: ' . $top . 'px; ';
	        }

	        //- MARGIN BOTTOM -//
	        if( isset( $_attr[ 'bottom' ] ) && !is_null( $_attr[ 'bottom' ] ) ){

	            $bottom = (int)$_attr[ 'bottom' ];
	            $prop .= 'margin-bottom: ' . $bottom . 'px; ';
	        }


	        /**
	         *	PADDING
	         *
	         *	Attributes:
	         *	- inner
	         */
	        if( isset( $_attr[ 'inner' ] ) && !is_null( $_attr[ 'inner' ] ) ){

	            $inner = (int)$_attr[ 'inner' ];
	            $prop .= 'padding-top: ' . $inner . 'px; ';
	            $prop .= 'padding-bottom: ' . $inner . 'px; ';
	        }


	        /**
	         *	SIZE
	         *
	         *	Attributes:
	         *	- size
	         *	- width
	         *	- height
	         *
	         *	RESPONSIVE SIZE
	         *
	         *	Attributes:
	         *	- max_size
	         *	- max_width
	         *	- max_height
	         */

	        // size, the width is equal with the height
	        if( isset( $_attr[ 'size' ] ) && !empty( $_attr[ 'size' ] ) ){

	            $size = (int)$_attr[ 'size' ];

	            if( $size > 0 ){
	            	$prop .= 'width: ' . $size . 'px; ';
	            	$prop .= 'height: ' . $size . 'px; ';
	        	}
	        }

	        else{

	            // width
	            if( isset( $_attr[ 'width' ] ) && !empty( $_attr[ 'width' ] ) ){

	                $width = (int)$_attr[ 'width' ];
	                $prop .= 'width: ' . $width . 'px; ';
	            }

	            // height
	            if( isset( $_attr[ 'height' ] ) && !empty( $_attr[ 'height' ] ) ){

	                $height = (int)$_attr[ 'height' ];
	                $prop .= 'height: ' . $height . 'px; ';
	            }
	        }

	        // responsive size
	        if( isset( $_attr[ 'max_size' ] ) && !empty( $_attr[ 'max_size' ] ) ){

	            $max_size = (int)$_attr[ 'max_size' ];
	            $prop .= 'max-width: ' . $max_size . 'px; width: 100%; ';
	            $prop .= 'height: ' . $max_size . 'px; ';
	        }


	        else{
	            // responsive width
	            if( isset( $_attr[ 'max_width' ] ) && !empty( $_attr[ 'max_width' ] ) ){

	                $max_width = (int)$_attr[ 'max_width' ];
	                $prop .= 'max-width: ' . $max_width . 'px; width: 100%; ';
	            }

	            // responsive height
	            if( isset( $_attr[ 'max_height' ] ) && !empty( $_attr[ 'max_height' ] ) ){

	                $max_height = (int)$_attr[ 'max_height' ];
	                $prop .= 'height: ' . $max_height . 'px; ';
	            }
	        }

	        // responsive size
	        if( isset( $_attr[ 'text_align' ] ) && !empty( $_attr[ 'text_align' ] ) ){

	            $text_align = esc_attr( $_attr[ 'text_align' ] );
	            $prop .= 'text-align: ' . $text_align . ';';
	        }

	        return $prop;
	    }
	}
?>
