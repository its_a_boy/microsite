<?php
	global $post;

	$audio_id 		= absint( zeon_meta::g( $post -> ID, 'tempo-audio-id' ) );

    $audio = null;
    $image = null;

    if( !empty( $audio_id ) )
    	$audio = get_post( $audio_id );

    /**
     *	Audio details from post meta
     */

    $title 			= zeon_meta::get( $post -> ID, 'tempo-audio-title' );
	$description 	= zeon_meta::get( $post -> ID, 'tempo-audio-description' );


    /**
     *	Audio details from media library post attachment
     */

    if( isset( $audio -> ID ) ){
    	$title 			= zeon_meta::get( $post -> ID, 'tempo-audio-title', $audio -> post_title );
    	$description 	= zeon_meta::get( $post -> ID, 'tempo-audio-description', $audio -> post_content );

    	// audio thumbnail
		$thumbnail 		= get_post( get_post_thumbnail_id( $audio -> ID ) );

		if( has_post_thumbnail( $audio ) && isset( $thumbnail -> ID ) && wp_attachment_is( 'image', $thumbnail ) ){
			$media = wp_get_attachment_image_src( $thumbnail -> ID, 'cronus-header' );

    		if( isset( $media[ 0 ] ) )
				$image = esc_url( $media[ 0 ] );
		}
    }


    // post thumbnail
	$thumbnail = get_post( get_post_thumbnail_id( $post ) );

	if( has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) && wp_attachment_is( 'image', $thumbnail ) ){
		$media = wp_get_attachment_image_src( $thumbnail -> ID, 'cronus-header' );
			
		if( isset( $media[ 0 ] )  )
			$image = esc_url( $media[ 0 ] );
	}


	if( empty( $image ) )
		return;


    /**
     *  Header Image Alt Attribute
     */

    $header_image_alt   = $title;

    if( !(empty( $title ) || empty( $description )) ){
        $header_image_alt .=  ' - ' . $description;
    }

    else if( !empty( $description ) ){
        $header_image_alt .=  $description;
    }

?>
	<div class="parallax" style="background-image: url(<?php echo esc_url( $image ); ?>);">
        <img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $header_image_alt ); ?>" class="parallax-image"/>
    </div>