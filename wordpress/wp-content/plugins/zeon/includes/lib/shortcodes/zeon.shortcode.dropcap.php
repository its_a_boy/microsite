<?php

    /*
        [dropcap type="circle" color="#333333" background="#f0f0f0"]S[/dropcap]uca
    */

    class zeon_shortcode_dropcap
    {
        static function run( $_attr, $_content = null )
        {
            $attr = shortcode_atts( array(
                'type' => '',
                'color' => '#333333'
            ), $_attr );

            if( isset( $attr[ 'type' ] ) && $attr[ 'type' ] == 'empty' )
                if( isset( $_attr[ 'bg_color' ] ) )
                    unset( $_attr[ 'bg_color' ] );

            /* STYLE AND PROPERTIES */
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );


            if( isset( $_attr[ 'bg_color' ] ) && !empty( $_attr[ 'bg_color' ] ) ){
                $prop .= 'margin-right: 7px; ';

                if( !isset( $_attr[ 'top' ] ) ){
                    $prop .= 'margin-top: 7px; ';
                }
            }

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $content = strip_tags( $_content );

            return '<p><span class="tempo-shortcode dropcap ' . esc_attr( $attr[ 'type' ] ) . '" ' . $style . '>' . mb_substr( $content , 0 , 1 ) . '</span> ' . mb_substr( $content , 1 , -1 ) . '</p>';
        }

        public $code = null;
        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields[ 'settings' ][] = array(
                'title'     => __( 'Type', 'zeon' ),
                'format'    => 'across',
                'input'     => array(
                    'type'      => 'select',
                    'name'      => 'dropcap-type',
                    'default'   => 'empty',
                    'theme_mod' => false,
                    'options'   => array(
                        'empty'     => __( 'Empty' , 'zeon' ),
                        'circled'   => __( 'Circled' , 'zeon' ),
                        'squared'   => __( 'Squared' , 'zeon' ),
                        'rounded'   => __( 'Rounded' , 'zeon' )
                    )
                )
            );
            $fields[ 'settings' ][] = array(
                'title'     => __( 'Letter Color' , 'zeon' ),
                'format'    => 'linear',
                'input'     => array(
                    'type'      => 'color',
                    'name'      => 'dropcap-color',
                    'default'   => '#333333',
                    'theme_mod' => false
                )
            );
            $fields[ 'settings' ][] = array(
                'title'     => __( 'Background Color' , 'zeon' ),
                'format'    => 'linear',
                'input'     => array(
                    'type'      => 'color',
                    'name'      => 'dropcap-bg-color',
                    'default'   => '#333333',
                    'theme_mod' => false
                )
            );
            $boxes[ 'left' ][] = array(
                'title'     => __( 'Dropcap Settings', 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );


            $fields[ 'content' ][] = array(
                'description'   => __( 'insert the paragraph what will be with a dropcap.' , 'zeon' ),
                'format'        => 'across',
                'input'         => array(
                    'type'      => 'textarea',
                    'name'      => 'dropcap-content',
                    'default'   => 'A...',
                    'theme_mod' => false
                )
            );
            $boxes[ 'right' ][] = array(
                'title'     => __( 'Dropcap Content', 'zeon' ),
                'fields'    => $fields[ 'content' ]
            );


            $type           = '<span class="dropcap-type attr">type="<span class="value">square</span>"</span>';
            $color          = '<span class="dropcap-color attr">color="<span class="value">#333333</span>"</span>';
            $bg_color       = '<span class="dropcap-bg-color attr">bg_color="<span class="value">#333333</span>"</span>';
            $content        = '<span class="dropcap-content text"><span class="value">A...</span></span>';

            $this -> code   = '[zeon_dropcap ' . $type . ' ' . $color . ' '. $bg_color . ']' . $content . '[/zeon_dropcap]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes[ 'right' ]
                )
            );
        }
    }
?>
