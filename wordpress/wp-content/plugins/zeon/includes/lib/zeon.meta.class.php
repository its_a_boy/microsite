<?php
if( !class_exists( 'zeon_meta' ) ){
class zeon_meta
{
    static function g( $post_id, $metakey )
    {
        return get_post_meta( $post_id , $metakey , true );
    }

    static function get( $post_id, $metakey, $default = null )
    {   
        $rett = $default;

        if( self::exists( $post_id, $metakey ) ){
            $rett = get_post_meta( $post_id , $metakey , true );    
        }
        
        return $rett;
    }

    static function exists( $post_id, $metakey )
    {
        $value = get_post_meta( $post_id, $metakey, true );

        $rett = false;

        if( ! add_post_meta( $post_id, $metakey, $value, true ) ){
            $rett = true;
        }
        else{
            delete_post_meta( $post_id, $metakey );
        }
            

        return $rett;
    }
    
    static function set( $post_id, $metakey, $value )
    {
        if( self::exists( $post_id, $metakey ) ){
            update_post_meta( $post_id , $metakey , $value );
        }
        else{
            add_post_meta( $post_id , $metakey , $value, true );
            update_post_meta( $post_id , $metakey , $value );
        }
    }

    static function delete( $post_id, $metakey )
    {
        delete_post_meta( $post_id, $metakey );
    }
}

}
?>