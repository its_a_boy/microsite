<?php

	/**
	 *	Appearance / Customize  - args config settings
	 */

	$ids = array();

	$categories = get_categories();

	foreach( $categories as $i => $cat ){
		$ids[] = $cat -> term_id;
	}

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize-js-args' ), array(
	 	'categories' => $ids
	));

	tempo_cfgs::set( 'customize-js-args', $cfgs );