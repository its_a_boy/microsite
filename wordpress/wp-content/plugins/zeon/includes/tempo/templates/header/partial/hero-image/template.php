<?php global $post; ?>


<!-- POST TITLE -->
<div <?php echo tempo_full_class(); ?>>

    <h1 class="tempo-headline"><?php echo get_the_title( $post ); ?></h1>

    <hr/>
</div>


<!-- NAVIGATION -->
<div <?php echo tempo_large_class(); ?>>

    <nav class="tempo-navigation">
        <ul class="tempo-menu-list">

            <?php echo tempo_breadcrumbs::home(); ?>

            <?php echo tempo_breadcrumbs::pages( $post ); ?>

        </ul>
    </nav>

</div>


<!-- COUNTER -->
<div <?php echo tempo_small_class( 'details' ); ?>>

	<?php echo tempo_breadcrumbs::count( null ); ?>

</div>