<?php

	/**
	 *	Appearance / Customize / Header Settings - config settings
	 */

	$height 		= ( $h = absint( tempo_options::val( 'header-height' ) ) ) && !empty( $h ) ? $h : 750;

	$height_70 		= absint( $height * 0.7 );
	$height_30 		= absint( $height * 0.3 );

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'tempo-header' => array(
			'sections'		=> array(
				'tempo-header-appearance' => array(
					'fields'		=> array(
						'header-height' => array(
							'title'			=> __( 'Header height', 'cronus' ),
							'priority'		=> 5,
							'callback'		=> function(){
								return !cronus_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 300,
	                        	'max'       	=> 900,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> 750
							)
						),
						'header-mask-color' => array(
							'input'			=> array(
	                        	'default' 		=> '#1c2633'
							)
						),
						'header-mask-transp' => array(
							'input'			=> array(
	                        	'default' 		=> 60
							)
						),
						'header-horizontal-align' => array(
							'title'			=> __( 'Horizontal Align ', 'cronus' ),
							'description'	=> __( 'align horizontal the header elements', 'cronus' ),
							'priority'		=> 25,
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'center',
								'options'		=> array(
									'left'			=> __( 'Left', 'cronus' ),
									'center'		=> __( 'Center', 'cronus' ),
									'right'			=> __( 'Right', 'cronus' )
								)
							)
						),
						'header-text-space' => array(
							'title'			=> __( 'Text Space', 'cronus' ),
							'priority'		=> 26,
							'callback'		=> function(){
								return cronus_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 250,
	                        	'max'       	=> 900,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> $height_70
							)
						),
						'header-text-vertical-align' => array(
							'title'			=> __( 'Text vertical Align ', 'cronus' ),
							'description'	=> __( 'Vertical align Headline and Description. This options is available only if is enable Headline or Description', 'cronus' ),
							'priority'		=> 27,
							'callback'		=> function(){
								return cronus_mix_height();
							},
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'middle',
								'options'		=> array(
									'top'			=> __( 'Top', 'cronus' ),
									'middle'		=> __( 'Middle', 'cronus' ),
									'bottom'		=> __( 'Bottom', 'cronus' )
								)
							)
						),
						'header-btns-space' => array(
							'title'			=> __( 'Buttons Space', 'cronus' ),
							'priority'		=> 28,
							'callback'		=> function(){
								return cronus_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 125,
	                        	'max'       	=> 900,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> $height_30
							)
						),
						'header-btns-vertical-align' => array(
							'title'			=> __( 'Buttons vertical Align ', 'cronus' ),
							'description'	=> __( 'Vertical align the buttons. This options is available only if is enable First Button or Second Button', 'cronus' ),
							'priority'		=> 29,
							'callback'		=> function(){
								return cronus_mix_height();
							},
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'top',
								'options'		=> array(
									'top'			=> __( 'Top', 'cronus' ),
									'middle'		=> __( 'Middle', 'cronus' ),
									'bottom'		=> __( 'Bottom', 'cronus' )
								)
							)
						),
					)
				),
				'tempo-header-btn-1' => array(
					'title' 	=> __( 'First Button', 'cronus' ),
					'priority'	=> 30,
					'fields'	=> array(
						'header-btn-1' => array(
							'title'			=> __( 'Display First Button', 'cronus' ),
							'description'	=> __( 'enable / disable first Button', 'cronus' ),
							'priority'		=> 5,
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> false
							)
						),
						'header-btn-1-text' => array(
							'title'			=> __( 'Text', 'cronus' ),
							'priority'		=> 10,
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'First Button', 'cronus' )
							)
						),
						'header-btn-1-description' => array(
							'title'			=> __( 'Description', 'cronus' ),
							'priority'		=> 15,
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'first button description', 'cronus' )
							)
						),
						'header-btn-1-url' => array(
							'title'			=> __( 'URL', 'cronus' ),
							'transport'		=> 'postMessage',
							'priority'		=> 20,
							'input'			=> array(
								'type'			=> 'url'
							)
						),
						'header-btn-1-target' => array(
							'title'			=> __( 'Open url in new window', 'cronus' ),
							'description'	=> __( 'enable / disable link attribut target="_blank"', 'cronus' ),
							'transport'		=> 'postMessage',
							'priority'		=> 25,
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> true
							)
						),
						'header-btn-1-text-color' => array(
							'title'			=> __( 'Text Color', 'cronus' ),
							'priority'		=> 30,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff'
							)
						),
						'header-btn-1-text-transp' => array(
							'title'			=> __( 'Text Transparency', 'cronus' ),
							'priority'		=> 35,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-1-text-h-color' => array(
							'title'			=> __( 'Text Color ( over )', 'cronus' ),
							'description'	=> __( 'the Text Color when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 40,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff'
							)
						),
						'header-btn-1-text-h-transp' => array(
							'title'			=> __( 'Text Transparency ( over )', 'cronus' ),
							'description'	=> __( 'the Text Transparency when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 45,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-1-bkg-color' => array(
							'title'			=> __( 'Background Color', 'cronus' ),
							'priority'		=> 50,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#9e7dd3'
							)
						),
						'header-btn-1-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'cronus' ),
							'priority'		=> 55,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-1-bkg-h-color' => array(
							'title'			=> __( 'Background Color ( over )', 'cronus' ),
							'description'	=> __( 'the Background Color when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 60,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#9476c4'
							)
						),
						'header-btn-1-bkg-h-transp' => array(
							'title'			=> __( 'Background Transparency ( over )', 'cronus' ),
							'description'	=> __( 'the Background Transparency when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 65,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						)
					)
				),
				'tempo-header-btn-2' => array(
					'title' 	=> __( 'Second Button', 'cronus' ),
					'priority'	=> 35,
					'fields'	=> array(
						'header-btn-2' => array(
							'title'			=> __( 'Display Second Button', 'cronus' ),
							'description'	=> __( 'enable / disable second Button', 'cronus' ),
							'priority'		=> 5,
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> false
							)
						),
						'header-btn-2-text' => array(
							'title'			=> __( 'Text', 'cronus' ),
							'priority'		=> 10,
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'Second Button', 'cronus' )
							)
						),
						'header-btn-2-description' => array(
							'title'			=> __( 'Description', 'cronus' ),
							'priority'		=> 15,
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'second button description', 'cronus' )
							)
						),
						'header-btn-2-url' => array(
							'title'			=> __( 'URL', 'cronus' ),
							'priority'		=> 20,
							'input'			=> array(
								'type'			=> 'url'
							)
						),
						'header-btn-2-target' => array(
							'title'			=> __( 'Open url in new window', 'cronus' ),
							'description'	=> __( 'enable / disable link attribut target="_blank"', 'cronus' ),
							'priority'		=> 25,
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> true
							)
						),
						'header-btn-2-text-color' => array(
							'title'			=> __( 'Text Color', 'cronus' ),
							'priority'		=> 30,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff'
							)
						),
						'header-btn-2-text-transp' => array(
							'title'			=> __( 'Text Transparency', 'cronus' ),
							'priority'		=> 35,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-2-text-h-color' => array(
							'title'			=> __( 'Text Color ( over )', 'cronus' ),
							'description'	=> __( 'the Text Color when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 40,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff'
							)
						),
						'header-btn-2-text-h-transp' => array(
							'title'			=> __( 'Text Transparency ( over )', 'cronus' ),
							'description'	=> __( 'the Text Transparency when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 45,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-2-bkg-color' => array(
							'title'			=> __( 'Background Color', 'cronus' ),
							'priority'		=> 50,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#36d678'
							)
						),
						'header-btn-2-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'cronus' ),
							'priority'		=> 55,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-btn-2-bkg-h-color' => array(
							'title'			=> __( 'Background Color ( over )', 'cronus' ),
							'description'	=> __( 'the Background Color when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 60,
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#32c770'
							)
						),
						'header-btn-2-bkg-h-transp' => array(
							'title'			=> __( 'Background Transparency ( over )', 'cronus' ),
							'description'	=> __( 'the Background Transparency when the mouse cursor is over the button', 'cronus' ),
							'priority'		=> 65,
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						)
					)
				),
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>
