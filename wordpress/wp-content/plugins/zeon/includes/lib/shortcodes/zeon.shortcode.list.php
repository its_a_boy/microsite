<?php

    /*
        [list type="ordered|unordered" style="squared|circled|rounded" marker_size="small|normal|large" mrker_icon="[get from fontello]" marker_index="1" marker_color="#39b54a" marker_bkg="#f7f7f7"]

            [list_item] ... [/list_item]
            [list_item] ... [/list_item]
            [list_item] ... [/list_item]
            [list_item] ... [/list_item]
            [list_item] ... [/list_item]

        [/list]
    */

    class zeon_shortcode_list
    {
        private static $run = false;
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( !self::$run ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'type'          => 'unordered',
                    'style'         => 'circled',
                    'marker_size'   => 'normal',
                    'marker_icon'   => 'ok',
                    'marker_index'  => 1,
                    'marker_color'  => '#39b54a',
                    'marker_bkg'    => '#f7f7f7'
                ), $_attr );

                /* STYLES AND PROPERTIES */
                $prop   = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $type = esc_attr( trim( $attr[ 'type' ] ) );

                zeon_shortcode_list_item::set_type( $type );
                zeon_shortcode_list_item::set_icon( $attr[ 'marker_icon' ] );
                zeon_shortcode_list_item::set_index( absint( $attr[ 'marker_index' ] ) );
                zeon_shortcode_list_item::set_marker_color( esc_attr( $attr[ 'marker_color' ] ) );
                zeon_shortcode_list_item::set_marker_bkg( esc_attr( $attr[ 'marker_bkg' ] ) );


                /* THE CONTENT */
                $rett .= '<div class="tempo-shortcode list ' . esc_attr( $attr[ 'type' ] ) . ' ' . esc_attr( $attr[ 'style' ] ) . ' ' . esc_attr( $attr[ 'marker_size' ] ) . '" ' . $style . '>';
                $rett .= $type == 'ordered' ? '<ol>' : '<ul>';

                $rett .= do_shortcode( strip_tags( $_content , '<p><span><em><b><stroke><i><u><ul><li><ol><address><br><hr><cite><a><abbr><acronym>' ) );

                $rett .= $type == 'ordered' ? '</ol>' : '</ul>';
                $rett .= '</div>';

                self::$run = false;
            }

            return $rett;
        }

        public $code;
        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Type', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'list-type',
                        'theme_mod' => false,
                        'default'   => 'ordered',
                        'options'   => array(
                            'ordered'   => __( 'Ordered' , 'zeon' ),
                            'unordered' => __( 'Unordered' , 'zeon' )
                        ),
                        'actions'    => array(
                            'hide'      => array( '.zeon-list-ordered', '.zeon-list-unordered' ),
                            'ordered'   => array( '.zeon-list-ordered' ),
                            'unordered' => array( '.zeon-list-unordered' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Style', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'list-style',
                        'theme_mod' => false,
                        'default'   => 'rounded',
                        'options'   => array(
                            'squared'   => __( 'Squared' , 'zeon' ),
                            'circled'   => __( 'Circled' , 'zeon' ),
                            'rounded'   => __( 'Rounded' , 'zeon' )
                        )
                    )
                )
            );

            $fields[ 'marker' ] = array(
                array(
                    'title'     => __( 'Size', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'list-marker-size',
                        'theme_mod' => false,
                        'default'   => 'normal',
                        'options'   => array(
                            'small'     => __( 'Small' , 'zeon' ),
                            'normal'    => __( 'Normal' , 'zeon' ),
                            'large'     => __( 'Large' , 'zeon' )
                        )
                    )
                ),
                array(
                    'class'     => 'zeon-list-unordered hidden',
                    'title'     => __( 'Icon', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'icon_select',
                        'name'      => 'list-marker-icon',
                        'theme_mod' => false,
                        'default'   => 'tempo-icon-check-2',
                        'options'   => tempo_cfgs::get( 'icons' )
                    )
                ),
                array(
                    'class'     => 'zeon-list-ordered',
                    'title'     => __( 'Index', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'list-marker-index',
                        'theme_mod' => false,
                        'default'   => 1
                    )
                ),
                array(
                    'title'     => __( 'Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'list-marker-color',
                        'theme_mod' => false,
                        'default'   => '#333333'
                    )
                ),
                array(
                    'title'     => __( 'Background Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'list-marker-bg-color',
                        'theme_mod' => false,
                        'default'   => '#f7f7f7'
                    )
                )
            );


            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'List Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'title'     => __( 'List Marker Settings', 'zeon' ),
                    'fields'    => $fields[ 'marker' ]
                )
            );

            $fields[ 'content' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'list-items',
                        'theme_mod' => false,
                        'default'   =>  '[zeon_list_item] ... [/zeon_list_item]' . "\n" .
                                        '[zeon_list_item] ... [/zeon_list_item]' . "\n" .
                                        '[zeon_list_item] ... [/zeon_list_item]' . "\n" .
                                        '[zeon_list_item] ... [/zeon_list_item]' . "\n" .
                                        '[zeon_list_item] ... [/zeon_list_item]' . "\n" .
                                        '...'
                    )
                )
            );

            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'List Items', 'zeon' ),
                    'fields'    => $fields[ 'content' ]
                )
            );


            $type           = '<span class="list-type attr">type="<span class="value">ordered</span>"</span>';
            $style          = '<span class="list-style attr">style="<span class="value">rounded</span>"</span>';
            $marker_size    = '<span class="list-marker-size attr">marker_size="<span class="value">normal</span>"</span>';
            $marker_icon    = '<span class="list-marker-icon zeon-list-unordered hidden attr">marker_icon="<span class="value">check-2</span>"</span>';
            $marker_index   = '<span class="list-marker-index zeon-list-ordered attr">marker_index="<span class="value">1</span>"</span>';
            $marker_color   = '<span class="list-marker-color attr">marker_color="<span class="value">#333333</span>"</span>';
            $marker_bkg     = '<span class="list-marker-bg-color attr">marker_bg_color="<span class="value">#f7f7f7</span>"</span>';

            $this -> code   =   '[zeon_list ' . $type . ' ' . $style . ' ' . $marker_size . ' ' . $marker_icon . ' ' . $marker_index . ' ' . $marker_color . ' ' . $marker_bkg . ']' . '<br/>' . "\n" .
                                '<span class="list-items text"><span class="value">' .

                                '[zeon_list_item] ... [/zeon_list_item]' . '<br/>' . "\n" .
                                '[zeon_list_item] ... [/zeon_list_item]' . '<br/>' . "\n" .
                                '[zeon_list_item] ... [/zeon_list_item]' . '<br/>' . "\n" .
                                '[zeon_list_item] ... [/zeon_list_item]' . '<br/>' . "\n" .
                                '[zeon_list_item] ... [/zeon_list_item]' . '<br/>' . "\n" .
                                '...' .

                                '</span></span>' . '<br/>' . "\n" .
                                '[/zeon_list]';


            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        static function is_run()
        {
            return self::$run;
        }
    }

    class zeon_shortcode_list_item
    {
        private static $run             = false;
        private static $type            = 'unordered';
        private static $icon            = 'ok';
        private static $index           = 1;
        private static $marker_color    = 1;
        private static $marker_bkg      = 1;

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( !self::$run && zeon_shortcode_list::is_run() ){

                self::$run = true;

                /* STYLES AND PROPERTIES */
                $prop   = esc_attr( zeon_shortcode_prop( $_attr ) );
                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $marker_style = 'style="background-color: ' . self::$marker_bkg . '; color: ' . self::$marker_color . '"';

                $marker = '<i class="tempo-icon-' . esc_attr( self::$icon ) . '" ' . $marker_style . '></i>';

                if( self::$type == 'ordered' ){
                    $marker = '<i class="marker-index" ' . $marker_style . '>' . absint( self::$index ) . '</i>';
                }

                $rett .= '<li ' . $style . '>';
                $rett .= $marker . '<div>' . $_content . '</div>';
                $rett .= '</li>';

                self::$index++;
                self::$run = false;
            }

            return $rett;
        }

        static function set_type( $type )
        {
            self::$type = $type;
        }

        static function set_icon( $icon )
        {
            self::$icon = $icon;
        }

        static function set_index( $index )
        {
            self::$index = $index;
        }

        static function set_marker_color( $marker_color )
        {
            self::$marker_color = $marker_color;
        }

        static function set_marker_bkg( $marker_bkg )
        {
            self::$marker_bkg = $marker_bkg;
        }
    }
?>
