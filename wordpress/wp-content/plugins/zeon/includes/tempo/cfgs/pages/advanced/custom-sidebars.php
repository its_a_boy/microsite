<?php

	/**
	 *	Zeon / Custom Sidebars - config settings
	 */

	$settings = tempo_cfgs::merge( (array)tempo_cfgs::get( 'settings' ), array(
		'advanced' => array(
			'zeon-custom-sidebars' => array(
			    'menu' => array(
			        'title'     => __( 'Custom Sidebars', 'zeon' )
			    ),
			    'update'	=> false,
			    'priority'	=> 5,
			    'sections' 	=> array(
			    	array(
			    		'columns' 		=> array(

			    			// COLUMNS
							array(
								'layout' 			=> array(
									'sm'	=> 12,
									'md'	=> 6,
									'lg'	=> 6,
								),
								'template'			=> 'templates/admin/zeon/add-new-sidebar'
							),
							array(
								'layout' 			=> array(
									'sm'	=> 12,
									'md'	=> 6,
									'lg'	=> 6,
								),
								'template'			=> 'templates/admin/zeon/sidebars-list'
							)
			    		)
			    	)
				)
			)
		)
	));

	tempo_cfgs::set( 'settings', $settings );
?>
