<?php

    /**
     *  [alert type=""] ... [/alert]
     */

    class zeon_shortcode_alert
    {
        private static $run     = false;
        private static $icons   = array(
            'message'   => 'tempo-icon-mail-alt',
            'notice'    => 'tempo-icon-info-circled-1',
            'warning'   => 'tempo-icon-warning',
            'success'   => 'tempo-icon-flag-1',
            'help'      => 'tempo-icon-lifebuoy',
            'info'      => 'tempo-icon-lightbulb-1'
        );

        static function run( $_attr, $_content = null ){
            $rett = '';

            self::$run = true;

            $attr = shortcode_atts( array(
                'type' => null
            ), $_attr );

            // custom style
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            if( !empty( $attr[ 'bg_color' ] ) ){
                $prop .= 'border-color: ' . $attr[ 'color' ] . '; ';
            }

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $icon = isset( self::$icons[ $attr[ 'type' ]  ] ) ? self::$icons[ $attr[ 'type' ]  ] : self::$icons[ 'message'  ];

            $rett .= '<div class="tempo-shortcode alert ' . esc_attr( $attr[ 'type' ] ) . '" ' . $style . '>';
            $rett .= '<i class="' . esc_attr( $icon ) . '"></i>' . strip_tags( $_content , '<cite><address><br><a><u><em><i><b><strong>' );
            $rett .= '</div>';

            self::$run = false;

            return $rett;
        }

        public $code;

        public function builder()
        {
            $fields = array();
            $boxes  = array();


            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Type', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'alert-type',
                        'theme_mod' => false,
                        'default'   => 'general',
                        'options'   => array(
                            'message'   => __( 'Message', 'zeon' ),
                            'notice'    => __( 'Notice', 'zeon' ),
                            'warning'   => __( 'Warning', 'zeon' ),
                            'success'   => __( 'Success', 'zeon' ),
                            'help'      => __( 'Help', 'zeon' ),
                            'info'      => __( 'Info', 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'alert-top',
                        'theme_mod' => false,
                        'default'   => 15
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'alert-bottom',
                        'theme_mod' => false,
                        'default'   => 15
                    )
                )
            );

            $boxes[ 'left' ][] = array(
                'title'     => __( 'Alert Settings' , 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );

            $fields[ 'content' ] = array(
                array(
                    'format'        => 'across',
                    'input'         => array(
                        'type'      => 'textarea',
                        'name'      => 'alert-content',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $boxes[ 'right' ][] = array(
                'title'         => __( 'Alert Content', 'zeon' ),
                'fields'        => $fields[ 'content' ]
            );

            $content            = '<span class="alert-content text"><span class="value">...</span></span>';
            $type               = '<span class="alert-type attr">type="<span class="value">general</span>"</span>';
            $top                = '<span class="alert-top attr">top="<span class="value">15</span>"</span>';
            $bottom             = '<span class="alert-bottom attr">bottom="<span class="value">15</span>"</span>';

            $this -> code       = '[zeon_alert ' . $type . ' ' . $top . ' ' . $bottom . ']' . $content . '[/zeon_alert]';

            return array(
                array(
                    'boxes' => $boxes['left']
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }
    }

?>
