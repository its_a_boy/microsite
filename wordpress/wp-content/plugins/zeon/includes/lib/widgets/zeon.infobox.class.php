<?php

	/**
     *  Infobox with Icon and Link
     */

	if( !class_exists( 'zeon_widget_infobox' ) ){

		class zeon_widget_infobox extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_infobox', __( 'Infobox', 'zeon' ) . ' [zeon]', array( 
		            'classname'     => 'zeon_widget_infobox',
		            'description'   => __( 'The Infobox widget with Title, Icon, Description and Link', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		    	// extract args
		        extract( $args , EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		        	'icon'     	=> null,
		            'title'     => null,
		            'content'   => null,
		            'link-text'	=> null,
		            'link-url'	=> null
		        ));

		       	$icon    	= esc_attr( $instance[ 'icon' ] );
		        $title    	= sanitize_text_field( $instance[ 'title' ] );
		        $content  	= strip_shortcodes( strip_tags( $instance[ 'content' ], '<p><br><span><a><small><i><b>' ) );
		        $link_text  = sanitize_text_field( $instance[ 'link-text' ] );
		        $link_url 	= esc_url( $instance[ 'link-url' ] );

		        $s 			= $icon . $title . $content . $link_text . $link_url;

		        if( !empty( $s ) ){

		            echo $before_widget;

		            if( !empty( $title ) ){
		                echo $before_title;
		                echo '<i class="' . esc_attr( $icon ) . '"></i> ' . apply_filters( 'widget_title', $title, $instance, $this -> id_base );
		                echo $after_title;
		            }

		            if( !empty( $content ) ){
		            	echo '<div class="content">' . $content . '</div>';
		            }

		            $link = $link_text . $link_url;

		            if( !empty( $link ) ){
		            	echo '<a class="read-more" href="' . esc_url( $link_url ) . '">' . sanitize_text_field( $link_text ) . '</a>';
		            }

		            echo $after_widget;
		        }
		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               	= $old_instance;
		        $instance[ 'icon' ]    		= esc_attr( $new_instance[ 'icon' ] );
		        $instance[ 'title' ]    	= sanitize_text_field( $new_instance[ 'title' ] );
		        $instance[ 'content' ]  	= strip_shortcodes( strip_tags( $new_instance[ 'content' ], '<p><br><span><a><small><i><b>' ) );
		        $instance[ 'link-text' ]  	= sanitize_text_field( $new_instance[ 'link-text' ] );
		        $instance[ 'link-url' ]  	= esc_url( $new_instance[ 'link-url' ] );

		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		        $instance = wp_parse_args( (array) $instance, array(
		        	'icon'     	=> null,
		            'title'     => null,
		            'content'   => null,
		            'link-text'	=> __( 'Read More &rarr;', 'zeon' ),
		            'link-url'	=> null
		        ));

		       	$icon    	= esc_attr( $instance[ 'icon' ] );
		        $title    	= sanitize_text_field( $instance[ 'title' ] );
		        $content  	= strip_shortcodes( strip_tags( $instance[ 'content' ], '<p><br><span><a><small><i><b>' ) );
		        $link_text  = sanitize_text_field( $instance[ 'link-text' ] );
		        $link_url 	= esc_url( $instance[ 'link-url' ] );

		        ?>
		        	<script type="text/javascript">
		        		if( typeof tempo_input_icon_select === 'undefined' ){
		        			var tempo_input_icon_select = function( input ){
								jQuery(document).ready(function(){

									var self 	= jQuery( input );
									var parent  = jQuery( input ).parent();

									// open panel
									var panel = jQuery( parent ).find( '.tempo-icon-select-options' );

									if( !panel.hasClass( 'enabled' ) ){
										jQuery( 'body' ).css({ 'overflow' : 'hidden' });
										panel.addClass( 'enabled' );
									}

									// init close action
									jQuery( panel ).find( 'span.tempo-icon-options-cancel a' ).click(function(){
										if( panel.hasClass( 'enabled' ) ){
											jQuery( 'body' ).css({ 'overflow' : 'initial' });
											panel.removeClass( 'enabled' );
										}
									});

									// init search action
									jQuery( panel ).find( 'input[type="text"].tempo-icon-search-input').on( 'focusin' , function(){
							            jQuery( this ).on( 'keyup' , function(){
							                jQuery( panel ).find( '.tempo-icon-select-options-content i[class^="tempo-icon-"]' ).parent('div.tempo-icon-select-option').hide();

							                var value = jQuery( this ).val();
							                jQuery( panel ).find( '.tempo-icon-select-options-content i[class^="tempo-icon-' + value + '"]' ).parent('div.tempo-icon-select-option').show();
							            });
							        });

							        // init select item
							        jQuery( parent ).find( '.tempo-icon-select-option' ).click(function(){
										var value = jQuery( this ).find( 'i' ).attr( 'data-value' );

										jQuery( parent ).find( '.tempo-icon-select-option' ).removeClass( 'selected' );
										jQuery( this ).addClass( 'selected' );
										jQuery( parent ).find( '.tempo-icon-select-value i' ).attr( 'class' , value );
										jQuery( parent ).find( '.tempo-icon-select-value i' ).attr( 'data-value' , value );
										jQuery( parent ).find( 'input[type="hidden"]' ).val( value );
										jQuery( parent ).find( 'input[type="text"].tempo-icon-search-input' ).val( value.replace( "tempo-icon-" , '' , "gi" ) );

										var show 	= [];
										var hide    = [];

										if( jQuery( this ).find( 'i' ).hasAttr( 'data-action' ) ){
											show = jQuery( this ).find('i').attr( 'data-action' ).toString().split(',');
										}

										jQuery( parent ).find( '.tempo-icon-select-option' ).each(function(){

											if( jQuery( this ).find( 'i' ).hasAttr( 'data-action' ) ){
												var array = jQuery( this ).find('i').attr( 'data-action' ).toString().split(',');

												for( var i = 0; i < array.length; i++ ){
													if( !tempo_ahtml.in_array( array[ i ] , show ) ){
														hide[ hide.length ] = array[ i ];
													}
												}
											}
										});

										if( hide.length ){
											jQuery( hide.toString() ).hide( 'slow' );
										}

										if( show.length ){
											jQuery( show.toString() ).show( 'slow' );
										}

										var panel = jQuery( parent ).find( '.tempo-icon-select-options' );

										if( panel.hasClass( 'enabled' ) ){
											jQuery( 'body' ).css({ 'overflow' : 'initial' });
											panel.removeClass( 'enabled' );
										}
									});
								});
							}
		        		}
		        	</script>
		        <?php

		        echo tempo_html::field(array(
		        	'title'     => __( 'Icon', 'zeon' ),
		        	'id'		=> $this -> get_field_id( 'icon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'icon_select',
                        'name'      => $this -> get_field_name( 'icon' ),
                        'default'   => null,
                        'value'		=> $icon,
                        'theme_mod' => false,
                        'options'   => tempo_cfgs::get( 'icons' )
                    )
		        ));

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'content' ) . '">' . __( 'Content', 'zeon' );
		        echo '<textarea class="widefat" id="' . $this -> get_field_id( 'content' ) . '" rows="8" name="' . $this -> get_field_name( 'content' ) . '">' . $content . '</textarea>';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'link-text' ) . '">' . __( 'Link Text', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'link-text' ) . '" name="' . $this -> get_field_name( 'link-text' ) . '" value="' . sanitize_text_field( $link_text ) . '" />';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'link-url' ) . '">' . __( 'Link URL', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'link-url' ) . '" name="' . $this -> get_field_name( 'link-url' ) . '" value="' . esc_url( $link_url ) . '" />';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
