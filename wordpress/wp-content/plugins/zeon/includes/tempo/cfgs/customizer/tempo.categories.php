<?php

	/**
	 *	Appearance / Customize / Categories - config settings
	 */

	$fields  	= array();
	$categories = get_categories();

	foreach( $categories as $i => $cat ){
		$fields[ 'category-' . $cat -> term_id ] = array(
			'title'			=> $cat -> name,
			'description'	=> __( 'set color for this category' , 'zeon' ),
			'input'			=> array(
				'type'			=> 'color',
				'default'		=> '#2f3238'
			)
		);
	}

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
	 	'tempo-categories' => array(
	 		'title'		=> __( 'Categories', 'zeon' ),
	 		'priority' 	=> 55,
			'fields'	=> $fields
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );