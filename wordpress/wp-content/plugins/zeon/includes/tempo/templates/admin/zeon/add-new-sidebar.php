<form method="post" action="?page=tempo-custom-sidebars&amp;add-new-sidebar=true">

	<?php
		$field = tempo_html::field(array(
			'title' 	=> __( 'Sidebar Name', 'zeon' ),
			'format'    => 'across',
			'input'		=> array(
				'type'		=> 'text',
				'name'		=> 'sidebar-name',
				'theme_mod' => false,
			)
		));

		$button = '<button type="submit" class="tempo-no-margin tempo-button ">' . __( 'Add the Sidebar', 'zeon' ) . '</button>';

		echo tempo_html::box(array(
			'title'		=> __( 'Add new Sidebar', 'zeon' ),
			'content'	=> $field . $button
		));
	?>

</form>