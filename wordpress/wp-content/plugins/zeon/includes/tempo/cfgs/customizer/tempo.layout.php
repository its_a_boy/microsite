<?php
    
    /**
     *  Appearance / Customize / Layout - config settings
     */

    $default = array(
        'main'          => __( 'Main Sidebar' , 'zeon' ),
        'front-page'    => __( 'Front Page Sidebar' , 'zeon' ),
        'page'          => __( 'Page Sidebar' , 'zeon' ),
        'post'          => __( 'Single Post Sidebar' , 'zeon' )
    );

    $sidebars   = $default;
    $custom     = tempo_validator::get_json( tempo_options::val( 'custom-sidebars' ) );

    if( !empty( $custom ) && is_array( $custom ) )
        $sidebars = array_merge( $default,  $custom );
    

    $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
    	'tempo-layout' => array(
    		'title'		=> __( 'Layout' , 'zeon' ),
    		'priority' 	=> 47,
    		'sections'	=> array(
                'tempo-layout-general' => array(
                    'title'             => __( 'General' , 'zeon' ),
                    'description'       => sprintf( __( '%s: On the premium version, the content width for layout with sidebars is 945 pixels.' , 'zeon' ), '<b>' . __( 'IMPORANT', 'zeon' ) . '</b>' ),
                    'fields'            => array(
                        'content-width' => array(
                            'title'         => __( 'The content max width', 'zeon' ),
                            'description'   => sprintf( __( 'Set the website content max width. This option is available only for the layouts %s.', 'zeon' ), '<b>' . __( 'without sidebars', 'zeon' ) . '</b>' ),
                            'priority'      => 1,
                            'input'     => array(
                                'type'      => 'select',
                                'default'   => 8,
                                'options'   => array(
                                    8   => __( '750 px', 'zeon' ),
                                    10  => __( '945 px', 'zeon' )
                                )
                            )
                        )
                    )
                ),
    			'tempo-layout' => array(
    				'title'             => __( 'Blog & Archives' , 'zeon' ),
                	'description'       => __( 'Default Layout is used for the next templates: Blog, Archives, Categories, Tags, Author and Search Results.' , 'zeon' ),
                	'fields'			=> array(
                		'layout'	=> array(
                			'title'             => __( 'Layout' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'full',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'zeon' ),
                    				'full'  => __( 'Full Width', 'zeon' ),
                    				'right' => __( 'Right Sidebar', 'zeon' )
                				)
                			)
                		),
                		'sidebar'	=> array(
                			'title'             => __( 'Sidebar' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'main',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
    			'tempo-front-page-layout' => array(
    				'title'             => __( 'Front Page' , 'zeon' ),
                	'description'       => __( 'In order to use this option set you need to activate a staic page on Front Page from - "Static Front Page" tab.' , 'zeon' ),
                	//'callback' 			=> 'is_front_page',
                	'fields'			=> array(
                		'front-page-layout' => array(
                			'title'             => __( 'Layout' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'full',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'zeon' ),
                    				'full'  => __( 'Full Width', 'zeon' ),
                    				'right' => __( 'Right Sidebar', 'zeon' )
                				)
                			)
                		),
                		'front-page-sidebar' => array(
                			'title'             => __( 'Sidebar' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'front-page',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
    			'tempo-page-layout' => array(
    				'title'             => __( 'Page' , 'zeon' ),
                	'description'       => __( 'for the each page you can overwrite the Layout options with the custom settings ( on edit page meta box "Layout" ).' , 'zeon' ),
                	'fields'			=> array(
                		'page-layout' => array(
                			'title'             => __( 'Layout' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'full',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'zeon' ),
                    				'full'  => __( 'Full Width', 'zeon' ),
                    				'right' => __( 'Right Sidebar', 'zeon' )
                				)
                			)
                		),
                		'page-sidebar' => array(
                			'title'             => __( 'Sidebar' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'page',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
    			'tempo-post-layout' => array(
    				'title'             => __( 'Post' , 'zeon' ),
                	'description'       => __( 'for the each post you can overwrite the Layout options with the custom settings ( on edit page meta box "Layout" ).' , 'zeon' ),
                	'fields'			=> array(
                		'post-layout' => array(
                			'title'             => __( 'Layout' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'full',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'zeon' ),
                    				'full'  => __( 'Full Width', 'zeon' ),
                    				'right' => __( 'Right Sidebar', 'zeon' )
                				)
                			)
                		),
                		'post-sidebar' => array(
                			'title'             => __( 'Sidebar' , 'zeon' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'post',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			)
    		)
    	)
    ));

    tempo_cfgs::set( 'customize', $cfgs );
?>