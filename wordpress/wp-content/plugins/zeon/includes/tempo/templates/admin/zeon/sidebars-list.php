<?php

	/**
	 *	Sidebars Settings
	 */

	$sidebars = tempo_options::get( 'custom-sidebars' );


	/**
	 *	Not found Custom Sidebars
	 */

	$content = tempo_html::notification(array(
		'type'			=> 'notify',
		'description'	=> __( 'Not found custom Sidebars !', 'zeon' )
	));

	if( !empty( $sidebars ) ){

		/**
		 *	Reset Content
		 */

		$content = '';

		foreach( $sidebars as $id => $sidebar ){
			$url = '?page=tempo-custom-sidebars&amp;sidebar=' . esc_attr( $id ) . '&amp;remove-sidebar=true';

	        $content .= '<div class="tempo-notification info tempo-custom-sidebar">';
	        $content .= esc_attr( $sidebar );
	        $content .= '<a href="javascript:(function(){ if(confirm( \'' . __( 'Confirm if you want to delete the sidebar!' , 'zeon' ) . '\' )){ document.location.href=\'' . $url . '\'; } })()" class="tempo-remove-sidebar"><i class="tempo-icon-cancel-circled-2"></i></a>';
	        $content .= '</div>';
		}
	}


	/**
	 *	Content Box 
	 */

	echo tempo_html::box(array(
		'title'		=> __( 'List of Custom Sidebars', 'zeon' ),
		'content'	=> $content
	));
?>