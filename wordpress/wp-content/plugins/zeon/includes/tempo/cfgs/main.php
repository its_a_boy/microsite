<?php

	/**
	 *	Theme Configs
	 */

	//include_once zeon_plugin_dir() . '/includes/tempo/cfgs/appearance/faq.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/site-identity.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.additional.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.blog.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.categories.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.comments.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.fonts.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.header.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.js-args.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.layout.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.others.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.post.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/customizer/tempo.sidebars.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/menus/footer.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/meta-boxes/cfgs.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/pages/advanced/custom-sidebars.php';
	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/pages/advanced/sidebars.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/posts/cfgs.php';

	include_once zeon_plugin_dir() . '/includes/tempo/cfgs/sidebars/cfgs.php';

	/**
	 *	Theme Config
	 */

	$cfgs = (array)tempo_cfgs::get( 'theme' );

	if( isset( $cfgs[ 'premium_url' ] ) )
		unset( $cfgs[ 'premium_url' ] );

	tempo_cfgs::set( 'theme', $cfgs );

	/**
	 *	General Options
	 *	This settings are used for validator
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'options' ), array(
		'custom-sidebars' => array(
			'input'	=> array(
				'validator'	=> 'json'
			)
		)
	));

	tempo_cfgs::set( 'options', $cfgs );


	/**
	 *	Images Size
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'images-size' ), array(
		'tempo-portfolio' => array(
			'width' 	=> 767,
			'height'	=> 859,
			'crop' 		=> true
		)
	));

	tempo_cfgs::set( 'images-size', $cfgs );
?>
