<?php global $post; ?>

<div <?php echo tempo_full_class(); ?>>

    <!-- POST TITLE -->
    <h1 class="tempo-headline"><?php echo get_the_title( $post ); ?></h1>

    <hr/>


    <!-- NAVIGATION -->
    <nav class="tempo-navigation">
        <ul class="tempo-menu-list">

            <?php echo tempo_breadcrumbs::home(); ?>

            <?php echo tempo_breadcrumbs::pages( $post ); ?>

        </ul>
    </nav>

</div>