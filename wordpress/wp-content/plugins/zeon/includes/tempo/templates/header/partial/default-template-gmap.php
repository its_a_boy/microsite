<?php
	global $post;

    $height 		= zeon_meta::get( $post -> ID, 'tempo-gmap-height', 380 );
    $zoom 			= zeon_meta::get( $post -> ID, 'tempo-gmap-zoom', 4 );
    $lat 			= zeon_meta::get( $post -> ID, 'tempo-gmap-lat', '47.772594932483' );
    $lng 			= zeon_meta::get( $post -> ID, 'tempo-gmap-lng', '1.8596535022735' );

    $title 			= zeon_meta::g( $post -> ID, 'tempo-gmap-marker-title' );
    $description 	= zeon_meta::g( $post -> ID, 'tempo-gmap-marker-description' );

?>

	<div class="tempo-header-partial tempo-google-map">

        <?php
            echo do_shortcode(
            	'[zeon_gmap height="' . absint( $height ) . '" zoom="' . absint( $zoom 	) . '" lat="' . esc_attr( $lat ) . '" lng="' . esc_attr( $lng ) . '"]' .
				'[zeon_marker title="' . sanitize_text_field( $title ) . '"]' . esc_textarea( $description ) . '[/zeon_marker]' .
				'[/zeon_gmap]'
            )
        ?>

	</div>
