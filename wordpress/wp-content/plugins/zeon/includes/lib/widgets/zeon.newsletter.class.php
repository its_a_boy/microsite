<?php

	/**
     *  Newsletter ( Google FeedBurner )
     */

	if( !class_exists( 'zeon_widget_newsletter' ) ){

		class zeon_widget_newsletter extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_newsletter', __( 'Newsletter', 'zeon' ) . ' [zeon]', array(
		            'classname'     => 'zeon_widget_newsletter',
		            'description'   => __( 'The google FeedBurner Newsletter.', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		    	// extract args
		        extract( $args , EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => __( 'Newsletter', 'zeon' ),
		            'ID'    => ''
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $ID     = esc_attr( $instance[ 'ID' ] );

		        echo $before_widget;

		        if( !empty( $title ) ){
		            echo $before_title;
		            echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
		            echo $after_title;
		        }

		        if( !empty( $ID ) ){
		            echo '<span class="description">' . __( 'subscribe with FeedBurner', 'zeon' ) . '</span>';
		            echo '<form class="subscribe" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="javascript:utils.feedburner( \'' . esc_attr( $ID ) . '\' );">';
		            echo '<fieldset>';
		            echo '<input type="email" class="text" name="email" value="' . __( 'E-mail', 'zeon' ) . '" onfocus="if (this.value == \'' . __( 'E-mail', 'zeon' ) . '\') {this.value = \'\';}" onblur="if ( this.value == \'\' ) { this.value = \'' . __( 'E-mail', 'zeon' ) . '\';}"><span class="email"></span>';
		            echo '<input type="hidden" value="' . esc_attr( $ID ) . '" name="uri">';
		            echo '<input type="hidden" name="loc" value="' . str_replace( '-' , '_' , esc_attr( get_bloginfo( 'language' ) ) ) . '">';
		            echo '<button type="submit" value="" class="btn-newsletter"><i class="tempo-icon-right-open-1"></i></button>';
		            echo '</fieldset>';
		            echo '</form>';
		        }

		        else{
		            echo '<p>' . __( 'The FeedBurner ID is invalid !' , 'zeon' ) . '</p>';
		        }

		        echo $after_widget;
		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               = $old_instance;
		        $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );
		        $instance[ 'ID' ]       = esc_attr( strip_tags( $new_instance[ 'ID' ] ) );
		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		        $instance = wp_parse_args( (array) $instance, array(
		            'title' => null,
		            'ID'    => null
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $ID     = esc_attr( $instance[ 'ID' ] );


		        if( empty( $ID ) ){
		            echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "Google FeedBurner ID"', 'zeon' ) . '</p>';
		        }

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'ID' ) . '">' . __( 'Google FeedBurner ID', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'ID' ) . '" name="' . $this -> get_field_name( 'ID' ) . '" value="' . esc_attr( $ID ) . '" />';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
