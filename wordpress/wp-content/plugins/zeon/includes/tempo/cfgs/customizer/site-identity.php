<?php

	/**
	 *	Appearance / Customize / Site Identity - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'title_tagline' => array(
			'title' 		=> __( 'Site Identity', 'zeon' ),
			'priority'      => 10,
			'fields'		=> array(
				'icon-logo' => array(
					'title'			=> __( 'Icon Logo', 'zeon' ),
					'description'	=> __( 'Is recommended to use an icon image with 70px width and 70px height.', 'zeon' ),
					'priority' 		=> 6,
					'input'			=> array(
						'type'			=> 'upload'
					)
				)
			),
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>