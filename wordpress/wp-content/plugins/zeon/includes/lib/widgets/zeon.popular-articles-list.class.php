<?php

    /**
     *  Popular Articles List ( with thumbnail )
     */

    if( !class_exists( 'zeon_widget_popular_articles_list' ) ){

        class zeon_widget_popular_articles_list extends WP_Widget
        {
            /**
             *  Widget Constructor
             */

            function __construct()
            {
                parent::__construct( 'zeon_widget_popular_articles_list', __( 'Popular Articles List', 'zeon' ) . ' [zeon]', array( 
                    'classname'     => 'zeon_widget_popular_articles_list',
                    'description'   => __( 'The list of popular Articles ( with thumbnail ).', 'zeon' )
                ));
            }

            /**
             *  Widget Preview
             */

            function widget( $args, $instance )
            {
                // extract args
                extract( $args , EXTR_SKIP );

                $instance = wp_parse_args( (array) $instance, array(
                    'title'     => null,
                    'number'    => null
                ));

                $number = absint( $instance[ 'number' ] );

                if( $number > 0 ){

                    echo $before_widget;

                    if( !empty( $title ) ){
                        echo $before_title;
                        echo apply_filters( 'widget_title', sanitize_text_field( $instance[ 'title' ] ), $instance, $this -> id_base );
                        echo $after_title;
                    }

                    // latest posts query
                    // to do
                    $wp_query = new WP_Query(array(
                        'posts_per_page' => absint( $number )
                    ));

                    if( count( $wp_query -> posts ) ){

                        echo '<ul class="zeon-articles-list">';

                        foreach( $wp_query -> posts as $p ){

                            echo '<li>';

                            // post thumbnail wrapper
                            echo '<div class="post-thumbnail-wrapper">';

                            $thumbnail      = get_post( get_post_thumbnail_id( $p -> ID ) );
                            $post_thumbnail = has_post_thumbnail( $p -> ID ) && isset( $thumbnail -> ID );

                            // post thumbnail image
                            if( $post_thumbnail ){
                                echo get_the_post_thumbnail( $p -> ID , 'thumbnail' , array(
                                    'alt'   => esc_attr( get_the_title( $p -> ID ) ),
                                    'class' => 'post-thumbnail'
                                ));
                            }

                            // default post thumbnail
                            else{
                                echo '<img alt="' . esc_attr( get_the_title( $p -> ID ) ) . '" class="post-thumbnail" src="' . zeon_plugin_uri() . '/includes/' . zeon_active_pack() . '/media/img/widget-article-list-no-image.png"/>';
                            }

                            // post thumbnail mask + link
                            echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p -> ID ) ) . '"></a>';
                            echo '</div>';

                            // post title
                            echo '<h5>';
                            echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p -> ID ) ) . '">';
                            echo get_the_title( $p -> ID );
                            echo '</a>';
                            echo '</h5>';

                            // post meta / date
                            $y          = esc_attr( get_post_time( 'Y', false, $p -> ID ) );
                            $m          = esc_attr( get_post_time( 'm', false, $p -> ID ) );
                            $d          = esc_attr( get_post_time( 'd', false, $p -> ID ) );
                            $time       = esc_attr( get_post_time( 'Y-m-d', false, $p -> ID  ) );
                            $wp_time    = get_post_time( esc_attr( get_option( 'date_format' ) ), false , $p -> ID, true );

                            echo '<span class="post-meta time">';
                            echo '<i class="tempo-icon-clock-1"></i> ';
                            echo '<a href="' . esc_url( get_day_link( $y , $m , $d ) )  . '" title="' . sprintf( __( 'posted on %s', 'zeon' ), $wp_time ) . '">';
                            echo '<time datetime="' . esc_attr( $time ) . '">' . $wp_time . '</time>';
                            echo '</a>';
                            echo '</span>';

                            echo '<div class="clear clearfix"></div>';
                            echo '</li>';
                        }

                        echo '</ul>';
                    }

                    echo $after_widget;
                }
            }

            /**
             *  Widget Update
             */

            function update( $new_instance, $old_instance )
            {
                $instance               = $old_instance;
                $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );
                $instance[ 'number' ]   = absint( $new_instance[ 'number' ] );

                return $instance;
            }

            /**
             *  Widget Form ( admin side )
             */

            function form( $instance )
            {
                $instance = wp_parse_args( (array) $instance, array(
                    'title'     => '',
                    'number'    => 3
                ));

                $title  = sanitize_text_field( $instance[ 'title' ] );
                $number = absint( $instance[ 'number' ] );

                if( empty( $number ) ){
                    echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "Number of posts"' , 'zeon' ) . '</p>';
                }

                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
                echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
                echo '</label>';
                echo '</p>';

                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'number' ) . '">' . __( 'Number of posts', 'zeon' );
                echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'number' ) . '" name="' . $this -> get_field_name( 'number' ) . '" value="' . absint( $number ) . '" />';
                echo '</label>';
                echo '</p>';
            }
        }
    }
?>
