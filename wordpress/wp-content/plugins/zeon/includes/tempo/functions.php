<?php

	{	/////	REGISTER WIDGETS    /////

		function zeon_register_widgets()
		{
			register_widget( 'zeon_widget_posts' );
			register_widget( 'zeon_widget_posts_list' );
			register_widget( 'zeon_widget_related_posts' );
			register_widget( 'zeon_widget_related_posts_list' );
			register_widget( 'zeon_widget_comments' );
			//register_widget( 'zeon_widget_facebook' );
			//register_widget( 'zeon_widget_twitter' );
			//register_widget( 'zeon_widget_flickr' );
			//register_widget( 'zeon_widget_instagramm' );

			register_widget( 'zeon_widget_infobox' );
			register_widget( 'zeon_widget_newsletter' );
			//

			/**
			 *	Popular Articles Widgets are available
			 *	only with free plugin jetPack
			 */

			//register_widget( 'zeon_widget_popular_posts' );
			//register_widget( 'zeon_widget_popular_posts_list' );

			/**
			 *	Next Widgets can be used only for single template
			 */

			register_widget( 'zeon_widget_categories' );
			register_widget( 'zeon_widget_post_categories' );
			register_widget( 'zeon_widget_post_tags' );
			register_widget( 'zeon_widget_post_meta' );
		}

		add_action( 'widgets_init', 'zeon_register_widgets' );

	}


	{	///// SETUP LAYOUT /////

		/**
		 *	Setup Layout Content Classes
		 */
		function tempo__layout_content_class( $classes, $obj )
		{
			if( $obj -> layout == 'left' )
				$classes = 'col-sm-9 col-md-9 col-lg-8';

			if( $obj -> layout == 'right' )
				$classes = 'col-sm-9 col-md-9 col-lg-8';

			if( $obj -> layout == 'full' )
				$classes = 'col-lg-12';

			return $classes;
		}

		add_filter( 'tempo_layout_content_class', 'tempo__layout_content_class', 10, 2 );

		/**
		 *	Setup Layout Sidebar Classes
		 */
		function tempo__layout_sidebar_class( $classes, $obj )
		{
			if( $obj -> layout == 'left' )
				$classes = 'col-sm-3 col-md-3 col-lg-4';

			if( $obj -> layout == 'right' )
				$classes = 'col-sm-3 col-md-3 col-lg-4';

			if( $obj -> layout == 'full' )
				$classes = 'col-lg-0';

			return $classes;
		}

		add_filter( 'tempo_layout_sidebar_class', 'tempo__layout_sidebar_class', 10, 2 );

		/**
		 *	Read Meta Settings for Singular Layouts
		 */
		function tempo__singular_get_layout( $layout, $post_id )
		{
			if( (bool)zeon_meta::g( absint( $post_id ), 'tempo-overwrite-layout' ) )
				$layout = zeon_meta::g( absint( $post_id ), 'tempo-layout' );

			return $layout;
		}

		add_filter( 'tempo_post_get_layout', 'tempo__singular_get_layout', 10, 2 );
		add_filter( 'tempo_page_get_layout', 'tempo__singular_get_layout', 10, 2 );

		/**
		 *	Read Meta Settings for Singular Sidebar
		 */
		function tempo__singular_get_sidebar( $sidebar, $post_id )
		{
			if( (bool)zeon_meta::g( absint( $post_id ), 'tempo-overwrite-layout' ) )
				$sidebar = zeon_meta::g( absint( $post_id ), 'tempo-sidebar' );

			return $sidebar;
		}

		add_filter( 'tempo_post_get_sidebar', 'tempo__singular_get_sidebar', 10, 2 );
		add_filter( 'tempo_page_get_sidebar', 'tempo__singular_get_sidebar', 10, 2 );

	}


	{	///// SETUP SIDEBARS /////

		// Front Page Header Sidebars

		function tempo__merge_header_front_page_sidebars( $cfgs )
		{
			$classes 		= tempo_cfgs::get( 'sidebars-classes' );
		    $nr 			= tempo_options::get( 'header-front-page-nr-columns' );
		    $description 	= _n( 'Inside of the Sidebar the widgets will be arranged in one column. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars.' , 'Inside of the Sidebar the widgets will be arranged in %s columns. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars',  $nr, 'zeon' );

		    if( $nr > 0 && isset( $classes[ $nr ] ) ){
		        $cfgs[ 'header' ][ 'front-page-header' ] = array(
		            'id'            => 'front-page-header',
		            'name'          => __( 'Front Page Header' , 'zeon' ),
		            'description'   => sprintf( $description, number_format_i18n( $nr ) ),
		            'before_widget' => '<div id="%1$s" class="widget %2$s ' . $classes[ $nr ] . '">',
		            'after_widget'  => '</div>',
		            'before_title'  => '<h3 class="widget-title">',
		            'after_title'   => '</h3>'
		        );
		    }

		    return $cfgs;
		}

		add_filter( 'tempo_merge_sidebars', 'tempo__merge_header_front_page_sidebars' );


		// Blog Header Sidebars

		function tempo__merge_header_blog_sidebars( $cfgs )
		{
			$classes 		= tempo_cfgs::get( 'sidebars-classes' );
		    $nr 			= tempo_options::get( 'header-blog-nr-columns' );
		    $description 	= _n( 'Inside of the Sidebar the widgets will be arranged in one column. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars.' , 'Inside of the Sidebar the widgets will be arranged in %s columns. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars',  $nr, 'zeon' );

		    if( $nr > 0 && isset( $classes[ $nr ] ) ){
		        $cfgs[ 'header' ][ 'blog-header' ] = array(
		            'id'            => 'blog-header',
		            'name'          => __( 'Blog Header' , 'zeon' ),
		            'description'   => sprintf( $description, number_format_i18n( $nr ) ),
		            'before_widget' => '<div id="%1$s" class="widget %2$s ' . $classes[ $nr ] . '">',
		            'after_widget'  => '</div>',
		            'before_title'  => '<h3 class="widget-title">',
		            'after_title'   => '</h3>'
		        );
		    }

		    return $cfgs;
		}

		add_filter( 'tempo_merge_sidebars', 'tempo__merge_header_blog_sidebars' );


		// Footer Light Sidebars

		function tempo__merge_footer_light_sidebars( $cfgs )
		{
			$classes 		= tempo_cfgs::get( 'sidebars-classes' );
		    $nr 			= tempo_options::get( 'footer-light-nr-columns' );
		    $description 	= _n( 'Inside of the Sidebar the widgets will be arranged in one column. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars.' , 'Inside of the Sidebar the widgets will be arranged in %s columns. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars',  $nr, 'zeon' );

		    if( $nr > 0 && isset( $classes[ $nr ] ) ){
		        $cfgs[ 'footer' ][ 'footer-light' ] = array(
		            'id'            => 'footer-light',
		            'name'          => __( 'Footer Light Side' , 'zeon' ),
		            'description'   => sprintf( $description, number_format_i18n( $nr ) ),
		            'before_widget' => '<div id="%1$s" class="widget %2$s ' . $classes[ $nr ] . '">',
		            'after_widget'  => '</div>',
		            'before_title'  => '<h5 class="widget-title">',
		            'after_title'   => '</h5>'
		        );
		    }

		    return $cfgs;
		}

		add_filter( 'tempo_merge_sidebars', 'tempo__merge_footer_light_sidebars' );


		// Footer Dark Sidebars

		function tempo__merge_footer_dark_sidebars( $cfgs )
		{
			$classes 		= tempo_cfgs::get( 'sidebars-classes' );
		    $nr 			= tempo_options::get( 'footer-dark-nr-columns' );
		    $description 	= _n( 'Inside of the Sidebar the widgets will be arranged in one column. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars.' , 'Inside of the Sidebar the widgets will be arranged in %s columns. You can change the number of columns from Admin Dashboard &rsaquo; Zeon &rsaquo; Sidebars',  $nr, 'zeon' );

		    if( $nr > 0 && isset( $classes[ $nr ] ) ){
		        $cfgs[ 'footer' ][ 'footer-dark' ] = array(
		            'id'            => 'footer-dark',
		            'name'          => __( 'Footer Dark Side' , 'zeon' ),
		            'description'   => sprintf( $description, number_format_i18n( $nr ) ),
		            'before_widget' => '<div id="%1$s" class="widget %2$s ' . $classes[ $nr ] . '">',
		            'after_widget'  => '</div>',
		            'before_title'  => '<h5 class="widget-title">',
		            'after_title'   => '</h5>'
		        );
		    }

		    return $cfgs;
		}

		add_filter( 'tempo_merge_sidebars', 'tempo__merge_footer_dark_sidebars' );


		// Custom Sidebars

		function tempo__merge_custom_sidebars( $cfgs )
		{
			$s = tempo_options::get( 'custom-sidebars' );

		    if( !empty( $s ) ){

		        $sidebars = array();

		        foreach( $s as $id => $name ){
		            $sidebars[ $id ] = array(
		                'id'            => $id,
		                'name'          => $name,
		                'description'   => __( 'This is a custom sidebar. You can add new custom sidebars from Admin Dashboard &rsaquo; Zeon &rsaquo; Custom Sidebars', 'zeon' ),
		                'before_widget' => '<div id="%1$s" class="widget %2$s">',
		                'after_widget'  => '</div>',
		                'before_title'  => '<h4 class="widget-title">',
		                'after_title'   => '</h4>'
		            );
		        }

		        /**
		         *  Add the sidebars to config
		         */

		        if( !empty( $sidebars ) )
		            $cfgs[ 'custom' ] = $sidebars;
		    }

		    return $cfgs;
		}

		add_filter( 'tempo_merge_sidebars', 'tempo__merge_custom_sidebars' );

	}


	///// TOPPER MENU CLASSES /////


	{	/////	FILTERS - HEADER    /////

		function tempo_mix_height()
		{
	    	$headline       = tempo_options::get( 'header-headline' );
    		$description    = tempo_options::get( 'header-description' );

			$btn_1 			= tempo_options::get( 'header-btn-1' );
    		$btn_2 			= tempo_options::get( 'header-btn-2' );

    		return ( $headline || $description ) && ( $btn_1 || $btn_2 );
		}

		function tempo__header_height( $height )
		{
			if( tempo_mix_height() )
        		$height = tempo_options::get( 'header-text-space' ) + tempo_options::get( 'header-btns-space' );

        	return $height;
		}

		add_filter( 'tempo_header_height', 'tempo__header_height' );


		/**
		 *  Flex Container - get vertical position from plugin settings
		 */

		function tempo__flex_container_class( $valign )
		{
			if( empty( $valign ) && $mix_height = tempo_mix_height() )
				$valign = 'tempo-valign-' . tempo_options::get( 'header-text-vertical-align' );

			return esc_attr( trim( $valign ) );
		}

		add_filter( 'tempo_flex_container_class', 'tempo__flex_container_class' );


		/**
		 *	Flex text Container - get style
		 */
		function tempo__header_text_wrapper_style( $style )
		{
			if( tempo_mix_height() )
		        $style	= 'zeon: true; height: ' . absint( tempo_options::get( 'header-text-space' ) ) . 'px;';

		    return $style;
		}

		add_filter( 'tempo_header_text_wrapper_style', 'tempo__header_text_wrapper_style' );


		/**
		 *  Flex Item - get horizontal position from plugin settings
		 */

		function tempo__flex_item_class( $align )
		{
			if( empty( $align ) )
				$align = 'tempo-align-' . tempo_options::get( 'header-horizontal-align' );

			return esc_attr( trim( $align ) );
		}

		add_filter( 'tempo_flex_item_class', 'tempo__flex_item_class' );
	}


	{	/////	HEADER TEMPLATES    /////

		/**
		 *	Custom Header Image type "Sample Image"
		 */

		function tempo__singular_thumbnail( $show, $post_id )
		{
			$template = zeon_meta::get( $post_id, 'tempo-header-template', 'default' );

			switch ( $template ) {
				case 'default' : {
					break;
				}
				case 'sample-image' :
				case 'hero-image' :
				case 'portfolio' :
				case 'video' :
				case 'audio' : {
					$show = false;
					break;
				}
				default : {
					break;
				}
			}

			return $show;
		}

		add_filter( 'tempo_page_thumbnail', 'tempo__singular_thumbnail', 10, 2 );
		add_filter( 'tempo_post_thumbnail', 'tempo__singular_thumbnail', 10, 2 );


		function tempo__singular_breadcrumbs( $show, $post_id )
		{
			$template = zeon_meta::get( $post_id, 'tempo-header-template', 'default' );

			if( 'hero-image' == $template || 'portfolio' == $template )
				$show = false;

			return $show;
		}

		add_filter( 'tempo_template_breadcrumbs', 	'tempo__singular_breadcrumbs', 10, 2 );
		add_filter( 'tempo_page_breadcrumbs', 		'tempo__singular_breadcrumbs', 10, 2 );
		add_filter( 'tempo_post_breadcrumbs', 		'tempo__singular_breadcrumbs', 10, 2 );
		add_filter( 'tempo_post_title', 			'tempo__singular_breadcrumbs', 10, 2 );
		add_filter( 'tempo_post_categories', 		'tempo__singular_breadcrumbs', 10, 2 );
		add_filter( 'tempo_post_meta', 				'tempo__singular_breadcrumbs', 10, 2 );
	}


	{

		function tempo__the_content( $content )
		{
	        $content = str_replace( "<br /></p>",               "</p>", $content );
	        $content = str_replace( "<br />\n</p>",             "</p>", $content );

	        $content = str_replace( "<p></p>",                  "", $content );
	        $content = str_replace( "<p><p>",                   "<p>", $content );
	        $content = str_replace( "</p></p>",                 "</p>", $content );

	        $content = str_replace( "<p><script>",              "<script>", $content );
	        $content = str_replace( "</script></p>",            "</script>", $content );

	        $content = str_replace( "<p><style>",              	"<style>", $content );
	        $content = str_replace( "</style></p>",            	"</style>", $content );

	        $content = str_replace( "<p><iframe>",              "<iframe>", $content );
	        $content = str_replace( "</iframe></p>",            "</iframe>", $content );

	        $content = str_replace( "<p><h",                    "<h", $content );
	        $content = str_replace( "h></p>",                   "h>", $content );

	        $content = str_replace( "<p><img",                  "<img", $content );
	        $content = str_replace( "/></p>",                   "/>", $content );

	        $content = str_replace( "<br />\n<h",               "<h", $content );
	        $content = str_replace( "<br /><h",                 "<h", $content );
	        $content = str_replace( "h><br />\n",               "h>", $content );
	        $content = str_replace( "h><br />",                 "h>", $content );

	        $content = str_replace( "<p>[",                     "[", $content );
	        $content = str_replace( "/]</p>",                   "/]", $content );
	        $content = str_replace( "]</p>",                    "]", $content );

	        $content = str_replace( "<br />[",                  "[", $content );
	        $content = str_replace( "<br />\n[",                "[", $content );

	        $content = str_replace( "/]<br />",                 "/]", $content );
	        $content = str_replace( "/]<br />\n",               "/]", $content );

	        $content = str_replace( "]<br />",                  "]", $content );
	        $content = str_replace( "]<br />\n",                "]", $content );

	        return $content;
    	}

    	add_filter( 'the_content', 'tempo__the_content' );


		if( is_singular() ){
			global $post;

			if( false ){
				$cfgs = array(
			        'require' => array(
			            'module'    => zeon_plugin_uri() . '/media/js/jquery.tubular.js'
			        ),
			        'args' => array(
			        )
			    );

			    zeon_modules::set( 'tubular',  $cfgs );
			}
		}
	}

	{	/////	PAGE BREADCRUMBS    /////


		function tempo__meta_breadcrumbs( $is_enable, $post_id )
		{
			if( zeon_meta::g( $post_id, 'tempo-overwrite-breadcrumbs' ) )
				$is_enable = (bool)zeon_meta::get( $post_id, 'tempo-breadcrumbs', $is_enable );

			return $is_enable;
		}

		add_filter( 'tempo_post_breadcrumbs', 		'tempo__meta_breadcrumbs', 10, 2 );
		add_filter( 'tempo_page_breadcrumbs', 		'tempo__meta_breadcrumbs', 10, 2 );
		add_filter( 'tempo_template_breadcrumbs', 	'tempo__meta_breadcrumbs', 10, 2 );
	}



	{	///// LAYOUT & SIDEBARS FILTERS /////


		/**
		 *	Content Width
		 */

		function tempo_opt_content_width()
		{
			$rett = tempo_options::get( 'content-width' );

			if( is_singular() ){
				global $post;

				if( (bool)zeon_meta::g( $post -> ID, 'tempo-overwrite-content-width' ) )
					$rett = absint( zeon_meta::get( $post -> ID, 'tempo-content-width', $rett ) );
			}

			return $rett;
		}

		function tempo__content_width( $width )
		{
			if( 10 == absint( tempo_opt_content_width() ) )
				$width = 945;

			return absint( $width );
		}

		add_filter( 'tempo_content_width', 'tempo__content_width' );


		/**
		 *	Content Length
		 */

		function tempo__content_length( $length )
		{
			if( 10 == absint( tempo_opt_content_width() ) )
				$length  = 'col-lg-10 col-lg-offset-1';

			return $length;
		}

		add_filter( 'tempo_content_length', 'tempo__content_length' );


	    /**
	     *	Front Page Section Length
	     */

	    function tempo__front_page_section_length( $length )
	    {
	        $layout = new tempo_layout( 'front-page' );
	        return $layout -> classes();
	    }

	    add_filter( 'tempo_front_page_section_length', 'tempo__front_page_section_length' );


	    /**
	     *	Singular Post / Page Section Length
	     */

	    function tempo__singular_section_length( $length, $post_id )
	    {
	    	if( empty( $post_id ) )
	    		return;

	    	$post = get_post( $post_id );

	        $layout = new tempo_layout( $post -> post_type, $post -> ID );
	        return $layout -> classes();
	    }

	    add_filter( 'tempo_page_section_length', 'tempo__singular_section_length', 10, 2 );
	    add_filter( 'tempo_single_section_length', 'tempo__singular_section_length', 10, 2 );

	    /**
	     *	Loop Section Length + Blog View Class
	     */

	    function tempo__loop_section_length( $layout )
	    {
	        $layout = new tempo_layout();
	        return esc_attr( $layout -> classes() . ' tempo-blog-' . tempo_options::get( 'blog-view' ) );
	    }

	    add_filter( 'tempo_loop_section_length', 'tempo__loop_section_length' );



	    ///// SIDEBARS ACTIONS /////

	    /**
	     *	Left Sidebars
	     */

	    function tempo__left_sidebar( $slug, $name )
	    {
	    	if( is_404() )
	    		return;

		    /**
		     *	get sidebar
		     */

	    	$layout = new tempo_layout( $name );

	    	if( is_singular() && !( is_front_page() || is_home() ) ){
	    		global $post;

	    		$layout = new tempo_layout( $name, $post -> ID );
	    	}


	    	/**
	    	 *	load sidebars scroll module
	    	 */

	    	if( $layout -> layout == 'left' && tempo_options::get( 'aside-scroll' ) )
			    zeon_modules::set( 'aside_scroll',  array(
			        'require' => array(
			            'module'    => zeon_plugin_uri() . '/includes/media/js/modules/aside_scroll.js'
			        )
			    ));

			// left sidebar
	        $layout -> sidebar( 'left' );
	    }

	    add_action( 'get_template_part_templates/section/before', 'tempo__left_sidebar', 10, 2 );

	    /**
	     *	Right Sidebars
	     */

	    function tempo__right_sidebar( $slug, $name )
	    {
	    	if( is_404() )
	    		return;

		    /**
		     *	get sidebar
		     */

	    	$layout = new tempo_layout( $name );

	    	if( is_singular() && !( is_front_page() || is_home() ) ){
	    		global $post;

	    		$layout = new tempo_layout( $name, $post -> ID );
	    	}


	    	/**
	    	 *	load sidebars scroll module
	    	 */

	    	if( $layout -> layout == 'right' && tempo_options::get( 'aside-scroll' ) )
			    zeon_modules::set( 'aside_scroll',  array(
			        'require' => array(
			            'module'    => zeon_plugin_uri() . '/includes/media/js/modules/aside_scroll.js'
			        )
			    ));

	    	// right sidebar
	        $layout -> sidebar( 'right' );
	    }

	    add_action( 'get_template_part_templates/section/after', 'tempo__right_sidebar', 10, 2 );
	}


	{
		/**
	     *	Blog View - Article Template
	     */
	    function tempo__blog_view( $view )
		{
			return tempo_options::get( 'blog-view' );
		}

		add_filter( 'tempo_blog_view', 'tempo__blog_view' );
	}


	{	/////	COMMENTS    /////

		function tempo__custom_comments( $use_custom )
		{
			global $post;

			/* FACEBOOK COMMENTS */
		    $fb_nr_comments = tempo_options::get( 'facebook-nr-comments' );

		    if( 'facebook' == tempo_options::get( 'comments-type' ) && $fb_nr_comments > 0  ){

		        $permalink = get_permalink( $post -> ID );

		        echo '<div class="tempo-comments">';
		        echo '<div class="fb-comments" data-href="' . esc_url( $permalink ) . '" data-numposts="' . absint( $fb_nr_comments ) . '" data-colorscheme="light"></div>';
		        echo '</div>';
		        echo '</div>';

		        return true;
		    }


		    /* DISQUS COMMENTS */
		    $dq_shortname = tempo_options::get( 'disqus-shortname' );

		    if( 'disqus' == tempo_options::get( 'comments-type' ) && !empty( $dq_shortname ) ){

		        echo '<div class="tempo-comments">';
		        echo '<div id="disqus_thread"></div>';
		        ?>
		            <script type="text/javascript">
		                /* * * CONFIGURATION VARIABLES * * */
		                var disqus_shortname = '<?php echo esc_attr( $dq_shortname ); ?>';

		                /* * * DON'T EDIT BELOW THIS LINE * * */
		                (function() {
		                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		                })();
		            </script>
		            <script id="dsq-count-scr" src="//<?php echo esc_attr( $dq_shortname ); ?>.disqus.com/count.js" async></script>
		        <?php
		        echo '<noscript>' . sprintf( __( 'Please enable JavaScript to view the %s.', 'zeon' ), '<a href="https://disqus.com/?ref_noscript" rel="nofollow">' . __( 'comments powered by Disqus' , 'zeon' ) . '</a>' ) . '</noscript>';
		        echo '</div>';
		        echo '</div>';

		        return true;
		    }

		    return $use_custom;
		}

		add_filter( 'tempo_custom_comments', 'tempo__custom_comments' );

		/**
		 *	Number of comments from post meta widget
		 */
		function zeon_widget_post_meta_comments( $content )
		{
			global $post;

			if( !isset( $post -> ID ) )
				return $content;

			/* FACEBOOK COMMENTS */
		    $fb_nr_comments = tempo_options::get( 'facebook-nr-comments' );

            if( 'facebook' == tempo_options::get( 'comments-type' ) && $fb_nr_comments > 0  ){
                $content = sprintf( __( '%s Comment(s)', 'zeon' ) , '<span class="fb-comments-count" data-href="' . get_permalink( $post -> ID ) . '"></span>' );
            }

            /* DISQUS COMMENTS */
		    $dq_shortname = tempo_options::get( 'disqus-shortname' );

            if( 'disqus' == tempo_options::get( 'comments-type' ) && !empty( $dq_shortname ) ){
                $content = '<span class="disqus-comment-count" data-disqus-url="' . get_permalink( $post -> ID ) . '"></span>';
            }

		    return $content;
		}

		add_filter( 'zeon_widget_post_meta_comments', 'zeon_widget_post_meta_comments' );


		function tempo__comments_class( $classes )
		{
			// facebook class
			$fb_nr_comments = tempo_options::get( 'facebook-nr-comments' );

			if( 'facebook' == tempo_options::get( 'comments-type' ) && $fb_nr_comments > 0  )
		        $classes = trim( $classes ) . ' facebook-comments';


		    // disqus class
		    $dq_shortname = tempo_options::get( 'disqus-shortname' );

		    if( 'disqus' == tempo_options::get( 'comments-type' ) && !empty( $dq_shortname ) )
		        $classes = trim( $classes ) . ' disqus-comments';

		    // collapsing classic
		    if( !tempo_options::get( 'collapsing-classic' ) ){
		    	$classes .= ' tempo-not-collapsing';
		    }

			return esc_attr( trim( $classes ) );
		}

		add_filter( 'tempo_comments_class', 'tempo__comments_class' );

	}


	{	/////	GALLERY

		function tempo__gallery_image_thumbnail( $image, $p )
		{
			$url 	= esc_url( zeon_meta::g( $p -> ID, '_tempo_gallery_url' ) );
			$target = esc_attr( zeon_meta::g( $p -> ID, '_tempo_gallery_url_target' ) );
			$rett 	= $image;
			$target = !empty( $target ) ? $target : '_self';

			if( !empty( $url ) )
				$rett = '<a href="' . esc_url( $url ) . '" target="' . esc_attr( $target ) . '"></a>' . $image;

			return $rett;
		}

		add_filter( 'tempo_gallery_image_thumbnail', 'tempo__gallery_image_thumbnail', 10, 2 );


		function tempo__gallery_image_title( $title, $p )
		{
			$url 	= esc_url( zeon_meta::g( $p -> ID, '_tempo_gallery_url' ) );
			$target = esc_attr( zeon_meta::g( $p -> ID, '_tempo_gallery_url_target' ) );
			$rett 	= $title;
			$target = !empty( $target ) ? $target : '_self';

			if( !empty( $url ) )
				$rett = '<a href="' . esc_url( $url ) . '" target="' . esc_attr( $target ) . '">' . $title . '</a>';

			return $rett;
		}

		add_filter( 'tempo_gallery_image_title', 'tempo__gallery_image_title', 10, 2 );
	}


	{	///// CUSTOM META BOXES /////

		{	///// THUMBNAIL /////

		}


		{	///// HEADER /////

			/*function tempo__box_settings_save( $post_id )
			{
				if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			        return;

			    if( isset( $_POST ) && !empty( $_POST ) ){

			    }
			}

			function tempo__box_settings( $post )
			{

			}*/


			function tempo__box_header_save( $post_id )
			{
				if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			        return;

			    if( isset( $_POST ) && !empty( $_POST ) ){
			     	if( isset( $_POST[ 'tempo-header-template' ] ) ){

			     		$value = $_POST[ 'tempo-header-template' ];

			     		if( is_int( $value ) || ctype_digit( $value ) ){
			     			zeon_meta::set( $post_id, 'tempo-header-template', intval( $value ) );
			     		}

			     		else{
			     			zeon_meta::set( $post_id, 'tempo-header-template', esc_attr( $value ) );

			     			if( $value == 'sample-image' ){
			     				//do crop
			     			}
			     		}
			     	}


			     	/**
			     	 *	Audio Template
			     	 */

			     	// video url
			     	if( isset( $_POST[ 'tempo-video-url' ] ) && ( $url = esc_url( $_POST[ 'tempo-video-url' ] ) ) && !empty( $url ) )
			     		zeon_meta::set( $post_id, 'tempo-video-url', $url );


			     	/**
			     	 *	Audio Template
			     	 */

			     	// audio url
			     	if( isset( $_POST[ 'tempo-audio-url' ] ) && ( $url = esc_url( $_POST[ 'tempo-audio-url' ] ) ) && !empty( $url ) )
			     		zeon_meta::set( $post_id, 'tempo-audio-url', $url );

			     	// audio ID
			     	if( isset( $_POST[ 'tempo-audio-id' ] ) && ( $id = absint( $_POST[ 'tempo-audio-id' ] ) ) && !empty( $id ) )
			     		zeon_meta::set( $post_id, 'tempo-audio-id', $id );

			     	// audio autoplay
			     	if( isset( $_POST[ 'tempo-audio-autoplay' ] ) )
			     		zeon_meta::set( $post_id, 'tempo-audio-autoplay', absint( $_POST[ 'tempo-audio-autoplay' ] ) );

			     	// audio title
			     	if( isset( $_POST[ 'tempo-audio-title' ] ) && ( $title = sanitize_text_field( $_POST[ 'tempo-audio-title' ] ) ) && !empty( $title ) ){
			     		zeon_meta::set( $post_id, 'tempo-audio-title', $title );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-audio-title' );
			     	}

			     	// audio composer
			     	if( isset( $_POST[ 'tempo-audio-composer' ] ) && ( $composer = sanitize_text_field( $_POST[ 'tempo-audio-composer' ] ) ) && !empty( $composer ) ){
			     		zeon_meta::set( $post_id, 'tempo-audio-composer', $composer );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-audio-composer' );
			     	}

			     	// audio artist
			     	if( isset( $_POST[ 'tempo-audio-artist' ] ) && ( $artist = sanitize_text_field( $_POST[ 'tempo-audio-artist' ] ) ) && !empty( $artist ) ){
			     		zeon_meta::set( $post_id, 'tempo-audio-artist', $artist );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-audio-artist' );
			     	}

			     	// audio album
			     	if( isset( $_POST[ 'tempo-audio-album' ] ) && ( $album = sanitize_text_field( $_POST[ 'tempo-audio-album' ] ) ) && !empty( $album ) ){
			     		zeon_meta::set( $post_id, 'tempo-audio-album', $album );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-audio-album' );
			     	}

			     	// audio description
			     	if( isset( $_POST[ 'tempo-audio-description' ] ) && ( $description = esc_textarea( $_POST[ 'tempo-audio-description' ] ) ) && !empty( $description ) ){
			     		zeon_meta::set( $post_id, 'tempo-audio-description', $description );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-audio-description' );
			     	}

			     	/**
			     	 *	Google Map Template
			     	 */

			     	if( isset( $_POST[ 'tempo-gmap-height' ] ) && ( $height = absint( $_POST[ 'tempo-gmap-height' ] ) ) && $height > 0 )
			     		zeon_meta::set( $post_id, 'tempo-gmap-height', $height );

			     	if( isset( $_POST[ 'tempo-gmap-zoom' ] ) && ( $zoom = absint( $_POST[ 'tempo-gmap-zoom' ] ) ) && $zoom > 0 )
			     		zeon_meta::set( $post_id, 'tempo-gmap-zoom', $zoom );

			     	if( isset( $_POST[ 'tempo-gmap-lat' ] ) && ( $lat = esc_attr( $_POST[ 'tempo-gmap-lat' ] ) ) && !empty( $lat ) )
			     		zeon_meta::set( $post_id, 'tempo-gmap-lat', $lat );

			     	if( isset( $_POST[ 'tempo-gmap-lng' ] ) && ( $lng = esc_attr( $_POST[ 'tempo-gmap-lng' ] ) ) && !empty( $lng ) )
			     		zeon_meta::set( $post_id, 'tempo-gmap-lng', $lng );

			     	if( isset( $_POST[ 'tempo-gmap-marker-title' ] ) && ( $title = sanitize_text_field( $_POST[ 'tempo-gmap-marker-title' ] ) ) && !empty( $title ) )
			     		zeon_meta::set( $post_id, 'tempo-gmap-marker-title', $title );

			     	if( isset( $_POST[ 'tempo-gmap-marker-description' ] ) && ( $description = esc_textarea( $_POST[ 'tempo-gmap-marker-description' ] ) ) && !empty( $description ) )
			     		zeon_meta::set( $post_id, 'tempo-gmap-marker-description', $description );
			    }
			}

			function tempo__box_header( $post )
			{
			    $header_type = zeon_meta::get( $post -> ID, 'tempo-header-type', 'custom-image' );

			    /*echo tempo_html::notification( array(
					'type'			=> 'notify',
					'class'			=> 'tempo-template-default',
					'description'	=> __( 'It is possible to create a custom header template. Add a new custom post Header and choose your favorite settings for this template.', 'zeon' ),
					'style'			=> array(
						'margin-bottom' => '15px;'
					)
			    ));*/

			    $query = new WP_Query(array(
			    	'post_type'			=> 'header',
			    	'posts_per_page'	=> -1
			    ));

			    $options = array(
	            	'default'			=> __( 'Default', 'zeon' ),
	            	'sample-image'		=> __( 'Sample Image', 'zeon' ),
	            	'hero-image'		=> __( 'Hero Image', 'zeon' ),
	            	'portfolio'			=> __( 'Portfolio', 'zeon' ),
	                'video'      		=> __( 'Video', 'zeon' ),
	                'audio'  			=> __( 'Audio', 'zeon' ),
	                'gmap'  			=> __( 'Google Map', 'zeon' )
	            );

	            if( count( $query -> posts ) ){
	            	$groups = array();
	            	$groups[ 'default' ] = $options;

	            	foreach( $query -> posts as $p ){
	            		$groups[ 'custom' ][ $p -> ID ] = $p -> post_title;
	            	}
	            }

	            else{
	            	$groups = $options;
	            }

	            $template = zeon_meta::get( $post -> ID, 'tempo-header-template', 'default' );

			    echo tempo_html::field( array(
					'title'			=> __( 'Header Template', 'zeon' ),
					'description'	=> '<span class="tempo-featured-image hidden">' . __( 'The featured image is used as header image.', 'zeon' ) . '</span>' ,
			        'format'    	=> 'across',
			        'input'     	=> array(
			            'type'      	=> 'select',
			            'slug'      	=> 'tempo-header-template',
			            'theme_mod' 	=> false,
			            'value'     	=> $template,
			            'options'   	=> $groups,
			            'actions'		=> array(
			            	'hide'			=> array( '.tempo-video-wrapper', '.tempo-audio-wrapper', '.tempo-gmap-wrapper', '.tempo-featured-image' ),
			            	'video'			=> array(
			            		'show'			=> array( '.tempo-video-wrapper' )
			            	),
							'sample-image'	=> array(
								'show'			=> array( '.tempo-featured-image' )
							),
							'hero-image'	=> array(
								'show'			=> array( '.tempo-featured-image' )
							),
							'portfolio'		=> array(
								'show'			=> array( '.tempo-featured-image' )
							),
			            	'audio'			=> array(
			            		'show'			=> array( '.tempo-audio-wrapper', '.tempo-featured-image' )
			            	),
			            	'gmap'			=> array(
			            		'show'			=> array( '.tempo-gmap-wrapper' )
			            	)
			            )
			        )
			    ));

			    $video_class_wrapper = 'tempo-video-wrapper hidden';

			    if( $template == 'video' )
			    	$video_class_wrapper = 'tempo-video-wrapper';

			    echo tempo_html::field( array(
			    	'class'			=> $video_class_wrapper,
					'title'			=> __( 'Video URL', 'zeon' ),
					'description'	=> __( 'Insert the url to the video like: YouTube, Vimeo or Self hosted.', 'zeon' ),
			        'format'    	=> 'across',
			        'input'     	=> array(
			            'type'      	=> 'url',
			            'slug'      	=> 'tempo-video-url',
			            'theme_mod' 	=> false,
			            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-video-url' ),
			        )
			    ));

				$audio_class_wrapper = 'tempo-audio-wrapper hidden';

			    if( $template == 'audio' )
			    	$audio_class_wrapper = 'tempo-audio-wrapper';

			    {
			    	echo '<div class="' . $audio_class_wrapper . '">';

		            echo tempo_html::box( array(
                		'title'		=> __( 'Audio Settings', 'zeon' ),
				    	'fields'	=> array(
				    		array(
				    			'title'			=> __( 'Autoplay', 'zeon' ),
								'description'	=> __( 'Will start automatically play audio', 'zeon' ),
						        'format'    	=> 'linear',
						        'input'     	=> array(
						            'type'      	=> 'logic',
						            'slug'			=> 'tempo-audio-autoplay',
						            'theme_mod' 	=> false,
						            'value'     	=> zeon_meta::get( $post -> ID, 'tempo-audio-autoplay', 1 )
						        )
				    		),
			    			array(
								'title'			=> __( 'Audio File', 'zeon' ),
								'description'	=> __( 'Upload or select from media library an audio file.', 'zeon' ),
						        'format'    	=> 'across',
						        'input'     	=> array(
						            'type'      	=> 'upload',
						            'slug'      	=> 'tempo-audio-url',
						            'theme_mod' 	=> false,
						            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-url' ),
						            'callback'  	=> 'id',
			                        'args'      	=> array(
			                            'id' 			=> '.tempo-audio-id-field'
			                        )
						        )
						    ),
							array(
								'class'			=> 'tempo-audio-id-field',
						        'format'    	=> 'none',
						        'input'     	=> array(
						            'type'      	=> 'hidden',
						            'slug'      	=> 'tempo-audio-id',
						            'theme_mod' 	=> false,
						            'value'     	=> absint( zeon_meta::g( $post -> ID, 'tempo-audio-id' ) ),
						        )
						    )
				    	)
				    ));

					echo tempo_html::notification( array(
						'type'			=> 'notify',
						'description'	=> __( 'Below you can provide additional details about audio file. If you will fill this fields then you will overwrite audio file details attached to media library.', 'zeon' ),
						'style'			=> array(
							'margin-bottom' => '15px;'
						)
				    ));

					echo tempo_html::$column -> wrapper( 'before' );

		            echo tempo_html::$column -> get(array(
		            	'layout' => array(
		                    'sm' 	=> 12,
		                    'md' 	=> 6,
		                    'lg' 	=> 6
		                ),
		                'boxes' => array(
		                	array(
						    	'title'		=> __( 'Title and Description', 'zeon' ),
						    	'fields'	=> array(
						    		array(
										'title'			=> __( 'Title', 'zeon' ),
								        'format'    	=> 'across',
								        'input'     	=> array(
								            'type'      	=> 'text',
								            'slug'      	=> 'tempo-audio-title',
								            'theme_mod' 	=> false,
								            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-title' )
								        )
								    ),
								    array(
								    	'class'			=> 'tempo-textarea-small',
										'title'			=> __( 'Description', 'zeon' ),
								        'format'    	=> 'across',
								        'input'     	=> array(
								            'type'      	=> 'textarea',
								            'slug'      	=> 'tempo-audio-description',
								            'theme_mod' 	=> false,
								            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-description' )
								        )
								    )
						    	)
						    )
		                )
		            ));

		            echo tempo_html::$column -> get(array(
		            	'layout' => array(
		                    'sm' 	=> 12,
		                    'md' 	=> 6,
		                    'lg' 	=> 6
		                ),
		                'boxes' => array(
		                	array(
						    	'title'		=> __( 'Composer, Artist and Album', 'zeon' ),
						    	'fields'	=> array(
						    		array(
										'title'			=> __( 'Composer', 'zeon' ),
								        'format'    	=> 'across',
								        'input'     	=> array(
								            'type'      	=> 'text',
								            'slug'      	=> 'tempo-audio-composer',
								            'theme_mod' 	=> false,
								            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-composer' )
								        )
								    ),
								    array(
										'title'			=> __( 'Artist', 'zeon' ),
								        'format'    	=> 'across',
								        'input'     	=> array(
								            'type'      	=> 'text',
								            'slug'      	=> 'tempo-audio-artist',
								            'theme_mod' 	=> false,
								            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-artist' )
								        )
								    ),
								    array(
										'title'			=> __( 'Album', 'zeon' ),
								        'format'    	=> 'across',
								        'input'     	=> array(
								            'type'      	=> 'text',
								            'slug'      	=> 'tempo-audio-album',
								            'theme_mod' 	=> false,
								            'value'     	=> zeon_meta::g( $post -> ID, 'tempo-audio-album' )
								        )
								    )
						    	)
						    )
		                )
		            ));

		        	echo tempo_html::$column -> wrapper( 'after' );

				    echo '</div>';
				}

				$gmap_class_wrapper = 'tempo-gmap-wrapper hidden';

			    if( $template == 'gmap' )
			    	$gmap_class_wrapper = 'tempo-gmap-wrapper';

				{
					$height 		= zeon_meta::get( $post -> ID, 'tempo-gmap-height', 380 );
				    $zoom 			= zeon_meta::get( $post -> ID, 'tempo-gmap-zoom', 4 );
				    $lat 			= zeon_meta::get( $post -> ID, 'tempo-gmap-lat', 47.772594932483 );
				    $lng 			= zeon_meta::get( $post -> ID, 'tempo-gmap-lng', 1.8596535022735 );

				    $title 			= zeon_meta::get( $post -> ID, 'tempo-gmap-marker-title', __( 'Marker title' , 'zeon' ) );
				    $description 	= zeon_meta::get( $post -> ID, 'tempo-gmap-marker-description', __( 'Google map marker with short description.', 'zeon' ) );

					echo '<div class="' . $gmap_class_wrapper . '">';

					echo tempo_html::$column -> wrapper( 'before' );

					echo tempo_html::$column -> get(array(
		            	'layout' => array(
		                    'sm' 	=> 12,
		                    'md' 	=> 12,
		                    'lg' 	=> 12
		                ),
		                'boxes' => array(
		                	array(
						    	'title'		=> __( 'Google Map Preview', 'zeon' ),
						    	'content'   => do_shortcode(
					            	'[zeon_gmap class="tempo-header-template" height="' . absint( $height ) . '" zoom="' . absint( $zoom 	) . '" lat="' . esc_attr( $lat ) . '" lng="' . esc_attr( $lng ) . '"]' .
									'[zeon_marker title="' . sanitize_text_field( $title ) . '"]' . esc_textarea( $description ) . '[/zeon_marker]' .
									'[/zeon_gmap]'
					            )
						    )
		                )
		            ));

		            echo tempo_html::$column -> get(array(
		            	'layout' => array(
		                    'sm' 	=> 12,
		                    'md' 	=> 6,
		                    'lg' 	=> 6
		                ),
		                'boxes' => array(
		                	array(
						    	'title'		=> __( 'Google Map Settings', 'zeon' ),
						    	'fields'	=> array(
						    		array(
					                    'title'     	=> __( 'Height', 'zeon' ),
					                    'format'    	=> 'linear',
					                    'input'     	=> array(
					                        'id'        	=> 'zeon-gmap-height',
					                        'type'      	=> 'number',
					                        'name'      	=> 'tempo-gmap-height',
					                        'default'   	=> absint( $height ),
					                        'theme_mod' 	=> false
					                    )
					                ),
					                array(
					                    'title'     	=> __( 'Zoom', 'zeon' ),
					                    'format'    	=> 'linear',
					                    'input'     		=> array(
					                        'id'        	=> 'zeon-gmap-zoom',
					                        'type'      	=> 'number',
					                        'name'      	=> 'tempo-gmap-zoom',
					                        'default'   	=> absint( $zoom ),
					                        'min'       	=> 1,
					                        'max'       	=> 21,
					                        'theme_mod' 	=> false
					                    )
					                ),
					                array(
					                    'title'         => __( 'Latitude' , 'zeon' ),
					                    'description'   => __( 'the center of the map' , 'zeon' ),
					                    'format'        => 'across',
					                    'input'         => array(
					                        'id'            => 'zeon-gmap-lat',
					                        'type'          => 'text',
					                        'name'          => 'tempo-gmap-lat',
					                        'default'       => esc_attr( $lat ),
					                        'theme_mod'     => false
					                    )
					                ),
					                array(
					                    'title'         => __( 'Longitude' , 'zeon' ),
					                    'description'   => __( 'the center of the map' , 'zeon' ),
					                    'format'        => 'across',
					                    'input'         => array(
					                        'id'            => 'zeon-gmap-lng',
					                        'type'          => 'text',
					                        'name'          => 'tempo-gmap-lng',
					                        'default'       => esc_attr( $lng ),
					                        'theme_mod'     => false
					                    )
					                )
						    	)
						    )
		                )
		            ));

		            echo tempo_html::$column -> get(array(
		            	'layout' => array(
		                    'sm' 	=> 12,
		                    'md' 	=> 6,
		                    'lg' 	=> 6
		                ),
		                'boxes' => array(
		                	array(
						    	'title'		=> __( 'Marker Settings', 'zeon' ),
						    	'fields'	=> array(
						    		array(
					                    'title'     	=> __( 'Title', 'zeon' ),
					                    'format'    	=> 'across',
					                    'input'     	=> array(
					                        'id'        	=> 'zeon-gmap-marker-title',
					                        'type'      	=> 'text',
					                        'name'      	=> 'tempo-gmap-marker-title',
					                        'default'       => sanitize_text_field( $title ),
					                        'theme_mod' 	=> false
					                    )
					                ),
					                array(
					                    'title'     	=> __( 'Description', 'zeon' ),
					                    'format'    	=> 'across',
					                    'input'     	=> array(
					                        'id'        	=> 'zeon-gmap-marker-description',
					                        'type'      	=> 'textarea',
					                        'name'      	=> 'tempo-gmap-marker-description',
					                        'default'       => esc_textarea( $description ),
					                        'theme_mod' 	=> false
					                    )
					                )
						    	)
						    )
		                )
		            ));

		            echo tempo_html::$column -> wrapper( 'after' );

					echo '</div>';
				}
			}
		}

		{	///// LAYOUT /////

			function tempo__box_layout_save( $post_id )
			{
				if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			        return;

			    if( isset( $_POST ) && !empty( $_POST ) ){
			        if( isset( $_POST[ 'tempo-overwrite-layout' ] ) && (bool)$_POST[ 'tempo-overwrite-layout' ] ){
			            zeon_meta::set( $post_id, 'tempo-overwrite-layout', (bool)$_POST[ 'tempo-overwrite-layout' ] );

			            if( isset( $_POST[ 'tempo-layout' ] ) )
			                zeon_meta::set( $post_id, 'tempo-layout', esc_attr( $_POST[ 'tempo-layout' ] ) );

			            if( isset( $_POST[ 'tempo-sidebar' ] ) )
			                zeon_meta::set( $post_id, 'tempo-sidebar', esc_attr( $_POST[ 'tempo-sidebar' ] ) );
			        }
			        else{
			            zeon_meta::delete( $post_id, 'tempo-overwrite-layout' );
			            zeon_meta::delete( $post_id, 'tempo-layout' );
			            zeon_meta::delete( $post_id, 'tempo-sidebar' );
			        }
			    }
			}

			function tempo__box_layout( $post )
			{
			    $overwrite = (bool)zeon_meta::get( $post -> ID, 'tempo-overwrite-layout', false );

				echo tempo_html::field( array(
					'title'			=> __( 'Overwrite', 'zeon' ),
					'description'	=> __( 'overwrite layout settings', 'zeon' ),
			        'format'    => 'linear',
			        'input'     => array(
			            'type'      => 'logic',
			            'slug'      => 'tempo-overwrite-layout',
			            'theme_mod' => false,
			            'value'     => $overwrite,
			            'action'    => '.tempo-overwrite-layout'
			        )
			    ));

			    $classes = 'tempo-overwrite-layout';

			    if( !$overwrite )
			        $classes = 'tempo-overwrite-layout hidden';

			    /**
			     *	Layout Options
			     */
			    echo '<div class="' . esc_attr( $classes ) . '">';

			    $default_layout = tempo_options::get( $post -> post_type . '-layout' );
			    $layout = zeon_meta::get( $post -> ID, 'tempo-layout', $default_layout );

			    $classes = 'tempo-layout-sidebar';

			    if( $layout == 'full' )
			        $classes = 'tempo-layout-sidebar hidden';

			    echo tempo_html::field( array(
			        'title'     => __( 'Layout', 'zeon' ),
			        'format'    => 'across',
			        'input'     => array(
			            'type'      => 'select',
			            'slug'      => 'tempo-layout',
			            'theme_mod' => false,
			            'value'     => $layout,
			            'options'   => array(
			                'left'      => __( 'Left Sidebar', 'zeon' ),
			                'full'      => __( 'Full Width', 'zeon' ),
			                'right'     => __( 'Right Sidebar', 'zeon' )
			            ),
			            'actions'	=> array(
			            	'show'		=> array( '.tempo-layout-sidebar' ),
			            	'full'		=> array(
			            		'hide'		=> array( '.tempo-layout-sidebar' )
			            	)
			            )
			        )
			    ));

			    $default_sidebar = tempo_options::get( $post -> post_type . '-sidebar' );

			    $default = array(
			        'main'          => __( 'Main Sidebar' , 'zeon' ),
			        'front-page'    => __( 'Front Page Sidebar' , 'zeon' ),
			        'page'          => __( 'Page Sidebar' , 'zeon' ),
			        'post'          => __( 'Single Post Sidebar' , 'zeon' )
			    );

			    $sidebars   = $default;
			    $custom     = tempo_validator::get_json( tempo_options::val( 'custom-sidebars' ) );

			    if( !empty( $custom ) && is_array( $custom ) )
			        $sidebars = array_merge( $default,  $custom );

			    echo tempo_html::field( array(
			        'title'     => __( 'Sidebar', 'zeon' ),
			        'class'     => $classes,
			        'format'    => 'across',
			        'input'     => array(
			            'type'      => 'select',
			            'slug'      => 'tempo-sidebar',
			            'theme_mod' => false,
			            'value'     => zeon_meta::get( $post -> ID, 'tempo-sidebar', $default_sidebar ),
			            'options'   => $sidebars
			        )
			    ));

			    echo '</div>';
			}
		}


		{	///// TEMPLATES //////

			function tempo__box_template_save( $post_id )
			{
				if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			        return;

			    if( isset( $_POST ) && !empty( $_POST ) ){
				    if( isset( $_POST[ 'page_template' ] ) && $_POST[ 'page_template' ] !== 'default' ){
			    		if( isset( $_POST[ 'tempo-template-tag' ] ) )
			                zeon_meta::set( $post_id, 'tempo-template-tag', esc_attr( $_POST[ 'tempo-template-tag' ] ) );

			            if( isset( $_POST[ 'tempo-template-category' ] ) )
			                zeon_meta::set( $post_id, 'tempo-template-category', esc_attr( $_POST[ 'tempo-template-category' ] ) );

			            if( isset( $_POST[ 'tempo-template-nr-columns' ] ) )
				            zeon_meta::set( $post_id, 'tempo-template-nr-columns', absint( $_POST[ 'tempo-template-nr-columns' ] ) );
				    }
				    else{
				    	zeon_meta::delete( $post_id, 'tempo-template-tag' );
						zeon_meta::delete( $post_id, 'tempo-template-category' );
						zeon_meta::delete( $post_id, 'tempo-template-nr-columns' );
				    }
				}
			}

			function tempo__box_template( $post )
			{
				$current = str_replace( '.php', '', zeon_meta::get( $post -> ID, '_wp_page_template' ) );

				$classes[ 'columns' ] 		= 'zeon-template-columns';
				$classes[ 'not-default'] 	= 'zeon-template-not-default';

				if( $current == 'default' || empty( $current ) ){
					$classes[ 'columns' ] 		.= ' hidden';
					$classes[ 'not-default'] 	.= ' hidden';
				}

				else if( $current !== 'default' ){
					if( !($current == 'blog-grid' || $current == 'blog-portfolio') ){
						$classes[ 'columns' ] 		.= ' hidden';
					}
				}

				// message
				echo tempo_html::notification( array(
					'type'			=> 'notify',
					'class'			=> 'zeon-template-default',
					'description'	=> __( 'The settings from this meta box are dedicated to customize the custom blog templates.', 'zeon' ),
					'style'			=> array(
						'margin-bottom' => '15px;'
					)
			    ));

				// tag
				$tags = get_tags();

				if( !empty( $tags ) ){
					$tag_options = array( __( ' -- Select a Tag --', 'zeon' ) );

					foreach( $tags as $tag )
						$tag_options[ $tag -> slug ] = $tag -> name . " ({$tag -> count})";

					echo tempo_html::field( array(
						'class'			=> $classes[ 'not-default' ],
						'title'			=> __( 'Tag', 'zeon' ),
						'description'	=> __( 'get articles just from a specified tag', 'zeon' ),
				        'format'    	=> 'across',
				        'input'     	=> array(
				            'type'      	=> 'select',
				            'slug'      	=> 'tempo-template-tag',
				            'theme_mod' 	=> false,
				            'value'     	=> esc_attr( zeon_meta::g( $post -> ID, 'tempo-template-tag' )),
				            'options'   	=> $tag_options
				        )
				    ));
				}

				// category
				$categories = get_categories();

				if( !empty( $categories ) ){
					$cat_options = array( __( ' -- Select a Category --', 'zeon' ) );

					foreach( $categories as $cat )
						$cat_options[ $cat -> slug ] = $cat -> name . " ({$cat -> count})";

					echo tempo_html::field( array(
						'class'			=> $classes[ 'not-default' ],
						'title'			=> __( 'Category', 'zeon' ),
						'description'	=> __( 'get articles just from a specified category', 'zeon' ),
				        'format'    	=> 'across',
				        'input'     	=> array(
				            'type'      	=> 'select',
				            'slug'      	=> 'tempo-template-category',
				            'theme_mod' 	=> false,
				            'value'     	=> esc_attr( zeon_meta::g( $post -> ID, 'tempo-template-category' )),
				            'options'   	=> $cat_options
				        )
				    ));
				}

				// columns
				echo tempo_html::field( array(
					'class'			=> $classes[ 'columns' ],
					'title'			=> __( 'Number of Columns', 'zeon' ),
			        'format'    	=> 'across',
			        'input'     	=> array(
			            'type'      	=> 'select',
			            'slug'      	=> 'tempo-template-nr-columns',
			            'theme_mod' 	=> false,
			            'value'     	=> absint( zeon_meta::get( $post -> ID, 'tempo-template-nr-columns', 2 ) ),
			            'options'   	=> array(
			                2       		=> number_format_i18n( 2 ),
                			3       		=> number_format_i18n( 3 )
			            )
			        )
			    ));
			}
		}

		{	/////	SINGLE SETTINGS

			function tempo__box_single_settings_save( $post_id )
			{
				if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			        return;

			    if( isset( $_POST ) && !empty( $_POST ) ){

			    	// overwrite breadcrumbs
			    	if( isset( $_POST[ 'tempo-overwrite-breadcrumbs' ] ) && (bool)$_POST[ 'tempo-overwrite-breadcrumbs' ] ){
			    		zeon_meta::set( $post_id, 'tempo-overwrite-breadcrumbs', 1 );

			    		$breadcrumbs = isset( $_POST[ 'tempo-breadcrumbs' ] ) && (bool)$_POST[ 'tempo-breadcrumbs' ] ? 1 : 0;
			    		zeon_meta::set( $post_id, 'tempo-breadcrumbs', $breadcrumbs );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-overwrite-breadcrumbs' );
			     		zeon_meta::delete( $post_id, 'tempo-breadcrumbs' );
			     	}

			     	// overwrite content width
			     	if( isset( $_POST[ 'tempo-overwrite-content-width' ] ) && (bool)$_POST[ 'tempo-overwrite-content-width' ] ){
			    		zeon_meta::set( $post_id, 'tempo-overwrite-content-width', 1 );

			    		$width = isset( $_POST[ 'tempo-content-width' ] ) ? absint( $_POST[ 'tempo-content-width' ] ) : 0;

			    		if( $width == 8 || $width == 10 )
			    			zeon_meta::set( $post_id, 'tempo-content-width', $width );
			     	}

			     	else{
			     		zeon_meta::delete( $post_id, 'tempo-overwrite-content-width' );
			     		zeon_meta::delete( $post_id, 'tempo-content-width' );
			     	}
			    }
			}

			function tempo__box_single_settings( $post )
			{
				$overwrite_breadcrumbs = (bool)zeon_meta::g( $post -> ID, 'tempo-overwrite-breadcrumbs' );

				echo tempo_html::field(array(
	    			'title'			=> __( 'Overwrite Breadcrumbs Settings', 'zeon' ),
					'description'	=> __( 'overwrite default breadcrumbs settings for this page.', 'zeon' ),
			        'format'    	=> 'linear',
			        'input'     	=> array(
			            'type'      	=> 'logic',
			            'slug'			=> 'tempo-overwrite-breadcrumbs',
			            'theme_mod' 	=> false,
			            'value'     	=> $overwrite_breadcrumbs,
			            'action'    	=> '.tempo-overwrite-breadcrumbs-wrapper'
			        )
	    		));

	    		$classes = 'tempo-overwrite-breadcrumbs-wrapper';

			    if( !$overwrite_breadcrumbs )
			        $classes = 'tempo-overwrite-breadcrumbs-wrapper hidden';

				$breadcrumbs = (bool)zeon_meta::get( $post -> ID, 'tempo-breadcrumbs', tempo_options::get( 'breadcrumbs' ) );

				echo tempo_html::field(array(
	    			'title'			=> __( 'Display Breadcrumbs', 'zeon' ),
	    			'class'			=> $classes,
			        'format'    	=> 'linear',
			        'input'     	=> array(
			            'type'      	=> 'logic',
			            'slug'			=> 'tempo-breadcrumbs',
			            'theme_mod' 	=> false,
			            'value'     	=> $breadcrumbs
			        )
	    		));

				$overwrite_width = (bool)zeon_meta::g( $post -> ID, 'tempo-overwrite-content-width' );

	    		echo tempo_html::field(array(
	    			'title'			=> __( 'Overwrite Content Width Settings', 'zeon' ),
					'description'	=> __( 'overwrite default Content Width settings for this page.', 'zeon' ),
			        'format'    	=> 'linear',
			        'input'     	=> array(
			            'type'      	=> 'logic',
			            'slug'			=> 'tempo-overwrite-content-width',
			            'theme_mod' 	=> false,
			            'value'     	=> $overwrite_width,
			            'action'    	=> '.tempo-overwrite-content-width-wrapper'
			        )
	    		));

	    		$classes = 'tempo-overwrite-content-width-wrapper';

			    if( !$overwrite_width )
			        $classes = 'tempo-overwrite-content-width-wrapper hidden';

	    		$width = absint( zeon_meta::get( $post -> ID, 'tempo-content-width', tempo_options::get( 'content-width' ) ) );

	    		echo tempo_html::field(array(
	    			'title'			=> __( 'Content Width', 'zeon' ),
	    			'class'			=> $classes,
			        'format'    	=> 'across',
			        'input'     	=> array(
			            'type'      	=> 'select',
			            'slug'			=> 'tempo-content-width',
			            'theme_mod' 	=> false,
			            'value'     	=> $width,
			            'options'		=> array(
			            	8 				=> '750 px',
			            	10 				=> '945 px'
			            )
			        )
	    		));
			}
		}

		{	///// SLIDESHOW /////

		}

		{	///// MEDIA LIBARARY


			function tempo__field_custom_url( $fields, $post )
			{
				$fields[ 'tempo_gallery_url' ] = array(
					'label' 	=> __( 'Gallery URL', 'zeon' ),
					'input' 	=> 'url',
					'value' 	=> esc_url( zeon_meta::g( $post -> ID, '_tempo_gallery_url' ) )
				);

				$target = esc_attr( zeon_meta::g( $post -> ID, '_tempo_gallery_url_target' ) );

				$fields[ 'tempo_gallery_url_target' ] = array(
					'label' 	=> __( 'URL Target', 'zeon' ),
					'input'		=> 'html',
					'html'		=>
						'<select name="attachments[' . $post -> ID . '][tempo_gallery_url_target]" id="attachments[' . $post -> ID . '][tempo_gallery_url_target]" class="zeon-select">
							<option value="_self" ' . selected( $target, '_self', false ) . '>' . __( 'Same Window', 'zeon' ).'</option>
							<option value="_blank" ' . selected( $target, '_blank', false ) . '>' . __( 'New blank Window', 'zeon' ).'</option>
						</select>'
				);

				return $fields;
			}

			add_filter( 'attachment_fields_to_edit', 'tempo__field_custom_url', 10, 2 );

			function tempo__field_custom_url_save( $post, $attachment )
			{
				if( isset( $attachment[ 'tempo_gallery_url' ] ) )
					zeon_meta::set( absint( $post[ 'ID' ] ), '_tempo_gallery_url', esc_url( $attachment[ 'tempo_gallery_url' ] ) );

				if( isset( $attachment[ 'tempo_gallery_url_target' ] ) )
					zeon_meta::set( absint( $post[ 'ID' ] ), '_tempo_gallery_url_target', esc_attr( $attachment[ 'tempo_gallery_url_target' ] ) );

				return $post;
			}

			add_filter( 'attachment_fields_to_save', 'tempo__field_custom_url_save', 10, 2 );

		}
	}

?>
