<?php

    /**
     *  Related Posts ( with thumbnail )
     */

    if( !class_exists( 'zeon_widget_related_posts' ) ){

        class zeon_widget_related_posts extends WP_Widget
        {
            /**
             *  Widget Constructor
             */
            function __construct()
            {
                parent::__construct( 'zeon_widget_related_posts', __( 'Related Posts', 'zeon' ) . ' [zeon]', array( 
                    'classname'     => 'zeon_widget_posts',
                    'description'   => __( 'Related Posts by Taxonomy. This widget can be used only for single post template.', 'zeon' )
                ));
            }

            /**
             *  Widget Preview
             */

            function widget( $args, $instance )
            {
                global $post;

                // extract args
                extract( $args , EXTR_SKIP );

                $instance   = wp_parse_args( (array) $instance, array(
                    'title'     => __( 'Related Posts', 'zeon' ),
                    'tax'       => 'post_tag',
                    'number'    => 5
                ));

                $title      = sanitize_text_field( $instance[ 'title' ] );
                $tax        = esc_attr( $instance[ 'tax' ] );
                $number     = absint( $instance[ 'number' ] );
                $posts      = tempo_related_posts( $post, $tax, $number );

                if( count( $posts ) ){

                    echo $before_widget;

                    if( !empty( $title ) ){
                        echo $before_title;
                        echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
                        echo $after_title;
                    }

                    echo '<ul>';

                    foreach( $posts as $p ){

                        echo '<li>';

                        $thumbnail      = get_post( get_post_thumbnail_id( $p ) );
                        $post_thumbnail = has_post_thumbnail( $p ) && isset( $thumbnail -> ID );

                        // post thumbnail image
                        if( $post_thumbnail ){

                            // post thumbnail wrapper
                            echo '<div class="post-thumbnail-wrapper overflow-wrapper">';

                            echo get_the_post_thumbnail( $p -> ID, 'tempo-classic' , array(
                                'alt'   => esc_attr( get_the_title( $p ) ),
                                'class' => 'post-thumbnail effect-scale'
                            ));

                            // post thumbnail mask + link
                            echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p ) ) . '" ' . tempo_flex_container_class( null, 'tempo-valign-middle' ) . ' target="_blank">';
                            echo '<span ' . tempo_flex_item_class( null, 'tempo-align-center' ) . '>';
                            echo __( 'Read Article', 'zeon' );
                            echo '</span>';
                            echo '</a>';

                            echo '</div>';
                        }

                        // post title
                        echo '<h5>';
                        echo '<a href="' . esc_url( get_permalink( $p -> ID ) ) . '" title="' . esc_attr( get_the_title( $p ) ) . '">';
                        echo get_the_title( $p -> ID );
                        echo '</a>';
                        echo '</h5>';

                        // post meta / date
                        $y          = esc_attr( get_post_time( 'Y', false, $p ) );
                        $m          = esc_attr( get_post_time( 'm', false, $p ) );
                        $d          = esc_attr( get_post_time( 'd', false, $p ) );
                        $time       = esc_attr( get_post_time( 'Y-m-d', false, $p ) );
                        $wp_time    = get_post_time( esc_attr( get_option( 'date_format' ) ), false , $p, true );

                        echo '<span class="post-meta time">';
                        echo '<i class="tempo-icon-clock-1"></i> ';
                        echo '<a href="' . esc_url( get_day_link( $y , $m , $d ) )  . '" title="' . sprintf( __( 'posted on %s', 'zeon' ), $wp_time ) . '">';
                        echo '<time datetime="' . esc_attr( $time ) . '">' . $wp_time . '</time>';
                        echo '</a>';
                        echo '</span>';

                        echo '<div class="clear clearfix"></div>';

                        echo '</li>';
                    }

                    echo '</ul>';

                    echo $after_widget;
                }
            }

            /**
             *  Widget Update
             */

            function update( $new_instance, $old_instance )
            {
                $new_instance = wp_parse_args( (array) $new_instance, array(
                    'title'         => null,
                    'tax'           => null,
                    'number'        => null
                ));

                $instance                   = $old_instance;

                $instance[ 'title' ]        = sanitize_text_field( $new_instance[ 'title' ] );
                $instance[ 'tax' ]          = esc_attr( $new_instance[ 'tax' ] );
                $instance[ 'number' ]       = absint( $new_instance[ 'number' ] );

                return $instance;
            }

            /**
             *  Widget Form ( admin side )
             */

            function form( $instance )
            {
                $instance   = wp_parse_args( (array) $instance, array(
                    'title'     => null,
                    'tax'       => 'post_tag',
                    'number'    => 5
                ));

                $title      = sanitize_text_field( $instance[ 'title' ] );
                $tax        = esc_attr( $instance[ 'tax' ] );
                $number     = absint( $instance[ 'number' ] );

                if( empty( $number ) )
                    echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "Number of posts"', 'zeon' ) . '</p>';

                // title
                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
                echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
                echo '</label>';
                echo '</p>';

                // taxonomy
                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'tax' ) . '">' . __( 'Related posts by', 'zeon' );

                echo tempo_html::input(array(
                    'id'        => $this -> get_field_id( 'tax' ),
                    'name'      => $this -> get_field_name( 'tax' ),
                    'class'     => 'widefat',
                    'type'      => 'select',
                    'theme_mod' => false,
                    'value'     => esc_attr( $tax ),
                    'options'   => array(
                        'post_tag'  => __( 'Tags', 'zeon' ),
                        'category'  => __( 'Categories', 'zeon' )
                    )
                ));

                echo '</label>';
                echo '</p>';

                // number
                echo '<p>';
                echo '<label for="' . $this -> get_field_id( 'number' ) . '">' . __( 'Number of posts', 'zeon' );
                echo '<input type="number" class="widefat" id="' . $this -> get_field_id( 'number' ) . '" name="' . $this -> get_field_name( 'number' ) . '" value="' . absint( $number ) . '" />';
                echo '</label>';
                echo '</p>';
            }
        }
    }
?>
