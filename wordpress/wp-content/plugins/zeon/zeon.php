<?php
/*
Plugin Name: Zeon
Plugin URI: http://mythem.es/item/zeon-wordpress-plugin-extends-tempo-free-wordpress-theme/
Description: This is a GPL 2 WordPress plugin. With this plugin can be extended functionality for the theme Tempo and Tempo child themes. Tempo is a free WordPress theme. More about Tempo free WordPress Theme: http://mythem.es/items/tempo-freemium-wordpress-theme/
Version: 0.0.13
Author: myThem.es
Author URI: http://mythem.es/
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/*
    Copyright 2015 myThem.es ( email: zeon at mythem.es )

    Zeon, Copyright 2015 myThem.es
    Zeon is distributed under the terms of the GNU GPL


                     ________________
                    |_____    _______|
     ___ ___ ___   __ __  |  |  __       ____   ___ ___ ___       ____   ____
    |           | |_ |  | |  | |  |___  |  __| |           |     |  __| |  __|
    |   |   |   |  | |  | |  | |  __  | |  __| |   |   |   |  _  |  __| |__  |
    |___|___|___|   |  |  |__| |_ ||_ | |____| |___|___|___| |_| |____| |____|
                    |_|


*/


    /**
     *  Check the plugin compatibility
     *  Run plugin if is active theme Tempo or a Tempo Child theme.
     */

    $theme = wp_get_theme();

    if( $theme -> get( 'Name' ) == 'Tempo' || $theme -> get( 'Template' ) == 'tempo' ){

        /**
         *  Zeon Plugin Directory
         */
        function zeon_plugin_dir()
        {
            return rtrim( dirname( __FILE__ ), '/' );
        }

        /**
         *  Zeon Plugin Uri
         */
        function zeon_plugin_uri()
        {
            return rtrim( plugin_dir_url( __FILE__ ), '/' );
        }

        include_once zeon_plugin_dir() . '/includes/lib/main.php';
    }
?>
