<?php
    if( !class_exists( 'zeon_scripts' ) ){

    class zeon_scripts
    {
        static function modules()
        {
            $ver = tempo_core::theme( 'Version' );

            $modules = (array)tempo_cfgs::get( 'modules' );

            if( !empty( $modules ) ){
                foreach( $modules as $module => $cfgs ){
                    if( !isset( $cfgs[ 'require' ] ) )
                        continue;

                    foreach( $cfgs[ 'require' ] as $key => $uri ){

                        if( $key == 'style' ){
                            wp_register_style( "tempo-{$module}", $uri , null, $ver, true );
                            wp_enqueue_style( "tempo-{$module}" );
                            continue;
                        }

                        if( $key == 'module' ){
                            wp_register_script( "tempo-{$module}", $uri, null, $ver, true );
                            wp_localize_script( "tempo-{$module}", $module, array(
                                'args' => isset( $cfgs[ 'args' ] ) ? (array)$cfgs[ 'args' ] : array()
                            ));
                            wp_enqueue_script( "tempo-{$module}" );
                            continue;
                        }

                        wp_register_script( "tempo-{$module}-{$key}", $uri, null, $ver, true );
                        wp_enqueue_script( "tempo-{$module}-{$key}" );
                    }
                }
            }
        }

        static function footer()
        {
            $ver = tempo_core::theme( 'Version' );

            /**
             *  Modules
             */

            self::modules();


            /**
             *  Additional JavaScript Files
             */

            $js_files = apply_filters( 'zeon_js_files', (array)tempo_cfgs::get( 'js-files' ) );

            foreach( $js_files as $js_file_index => $js_file_uri ){
                wp_register_script( $js_file_index, $js_file_uri, null, $ver, true );
                wp_enqueue_script( $js_file_index );
            }
        }

        static function admin( $hook )
        {
            if( $hook !== 'appearance_page_tempo-faq' && $hook !== 'toplevel_page_zeon-sidebars' && $hook !== 'widgets.php' &&
                $hook !== 'zeon_page_zeon-custom-sidebars' && $hook !== 'post-new.php' && $hook !== 'post.php'  )
                return;

            $ver = tempo_core::theme( 'Version' );

            wp_enqueue_script( 'tempo-admin-html',                  get_template_directory_uri() . '/media/admin/js/tempo.html.js', array( 'jquery', 'wp-color-picker', 'jquery-ui-draggable' ), $ver );

            wp_register_style( 'tempo-fontello',                    get_template_directory_uri() . '/media/css/fontello.css', null, $ver );
            wp_register_style( 'tempo-admin-page',                  get_template_directory_uri() . '/media/admin/css/page.css', null, $ver );
            wp_register_style( 'tempo-admin-box',                   get_template_directory_uri() . '/media/admin/css/box.css', null, $ver );
            wp_register_style( 'tempo-admin-additional',            get_template_directory_uri() . '/media/admin/css/additional.css', null, $ver );
            wp_register_style( 'tempo-admin-fields',                get_template_directory_uri() . '/media/admin/css/fields.css', null, $ver );

            wp_enqueue_style( 'tempo-fontello' );
            wp_enqueue_style( 'tempo-admin-page' );
            wp_enqueue_style( 'tempo-admin-box' );
            wp_enqueue_style( 'tempo-admin-additional' );
            wp_enqueue_style( 'tempo-admin-fields' );
        }

        static function admin_footer()
        {
            $ver = tempo_core::theme( 'Version' );

            /**
             *  Modules
             */

            self::modules();


            /**
             *  For additional JavaScript Files
             */

            $js_files = apply_filters( 'zeon_admin_js_files', (array)tempo_cfgs::get( 'admin-js-files' ) );

            foreach( $js_files as $js_file_index => $js_file_uri ){
                wp_register_script( $js_file_index, $js_file_uri, null, $ver, true );
                wp_enqueue_script( $js_file_index );
            }
        }
    }

}   /* END IF CLASS EXISTS */
?>
