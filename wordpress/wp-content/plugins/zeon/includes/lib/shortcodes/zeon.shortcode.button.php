<?php

    /**
     *  [button id="" type="" size="default[extra-large|large|small]"][/button]
     */

    class zeon_shortcode_button
    {
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            $attr = shortcode_atts( array(
                'size'          => 'normal',
                'url'           => '#',
                'color'         => '#ffffff'
            ), $_attr );

            // custom style
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            $rett = '<a class="tempo-shortcode button ' . esc_attr( $attr[ 'size' ] ) . '" href="' . esc_url( $attr[ 'url' ] ) . '" target="_blank" ' . $style . '>' . do_shortcode( $_content ) . '</a>';

            return $rett;
        }

        public $code = null;

        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'         => __( 'Size', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'select',
                        'name'          => 'button-size',
                        'theme_mod'     => false,
                        'default'       => 'normal',
                        'options'       => array(
                            'small'         => __( 'Small' , 'zeon' ),
                            'normal'        => __( 'Normal' , 'zeon' ),
                            'large'         => __( 'Large' , 'zeon' )
                        ),
                        'actions'    => array(
                            'show'      => array( '.zeon-button-size' ),
                            'normal'    => array(
                                'hide'      => array( '.zeon-button-size' )
                            )
                        )
                    )
                ),
                array(
                    'title'         => __( 'Text Color', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'color',
                        'name'          => 'button-color',
                        'theme_mod'     => false,
                        'default'       => '#ffffff'
                    )
                ),
                array(
                    'title'         => __( 'Background Color', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'color',
                        'name'          => 'button-bg-color',
                        'theme_mod'     => false,
                        'default'       => '#337ab7',
                    )
                )
            );

            $boxes[ 'left' ][] = array(
                'title'     => __( 'Button Settings', 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );

            $fields[ 'content' ] = array(
                array(
                    'title'         => __( 'Text / Label', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'text',
                        'name'          => 'button-text',
                        'theme_mod'     => false,
                        'default'       => __( 'Text / Label' , 'zeon' ),
                    )
                ),
                array(
                    'title'         => __( 'Description', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'text',
                        'name'          => 'button-description',
                        'theme_mod'     => false,
                        'default'       => __( 'a short description for the attribute title' , 'zeon' ),
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'url',
                        'name'          => 'button-url',
                        'theme_mod'     => false,
                        'default'       => esc_url( home_url() )
                    )
                ),
                array(
                    'title'         => __( 'Open the Url in a new browser window', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'logic',
                        'name'          => 'button-target',
                        'theme_mod'     => false,
                        'action'        => '.button-target.attr'
                    )
                )
            );

            $boxes[ 'right' ][] = array(
                'title'     => __( 'Button Content', 'zeon' ),
                'fields'    => $fields[ 'content' ]
            );


            $text               = '<span class="button-text text"><span class="value">' . __( 'Text / Label', 'zeon' ) . '</span></span>';
            $description        = '<span class="button-description attr">description="<span class="value">' . __( 'a short description for the attribute title', 'zeon' ) . '</span>"</span>';
            $url                = '<span class="button-url attr">url="<span class="value">' . esc_url( home_url() ) . '</span>"</span>';
            $open_new_window    = '<span class="button-target attr hidden">open_new_window="<span class="value">false</span>"</span>';

            $size               = '<span class="button-size zeon-button-size attr hidden">size="<span class="value">normal</span>"</span>';
            $color              = '<span class="button-color attr">color="<span class="value">#ffffff</span>"</span>';
            $bg_color           = '<span class="button-bg-color attr">bg_color="<span class="value">#337ab7</span>"</span>';

            $this -> code       = '[zeon_button ' . $url . ' ' . $description . ' ' . $size . ' ' . $color . ' ' . $bg_color . ' ' . $open_new_window . ']' . $text . '[/zeon_button]';

            return array(
                array(
                    'boxes' => $boxes['left']
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }
    }
?>
