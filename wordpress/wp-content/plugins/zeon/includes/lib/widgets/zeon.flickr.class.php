<?php

	/**
     *  Flickr Gallery Widget
     */

	if( !class_exists( 'zeon_widget_flickr' ) ){

		class zeon_widget_flickr extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_flickr', __( 'Flickr Gallery', 'zeon' ) . ' [zeon]', array( 
		            'classname'     => 'zeon_widget_flickr',
		            'description'   => __( 'The small photos gallery from flickr.com', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		        // extract args
		        extract( $args, EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		            'title'     => __( 'Photo Gallery' , 'zeon' ),
		            'id'        => '',
		            'number'    => '',
		            'showing'   => ''
		        ));

		        $title      = sanitize_text_field( $instance[ 'title' ] );
		        $id         = esc_attr( $instance[ 'id' ] );
		        $number     = absint( $instance[ 'number' ] );
		        $showing    = esc_attr( $instance[ 'showing' ] );

		        if( !empty( $id ) ){
		            echo $before_widget;

		            if( !empty( $title ) ){
		                echo $before_title;
		                echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
		                echo $after_title;
		            }

		            if( !empty( $id ) && $number > 0 ){
		                echo '<div class="flickr">';
		                echo '<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=' . absint( $number ) . '&amp;display=' . esc_attr( $showing ) . '&amp;size=s&amp;layout=x&amp;source=user&amp;user=' . esc_attr( $id ) . '"></script>';
		                echo '<div class="clearfix"></div>';
		                echo '</div>';
		            }
		            else{
		                echo '<p>' . __( 'The Flickr ID is invalid !', 'zeon' ) . '</p>';
		            }

		            echo $after_widget;
		        }
		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               = $old_instance;
		        $instance[ 'id' ]       = esc_attr( strip_tags( $new_instance[ 'id' ] ) );
		        $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );
		        $instance[ 'number' ]   = absint( $new_instance[ 'number' ] );
		        $instance[ 'showing' ]  = esc_attr( strip_tags( $new_instance[ 'showing' ] ) );

		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		    	$instance = wp_parse_args( (array) $instance, array(
		            'title'     => '',
		            'id'        => '',
		            'number'    => '',
		            'showing'   => ''
		        ));

		        $title      = sanitize_text_field( $instance[ 'title' ] );
		        $id         = esc_attr( $instance[ 'id' ] );
		        $number     = absint( $instance[ 'number' ] );
		        $showing    = esc_attr( $instance[ 'showing' ] );

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' ) . ':';
		        echo '<input class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" type="text" value="' . sanitize_text_field( $title ) . '"/>';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'id' ) . '">' . __( 'Flickr ID', 'zeon' ) . ' (<a target="_blank" href="http://www.idgettr.com/">idGettr</a>):';
		        echo '<input class="widefat" id="' . $this -> get_field_id( 'id' ) . '" name="' . $this -> get_field_name( 'id' ) . '" type="text" value="' . esc_attr( $id ) . '"/>';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'number' ) . '">' . __( 'Number of photos', 'zeon' ) . ':';
		        echo '<input class="widefat" id="' . $this -> get_field_id( 'number' ) . '" name="' . $this -> get_field_name( 'number' ) . '" type="text" value="' . absint( $number ) . '"/>';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'showing' ) . '">' . __( 'Showing Method', 'zeon' ) . ':';
		        echo '<select size="1" name="' . $this -> get_field_name( 'showing' ) . '">';
		        echo '<option value="random" ' . selected( esc_attr( $showing ), 'random', false ) . '>' . __( 'Random Photo', 'zeon' ) . '</option>';
		        echo '<option value="latest" ' . selected( esc_attr( $showing ), 'latest', false ) . '>' . __( 'Latest Photo', 'zeon' ) . '</option>';
		        echo '</select>';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
