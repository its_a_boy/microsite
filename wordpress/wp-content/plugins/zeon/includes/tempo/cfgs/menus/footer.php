<?php
	
	/**
	 *	Footer Menu - config file
	 */

	$cfgs = array_merge( (array)tempo_cfgs::get( 'menus' ), array(
		'tempo-footer'	=> __( 'Footer Menu', 'zeon' )
	));

	tempo_cfgs::set( 'menus', $cfgs );
?>