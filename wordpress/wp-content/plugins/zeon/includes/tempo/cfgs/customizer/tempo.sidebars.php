<?php

	/**
     *  Appearance / Customize / Sidebars - config settings
     */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'tempo-sidebars' => array(
			'title'			=> __( 'Sidebars' , 'zeon' ),
			'priority' 		=> 46,
			'sections'		=> array(
				'tempo-header-front-page-sidebar' => array(
					'title'		=> __( 'Header Front Page' , 'zeon' ),
					'description' 	=> sprintf( __( 'Inside of the each Header or Footer Sidebar the widgets will be arranged in columns. For each Header or Footer Sidebar you can customize the number of columns. %s' , 'zeon' ), '<br/><br/><a href="' . esc_url( admin_url( '?page=zeon-sidebars' ) ) . '">' . __( 'Customize the Nr. of Columns &rarr;', 'zeon' ) . '</a>' ),
					'fields'	=> array(
						'header-front-page-sidebar-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#f6f7fa'
							)
						),
						'header-front-page-sidebar-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-front-page-sidebar-space' => array(
							'title'			=> __( 'Blank Space', 'zeon' ),
							'description'	=> __( 'The space above and below the sidebars content', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 75
							)
						),
						'header-front-page-widgets-space' => array(
							'title'			=> __( 'Widgets Space', 'zeon' ),
							'description'	=> __( 'The space between widgets', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 25
							)
						),
					)
				),
				'tempo-header-blog-sidebar' => array(
					'title' 	=> __( 'Header Blog', 'zeon' ),
					'description' 	=> sprintf( __( 'Inside of the each Header or Footer Sidebar the widgets will be arranged in columns. For each Header or Footer Sidebar you can customize the number of columns. %s' , 'zeon' ), '<br/><br/><a href="' . esc_url( admin_url( '?page=zeon-sidebars' ) ) . '">' . __( 'Customize the Nr. of Columns &rarr;', 'zeon' ) . '</a>' ),
					'fields'	=> array(
						'header-blog-sidebar-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#f6f7fa'
							)
						),
						'header-blog-sidebar-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'header-blog-sidebar-space' => array(
							'title'			=> __( 'Blank Space', 'zeon' ),
							'description'	=> __( 'The space above and below the sidebars content', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 75
							)
						),
						'header-blog-widgets-space' => array(
							'title'			=> __( 'Widgets Space', 'zeon' ),
							'description'	=> __( 'The space between widgets', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 25
							)
						),
					)
				),
				'tempo-footer-light-sidebar' => array(
					'title' 	=> __( 'Footer Light Section', 'zeon' ),
					'description' 	=> sprintf( __( 'Inside of the each Header or Footer Sidebar the widgets will be arranged in columns. For each Header or Footer Sidebar you can customize the number of columns. %s' , 'zeon' ), '<br/><br/><a href="' . esc_url( admin_url( '?page=zeon-sidebars' ) ) . '">' . __( 'Customize the Nr. of Columns &rarr;', 'zeon' ) . '</a>' ),
					'fields'	=> array(
						'footer-light-sidebar-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#f6f7fa'
							)
						),
						'footer-light-sidebar-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100
							)
						),
						'footer-light-sidebar-space' => array(
							'title'			=> __( 'Blank Space', 'zeon' ),
							'description'	=> __( 'The space above and below the sidebars content', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 75
							)
						),
						'footer-light-widgets-space' => array(
							'title'			=> __( 'Widgets Space', 'zeon' ),
							'description'	=> __( 'The space between widgets', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 25
							)
						),
					)
				),
				'tempo-footer-dark-sidebar' => array(
					'title' 	=> __( 'Footer Dark Section', 'zeon' ),
					'description' 	=> sprintf( __( 'Inside of the each Header or Footer Sidebar the widgets will be arranged in columns. For each Header or Footer Sidebar you can customize the number of columns. %s' , 'zeon' ), '<br/><br/><a href="' . esc_url( admin_url( '?page=zeon-sidebars' ) ) . '">' . __( 'Customize the Nr. of Columns &rarr;', 'zeon' ) . '</a>' ),
					'fields'	=> array(
						'footer-dark-sidebar-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#000000'
							)
						),
						'footer-dark-sidebar-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 70
							)
						),
						'footer-dark-sidebar-space' => array(
							'title'			=> __( 'Blank Space', 'zeon' ),
							'description'	=> __( 'The space above and below the sidebars content', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 50
							)
						),
						'footer-dark-widgets-space' => array(
							'title'			=> __( 'Widgets Space', 'zeon' ),
							'description'	=> __( 'The space between widgets', 'zeon' ),
							'input'			=> array(
								'type'			=> 'range',
								'max'			=> 120,
								'unit'			=> 'px',
								'default'		=> 45
							)
						),
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>
