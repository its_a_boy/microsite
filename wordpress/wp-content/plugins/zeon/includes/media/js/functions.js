jQuery(document).ready(function(){
	jQuery( 'select#page_template' ).change(function(){
		var val = jQuery( this ).val().toString().replace( ".php", "" );

		var box = jQuery( 'div#tempo-template-settings' );

		console.log( val );

		if( val == 'default' ){
			jQuery( box ).find( '.zeon-template-columns' ).hide( 'fast' );
			jQuery( box ).find( '.zeon-template-not-default' ).hide( 'fast' );
		}

		else if( val !== 'default' ){
			if( !(val == 'blog-grid' || val == 'blog-portfolio') ){
				jQuery( box ).find( '.zeon-template-columns' ).hide( 'fast' );
			}
			else{
				jQuery( box ).find( '.zeon-template-columns' ).show( 'slow' );
			}

			jQuery( box ).find( '.zeon-template-not-default' ).show( 'slow' );
		}
	});
});
