<?php

    /*
        [icon type="ok-squared"/]
    */

    class zeon_shortcode_icon
    {
        static function run( $_attr, $_content = null )
        {
            $attr = shortcode_atts( array(
                'type'  => 'ok-squared'
            ) , $_attr );

            /* STYLE AND PROPERTIES */
            $prop = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( isset( $_attr[ 'size' ] ) ){
                $prop .= 'font-size: ' . absint( $_attr[ 'size' ] ) . 'px; line-height: ' . absint( $_attr[ 'size' ] ) . 'px;';
            }

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            return '<i class="tempo-shortcode icon tempo-icon-' . esc_attr( $attr[ 'type' ] ) . '" ' . $style . '></i>';
        }

        public $code = null;
        public function builder()
        {
            $fields     = array();
            $boxes      = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Icon', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'icon_select',
                        'name'      => 'icon-name',
                        'default'   => 'tempo-icon-heart-1',
                        'theme_mod' => false,
                        'options'   => tempo_cfgs::get( 'icons' )
                    )
                ),
                array(
                    'title'     => __( 'Size' , 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'icon-size',
                        'default'   => 18,
                        'theme_mod' => false
                    )
                ),
                array(
                    'title'     => __( 'Color' , 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'icon-color',
                        'default'   => '#333333',
                        'theme_mod' => false
                    )
                )
            );

            $boxes[] = array(
                'title'     => __( 'Icon Settings', 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );

            $icon           = '<span class="icon-name attr">type="<span class="value">heart-1</span>"</span>';
            $size           = '<span class="icon-size attr">size="<span class="value">18</span>"</span>';
            $color          = '<span class="icon-color attr">color="<span class="value">#333333</span>"</span>';

            $this -> code   = '[zeon_icon ' . $icon . ' ' . $size . ' ' . $color . '/]';

            return array(
                array(
                    'boxes' => $boxes
                )
            );
        }
    }
?>
