<?php global $post ?>

<div <?php echo zeon_columns_class( 'tempo-grid-wrapper' ) ?>>

<article <?php post_class( 'tempo-article grid' ); ?>>

	<?php tempo_get_template_part( 'templates/article/thumbnail', 'grid' ); ?>

	<div class="tempo-content">

	    <?php tempo_get_template_part( 'templates/article/terms/categories', 'grid' ); ?>

	    <h3 class="tempo-title">

		    <?php if( !empty( $post -> post_title ) ) { ?>

		        <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title( $post ) ); ?>"><?php the_title(); ?></a>

		    <?php } else { ?>

		        <a href="<?php the_permalink() ?>"><?php _e( 'Read more about ..' , 'zeon' ) ?></a>

		    <?php } ?>
		</h3>


	    <div class="tempo-hentry">
	    	<p>
		    <?php
		    	if( !empty( $post -> post_excerpt )  ){
		    		echo mb_substr( strip_tags( strip_shortcodes( $post -> post_excerpt ) ), 0, 222 ) . '...';
		        }
		        else{
		            echo mb_substr( strip_tags( strip_shortcodes( $post -> post_content ) ), 0, 222 ) . '...';
		        }
		    ?>
		    </p>
	    </div>

	    <?php tempo_get_template_part( 'templates/article/meta/bottom', 'grid' ); ?>

    </div>

</article>

</div>