<?php
	$image 				= esc_url( get_header_image() );

    if( empty( $image ) )
        return;

    $headline_text   	= tempo_options::get( 'header-headline-text' );
    $description_text   = tempo_options::get( 'header-description-text' );

    /**
     *  Header Image Alt Attribute
     */

    $header_image_alt   = $headline_text;

    if( !(empty( $headline_text ) || empty( $description_text )) ){
        $header_image_alt .=  ' - ' . $description_text;
    }

    else if( !empty( $description_text ) ){
        $header_image_alt .=  $description_text;
    }

?>
	<div class="parallax" style="background-image: url(<?php echo esc_url( $image ); ?>);">
        <img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $header_image_alt ); ?>" class="parallax-image"/>
    </div>
