<?php

    /*
        [message_box bg_color="" bg_opacity="" top="" bottom=""]

            [message_text type="big/medium/small" color=""]Long content[/message_text]
            [message_button type="big/medium/small" url="" bg_color="" color=""]Label[/message_button]

        [/message_box]

    */

    class zeon_shortcode_message
    {
        private static $run = false;
        private static $C3X4 = 12;

        static function run( $_attr, $_content )
        {
            $rett = '';

            /* BREAK IF NULL CONTENT */
            if( empty( $_content ) ){
                return $rett;
            }

            self::$run = true;

            $attr = shortcode_atts( array(
                'bg_color'      => '#f9f9f9',
                'bg_opacity'    => 80,
                'top'           => 35,
                'bottom'        => 35,
            ), $_attr );


            /* STYLE AND CUSTOM PROPERTIES */
            $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            /* CONTENT */
            $content = do_shortcode( strip_tags( $_content ) );

            $rett .= '<div class="tempo-shortcode message" ' . $style . '>';
            $rett .= '<div class="row">';
            $rett .= str_replace( '-C3X4' , '-' . self::$C3X4, $content );
            $rett .= '</div>';
            $rett .= '</div>';

            self::$run = false;
            self::$C3X4 = 12;

            /* ALLOW DO FOR NEXT MESSGE BOX */
            zeon_shortcode_message_text::reset_do();
            zeon_shortcode_message_button::reset_do();

            return $rett;
        }

        public $code;

        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Text Size', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'message-text-size',
                        'theme_mod' => false,
                        'default'   => 'large',
                        'options'   => array(
                            'large'     => __( 'Large', 'zeon' ),
                            'normal'    => __( 'Normal', 'zeon' ),
                            'small'     => __( 'Small', 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Text Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'message-color',
                        'theme_mod' => false,
                        'default'   => '#333333'
                    )
                ),
                array(
                    'title'     => __( 'Background Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'message-bg-color',
                        'theme_mod' => false,
                        'default'   => '#f9f9f9'
                    )
                ),
                array(
                    'title'     => __( 'Background Transparency', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'percent',
                        'name'      => 'message-bg-transp',
                        'theme_mod' => false,
                        'default'   => 80
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'message-top',
                        'theme_mod' => false,
                        'default'   => 35
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'message-bottom',
                        'theme_mod' => false,
                        'default'   => 35
                    )
                )
            );

            $fields[ 'text' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'message-text',
                        'theme_mod' => false,
                        'default'   => __( '...' , 'zeon' )
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Message Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'title'     => __( 'Message Text', 'zeon' ),
                    'fields'    => $fields[ 'text' ]
                )
            );

            $fields[ 'button' ] = array(
                array(
                    'title'         => __( 'Button Size', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'select',
                        'name'          => 'message-button-size',
                        'theme_mod'     => false,
                        'default'       => 'normal',
                        'options'       => array(
                            'large'         => __( 'Large' , 'zeon' ),
                            'normal'        => __( 'Normal' , 'zeon' ),
                            'small'         => __( 'Small' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'         => __( 'Text Color', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'color',
                        'name'          => 'message-button-color',
                        'theme_mod'     => false,
                        'default'       => '#ffffff'
                    )
                ),
                array(
                    'title'         => __( 'Background Color', 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'color',
                        'name'          => 'message-button-bg-color',
                        'theme_mod'     => false,
                        'default'       => '#0066d9'
                    )
                ),
                array(
                    'title'         => __( 'Text', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'text',
                        'name'          => 'message-button-text',
                        'theme_mod'     => false,
                        'default'       => __( 'Text / Label', 'zeon' )
                    )
                ),
                array(
                    'title'         => __( 'Url', 'zeon' ),
                    'format'        => 'across',
                    'input'         => array(
                        'type'          => 'url',
                        'name'          => 'message-button-url',
                        'theme_mod'     => false,
                        'default'       => 'http://mythem.es'
                    )
                ),
                array(
                    'title'         => __( 'Open url in new window', 'zeon' ),
                    'description'   => __( 'enable / disable attribute target="_blank"' , 'zeon' ),
                    'format'        => 'linear',
                    'input'         => array(
                        'type'          => 'logic',
                        'name'          => 'message-button-target',
                        'theme_mod'     => false,
                        'default'       => false,
                        'action'        => '.message-button-target.attr'
                    )
                ),
            );


            $boxes[ 'right' ][] = array(
                'title'         => __( 'Message Button', 'zeon' ),
                'fields'        => $fields[ 'button' ]
            );


            $text               = '<span class="message-text text"><span class="value">' . __( '...' , 'zeon' ) . '</span></span>';

            $text_size          = '<span class="message-text-size attr">size="<span class="value">large</span>"</span>';
            $color              = '<span class="message-color attr">color="<span class="value">#f9f9f9</span>"</span>';
            $bg_color           = '<span class="message-bg-color attr">bg_color="<span class="value">#f9f9f9</span>"</span>';
            $bg_transp          = '<span class="message-bg-transp attr">bg_transp="<span class="value">80</span>"</span>';
            $top                = '<span class="message-top attr">top="<span class="value">35</span>"</span>';
            $bottom             = '<span class="message-bottom attr">bottom="<span class="value">35</span>"</span>';

            $button_label       = '<span class="message-button-text text"><span class="value">' . __( 'Text / Label' , 'zeon' ) . '</span></span>';
            $button_url         = '<span class="message-button-url attr">url="<span class="value">http://mythem.es/</span>"</span>';
            $button_open        = '<span class="message-button-target attr hidden">open_new_window="<span class="value">false</span>"</span>';

            $button_size        = '<span class="message-button-size attr">size="<span class="value">normal</span>"</span>';
            $button_color       = '<span class="message-button-color attr">color="<span class="value">#ffffff</span>"</span>';
            $button_bg_color    = '<span class="message-button-bg-color attr">bg_color="<span class="value">#0066d9</span>"</span>';


            $this -> code       =   '[zeon_message ' . $top . ' ' . $bottom . ' ' . $color . ' ' . $bg_color . ' ' . $bg_transp . ']' . '<br/>' . "\n" .
                                    '[zeon_message_text ' . $text_size . ']' . $text .'[/zeon_message_text]' . '<br/>' . "\n" .
                                    '[zeon_message_button ' . $button_size . ' ' . $button_url . ' ' . $button_color . ' ' . $button_bg_color . ' ' . $button_open . ']' . $button_label . '[/zeon_message_button]'  . '<br/>' . "\n" .
                                    '[/zeon_message]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        public static function is_run(){
            return self::$run;
        }

        public static function set_c3x4( $c3x4 ){
            self::$C3X4 = $c3x4;
        }
    }

    class zeon_shortcode_message_text
    {
        private static $do = true;
        private static $type = 'normal';
        static function run( $_attr, $_content )
        {
            $rett = '';

            if( self::$do && zeon_shortcode_message::is_run() ){

                /* RUN JUST SINGLE TIME IN PARENT WRAPPER*/
                self::$do = false;

                /* GET ATTR AND AUTOFILL WITH DEFAULT VALUES */
                $attr = shortcode_atts( array(
                    'type'  => self::$type,
                    'color' => '#333333'
                ), $_attr );

                /* STYLE AND CUSTOM PROPERTIES */
                $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

                self::$type = esc_attr( $attr[ 'type' ] );

                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $rett .= '<div class="col-lg-C3X4 col-md-C3X4 col-sm-C3X4 col-xs-C3X4 message-content">';
                $rett .= '<div class="' . esc_attr( $attr[ 'type' ] ) . '" ' . $style . '>' . $_content . '</div>';
                $rett .= '</div>';
            }

            return $rett;
        }

        public static function reset_do(){
            self::$do = true;
        }

        public static function get_type(){
            return self::$type;
        }
    }

    class zeon_shortcode_message_button
    {
        private static $do = true;
        static function run( $_attr, $_content )
        {
            $rett = '';

            $text_type = zeon_shortcode_message_text::get_type();
            $btn_type = '';

            if( !isset( $_attr[ 'type' ] ) || empty( $_attr[ 'type' ] ) ){
                $btn_type = 'medium';
            }
            else{
                $btn_type = $_attr[ 'type' ];
            }

            switch( $text_type ){
                case 'small' : {
                    if( $btn_type == 'small' ){
                        $_attr[ 'top' ] = 3;
                        $_attr[ 'bottom' ] = 3;
                    }

                    else if( $btn_type == 'big' || $btn_type == 'large' ){
                        $_attr[ 'top' ] = -3;
                        $_attr[ 'bottom' ] = -3;
                    }

                    else{
                        $_attr[ 'top' ] = 0;
                        $_attr[ 'bottom' ] = 0;
                    }

                    break;
                }
                case 'big' : {
                    if( $btn_type == 'small' ){
                        $_attr[ 'top' ] = 10;
                        $_attr[ 'bottom' ] = 10;
                    }

                    else if( $btn_type == 'big' || $btn_type == 'large' ){
                        $_attr[ 'top' ] = 3;
                        $_attr[ 'bottom' ] = 2;
                    }

                    else{
                        $_attr[ 'top' ] = 7;
                        $_attr[ 'bottom' ] = 7;
                    }
                    break;
                }
                default : {
                    if( $btn_type == 'small' ){
                        $_attr[ 'top' ] = 5;
                        $_attr[ 'bottom' ] = 5;
                    }

                    else if( $btn_type == 'big' || $btn_type == 'large' ){
                        $_attr[ 'top' ] = -1;
                        $_attr[ 'bottom' ] = -1;
                    }

                    else{
                        $_attr[ 'top' ] = 2;
                        $_attr[ 'bottom' ] = 2;
                    }
                    break;
                }
            }

            if( self::$do && zeon_shortcode_message::is_run() ){

                self::$do = false;

                zeon_shortcode_message::set_c3x4( 8 );

                $rett .= '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 message-button">';
                $rett .= zeon_shortcode_button::run( $_attr, $_content );
                $rett .= '</div>';
            }

            return $rett;

        }

        public static function reset_do(){
            self::$do = true;
        }
    }
?>
