<?php
	
	/**
	 *	[posts view="classic|grid|portfolio" tags="" categories="" infinite_scroll="null|true|false|1|0" pagination="null|true|false|1|0" per_page="default|NUMBER"/]
	 */

	 if( !class_exists( 'zeon_shortcode_posts' ) ){

	 	class zeon_shortcode_posts
	 	{
	 		static $size 	= 6;

	 		static function run( $_attr, $_content = null )
	 		{
	 			global $wp_query;

	 			$attr = shortcode_atts(array(
	 				'view'				=> 'classic',
	 				'columns'			=> 2,

	 				'tags'				=> null,
	 				'categories'		=> null,
	 				'offset'        	=> null,
            		'skip'          	=> null,

	 				'infinite_scroll'	=> false,
	 				'pagination'		=> false,
	 				'per_page'			=> intval( get_option( 'posts_per_page' ) )
	 			), $_attr );

	 			$rett = null;

	 			// column size
	 			$cols = absint( $attr[ 'columns' ] );

        		if( !in_array( $cols, array( 2, 3 ) ) ){
            		$cols = 2;
        		}

        		self::$size = (int)( 12 / $cols );


        		add_filter( 'zeon_columns_size_class', array( 'zeon_shortcode_posts', 'size_class' ) );

	 			// query args
	 			$args = array(
	 				'post_type'				=> 'post',
	                'posts_per_page'        => intval( $attr[ 'per_page' ] ),
	                'paged'                 => max( 1 , get_query_var( 'paged' ) ),
	                'ignore_sticky_posts'   => 1
	            );
	            
	            // additional args
	            if( $offset = absint( $attr[ 'offset' ] ) ){
	                $args[ 'offset' ] = $offset;
	            }
	            
	            if( $skip = !empty( $attr[ 'skip' ] ) ? explode( ',' , $attr[ 'skip' ] ) : false ){
	                $args[ 'post__not_in' ] = $skip;
	            }

	            if( !empty( $attr[ 'categories' ] ) ){
	                $args[ 'category_name' ] = esc_attr( $attr[ 'categories' ] );
	            }

	            if( !empty( $attr[ 'tags' ] ) ){
	                $args[ 'tag' ] = esc_attr( $attr[ 'tags' ] );
	            }

	            $is_front_page = is_front_page();

	            // wordpress query
	            $wp_query = new WP_Query( $args );

	            if( $wp_query -> posts ){

	            	ob_start();

	            	echo '<div class="tempo-shortcode posts ' . esc_attr( $attr[ 'view' ] ) . '">';

	            	// start wrapper
	            	if( $attr[ 'view' ] == 'grid' || $attr[ 'view' ] == 'portfolio' )
	            		echo '<div class="row loop-row">';


	            	foreach( $wp_query -> posts as $post ) {

                    	$wp_query -> the_post();

                    	tempo_get_template_part( 'templates/article', $attr[ 'view' ] );
                	}

                	// end wrapper
                	if( $attr[ 'view' ] == 'grid' || $attr[ 'view' ] == 'portfolio' ){
                		echo '</div>';
                	}

                	// pagination
                	if( (bool)$attr[ 'pagination' ] && $attr[ 'pagination' ] !== 'false' && !$is_front_page )
                		tempo_get_template_part( 'templates/loop/pagination' );

                	echo '</div>';

                	$rett = ob_get_clean();
	            }

	            // not found articles
	            else{
	            	tempo_get_template_part( 'templates/not-found' );
	            }

	            wp_reset_query();

	            self::$size = 6;

	            return $rett;
	 		}

	 		static function size_class( $size )
	 		{
	 			$size = self::$size;

        		return "col-lg-{$size}";
	 		}
	 	}
	 }
?>