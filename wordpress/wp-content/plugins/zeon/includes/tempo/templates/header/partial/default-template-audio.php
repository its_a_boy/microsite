<?php 
	global $post;

	$audio_url 		= esc_url( zeon_meta::g( $post -> ID, 'tempo-audio-url' ) );
	$audio_id 		= absint( zeon_meta::g( $post -> ID, 'tempo-audio-id' ) );
	$autoplay 		= (bool)zeon_meta::get( $post -> ID, 'tempo-audio-autoplay', 1 );


    if( empty( $audio_id ) && empty( $audio_url ) )
    	return;

    $audio = null;
    $image = null;

    if( !empty( $audio_id ) )
    	$audio = get_post( $audio_id );


    // get file type
    $data 	= pathinfo( $audio_url );
    $ext 	= isset( $data[ 'extension' ] ) ? $data[ 'extension' ] : 'src';


    /**
     *	Audio details from post meta
     */
    $title 			= zeon_meta::get( $post -> ID, 'tempo-audio-title' );
	$description 	= zeon_meta::get( $post -> ID, 'tempo-audio-description' );
	$composer 		= zeon_meta::get( $post -> ID, 'tempo-audio-composer' );
	$artist 		= zeon_meta::get( $post -> ID, 'tempo-audio-artist' );
	$album 			= zeon_meta::get( $post -> ID, 'tempo-audio-album' );


    /**
     *	Audio details from media library post attachment
     */
    if( isset( $audio -> ID ) ){
    	$data			= zeon_meta::get( $audio -> ID , '_wp_attachment_metadata' );

    	$ext 			= isset( $data[ 'dataformat' ] ) ? $data[ 'dataformat' ] : $ext;

    	$m_composer 	= isset( $data[ 'composer' ] ) ? $data[ 'composer' ] : null;
    	$m_artist 		= isset( $data[ 'artist' ] ) ? $data[ 'artist' ] : null;
    	$m_album 		= isset( $data[ 'album' ] ) ? $data[ 'album' ] : null;
    	
    	$title 			= zeon_meta::get( $post -> ID, 'tempo-audio-title', $audio -> post_title );
    	$description 	= zeon_meta::get( $post -> ID, 'tempo-audio-description', $audio -> post_content );
    	$composer 		= zeon_meta::get( $post -> ID, 'tempo-audio-composer', $m_composer );
    	$artist 		= zeon_meta::get( $post -> ID, 'tempo-audio-artist', $m_artist );
    	$album 			= zeon_meta::get( $post -> ID, 'tempo-audio-album', $m_album );

    	// audio thumbnail
		$thumbnail 		= get_post( get_post_thumbnail_id( $audio ) );

		if( has_post_thumbnail( $audio ) && isset( $thumbnail -> ID ) ){
			$media = wp_get_attachment_image_src( $thumbnail -> ID, apply_filters( 'zeon_header_thumbnail_size', 'full' ) );

    		if( isset( $media[ 0 ] ) )
				$image = esc_url( $media[ 0 ] );
		}
    }


    // post thumbnail
	$thumbnail 		= get_post( get_post_thumbnail_id( $post ) );

	if( has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) ){
		$media = wp_get_attachment_image_src( $thumbnail -> ID, apply_filters( 'zeon_header_thumbnail_size', 'full' ) );
			
		if( isset( $media[ 0 ] )  )
			$image = esc_url( $media[ 0 ] );
	}


    // header height
    $height 		= apply_filters( 'tempo_header_height', tempo_options::get( 'header-height' ) );

    // header mask
    $mask_rgb       = tempo_hex2rgb( tempo_options::get( 'header-mask-color' ) );
    $mask_transp    = number_format( floatval( tempo_options::get( 'header-mask-transp' ) / 100 ), 2 );
    $mask_rgba      = "rgba( {$mask_rgb}, {$mask_transp} );";


    // shortcode attributes
    $audio_autoplay = $autoplay ? 'autoplay="1"' : null;
    $audio_src 		= $ext . '="' . $audio_url . '"';

?>


	<div class="tempo-header-partial tempo-audio overflow-wrapper" style="height: <?php echo absint( $height ); ?>px; background-image: url(<?php echo esc_url( $image ); ?>);">

		<?php tempo_get_template_part( 'templates/header/partial/prepend', 'default-audio' ); ?>

		<!-- mask - a transparent foil over the header image -->
    	<div class="tempo-header-mask" style="background: <?php echo esc_attr( $mask_rgba ); ?>"></div>

		<!-- flex elements -->
	    <div class="tempo-flex-container tempo-music tempo-valign-bottom">
	    	<div class="tempo-flex-item tempo-align-center">

	    		<!-- container -->
	    		<div <?php echo tempo_container_class( 'main' ); ?>>
            		<div <?php echo tempo_row_class(); ?>>

            			<!-- content -->
            			<div <?php echo tempo_content_class(); ?>>
            				<div <?php echo tempo_row_class(); ?>>

	            			<?php

	            				echo '<div class="details">';


	            				/**
	            				 *	Audio Title and Description
	            				 */

	            				if( !empty( $title ) )
	            					echo '<h1 class="tempo-headline">' . $title . '</h1>';

	            				if( !empty( $description ) )
	            					echo '<p class="tempo-description">' . $description . '</p>';


	            				//echo '<a href="" class="btn btn-light-green">$1.99 buy from iTune</a>';

	            				/**
	            				 *	Audio meta Details
	            				 */

	            				if( !( empty( $composer ) || empty( $artist ) && empty( $album ) ) ){
	            					
	            					// delimiter
		            				if( !( empty( $title ) && empty( $description ) ) )
		            					echo '<hr>';

	            					echo '<div class="tempo-meta">';
	            					echo '<span class="artist"><i class="tempo-icon-edit-3"></i> ' . $composer . '</span>';
	            					echo '<span class="artist"><i class="tempo-icon-mic-4"></i> ' . $artist . '</span>';
	            					echo '<span class="album"><i class="tempo-icon-cd"></i> ' . $album . '</span>';
	            					echo '</div>';
	            				}


		            			echo '</div>';

		            		?>

		            		</div>
		            	</div><!-- end content -->


		            	<!-- audio player wrapper -->
		            	<div class="col-lg-12">

		            		<?php

	            				/**
	            				 *	Audio Player
	            				 */

	            				echo '<div class="tempo-music-player">';
	            				echo do_shortcode( "[audio {$audio_autoplay} {$audio_src}][/audio]" );
	            				echo '</div>';
	            			?>

            			</div><!-- end audio player wrapper -->

	    			</div>
	    		</div><!-- end container -->

	    	</div>
	    </div><!-- end flex elements -->

	</div>