<?php
    /*
        [counters cols="2|3|4" number_color="#454545" label_color="#787878" description_color="#999999"]

            [counter number="123"]
                ...
                [counter_description] ... [/counter_description]
            [/counter]

            [counter number="123"]
                ...
                [counter_description] ... [/counter_description]
            [/counter]

            [counter number="123"]
                ...
                [counter_description] ... [/counter_description]
            [/counter]

        [/counter]
    */

    class zeon_shortcode_counters
    {
        private static $run             = false;
        private static $cols            = 3;
        private static $cols_accept     = array( 2, 3, 4, 6 );

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( !self::is_run() ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'cols'              => self::$cols,
                    'number_color'      => '#454545',
                    'label_color'       => '#787878',
                    'description_color' => '#999999'
                ), $_attr );

                /* STYLES AND PROPERTIES */
                $prop   = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );
                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $cols = absint( $attr[ 'cols' ] );

                $cols = !empty( $cols ) && in_array( $cols ,   self::$cols_accept ) ? $cols : self::$cols;

                zeon_shortcode_counter::set_size( absint( 12 / $cols ) );
                zeon_shortcode_counter::set_number_color( esc_attr( $attr[ 'number_color' ] ) );
                zeon_shortcode_counter::set_label_color( esc_attr( $attr[ 'label_color' ] ) );
                zeon_shortcode_counter_description::set_color( esc_attr( $attr[ 'description_color' ] ) );

                $rett .= '<div class="tempo-shortcode counters" ' . $style . '>';
                $rett .= '<div class="row">';

                $rett .= do_shortcode( $_content );

                $rett .= '<div class="clearfix"></div>';
                $rett .= '</div>';
                $rett .= '</div>';


                self::$run = false;

                zeon_shortcode_counter::set_index( 0 );
                zeon_shortcode_counter::set_length( 0 );
            }

            return $rett;
        }

        public $code;
        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Number of counters', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'id'        => 'zeon-counters-nr',
                        'type'      => 'select',
                        'name'      => 'counters-nr',
                        'theme_mod' => false,
                        'default'   => 3,
                        'options'   => array(
                            2           => number_format_i18n( 2 ),
                            3           => number_format_i18n( 3 ),
                            4           => number_format_i18n( 4 )
                        ),
                        'actions'   => array(
                            'hide'  => array( '.zeon-counter-3-wrapper', '.zeon-counter-4-wrapper' ),
                            '3'     => array( '.zeon-counter-3-wrapper' ),
                            '4'     => array( '.zeon-counter-3-wrapper', '.zeon-counter-4-wrapper' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Number Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'counters-nr-color',
                        'theme_mod' => false,
                        'default'   => '#454545'
                    )
                ),
                array(
                    'title'     => __( 'Label Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'counters-label-color',
                        'theme_mod' => false,
                        'default'   => '#787878'
                    )
                ),
                array(
                    'title'     => __( 'Description Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'counters-desc-color',
                        'theme_mod' => false,
                        'default'   => '#999999'
                    )
                )
            );


            $fields[ 'counters-1' ] = array(
                array(
                    'title'     => __( 'Number', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'counters-1-nr',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                ),
                array(
                    'title'     => __( 'Label', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-1-label',
                        'theme_mod' => false,
                        'default'   => __( 'short text 1', 'zeon' )
                    )
                ),
                array(
                    'title'     => __( 'Description', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-1-description',
                        'theme_mod' => false,
                        'default'   => __( 'short description 1', 'zeon' )
                    )
                )
            );


            $fields[ 'counters-2' ] = array(
                array(
                    'title'     => __( 'Number', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'counters-2-nr',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                ),
                array(
                    'title'     => __( 'Label', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-2-label',
                        'theme_mod' => false,
                        'default'   => __( 'short text 2', 'zeon' )
                    )
                ),
                array(
                    'title'     => __( 'Description', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-2-description',
                        'theme_mod' => false,
                        'default'   => __( 'short description 2', 'zeon' )
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Counters Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'title'     => __( 'Counter 1', 'zeon' ),
                    'fields'    => $fields[ 'counters-1' ]
                ),
                array(
                    'title'     => __( 'Counter 2', 'zeon' ),
                    'fields'    => $fields[ 'counters-2' ]
                )
            );



            $fields[ 'counters-3' ] = array(
                array(
                    'title'     => __( 'Number', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'counters-3-nr',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                ),
                array(
                    'title'     => __( 'Label', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-3-label',
                        'theme_mod' => false,
                        'default'   => __( 'short text 3', 'zeon' )
                    )
                ),
                array(
                    'title'     => __( 'Description', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-3-description',
                        'theme_mod' => false,
                        'default'   => __( 'short description 3', 'zeon' )
                    )
                )
            );


            $fields[ 'counters-4' ] = array(
                array(
                    'title'     => __( 'Number', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'counters-4-nr',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                ),
                array(
                    'title'     => __( 'Label', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-4-label',
                        'theme_mod' => false,
                        'default'   => __( 'short text 4', 'zeon' )
                    )
                ),
                array(
                    'title'     => __( 'Description', 'zeon' ),
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'text',
                        'name'      => 'counters-4-description',
                        'theme_mod' => false,
                        'default'   => __( 'short description 4', 'zeon' )
                    )
                )
            );


            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'Counter 3', 'zeon' ),
                    'class'     => 'zeon-counter-3-wrapper',
                    'fields'    => $fields[ 'counters-3' ]
                ),
                array(
                    'title'     => __( 'Counter 4', 'zeon' ),
                    'class'     => 'zeon-counter-4-wrapper hidden',
                    'fields'    => $fields[ 'counters-4' ]
                )
            );


            $cnt1           =   '[zeon_counter <span class="counters-1-nr attr">number="<span class="value">10</span>"</span>]<span class="counters-1-label text"><span class="value">short text 1</span></span>' . '<br/>' . "\n" .
                                '[zeon_counter_description]<span class="counters-1-description text"><span class="value">short description 1</span></span>[/zeon_counter_description]' . '<br/>' . "\n" .
                                '[/zeon_counter]';

            $cnt2           =   '[zeon_counter <span class="counters-2-nr attr">number="<span class="value">10</span>"</span>]<span class="counters-2-label text"><span class="value">short text 2</span></span>' . '<br/>' . "\n" .
                                '[zeon_counter_description]<span class="counters-2-description text"><span class="value">short description 2</span></span>[/zeon_counter_description]' . '<br/>' . "\n" .
                                '[/zeon_counter]';

            $cnt3           =   '[zeon_counter <span class="counters-3-nr attr">number="<span class="value">10</span>"</span>]<span class="counters-3-label text"><span class="value">short text 3</span></span>' . '<br/>' . "\n" .
                                '[zeon_counter_description]<span class="counters-3-description text"><span class="value">short description 3</span></span>[/zeon_counter_description]' . '<br/>' . "\n" .
                                '[/zeon_counter]';

            $cnt4           =   '[zeon_counter <span class="counters-4-nr attr">number="<span class="value">10</span>"</span>]<span class="counters-4-label text"><span class="value">short text 4</span></span>' . '<br/>' . "\n" .
                                '[zeon_counter_description]<span class="counters-4-description text"><span class="value">short description 4</span></span>[/zeon_counter_description]' . '<br/>' . "\n" .
                                '[/zeon_counter]';

            $nr             = '<span class="counters-nr attr">nr="<span class="value">3</span>"</span>';
            $color          = '<span class="counters-nr-color attr">number_color="<span class="value">#454545</span>"</span>';
            $label          = '<span class="counters-label-color attr">label_color="<span class="value">#787878</span>"</span>';
            $desc           = '<span class="counters-desc-color attr">description_color="<span class="value">#999999</span>"</span>';

            $this -> code   =   '[zeon_counters ' . $nr . ' ' . $color . ' ' . $label . ' ' . $desc . ']' . '<br/>' . "\n" .
                                $cnt1 . '<br/>' . "\n" .
                                $cnt2 . '<br/>' . "\n" .
                                '<span class="zeon-counter-3 zeon-ln">' . $cnt3 . '<br/>' . "\n" . '</span>' .
                                '<span class="zeon-counter-4 zeon-ln hide-ln">' . $cnt4 . '<br/>' . "\n" . '</span>' .
                                '[/zeon_counters]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        static function is_run()
        {
            return self::$run;
        }
    }

    class zeon_shortcode_counter
    {
        private static $run             = false;
        private static $number_color    = '#454545';
        private static $label_color     = '#787878';
        private static $size            = 4;
        private static $length          = 0;
        private static $index           = 0;

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( zeon_shortcode_counters::is_run() && !self::is_run() ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'number'    => 0,
                    'color'     => null
                ), $_attr );

                if( !isset( $_attr[ 'color' ] ) || empty( $attr[ 'color' ] ) ){
                    $_attr[ 'color' ] = self::$label_color;
                }

                $nr_style = 'style="color: ' . esc_attr( self::$number_color ) . ';"';

                $prop   = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );
                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                /* ROW SEPARATOR */
                if( self::$size + self::$length > 12 ){
                    $rett .= '<div class="clearfix visible-md-block visible-lg-block counter-delimiter"></div>';
                    self::$length = 0;
                }

                $sm_size = 4;

                if( self::$size == 6 || self::$size == 3 ){
                    $sm_size = 6;
                }

                if( $sm_size == 4 && self::$index > 0 && self::$index % 3 == 0 ){
                    $rett .= '<div class="clearfix visible-sm-block counter-delimiter"></div>';
                }

                if( $sm_size == 6 && self::$index > 0 && self::$index % 2 == 0 ){
                    $rett .= '<div class="clearfix visible-sm-block counter-delimiter"></div>';
                }

                if( self::$index > 0 && self::$index % 2 == 0 ){
                    $rett .= '<div class="clearfix visible-xs-block counter-delimiter"></div>';
                }

                $rett .= '<div class="col-xs-6 col-sm-' . esc_attr( $sm_size ) . ' col-md-' . esc_attr( self::$size ) . ' col-lg-' . esc_attr( self::$size ) . ' counter-item" ' . $style . '>';
                $rett .= '<span class="counter" ' . $nr_style . '>' . absint( $attr[ 'number' ] ) . '</span>' . do_shortcode( $_content );
                $rett .= '</div>';

                self::$length += self::$size;
                self::$index++;

                self::$run = false;
            }

            return $rett;
        }

        static function is_run()
        {
            return self::$run;
        }

        static function set_number_color( $number_color )
        {
            self::$number_color = $number_color;
        }

        static function set_label_color( $label_color )
        {
            self::$label_color = $label_color;
        }

        static function set_size( $size )
        {
            self::$size = $size;
        }

        static function set_index( $index )
        {
            self::$index = $index;
        }

        static function set_length( $length )
        {
            self::$length = $length;
        }
    }

    class zeon_shortcode_counter_description
    {
        private static $run     = false;
        private static $color   = '#999999';
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( zeon_shortcode_counter::is_run() && !self::$run ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'color' => null
                ), $_attr );

                if( !isset( $_attr[ 'color' ] ) || empty( $attr[ 'color' ] ) ){
                    $_attr[ 'color' ] = self::$color;
                }

                $prop = esc_attr( zeon_shortcode_prop( $_attr ) );
                $style = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $rett .= '<div class="counter-description" ' . $style . '>';
                $rett .= do_shortcode( $_content );
                $rett .= '</div>';

                self::$run = false;
            }

            return $rett;
        }

        static function set_color( $color )
        {
            self::$color = $color;
        }
    }

?>
