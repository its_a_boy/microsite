<?php

	/**
	 *	Meta Boxes - config file
	 */

	$settings = array(
		'title'     => __( 'Settings' , 'zeon' ),
		'context'   => 'normal',
		'priority' 	=> 'high',
		'callback'  => 'cronus__box_single_settings',
		'save' 		=> 'cronus__box_single_settings_save',
		'args'      => null
	);

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-meta-boxes' ), array(
		'post'		=> array(
			'tempo-settings'	=> $settings,
		),
		'page'		=> array(
			'tempo-settings'	=> $settings,
		)
	));

	tempo_cfgs::set( 'custom-meta-boxes', $cfgs );
?>