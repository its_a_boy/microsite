<?php
	if( 'facebook' == tempo_options::get( 'comments-type' ) ){

		if( $appid = tempo_options::get( 'comments-fb-appid' ) && empty( $appid ) )
			$appid = '252156114891794';
?>
		<script>
		    (function(d, s, id) {
		        var js, fjs = d.getElementsByTagName(s)[0];
		        if (d.getElementById(id)) return;
		        js = d.createElement(s); js.id = id;
		        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=<?php echo esc_attr( $appid ); ?>&version=v2.3";
		        fjs.parentNode.insertBefore(js, fjs);
		    }(document, 'script', 'facebook-jssdk'));
		</script>
<?php
	}
?>