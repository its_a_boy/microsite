<?php

	/**
	 *	Appearance / Customize / Others / Copyright - config settings
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
	 	'tempo-others' => array(
			'sections'	=> array(
				'tempo-copyright' => array(
					'title'		=> __( 'Copyright', 'zeon' ),
					'fields' 	=> array(
						'website-copyright' => array(
							'priority'		=> 1,
							'description'   => __( 'here you can change the copyright for the website content.' , 'zeon' )
						),
						'author-copyright' => array(
							'title'			=> __( 'Author Theme Copyright', 'zeon' ),
							'priority'		=> 10,
							'input'			=> array(
								'type' 			=> 'copyright',
								'default'		=> sprintf( __( 'Designed by %s.' , 'tempo' ) , '<a href="' . esc_url( tempo_core::author( 'uri' ) ) . '" target="_blank" title="' . esc_attr( tempo_core::author( 'name' ) ) . '">' . tempo_core::author( 'name' ) . '</a>' )
							)
						)
					)
				)
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>
