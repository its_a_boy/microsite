<?php
    /*
        [columns cols="2/3/4"]

            [column cl_size="6/4/3" top="" bottom=""]text[/column]
            [column cl_size="6/4/3" top="" bottom=""]text[/column]
            [column cl_size="6/4/3" top="" bottom=""]text[/column]

        [/columns]
     */

    class zeon_shortcode_columns
    {
        private static $run             = false;

        private static $nr              = 3;

        private static $nr_accept       = array( 2, 3, 4 );
        private static $columns_sizes   = array( 6, 4, 3 );

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            /* BREAK IF EMPTY CONTENT */
            if( empty( $_content ) ){
                return $rett;
            }

            /* BLOK RECURSIVITY */
            self::$run = true;

            $attr = shortcode_atts( array(
                /* COLUMNS ATTR */
                'nr'      => self::$nr
            ), $_attr );

            /* STYLE AND CUSTOM PROPERTIES */
            $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

            $style = '';

            if( !empty( $prop ) ){
                $style = 'style="' . $prop . '"';
            }

            /* COLUMNS VALIDATE  */
            $nr = (int)$attr[ 'nr' ];

            if( !in_array( $nr , self::$nr_accept ) )
                $nr = self::$nr;

            self::$nr = $nr;

            /* CONTENT */
            $rett  .= '<div class="tempo-shortcode columns" ' . $style . '>';
            $rett  .= '<div class="row">';

            $rett  .= do_shortcode( $_content );

            $rett  .= '</div>';
            $rett  .= '</div>';

            zeon_shortcode_column::set_length( 0 );
            zeon_shortcode_column::set_index( 0 );
            self::$run = false;

            return $rett;
        }


        public $code;
        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Number of columns', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'id'        => 'columns-nr',
                        'type'      => 'select',
                        'name'      => 'columns-nr',
                        'theme_mod' => false,
                        'default'   => 2,
                        'options'   => array(
                            2           => number_format_i18n( 2 ),
                            3           => number_format_i18n( 3 ),
                            4           => number_format_i18n( 4 )
                        ),
                        'actions'   => array(
                            'hide'  => array( '.zeon-column-3-wrapper', '.zeon-column-4-wrapper' ),
                            '3'     => array( '.zeon-column-3-wrapper' ),
                            '4'     => array( '.zeon-column-3-wrapper', '.zeon-column-4-wrapper' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'columns-top',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'columns-bottom',
                        'theme_mod' => false,
                        'default'   => 10
                    )
                )
            );

            $fields[ 'col-1' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'columns-col-1',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $fields[ 'col-2' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'columns-col-2',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Columns Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                ),
                array(
                    'title'     => __( 'First column Content', 'zeon' ),
                    'fields'    => $fields[ 'col-1' ]
                ),
                array(
                    'title'     => __( 'Second column Content', 'zeon' ),
                    'fields'    => $fields[ 'col-2' ]
                )
            );

            $fields[ 'col-3' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'columns-col-3',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $fields[ 'col-4' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'columns-col-4',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );


            $boxes[ 'right' ] = array(
                array(
                    'title'         => __( 'Third column Content', 'zeon' ),
                    'class'         => 'zeon-column-3-wrapper hidden',
                    'fields'        => $fields[ 'col-3' ]
                ),
                array(
                    'title'         => __( 'Fourth column Content', 'zeon' ),
                    'class'         => 'zeon-column-4-wrapper hidden',
                    'fields'        => $fields[ 'col-4' ]
                ),
            );

            $col1           = '[zeon_column]<span class="columns-col-1 text"><span class="value">...</span></span>[/zeon_column]';
            $col2           = '[zeon_column]<span class="columns-col-2 text"><span class="value">...</span></span>[/zeon_column]';
            $col3           = '[zeon_column]<span class="columns-col-3 text"><span class="value">...</span></span>[/zeon_column]';
            $col4           = '[zeon_column]<span class="columns-col-4 text"><span class="value">...</span></span>[/zeon_column]';

            $nr             = '<span class="columns-nr attr">nr="<span class="value">2</span>"</span>';
            $top            = '<span class="columns-top attr">top="<span class="value">10</span>"</span>';
            $bottom         = '<span class="columns-bottom attr">bottom="<span class="value">10</span>"</span>';

            $this -> code   =   '[zeon_columns ' . $nr . ' ' . $top . ' ' . $bottom . ']' . '<br/>' . "\n" .
                                $col1 . '<br/>' . "\n" .
                                $col2 . '<br/>' . "\n" .
                                '<span class="zeon-column-3 zeon-ln hide-ln">' . $col3 . '<br/>' . "\n" . '</span>' .
                                '<span class="zeon-column-4 zeon-ln hide-ln">' . $col4 . '<br/>' . "\n" . '</span>' .
                                '[/zeon_columns]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        public static function is_run(){
            return self::$run;
        }

        public static function get_nr(){
            return self::$nr;
        }

        public static function get_lns(){
            return self::$columns_sizes;
        }
    }

    class zeon_shortcode_column
    {
        private static $length = 0;
        private static $index  = 0;

        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( zeon_shortcode_columns::is_run() ){

                /* GET DEFAULT COLUMN SIZE */
                $nr = zeon_shortcode_columns::get_nr();
                $ln = (int)( 12 / $nr );

                /* DEFAULT VALUES FOR ATTRS */
                $attr = shortcode_atts( array(
                    /* COLUMN ATTR */
                    'ln'        => $ln,

                    /* CSS ATTRS */
                    'top'       => 15,
                    'bottom'    => 15
                ), $_attr );

                /* STYLE AND CUSTOM PROPERTIES */
                $prop  = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );

                $style = '';

                if( !empty( $prop ) )
                    $style = 'style="' . $prop . '"';

                /* CHECK SIZE IF IS ACCEPTABLE */
                $ln = (int)$attr[ 'ln' ];

                if( !in_array( $ln, zeon_shortcode_columns::get_lns() ) )
                    $ln = (int)( 12 / $nr );

                /* ROW SEPARATOR */
                if( $ln + self::$length > 12 ){
                    $rett .= '<div class="clearfix visible-md-block visible-lg-block"></div>';
                    self::$length = 0;
                }

                if( self::$index > 0 && self::$index % 2 == 0 )
                    $rett .= '<div class="clearfix visible-xs-block visible-sm-block"></div>';

                $classes = 'col-xs-12 col-sm-6 col-md-' . $ln . ' col-lg-' . $ln;

                $rett .= '<div class="' . $classes . ' column" ' . $style . '>';
                $rett .= do_shortcode( $_content );
                $rett .= '</div>';

                self::$length += $ln;
                self::$index++;
            }

            return $rett;
        }

        public static function set_length( $length ){
            self::$length = $length;
        }

        public static function set_index( $index ){
            self::$index = $index;
        }
    }
?>
