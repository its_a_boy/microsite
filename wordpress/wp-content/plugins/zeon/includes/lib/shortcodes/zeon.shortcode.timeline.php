<?php

    /*
        [timeline style="squared|circled|rounded" marker_size="small|normal|large" marker_color="" marker_bkg="" title_color="" desc_color="" line_color=""]

            [timeline_item title=""]  ... [/timeline_item]
            [timeline_item title=""]  ... [/timeline_item]

            ...

        [/timeline]


            <span class="timeline-item-icon"></span>
            <span class="timeline-item-date"></span>
            <span class="timeline-item-author"></span>

            ... WILL BE CONTINUED ...
    */

    class zeon_shortcode_timeline
    {
        private static $run = false;
        static function run( $_attr, $_content = null )
        {
            $rett = '';

            if( !self::is_run() ){
                self::$run = true;

                $attr = shortcode_atts( array(
                    'style'         => 'circled',

                    'marker_size'   => 'normal',
                    'marker_color'  => '#ffffff',
                    'marker_bkg'    => '#39b54a',

                    'title_color'   => '#333333',
                    'desc_color'    => '#999999',

                    'line_color'    => '#dedede'
                ), $_attr );

                /* STYLES AND PROPERTIES */
                $prop   = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );
                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                zeon_shortcode_timeline_item::set_marker_color( esc_attr( $attr[ 'marker_color' ] ) );
                zeon_shortcode_timeline_item::set_marker_bkg( esc_attr( $attr[ 'marker_bkg' ] ) );

                zeon_shortcode_timeline_item::set_title_color( esc_attr( $attr[ 'title_color' ] ) );
                zeon_shortcode_timeline_item::set_desc_color( esc_attr( $attr[ 'desc_color' ] ) );

                zeon_shortcode_timeline_item::set_line_color( esc_attr( $attr[ 'line_color' ] ) );

                /* CONTENT */
                $rett .= '<div class="tempo-shortcode timeline ' . esc_attr( $attr[ 'style' ] ) . ' ' . esc_attr( $attr[ 'marker_size' ] ) . '" ' . $style . '>';

                $rett .= '<div class="timeline" style="background-color: ' . esc_attr( $attr[ 'line_color' ] ) . '"></div>';

                $rett .= '<div class="row">';

                $rett .= do_shortcode( strip_tags( $_content , '<br><p><i><u><em><b><a><strike><abbr><address>' ) );

                $rett .= '</div>';
                $rett .= '</div>';

                self::$run = false;

                zeon_shortcode_timeline_item::set_index( 1 );
            }

            return $rett;
        }

        public $code;
        public function builder()
        {
            $fields = array();
            $boxes  = array();

            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Style', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'timeline-style',
                        'theme_mod' => false,
                        'default'   => 'circled',
                        'options'   => array(
                            'squared'   => __( 'Squared' , 'zeon' ),
                            'circled'   => __( 'Circled' , 'zeon' ),
                            'rounded'   => __( 'Rounded' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Marker Size', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'select',
                        'name'      => 'timeline-marker-size',
                        'theme_mod' => false,
                        'default'   => 'normal',
                        'options'   => array(
                            'small'     => __( 'Small' , 'zeon' ),
                            'normal'    => __( 'Normal', 'zeon' ),
                            'large'     => __( 'Large' , 'zeon' )
                        )
                    )
                ),
                array(
                    'title'     => __( 'Marker Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'timeline-marker-color',
                        'theme_mod' => false,
                        'default'   => '#ffffff'
                    )
                ),
                array(
                    'title'     => __( 'Marker Background Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'timeline-marker-bg-color',
                        'theme_mod' => false,
                        'default'   => '#26ad60'
                    )
                ),
                array(
                    'title'     => __( 'Title Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'timeline-title-color',
                        'theme_mod' => false,
                        'default'   => '#333333'
                    )
                ),
                array(
                    'title'     => __( 'Description Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'timeline-desc-color',
                        'theme_mod' => false,
                        'default'   => '#999999'
                    )
                ),
                array(
                    'title'     => __( 'Line Color', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'color',
                        'name'      => 'timeline-line-color',
                        'theme_mod' => false,
                        'default'   => '#dedede'
                    )
                ),
            );

            $boxes[ 'left' ] = array(
                array(
                    'title'     => __( 'Timeline Settings', 'zeon' ),
                    'fields'    => $fields[ 'settings' ]
                )
            );

            $fields[ 'items' ] = array(
                array(
                    'format'    => 'across',
                    'input'     => array(
                        'type'      => 'textarea',
                        'name'      => 'timeline-items',
                        'theme_mod' => false,
                        'default'   =>  '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . "\n" .
                                        '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . "\n" .
                                        '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . "\n" .
                                        '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . "\n" .
                                        '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . "\n" .
                                        '...'
                    )
                )
            );

            $boxes[ 'right' ] = array(
                array(
                    'title'     => __( 'Timeline Items', 'zeon' ),
                    'fields'    => $fields[ 'items' ]
                )
            );

            $style          = '<span class="timeline-style attr">style="<span class="value">circled</span>"</span>';
            $marker_size    = '<span class="timeline-marker-size attr">marker_size="<span class="value">normal</span>"</span>';
            $marker_color   = '<span class="timeline-marker-color attr">marker_color="<span class="value">#ffffff</span>"</span>';
            $marker_bkg     = '<span class="timeline-marker-bg-color attr">marker_bg_color="<span class="value">#26ad60</span>"</span>';

            $title_color    = '<span class="timeline-title-color attr">title_color="<span class="value">#333333</span>"</span>';
            $desc_color     = '<span class="timeline-desc-color attr">desc_color="<span class="value">#999999</span>"</span>';

            $line_color     = '<span class="timeline-line-color attr">line_color="<span class="value">#dedede</span>"</span>';

            $this -> code   =   '[zeon_timeline ' . $style . ' ' . $marker_size . ' ' . $marker_color . ' ' . $marker_bkg . ' ' . $title_color . ' ' . $desc_color . ' ' . $line_color . ']' . '<br/>' . "\n" .
                                '<span class="timeline-items text"><span class="value">' .

                                '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . '<br>' . "\n" .
                                '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . '<br>' . "\n" .
                                '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . '<br>' . "\n" .
                                '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . '<br>' . "\n" .
                                '[zeon_timeline_item title=""] ... [/zeon_timeline_item]' . '<br>' . "\n" .
                                '...' .

                                '</span></span>' . '<br/>' . "\n" .
                                '[/zeon_timeline]';

            return array(
                array(
                    'boxes' => $boxes[ 'left' ]
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }

        static function is_run()
        {
            return self::$run;
        }
    }

    class zeon_shortcode_timeline_item
    {
        private static $run             = false;
        private static $index           = 1;

        private static $marker_color    = null;
        private static $marker_bkg      = null;

        private static $title_color     = null;
        private static $desc_color      = null;

        private static $line_color      = null;

        static function run( $_attr , $_content = null )
        {
            $rett = '';

            if( !self::is_run() && zeon_shortcode_timeline::is_run() ){

                self::$run = true;

                $attr = shortcode_atts( array(
                    'title' => null,
                    'color' => null
                ), $_attr );

                if( !isset( $_attr[ 'color' ] ) || empty( $attr[ 'color' ] ) ){
                    $_attr[ 'color' ] = self::$desc_color;
                }

                /* STYLES AND PROPERTIES */
                $prop   = esc_attr( zeon_shortcode_prop( $_attr, $attr ) );
                $style  = '';

                if( !empty( $prop ) ){
                    $style = 'style="' . $prop . '"';
                }

                $classes = absint( self::$index ) % 2 == 1 ? 'col-sm-offset-6  col-md-offset-6 col-lg-offset-6' : 'col-sm-offset-0  col-md-offset-0 col-lg-offset-0';

                /* INDEX STYLE */
                $index_style    = '';

                if( !empty( self::$marker_bkg ) ){
                    $box_shadow =

                        '-webkit-box-shadow: 2px 2px 0px 0px ' . tempo_brightness( self::$marker_bkg, -10 ) . '; ' .
                        '-mox-box-shadow: 2px 2px 0px 0px ' . tempo_brightness( self::$marker_bkg, -10 ) . '; ' .
                        'box-shadow: 2px 2px 0px 0px ' . tempo_brightness( self::$marker_bkg, -10 ) . ';';

                    $index_style = 'style="background: ' . esc_attr( self::$marker_bkg ) . '; color: ' . esc_attr( self::$marker_color ) .'; ' . $box_shadow . '"';
                }

                /* DOT STYLE */
                $dot_style      = '';

                if( !empty( self::$marker_bkg ) ){
                    $dot_style = 'style="background: ' . esc_attr( self::$marker_bkg ) . ';"';
                }

                /* TITLE STYLE */
                $title_style = '';
                if( !empty( self::$title_color ) ){
                    $title_style = 'style="color: ' . esc_attr( self::$title_color ) . ';"';
                }

                /* THE CONTENT */
                $rett .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ' . esc_attr( $classes )  . '" ' . $style . '>';
                $rett .= '<div class="timeline-item">';
                $rett .= '<span class="timeline-item-index" ' . $index_style . '>' . absint( self::$index ) . '</span>';

                $rett .= '<span class="timeline-item-dot" style="border: 3px solid ' . esc_attr( self::$line_color ) . '"><span class="dot-inner" ' . $dot_style . '></span></span>';

                $rett .= '<h3 ' . $title_style . '>' . esc_html( $attr[ 'title' ] ) . '</h3>';
                $rett .= '<div class="timeline-description">'. strip_shortcodes( strip_tags( $_content , '<br><p><i><u><em><b><a><strike><abbr><address>' ) ) . '</div>';

                $rett .= '</div>';
                $rett .= '</div>';
                $rett .= '<div class="clearfix"></div>';

                self::$index++;
                self::$run = false;
            }

            return $rett;
        }

        static function is_run()
        {
            return self::$run;
        }

        static function set_index( $index )
        {
            self::$index = $index;
        }

        static function set_marker_color( $marker_color )
        {
            self::$marker_color = $marker_color;
        }

        static function set_marker_bkg( $marker_bkg )
        {
            self::$marker_bkg = $marker_bkg;
        }

        static function set_title_color( $title_color )
        {
            self::$title_color = $title_color;
        }

        static function set_desc_color( $desc_color )
        {
            self::$desc_color = $desc_color;
        }

        static function set_line_color( $line_color )
        {
            self::$line_color = $line_color;
        }
    }
?>
