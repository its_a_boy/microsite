<?php

    /**
     *  [code] ... [/code]
     */

    class zeon_shortcode_code
    {
        static function run( $_attr, $_content = null )
        {
            $attr = shortcode_atts( array(
                'bg_color'      => '#f5f5f5',
                'bg_opacity'    => 80,
                'color'         => '#333333',
                'inner'         => 10
            ), $_attr );

            // custom style
            $prop  = esc_attr( zeon_shortcode_prop( $_attr ) );

            $style = '';

            if( !empty( $attr[ 'bg_color' ] ) ){
                $border_color = tempo_brightness( $attr[ 'bg_color' ] , -20 );
                $prop .= 'border-color: rgba( ' . tempo_hex2rgb( $border_color ) . ', ' . $attr[ 'bg_opacity' ] . ' ); ';
            }

            if( !empty( $prop ) ){
                $style = 'style="' . esc_attr( $prop ) . '"';
            }

            return 	'<pre class="tempo-shortcode code"' . $style .'>' . strip_tags( $_content ) . '</pre>';
        }

        public $code;

        public function builder()
        {
            $fields = array();
            $boxes  = array();


            $fields[ 'settings' ] = array(
                array(
                    'title'     => __( 'Space Above', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'code-top',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                ),
                array(
                    'title'     => __( 'Space Below', 'zeon' ),
                    'format'    => 'linear',
                    'input'     => array(
                        'type'      => 'number',
                        'name'      => 'code-bottom',
                        'theme_mod' => false,
                        'default'   => 30
                    )
                )
            );

            $boxes[ 'left' ][] = array(
                'title'     => __( 'Code Settings', 'zeon' ),
                'fields'    => $fields[ 'settings' ]
            );

            $fields[ 'content' ] = array(
                array(
                    'format'        => 'across',
                    'input'         => array(
                        'type'      => 'textarea',
                        'name'      => 'code-content',
                        'theme_mod' => false,
                        'default'   => '...'
                    )
                )
            );

            $boxes[ 'right' ][] = array(
                'title'         => __( 'Code Content', 'zeon' ),
                'fields'        => $fields[ 'content' ]
            );

            $content            = '<span class="code-content text"><span class="value">...</span></span>';
            $top                = '<span class="code-top attr">top="<span class="value">30</span>"</span>';
            $bottom             = '<span class="code-bottom attr">bottom="<span class="value">30</span>"</span>';

            $this -> code       = '[zeon_code ' . $top . ' ' . $bottom . ']' . $content . '[/zeon_code]';

            return array(
                array(
                    'boxes' => $boxes['left']
                ),
                array(
                    'boxes' => $boxes['right']
                )
            );
        }
    }
?>
