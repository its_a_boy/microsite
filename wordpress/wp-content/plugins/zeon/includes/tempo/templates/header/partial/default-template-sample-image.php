<?php 
	global $post;

    $thumbnail = get_post( get_post_thumbnail_id( $post ) );

    if( has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) ){
?>
		<div class="tempo-header-partial tempo-sample-image">
            <?php
                echo get_the_post_thumbnail( $post, apply_filters( 'zeon_header_thumbnail_size', 'full' ), array(
                    'alt' => esc_attr( get_the_title( $post ) )
                ));
            ?>
		</div>
<?php
	}
?>