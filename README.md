# ACHTUNG! Repo ist nicht länger mit der Live-Preview auf gleichen Stand



# Microsite Angebote Kath

Dies ist eine Vagrant-Entwicklungsumgebung für die Kath Angebots Microsite. 

## Installation

1. Repository clonen
2. In der hosts Datei `192.168.33.10` auf `vccw.dev` mappen
3. `vagrant up` ausführen

Die Seite ist dann unter der Domain *vccw.dev* erreichbar. 
Die Zugangsdaten für das Admin-Panel: `admin` und `admin`

