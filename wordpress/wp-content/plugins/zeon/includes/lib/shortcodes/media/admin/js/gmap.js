/**
 *  ZEON GMAP ( ADMIN SIDE )
 *  jQuery and Module
 */

var zeon_gmap_module = function( args ){

    var maps = [];

    return {
        map : function( cfgs ){

            var gmap = new google.maps.Map( cfgs.canvas, {
                mapTypeId   : cfgs.type,
                zoom        : cfgs.zoom,
                scrollwheel : false
            });

            var map_init = function(){
                gmap = new google.maps.Map( cfgs.canvas, {
                    mapTypeId   : cfgs.type,
                    zoom        : cfgs.zoom,
                    scrollwheel : false
                });

                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
            },

            map_load = function(){
                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );

                var code = jQuery( 'div.zeon-shortcode-gmap-fields div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' );
                var form = jQuery( 'div.zeon-shortcode-gmap-fields' );

                /* ON CHANGE THE ZOOM */
                google.maps.event.addListener( gmap, 'zoom_changed', function() {
                    jQuery(function(){

                        var zoom = gmap.getZoom();

                        if( jQuery( cfgs.canvas ).hasClass( 'tempo-shortcode' ) ){
                            jQuery( code ).find( 'span.gmap-zoom.attr span.value' ).text( zoom );
                            jQuery( form ).find( 'input#zeon-gmap-zoom' ).val( zoom );
                        }

                        if( jQuery( cfgs.canvas ).hasClass( 'tempo-header-template' ) )
                            jQuery( 'div.tempo-gmap-wrapper' ).find( 'input#zeon-gmap-zoom' ).val( zoom );

                        if( cfgs.zoom !== zoom )
                            map_set_zoom( zoom );

                        gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
                    });
                });
            
                /* ON CHANGE THE ZOOM */
                google.maps.event.addListener( gmap, 'bounds_changed', function() {
                    jQuery(function(){

                        var zoom = gmap.getZoom();

                        if( jQuery( cfgs.canvas ).hasClass( 'tempo-shortcode' ) ){
                            jQuery( code ).find( 'span.gmap-zoom.attr span.value' ).text( gmap.getZoom() );
                            jQuery( form ).find( 'input#zeon-gmap-zoom' ).val( gmap.getZoom() );
                        }

                        if( jQuery( cfgs.canvas ).hasClass( 'tempo-header-template' ) )
                            jQuery( 'div.tempo-gmap-wrapper' ).find( 'input#zeon-gmap-zoom' ).val( gmap.getZoom() );

                        if( cfgs.zoom !== zoom )
                            map_set_zoom( zoom );

                        gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
                    });
                });

                //console.log( cfgs );

                if( cfgs.hasOwnProperty( 'markers' ) && typeof cfgs.markers == 'object' && cfgs.markers.length > 0 )
                    map_add_markers();
            },

            map_reset = function(){
                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
            },

            map_add_markers = function(){
                var markers     = cfgs.markers;
                gmap._markers   = [];

                for( var i = 0; i < markers.length; i++ ){

                    (function( i, markers ){

                        var sett = {
                            position    : new google.maps.LatLng( markers[ i ].lat, markers[ i ].lng ),
                            map         : gmap,
                            draggable   : true,
                            visible     : true,
                            icon        : markers[ i ].icon
                        };
                    
                        var marker = new google.maps.Marker( sett );
                    
                        marker.setMap( gmap );

                        gmap._markers[ i ] = marker;

                        map_marker_html( i );

                        google.maps.event.addListener( marker , 'mouseup' , function( event ){

                            var lat = marker.getPosition().lat();
                            var lng = marker.getPosition().lng();

                            if( 'zeon-shortcode-gmap-fields' == jQuery( 'select#zeon-shortcodes-selector' ).val() ){
                                var code = jQuery( 'div.zeon-shortcode-gmap-fields div.zeon-shortcode-code-wrapper span.zeon-shortcode-code' );
                                var form = jQuery( 'div.zeon-shortcode-gmap-fields' );

                                jQuery( code ).find( 'span.gmap-lat.attr span.value').text( lat );
                                jQuery( form ).find( 'input#zeon-gmap-lat' ).val( lat );

                                jQuery( code ).find( 'span.gmap-lng.attr span.value' ).text( lng );
                                jQuery( form ).find( 'input#zeon-gmap-lng' ).val( lng );
                            }

                            if( 'gmap' == jQuery( 'select#tempo-header-template' ).val() ){
                                jQuery( 'div.tempo-gmap-wrapper' ).find( 'input#zeon-gmap-lat' ).val( lat );
                                jQuery( 'div.tempo-gmap-wrapper' ).find( 'input#zeon-gmap-lng' ).val( lng );
                            }

                            if( cfgs.lat !== lat )
                                map_set_lat( lat );

                            if( cfgs.lng !== lng )
                                map_set_lng( lng );

                            gmap.setCenter( new google.maps.LatLng( lat, lng ), cfgs.zoom );
                        });

                        gmap._markers[ i ] = marker;

                    })( i, markers );
                }
            },

            map_marker_html = function( i ){

                var markers = cfgs.markers;
                var marker  = gmap._markers[ i ];

                var _title       = markers[ i ].hasOwnProperty( 'title' ) && markers[ i ].title.length;
                var _description = markers[ i ].hasOwnProperty( 'description' ) && markers[ i ].description.length;

                if( _title || _description ){

                    var title       = '';
                    var description = '';

                    if( _title )
                        title = '<h3 class="zeon-marker-title">' + markers[ i ].title + '</h3>';

                    if( _description )
                        description = '<div class="zeon-marker-description">' + markers[ i ].description + '</div>';
                        
                    var html = title + description;

                    var tooltip = new google.maps.InfoWindow({
                        content: html
                    });

                    if( markers[ i ].hasOwnProperty( 'listner' ) )
                        google.maps.event.removeListener( markers[ i ].listner );

                    markers[ i ].listner = google.maps.event.addListener( marker, 'click', function(){
                        tooltip.open( gmap, marker );
                    });
                }
            },

            map_set_zoom = function( zoom ){
                gmap.setZoom( zoom );

                if( cfgs.hasOwnProperty( 'zoom' ) )
                    cfgs.zoom = zoom;

                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
            },

            map_set_lat = function( lat ){
                if( cfgs.markers.hasOwnProperty( 0 ) ){
                    cfgs.markers[ 0 ].lat = lat;
                    cfgs.lat = lat;

                    if( !gmap.hasOwnProperty( 'markers' ) )
                        gmap.markers = cfgs.markers;
                }

                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
            },

            map_set_lng = function( lng ){
                if( cfgs.markers.hasOwnProperty( 0 ) ){
                    cfgs.markers[ 0 ].lng = lng;
                    cfgs.lng = lng;

                    if( !gmap.hasOwnProperty( 'markers' ) )
                        gmap.markers = cfgs.markers;
                }

                gmap.setCenter( new google.maps.LatLng( cfgs.lat, cfgs.lng ) );
            },

            map_set_marker_title = function( title ){
                if( cfgs.markers.hasOwnProperty( 0 ) ){
                    cfgs.markers[ 0 ].title = title;

                    if( !gmap.hasOwnProperty( 'markers' ) )
                        gmap.markers = cfgs.markers;

                    map_marker_html( 0 );
                }
            },

            map_set_marker_description = function( description ){
                if( cfgs.markers.hasOwnProperty( 0 ) ){
                    cfgs.markers[ 0 ].description = description;

                    if( !gmap.hasOwnProperty( 'markers' ) )
                        gmap.markers = cfgs.markers;

                    map_marker_html( 0 );
                }
            };

            return {
                init                    : map_init,
                reset                   : map_reset,
                load                    : map_load,

                set_zoom                : map_set_zoom,
                set_lat                 : map_set_lat,
                set_lng                 : map_set_lng,
                set_marker_title        : map_set_marker_title,
                set_marker_description  : map_set_marker_description
            }
        }
    };
}([]);


var zeon_maps = [];

(function($){

    var cfgs = {
        type    : google.maps.MapTypeId.ROADMAP,
        zoom    : 4,
        lat     : 47.772594932483,
        lng     : 1.8596535022735,
        markers : {}
    };

    $.fn.zeon_gmap = function( args ){
        return this.each(function(i){

            var cfgs = $.extend( {}, cfgs, args[ i ] );
            cfgs.canvas = this;

            zeon_maps[ i ] = zeon_gmap_module.map( cfgs );

            google.maps.event.addDomListener( window, 'load', zeon_maps[ i ].load );

            jQuery(window).resize(function(){
                zeon_maps[ i ].reset();
            });
        });
    }
})(jQuery);



/////   ZEON GMAP MODULE    /////


jQuery(document).ready(function(){

    /**
     *  jQuery Init Google Map
     */

    jQuery( 'div.tempo-shortcode.gmap div.tempo-canvas' ).zeon_gmap( gmap.args );


    /**
     *  Prepare Admin Maps
     */

    var zeon_gmap_form = function( i, form ){
        var map;

        if( zeon_maps.hasOwnProperty( i ) ){

            map = zeon_maps[ i ];

            var zoom = function(){
                var val = parseInt( jQuery( this ).val() );
                map.set_zoom( val );
            }

            jQuery( form ).find( 'input#zeon-gmap-zoom' ).click( zoom );
            jQuery( form ).find( 'input#zeon-gmap-zoom' ).change( zoom );
            jQuery( form ).find( 'input#zeon-gmap-zoom' ).keypress( zoom );
            jQuery( form ).find( 'input#zeon-gmap-zoom' ).keyup( zoom );

            var lat = function(){
                var val = parseFloat( jQuery( this ).val() );
                map.set_lat( val );
            }

            jQuery( form ).find( 'input#zeon-gmap-lat' ).click( lat );
            jQuery( form ).find( 'input#zeon-gmap-lat' ).change( lat );
            jQuery( form ).find( 'input#zeon-gmap-lat' ).keypress( lat );
            jQuery( form ).find( 'input#zeon-gmap-lat' ).keyup( lat );

            var lng = function(){
                var val = parseFloat( jQuery( this ).val() );
                map.set_lng( val );
            }

            jQuery( form ).find( 'input#zeon-gmap-lng' ).click( lng );
            jQuery( form ).find( 'input#zeon-gmap-lng' ).change( lng );
            jQuery( form ).find( 'input#zeon-gmap-lng' ).keypress( lng );
            jQuery( form ).find( 'input#zeon-gmap-lng' ).keyup( lng );


            var height = function(){
                var val = parseInt( jQuery( this ).val() );
                jQuery( 'div.tempo-shortcode.gmap' ).css({ 'height' : val + 'px' });
                jQuery( 'div.tempo-shortcode.gmap div.tempo-canvas' ).css({ 'height' : val + 'px' });

                map.reset();
            }

            jQuery( form ).find( 'input#zeon-gmap-height' ).click( height );
            jQuery( form ).find( 'input#zeon-gmap-height' ).focusout( height );


            var title = function(){
                var val = jQuery( this ).val();
                map.set_marker_title( val );
            }

            jQuery( form ).find( 'input#zeon-gmap-marker-title' ).click( title );
            jQuery( form ).find( 'input#zeon-gmap-marker-title' ).focusout( title );


            var description = function(){
                var val = jQuery( this ).val();
                map.set_marker_description( val );
            }

            jQuery( form ).find( 'textarea#zeon-gmap-marker-description' ).click( description );
            jQuery( form ).find( 'textarea#zeon-gmap-marker-description' ).focusout( description );
        }

        return {
            init : function(){
                if( map.hasOwnProperty( 'init' ) )
                    map.init();
            },
            load : function(){
                if( map.hasOwnProperty( 'load' ) )
                    map.load();
            },
            reset : function(){
                if( map.hasOwnProperty( 'reset' ) )
                    map.reset();
            }
        }
    }


    /**
     *  Action to Run Prepare Function
     */

    // shortcode
    jQuery( 'select#zeon-shortcodes-selector' ).change(function(){
        if( 'zeon-shortcode-gmap-fields' == jQuery( this ).val() ){
            setTimeout(function(){

                var map = zeon_gmap_form( 0, jQuery( 'div.zeon-shortcode-gmap-fields' ) );

                    map.init();
                    map.load();

            }, 500 );
        }
    });

    // custom header meta box
    jQuery( 'select#tempo-header-template' ).change(function(){
        if( 'gmap' == jQuery( this ).val() )
            setTimeout(function(){

                var map = zeon_gmap_form( 1, jQuery( 'div.tempo-gmap-wrapper' ) );

                    map.init();
                    map.load();

            }, 500 );
    });

    // if current value of select is gmap
    if( 'gmap' == jQuery( 'select#tempo-header-template' ).val() ){

        var map = zeon_gmap_form( 1, jQuery( 'div.tempo-gmap-wrapper' ) );

            map.reset();
    }
});