(function($){

    {   /////   HEADER ELEMENTS


        {   /////   APPEARANCES

            wp.customize( 'header-text-space', function( value ){
                value.bind(function( newval ){
                    var text = jQuery( 'div.tempo-header-partial div.tempo-header-text-wrapper' );
                    var btns = jQuery( 'div.tempo-header-partial div.tempo-header-btns-wrapper' );

                    if( newval && text.length && btns.length ){
                        var height = parseInt( newval ) + parseInt( wp.customize.instance( 'header-btns-space' ).get() );

                        jQuery( 'div.tempo-header-partial' ).css( 'height' , parseInt( height ).toString() + 'px' );
                        jQuery( 'div.tempo-header-partial div.tempo-header-text-wrapper' ).css( 'height' , parseInt( newval ).toString() + 'px' );
                    }
                });
            });

            wp.customize( 'header-btns-space', function( value ){
                value.bind(function( newval ){
                    var text = jQuery( 'div.tempo-header-partial div.tempo-header-text-wrapper' );
                    var btns = jQuery( 'div.tempo-header-partial div.tempo-header-btns-wrapper' );

                    if( newval && text.length && btns.length ){
                        var height = parseInt( wp.customize.instance( 'header-text-space' ).get() ) + parseInt( newval );

                        jQuery( 'div.tempo-header-partial' ).css( 'height' , parseInt( height ).toString() + 'px' );
                        jQuery( 'div.tempo-header-partial div.tempo-header-btns-wrapper' ).css( 'height' , parseInt( newval ).toString() + 'px' );
                    }
                });
            });
        }


        {   /////   BUTTON 1

            // Text
            wp.customize( 'header-btn-1-text', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( button.length )
                        button.html( newval );
                });
            });

            // Description
            wp.customize( 'header-btn-1-description', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( button.length )
                        button.attr( 'title', newval );
                });
            });

            // Url
            wp.customize( 'header-btn-1-url', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( button.length )
                        button.attr( 'href', newval );
                });
            });

            // Target
            wp.customize( 'header-btn-1-target', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    if( newval ){
                        button.attr( 'target', "_blank" );
                    }

                    else{
                        button.removeAttr( 'target' );   
                    }
                });
            });

            // Text Color
            wp.customize( 'header-btn-1-text-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-1-text-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-text-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Transparency
            wp.customize( 'header-btn-1-text-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-1-text-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-text-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Color ( mouse over )
            wp.customize( 'header-btn-1-text-h-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-1-text-h-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-text-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1:hover{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Transparency ( mouse over )
            wp.customize( 'header-btn-1-text-h-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-1-text-h-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-text-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1:hover{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Color
            wp.customize( 'header-btn-1-bkg-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-1-bkg-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-bkg-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Transparency
            wp.customize( 'header-btn-1-bkg-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-1-bkg-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-bkg-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Color ( mouse over )
            wp.customize( 'header-btn-1-bkg-h-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-1-bkg-h-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-bkg-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1:hover{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Transparency ( mouse over )
            wp.customize( 'header-btn-1-bkg-h-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-1-bkg-h-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-1-bkg-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-1:hover{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });
        }


        {   /////   BUTTON 1

            // Text
            wp.customize( 'header-btn-2-text', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( button.length )
                        button.html( newval );
                });
            });

            // Description
            wp.customize( 'header-btn-2-description', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( button.length )
                        button.attr( 'title', newval );
                });
            });

            // Url
            wp.customize( 'header-btn-2-url', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( button.length )
                        button.attr( 'href', newval );
                });
            });

            // Target
            wp.customize( 'header-btn-2-target', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    if( newval ){
                        button.attr( 'target', "_blank" );
                    }

                    else{
                        button.removeAttr( 'target' );   
                    }
                });
            });

            // Text Color
            wp.customize( 'header-btn-2-text-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-2-text-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-text-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Transparency
            wp.customize( 'header-btn-2-text-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-2-text-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-text-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Color ( mouse over )
            wp.customize( 'header-btn-2-text-h-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-2-text-h-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-text-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2:hover{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Text Transparency ( mouse over )
            wp.customize( 'header-btn-2-text-h-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-2-text-h-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-text-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2:hover{' +
                        'color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Color
            wp.customize( 'header-btn-2-bkg-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-2-bkg-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-bkg-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Transparency
            wp.customize( 'header-btn-2-bkg-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-2-bkg-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-bkg-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Color ( mouse over )
            wp.customize( 'header-btn-2-bkg-h-color', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = newval;
                    var transp  = parseInt( wp.customize.instance( 'header-btn-2-bkg-h-transp' ).get() ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-bkg-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2:hover{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });

            // Background Transparency ( mouse over )
            wp.customize( 'header-btn-2-bkg-h-transp', function( value ){
                value.bind(function( newval ){
                    var button = jQuery( 'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2' );

                    if( !button.length )
                        return;

                    var hex     = wp.customize.instance( 'header-btn-2-bkg-h-color' ).get();
                    var transp  = parseInt( newval ) / 100;
                    var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                    jQuery( 'style#header-btn-2-bkg-h-color' ).html(
                        'header.tempo-header div.tempo-header-partial .tempo-header-btns-wrapper .tempo-btn.btn-2:hover{' +
                        'background-color: ' + rgba + ';' +
                        '}'
                    );
                });
            });
        }


        {   /////   WIDGETS

            {   /////   HEADER FRONT PAGE

                // Background Color
                wp.customize( 'header-front-page-sidebar-bkg-color', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-front-page.header-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = newval;
                        var transp  = parseInt( wp.customize.instance( 'header-front-page-sidebar-bkg-transp' ).get() ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#header-front-page-sidebar-bkg-color' ).html(
                            'aside.tempo-front-page.header-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Background Transparency
                wp.customize( 'header-front-page-sidebar-bkg-transp', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-front-page.header-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = wp.customize.instance( 'header-front-page-sidebar-bkg-color' ).get();
                        var transp  = parseInt( newval ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#header-front-page-sidebar-bkg-color' ).html(
                            'aside.tempo-front-page.header-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Sidebar Space
                wp.customize( 'header-front-page-sidebar-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-front-page.header-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#header-front-page-sidebar-space' ).html(
                            'aside.tempo-front-page.header-sidebar{' +
                            'padding-top: ' + parseInt( newval ) + 'px;' +
                            'padding-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });

                // Widgets Space
                wp.customize( 'header-front-page-widgets-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-front-page.header-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#header-front-page-widgets-space' ).html(
                            'aside.tempo-front-page.header-sidebar div.widget{' +
                            'margin-top: ' + parseInt( newval ) + 'px;' +
                            'margin-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });
            }


            {   /////   HEADER BLOG

                // Background Color
                wp.customize( 'header-blog-sidebar-bkg-color', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-blog.header-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = newval;
                        var transp  = parseInt( wp.customize.instance( 'header-blog-sidebar-bkg-transp' ).get() ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#header-blog-sidebar-bkg-color' ).html(
                            'aside.tempo-blog.header-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Background Transparency
                wp.customize( 'header-blog-sidebar-bkg-transp', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-blog.header-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = wp.customize.instance( 'header-blog-sidebar-bkg-color' ).get();
                        var transp  = parseInt( newval ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#header-blog-sidebar-bkg-color' ).html(
                            'aside.tempo-blog.header-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Sidebar Space
                wp.customize( 'header-blog-sidebar-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-blog.header-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#header-blog-sidebar-space' ).html(
                            'aside.tempo-blog.header-sidebar{' +
                            'padding-top: ' + parseInt( newval ) + 'px;' +
                            'padding-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });

                // Widgets Space
                wp.customize( 'header-blog-widgets-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-blog.header-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#header-blog-widgets-space' ).html(
                            'aside.tempo-blog.header-sidebar div.widget{' +
                            'margin-top: ' + parseInt( newval ) + 'px;' +
                            'margin-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });
            }


            {   /////   CATEGORIES    /////

                if( typeof tempo_customize_args == 'object' ){
                    if( tempo_customize_args.hasOwnProperty( 'categories' ) ){
                        var cats = tempo_customize_args.categories;
                        for( var i = 0; i < cats.length; i++ ){
                            (function( id ){
                                wp.customize( 'category-' + id, function( value ){
                                    value.bind(function( newval ){
                                        jQuery( 'style#category-' + id ).html(
                                            'aside div.widget.zeon_widget_categories a span.tempo-category-' + id + ',' +
                                            'header.tempo-header div.tempo-header-partial.tempo-portfolio div.tempo-categories a.tempo-category-' + id + ',' +
                                            'article.tempo-article.portfolio div.tempo-categories a.tempo-category-' + id + ',' +
                                            'article.tempo-article.grid div.tempo-categories a.tempo-category-' + id + '{' +
                                            'background-color: ' + newval + ';' +
                                            '}'
                                        );
                                    });
                                });
                            })( cats[ i ] );
                        }
                    }
                }
            }


            {   /////   FOOTER LIGHT

                // Background Color
                wp.customize( 'footer-light-sidebar-bkg-color', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.light-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = newval;
                        var transp  = parseInt( wp.customize.instance( 'footer-light-sidebar-bkg-transp' ).get() ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#footer-light-sidebar-bkg-color' ).html(
                            'aside.tempo-footer.light-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Background Transparency
                wp.customize( 'footer-light-sidebar-bkg-transp', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.light-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = wp.customize.instance( 'footer-light-sidebar-bkg-color' ).get();
                        var transp  = parseInt( newval ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#footer-light-sidebar-bkg-color' ).html(
                            'aside.tempo-footer.light-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Sidebar Space
                wp.customize( 'footer-light-sidebar-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.light-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#footer-light-sidebar-space' ).html(
                            'aside.tempo-footer.light-sidebar{' +
                            'padding-top: ' + parseInt( newval ) + 'px;' +
                            'padding-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });

                // Widgets Space
                wp.customize( 'footer-light-widgets-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.light-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#footer-light-widgets-space' ).html(
                            'aside.tempo-footer.light-sidebar div.widget{' +
                            'margin-top: ' + parseInt( newval ) + 'px;' +
                            'margin-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });
            }


            {   /////   FOOTER DARK

                // Background Color
                wp.customize( 'footer-dark-sidebar-bkg-color', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.dark-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = newval;
                        var transp  = parseInt( wp.customize.instance( 'footer-dark-sidebar-bkg-transp' ).get() ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#footer-dark-sidebar-bkg-color' ).html(
                            'aside.tempo-footer.dark-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Background Transparency
                wp.customize( 'footer-dark-sidebar-bkg-transp', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.dark-sidebar' );

                        if( !aside.length )
                            return;

                        var hex     = wp.customize.instance( 'footer-dark-sidebar-bkg-color' ).get();
                        var transp  = parseInt( newval ) / 100;
                        var rgba    = 'rgba( ' + tempo_hex2rgb( hex ) + ', ' + transp + ' )';

                        jQuery( 'style#footer-dark-sidebar-bkg-color' ).html(
                            'aside.tempo-footer.dark-sidebar{' +
                            'background-color: ' + rgba + ';' +
                            '}'
                        );
                    });
                });

                // Sidebar Space
                wp.customize( 'footer-dark-sidebar-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.dark-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#footer-dark-sidebar-space' ).html(
                            'aside.tempo-footer.dark-sidebar{' +
                            'padding-top: ' + parseInt( newval ) + 'px;' +
                            'padding-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });

                // Widgets Space
                wp.customize( 'footer-dark-widget-space', function( value ){
                    value.bind(function( newval ){
                        var aside = jQuery( 'aside.tempo-footer.dark-sidebar' );

                        if( !aside.length )
                            return;

                        jQuery( 'style#footer-dark-widgets-space' ).html(
                            'aside.tempo-footer.dark-sidebar div.widget{' +
                            'margin-top: ' + parseInt( newval ) + 'px;' +
                            'margin-bottom: ' + parseInt( newval ) + 'px;' +
                            '}'
                        );
                    });
                });
            }
        }
    }

})(jQuery);