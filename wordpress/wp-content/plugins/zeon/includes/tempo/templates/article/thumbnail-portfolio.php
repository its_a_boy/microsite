<?php
    global $post;

	$thumbnail = get_post( get_post_thumbnail_id( $post ) );

    if( has_post_thumbnail( $post ) && isset( $thumbnail -> ID ) ){

        $thumbnail_size     = 'tempo-portfolio';
        $thumbnail_class    = 'effect-scale';

        $thumbnail_args     = array(
            'alt'   => esc_attr( get_the_title( $post ) ),
            'class' => $thumbnail_class
        );
        
    	echo get_the_post_thumbnail( $post, $thumbnail_size, $thumbnail_args );

        // PERMALINK ( URL )
        $permalink = null; //apply_filter();

        if( empty( $permalink ) ){
            $permalink = get_permalink( $post );
        }

        // PERMALINK CLASS
        $permalink_class = null; //apply_filter();

        if( empty( $permalink_class ) ){
            $permalink_class = 'tempo-thumbnail-permalink';
        }

        // PERMALINK TITLE
        $permalink_title = null; //apply_filter();

        if( empty( $permalink_title ) ){
            $permalink_title = esc_attr( get_the_title( $post ) );
        }

        // PERMALINK TARGET
        $permalink_target = null; //apply_filter();

        if( empty( $permalink_target ) ){
            $permalink_target = '_self';
        }

        //$permalink_class .= apply_filter()

        echo '<a href="' . esc_url( $permalink ) . '" class="' . esc_attr( $permalink_class ) . '" title="' . esc_attr( $permalink_title ) . '" target="' . esc_attr( $permalink_target ) . '"></a>';

        /*
            $the_caption = apply_filter();

            if( !empty( $the_thumbnail ) ){
    
            }
            else

            ...
        */
    }
?>