<?php

	/**
     *  The list of Latest Comments with Avatar
     */

	if( !class_exists( 'zeon_widget_comments' ) ){

		class zeon_widget_comments extends WP_Widget
		{
			/**
             *  Widget Constructor
             */

		    function __construct()
		    {
		        parent::__construct( 'zeon_widget_comments', __( 'Comments', 'zeon' ) . ' [zeon]', array( 
		            'classname'     => 'zeon_widget_comments',
		            'description'   => __( 'The list of the Latest Comments with Avatar', 'zeon' )
		        ));
		    }

		    /**
             *  Widget Preview
             */

		    function widget( $args, $instance )
		    {
		    	// extract args
		        extract( $args , EXTR_SKIP );

		        $instance = wp_parse_args( (array) $instance, array(
		            'title'     => __( 'Latest Comments', 'zeon' ),
		            'number'    => 5
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $number = absint( $instance[ 'number' ] );

		        if( $number > 0 ){

		            echo $before_widget;

		            if( !empty( $title ) ){
		                echo $before_title;
		                echo apply_filters( 'widget_title', $title, $instance, $this -> id_base );
		                echo $after_title;
		            }

		            $comments = get_comments( array(
		                'status' => 'approve',
		                'number' => $number
		            ));

		            if( !empty( $comments ) ){

		                echo '<ul>';

		                foreach( $comments as $index => $c ){

		                    $email  	= is_email( $c -> comment_author_email );
		                    $nickname   = sanitize_text_field( $c -> comment_author );
		                    $content   	= strip_tags( $c -> comment_content );
		                    $url 		= zeon_is_url( $c -> comment_author_url ) ? esc_url( $c -> comment_author_url ) : null;

		                    echo '<li>';

		                    // comment avatar
		                    echo '<div class="comment-avatar overflow-wrapper">';

		                    if( !empty( $url ) ){
		                        echo get_avatar( is_email( $email ), 44, zeon_plugin_uri() . '/includes/' . zeon_active_pack() . '/media/img/avatar.png' );
		                    }

		                    // missing the url
		                    else{
		                        echo get_avatar( is_email( $email ), 44, zeon_plugin_uri() . '/includes/' . zeon_active_pack() . '/media/img/avatar.png' );
		                    }

		                    if( !empty( $url ) ){
			                    echo '<a href="' . esc_url( $url ) . '" title="' . sprintf( __( 'Comment by %', 'zeon' ) , esc_attr( $nickname ) ) . '" ' . tempo_flex_container_class( null, 'tempo-valign-middle' ) . ' target="_blank">';
		                        echo '<span ' . tempo_flex_item_class( null, 'tempo-align-center' ) . '>';
		                        echo '<i class="tempo-icon-link-2"></i>';
		                        echo '</span>';
		                        echo '</a>';
	                    	}

		                    echo '</div>';

		                    // comment nickname
		                    if( !empty( $url ) ){
		                        echo '<h5><a href="' . esc_url( $url ) . '" title="' . sprintf( __( 'Comment by %', 'zeon' ) , esc_attr( $nickname ) ) . '" target="_blank">' . $nickname . '</a></h5>';
		                    }

		                    else{
		                        echo '<h5>' . $nickname . '</h5> ';
		                    }

		                    // comment content
		                    $sub_content = mb_substr( $content, 0, 60 );

		                    if( strlen( $content ) > strlen( $sub_content ) )
		                        $sub_content .= ' ...';

		                    echo '<p>' . $sub_content . '</p>';

		                    echo '<div class="clearfix"></div>';
		                    echo '</li>';
		                }

		                echo '</ul>';
		            }

		            echo $after_widget;
		        }
		    }

		    /**
             *  Widget Update
             */

		    function update( $new_instance, $old_instance )
		    {
		        $instance               = $old_instance;
		        $instance[ 'title' ]    = sanitize_text_field( $new_instance[ 'title' ] );
		        $instance[ 'number' ]   = absint( $new_instance[ 'number' ] );

		        return $instance;
		    }

		    /**
             *  Widget Form ( admin side )
             */

		    function form( $instance )
		    {
		        $instance = wp_parse_args( (array) $instance, array(
		            'title'     => null,
		            'number'    => 5
		        ));

		        $title  = sanitize_text_field( $instance[ 'title' ] );
		        $number = absint( $instance[ 'number' ] );

		        if( empty( $number ) ){
		            echo '<p>' . __( 'This widget will display nothing because no data OR missing data for field "Number of comments"', 'zeon' ) . '</p>';
		        }

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'title' ) . '">' . __( 'Title', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'title' ) . '" name="' . $this -> get_field_name( 'title' ) . '" value="' . sanitize_text_field( $title ) . '" />';
		        echo '</label>';
		        echo '</p>';

		        echo '<p>';
		        echo '<label for="' . $this -> get_field_id( 'number' ) . '">' . __( 'Number of comments', 'zeon' );
		        echo '<input type="text" class="widefat" id="' . $this -> get_field_id( 'number' ) . '" name="' . $this -> get_field_name( 'number' ) . '" value="' . absint( $number ) . '" />';
		        echo '</label>';
		        echo '</p>';
		    }
		}
	}
?>
