<?php
	
	/**
	 *	Appearance / Customize / Header Settings / First Button - config settings
	 */

	$height = ( $h = absint( tempo_options::val( 'header-height' ) ) ) && !empty( $h ) ? $h : 400;

	$height_70 = absint( $height * 0.7 );
	$height_30 = absint( $height * 0.3 );

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
		'tempo-header' => array(
			'sections' => array(
				'tempo-header-appearance' => array(
					'fields'	=> array(
						'header-height' => array(
							'title'			=> __( 'Header height', 'zeon' ),
							'priority'		=> 5,
							'callback'		=> function(){
								return !tempo_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 0,
	                        	'max'       	=> 800,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> 400
							)
						),
						'header-horizontal-align' => array(
							'title'			=> __( 'Horizontal Align ', 'zeon' ),
							'description'	=> __( 'align horizontal the header elements', 'zeon' ),
							'priority'		=> 25,
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'center',
								'options'		=> array(
									'left'			=> __( 'Left', 'zeon' ),
									'center'		=> __( 'Center', 'zeon' ),
									'right'			=> __( 'Right', 'zeon' )
								)
							)
						),
						'header-text-space' => array(
							'title'			=> __( 'Text Space', 'zeon' ),
							'priority'		=> 26,
							'callback'		=> function(){
								return tempo_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 0,
	                        	'max'       	=> 800,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> $height_70
							)
						),
						'header-text-vertical-align' => array(
							'title'			=> __( 'Text vertical Align ', 'zeon' ),
							'description'	=> __( 'Vertical align Headline and Description. This options is available only if is enable Headline or Description', 'zeon' ),
							'priority'		=> 27,
							'callback'		=> function(){
								return tempo_mix_height();
							},
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'middle',
								'options'		=> array(
									'top'			=> __( 'Top', 'zeon' ),
									'middle'		=> __( 'Middle', 'zeon' ),
									'bottom'		=> __( 'Bottom', 'zeon' )
								)
							)
						),
						'header-btns-space' => array(
							'title'			=> __( 'Buttons Space', 'zeon' ),
							'priority'		=> 28,
							'callback'		=> function(){
								return tempo_mix_height();
							},
							'input'			=> array(
								'type'			=> 'range',
								'min'       	=> 0,
	                        	'max'       	=> 800,
	                        	'step'      	=> 1,
	                        	'unit' 			=> 'px',
	                        	'default' 		=> $height_30
							)
						),
						'header-btns-vertical-align' => array(
							'title'			=> __( 'Buttons vertical Align ', 'zeon' ),
							'description'	=> __( 'Vertical align the buttons. This options is available only if is enable First Button or Second Button', 'zeon' ),
							'priority'		=> 29,
							'callback'		=> function(){
								return tempo_mix_height();
							},
							'input'			=> array(
								'type'			=> 'select',
								'default'		=> 'top',
								'options'		=> array(
									'top'			=> __( 'Top', 'zeon' ),
									'middle'		=> __( 'Middle', 'zeon' ),
									'bottom'		=> __( 'Bottom', 'zeon' )
								)
							)
						),
					)
				),
				'tempo-header-btn-1' => array(
					'title' 	=> __( 'First Button', 'zeon' ),
					'priority'	=> 30,
					'fields'	=> array(
						'header-btn-1' => array(
							'title'			=> __( 'Display First Button', 'zeon' ),
							'description'	=> __( 'enable / disable first Button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> false
							)
						),
						'header-btn-1-text' => array(
							'title'			=> __( 'Text', 'zeon' ),
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'First Button', 'zeon' )
							)
						),
						'header-btn-1-description' => array(
							'title'			=> __( 'Description', 'zeon' ),
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'first button description', 'zeon' )
							)
						),
						'header-btn-1-url' => array(
							'title'			=> __( 'URL', 'zeon' ),
							'transport'		=> 'postMessage',
							'input'			=> array(
								'type'			=> 'url'
							)
						),
						'header-btn-1-target' => array(
							'title'			=> __( 'Open url in new window', 'zeon' ),
							'description'	=> __( 'enable / disable link attribut target="_blank"', 'zeon' ),
							'transport'		=> 'postMessage',
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> true 
							)
						),
						'header-btn-1-text-color' => array(
							'title'			=> __( 'Text Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff' 
							)
						),
						'header-btn-1-text-transp' => array(
							'title'			=> __( 'Text Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-1-text-h-color' => array(
							'title'			=> __( 'Text Color ( over )', 'zeon' ),
							'description'	=> __( 'the Text Color when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff' 
							)
						),
						'header-btn-1-text-h-transp' => array(
							'title'			=> __( 'Text Transparency ( over )', 'zeon' ),
							'description'	=> __( 'the Text Transparency when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-1-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#337ab7'
							)
						),
						'header-btn-1-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-1-bkg-h-color' => array(
							'title'			=> __( 'Background Color ( over )', 'zeon' ),
							'description'	=> __( 'the Background Color when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#3c8fd7'
							)
						),
						'header-btn-1-bkg-h-transp' => array(
							'title'			=> __( 'Background Transparency ( over )', 'zeon' ),
							'description'	=> __( 'the Background Transparency when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						)
					)
				),
				'tempo-header-btn-2' => array(
					'title' 	=> __( 'Second Button', 'zeon' ),
					'priority'	=> 35,
					'fields'	=> array(
						'header-btn-2' => array(
							'title'			=> __( 'Display Second Button', 'zeon' ),
							'description'	=> __( 'enable / disable second Button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> false
							)
						),
						'header-btn-2-text' => array(
							'title'			=> __( 'Text', 'zeon' ),
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'Second Button', 'zeon' )
							)
						),
						'header-btn-2-description' => array(
							'title'			=> __( 'Description', 'zeon' ),
							'input'			=> array(
								'type'			=> 'text',
								'default'		=> __( 'second button description', 'zeon' )
							)
						),
						'header-btn-2-url' => array(
							'title'			=> __( 'URL', 'zeon' ),
							'input'			=> array(
								'type'			=> 'url'
							)
						),
						'header-btn-2-target' => array(
							'title'			=> __( 'Open url in new window', 'zeon' ),
							'description'	=> __( 'enable / disable link attribut target="_blank"', 'zeon' ),
							'input'			=> array(
								'type'			=> 'checkbox',
								'default'		=> true 
							)
						),
						'header-btn-2-text-color' => array(
							'title'			=> __( 'Text Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff' 
							)
						),
						'header-btn-2-text-transp' => array(
							'title'			=> __( 'Text Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-2-text-h-color' => array(
							'title'			=> __( 'Text Color ( over )', 'zeon' ),
							'description'	=> __( 'the Text Color when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#ffffff' 
							)
						),
						'header-btn-2-text-h-transp' => array(
							'title'			=> __( 'Text Transparency ( over )', 'zeon' ),
							'description'	=> __( 'the Text Transparency when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-2-bkg-color' => array(
							'title'			=> __( 'Background Color', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#5cb854'
							)
						),
						'header-btn-2-bkg-transp' => array(
							'title'			=> __( 'Background Transparency', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						),
						'header-btn-2-bkg-h-color' => array(
							'title'			=> __( 'Background Color ( over )', 'zeon' ),
							'description'	=> __( 'the Background Color when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'color',
								'default'		=> '#64c85b'
							)
						),
						'header-btn-2-bkg-h-transp' => array(
							'title'			=> __( 'Background Transparency ( over )', 'zeon' ),
							'description'	=> __( 'the Background Transparency when the mouse cursor is over the button', 'zeon' ),
							'input'			=> array(
								'type'			=> 'percent',
								'default'		=> 100 
							)
						)
					)
				),
			)
		)
	));

	tempo_cfgs::set( 'customize', $cfgs );
?>