<?php 
	global $post, $wp_embed;

    $video = esc_url( zeon_meta::g( $post -> ID, 'tempo-video-url' ) );

    if( empty( $video ) )
    	return;
?>


	<div class="tempo-header-partial tempo-video-thumbnail overflow-wrapper">

	    <?php

	    	$result = $wp_embed -> run_shortcode( "[embed]{$video}[/embed]" );

	    	/**
	    	 *	HTML5 Video Embed
	    	 */

	    	if( preg_match( "/^\[video/", $result ) ){

	    		$data 	= pathinfo( $video );
	    		$ext 	= 'mp4';

	    		if( isset( $data[ 'extension' ] ) )
	    			$ext = $data[ 'extension' ];

				?>
					<video class="tempo-video-embed" controls>
						<source src="<?php echo esc_url( $video ); ?>" type="video/<?php echo $ext; ?>"/>
					</video>
	    		<?php
	    	}

	    	/**
	    	 *	Shortcode embeded Result
	    	 */

	    	else{
	    		echo $result;
	    	}
	    ?>

	</div>